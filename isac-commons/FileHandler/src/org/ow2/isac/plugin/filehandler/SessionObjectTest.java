/*
* CLIF is a Load Injection Framework
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.filehandler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.scenario.isac.util.ParameterParser;

/**
 * Test class for ISAC plug-in FileHandler.
 * Just run main method, no argument.
 * @author Bruno Dillenseger
 */
abstract class SessionObjectTest
{
	static public void main(String[] args)
	throws IOException, InterruptedException
	{
		File tmpFile = File.createTempFile("FileHandlerTest", null);
		tmpFile.deleteOnExit();
		Thread.sleep(1000);
		BufferedWriter bf = new BufferedWriter(new FileWriter(tmpFile));
		bf.write("0123456789");
		bf.close();
		File tmpDir = new File(tmpFile.getParentFile(), "FileHandlerTestDir");
		tmpDir.mkdir();
		tmpDir.deleteOnExit();
		File tmpLink = Files.createSymbolicLink(
			new File(tmpDir, "FileHandlerTestLink").toPath(),
			tmpFile.toPath())
			.toFile();
		tmpLink.deleteOnExit();
		SessionObject fhso = new SessionObject(null);
		fhso = (SessionObject) fhso.createNewSessionObject();
		// check existing regular file
		checkVars(fhso, tmpFile.getPath());
		checkERW(fhso, tmpFile.getPath(), true);
		checkFile(fhso, tmpFile.getPath(), true);
		checkDir(fhso, tmpFile.getPath(), false);
		checkLink(fhso, tmpFile.getPath(), false);
		// check existing directory
		checkVars(fhso, tmpDir.getPath());
		checkERW(fhso, tmpDir.getPath(), true);
		checkFile(fhso, tmpDir.getPath(), false);
		checkDir(fhso, tmpDir.getPath(), true);
		checkLink(fhso, tmpDir.getPath(), false);
		// check existing link
		checkVars(fhso, tmpLink.getPath());
		checkERW(fhso, tmpLink.getPath(), true);
		checkFile(fhso, tmpLink.getPath(), false);
		checkDir(fhso, tmpLink.getPath(), false);
		checkLink(fhso, tmpLink.getPath(), true);
		// delete file/directory/link
		Map<String,String> params = new HashMap<String,String>();
		params.put(
			SessionObject.CONTROL_DELETEFILES_PATHS,
			ParameterParser.addEscapeCharacter("\\", tmpLink.getPath())
			+ ";"
			+ ParameterParser.addEscapeCharacter("\\", tmpFile.getPath()));
		fhso.doControl(SessionObject.CONTROL_DELETEFILES, params);
		params.clear();
		params.put(SessionObject.CONTROL_DELETEEMPTYDIR_PATH, tmpDir.getPath());
		fhso.doControl(SessionObject.CONTROL_DELETEEMPTYDIR, params);
		// check missing file/directory/link
		checkERW(fhso, tmpLink.getPath(), false);
		checkLink(fhso, tmpLink.getPath(), false);
		checkERW(fhso, tmpFile.getPath(), false);
		checkFile(fhso, tmpFile.getPath(), false);
		checkERW(fhso, tmpDir.getPath(), false);
		checkDir(fhso, tmpDir.getPath(), false);
	}

	static void checkERW(SessionObject fhso, String path, boolean expected)
	{
		Map<String,String> params;

		// file exists and is both readable and writable
		params = new HashMap<String,String>();
		params.put(SessionObject.TEST_EXISTS_PATH, path);
		assert fhso.doTest(SessionObject.TEST_EXISTS, params) == expected : "failed: exists";
		params.put(SessionObject.TEST_READABLE_PATH, path);
		assert fhso.doTest(SessionObject.TEST_READABLE, params) == expected : "failed: readable";
		params.put(SessionObject.TEST_WRITEABLE_PATH, path);
		assert fhso.doTest(SessionObject.TEST_WRITEABLE, params) == expected : "failed: writeable";
		
		// file does not exist, is not readable nor writable
		params = new HashMap<String,String>();
		params.put(SessionObject.TEST_EXISTSNOT_PATH, path);
		assert !fhso.doTest(SessionObject.TEST_EXISTSNOT, params) == expected : "failed: exists not";
		params.put(SessionObject.TEST_NOTREADABLE_PATH, path);
		assert !fhso.doTest(SessionObject.TEST_NOTREADABLE, params) == expected : "failed: not readable";
		params.put(SessionObject.TEST_NOTWRITEABLE_PATH, path);
		assert !fhso.doTest(SessionObject.TEST_NOTWRITEABLE, params) == expected : "failed: not writeable";
	}

	static void checkLink(SessionObject fhso, String path, boolean expected)
	{
		Map<String,String> params = new HashMap<String,String>();
		params.put(SessionObject.TEST_ISLINK_PATH, path);
		assert fhso.doTest(SessionObject.TEST_ISLINK, params) == expected : "failed: is link";
		params.put(SessionObject.TEST_ISNOTLINK_PATH, path);
		assert !fhso.doTest(SessionObject.TEST_ISNOTLINK, params) == expected : "failed: is not link";
	}

	static void checkDir(SessionObject fhso, String path, boolean expected)
	{
		Map<String,String> params = new HashMap<String,String>();
		params.put(SessionObject.TEST_ISDIR_PATH, path);
		assert fhso.doTest(SessionObject.TEST_ISDIR, params) == expected : "failed: is directory";
	}

	static void checkFile(SessionObject fhso, String path, boolean expected)
	{
		Map<String,String> params = new HashMap<String,String>();
		params.put(SessionObject.TEST_ISFILE_PATH, path);
		assert fhso.doTest(SessionObject.TEST_ISFILE, params) == expected : "failed: is regular file";
	}

	static void checkVars(SessionObject fhso, String path)
	throws IOException
	{
		BasicFileAttributes attributes = Files.readAttributes(new File(path).toPath(), BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS); 
		long ctime = attributes.creationTime().toMillis();
		assert fhso.doGet(path + ";" + SessionObject.VAR_CTIME).equals(String.valueOf(ctime)) : "failed: ctime variable";
		long mtime = attributes.lastModifiedTime().toMillis();
		assert fhso.doGet(path + ";" + SessionObject.VAR_MTIME).equals(String.valueOf(mtime)) : "failed: mtime variable";
		long size = attributes.size();
		assert fhso.doGet(path + ";" + SessionObject.VAR_SIZE).equals(String.valueOf(size)) : "failed: size variable";
	}
}
