/*
* CLIF is a Load Injection Framework
* Copyright (C) 2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import org.jdom.DocType;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.ProcessingInstruction;

/**
 * ISAC Plug-in help parser.
 * This utility class generates an html documentation giving a description of all Isac plug-ins.
 * 
 * @author Escalle Thomas
 * @author Bruno Dillenseger
 */

public abstract class PluginHelpParser {
    // root path to access to the generated help files
	static String helpPath;

	// XML help files list
	static SortedMap<String,File> pluginXMLFileDestList;
	static SortedMap<String,String> pluginXMLRelatifPathList;
	
	/**
	 * Launches plug-in help doc generation for all ISAC plug-ins contained
	 * in a given set of root directories.
	 * @param args paths of one or several directories containing ISAC plug-ins,
	 * and then a target directory where to write generated documentation.
	 */
    public static void main(String[] args) throws JDOMException {
    	helpPath = args[args.length - 1];
    	
    	pluginXMLFileDestList = new TreeMap<String,File>();
    	pluginXMLRelatifPathList = new TreeMap<String,String>();

    	for (int i=0 ; i<args.length-1 ; ++i)
    	{
    		getPluginInfo(args[i]);
    	}
    	
    	// Generate the index.html file
    	generateIndex();
    	// Generate the overview-frame.html file
		generateOverviewFrame();
		// Generate the overview-summary.html file
		generateOverviewSummary(); 
		// Generate the style.css file
		generateStyleFile(); 
		// Generate the plugin.xsl file
		generatePlugInXSLFile(); 
    	
    }
    
    private static void getPluginInfo(String path) {
    	File isacPluginDirectory = new File(path);
    	
    	if (isacPluginDirectory.isDirectory()) {
    		File[] listDirectory = isacPluginDirectory.listFiles();
    		if (listDirectory != null) {
    			for (int i = 0; i< listDirectory.length; i++) {
    				File[] listFileInDirectory = listDirectory[i].listFiles();
    				
    				if (listFileInDirectory != null) {
    				
	    				for (int j = 0; j < listFileInDirectory.length; j++) {
		    				if ("isac-plugin.properties".equals(listFileInDirectory[j].getName())) {
		    					try {
		    						Properties prop = new Properties();
									prop.load(new FileInputStream(listFileInDirectory[j].getPath()));
									String pluginFileName = prop.getProperty("plugin.xmlFile");
									File pluginXmlFile = new File(listDirectory[i].getPath() + File.separator + pluginFileName);
									String pluginName = getPluginName(pluginXmlFile);
									pluginXMLRelatifPathList.put(
										pluginName,
										listDirectory[i].getName() + "/" + pluginFileName);
									modifyAndCopyPluginXmlFile(pluginName, pluginXmlFile, pluginFileName);
								} catch (FileNotFoundException e) {
									System.out.println("[ERROR] - File not found exception- "+e.getCause());
								} catch (IOException e) {
									System.out.println("[ERROR] - IO exception - "+e.getCause());
								}    					
		    				}
	    				}	
    				}	
    			}
    		} else {
    			System.err.println(isacPluginDirectory + ": Read error.");
    		} 
    	}	
    }
    
    

    /**
     *  This method gets the plug-in name from the xml plug-in file.
     * 
     * @return String The plug-in name
     * 
     **/
    private static String getPluginName(File pluginXMLFile) {
    	Document document;
    	Element racine;
    	String name = null;

    	SAXBuilder sxb = new SAXBuilder();
    	try
    	  {
    	  // These two line allow not to control the external load files (DTD)
    	  sxb.setValidation(false);
    	  sxb.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
    	  
    	  // Create a new JDOM document with the xml file
    	  document = sxb.build(pluginXMLFile);
    	  
    	  //Initialize a new root element with the root element of the document
      	  racine = document.getRootElement();
      	  
      	  //check the name attribute value of the root element
      	  name = racine.getAttributeValue("name");
    	} catch(Exception e){
    		e.printStackTrace();
    	}
    	return name;
    }
    
    /**
     * This method generate the content of the overview-summary.html page.
     * It gets information from the xml plug-in file.
	 *
     * @return String Content of the overview-summary.html page 
     */      
    private static String getPluginSummary() {
    	StringBuilder sb = new StringBuilder();
    	Document document;
    	Element racine;

    	SAXBuilder sxb = new SAXBuilder();
    	try
    	  {
    	  sxb.setValidation(false);
    	  sxb.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
    	  
    	  for (Map.Entry<String,File> i : pluginXMLFileDestList.entrySet())
    	  {
    		  document = sxb.build(i.getValue());
    		  racine = document.getRootElement();
    		  String name = racine.getAttributeValue("name");
    		  Element object = racine.getChild("object");
    		  String className = object.getAttributeValue("class");
    		  String summary = object.getChildText("help");
    		  sb.append("<TR>");
    		  sb.append("  <TD><A HREF=\"" + pluginXMLRelatifPathList.get(i.getKey()) + "\" target=\"pluginFrame\" >" + name + "</A></TD>");
    		  sb.append("  <TD>" + className + "</TD>");
    		  sb.append("  <TD>" + summary + "</TD>");	      	
    		  sb.append("</TR>");
    	  }
    	} catch(Exception e){
    		return "";
    	}
    	
    	return sb.toString();
    }

    
    /**
     * This method modifies the plugin.xml file:
     *    - It removes the DOCTYPE
     *    - It adds xml-stylesheet instruction to be able to display this XML file with XSL sheet.
     * After these actions, it copies the XML result document to the destination file.   
     * 
     * @param File the xml plugin file.
     * @param String the plugin name
     */          
    private static void modifyAndCopyPluginXmlFile(String pluginName, File pluginXmlFile, String pluginFileName)
    {
        String directoryToCopyXMLFile = helpPath + File.separator + pluginName;		
        if ((new File(directoryToCopyXMLFile)).mkdir())
        {
          System.out.println("Created directory " + pluginName); 
        }
        File destination = new File(directoryToCopyXMLFile + File.separator + pluginFileName);
        pluginXMLFileDestList.put(pluginName, destination);
        SAXBuilder sxb = new SAXBuilder();
        Document document;
		try {
	    	sxb.setValidation(false);
	    	sxb.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			
			document = sxb.build(pluginXmlFile);
			
      	    DocType docType = document.getDocType();
	    	  
	    	if (docType != null)
	    	    document.removeContent(document.getContent(0));

			Map<String, String> map = new HashMap<String, String>();
			map.put("type", "text/xsl");
			map.put("href", "../plugin.xsl");	    	
	    	
			ProcessingInstruction pi = new ProcessingInstruction("xml-stylesheet", map);
			document.addContent(0, pi);	    	
	    	
	         XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
	         sortie.output(document, new FileOutputStream(directoryToCopyXMLFile+File.separator+pluginFileName));
	         
		} catch (JDOMException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    }	
        
    /**
     * This method creates the index.html page.
     * This file display all the ISAC plugin name with a link to their description. 
     */ 
    private static void generateIndex() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\">");
    	sb.append("<HTML>");
    	sb.append("  <HEAD>");
    	sb.append("    <TITLE>ISAC Plug-ins Help</TITLE>");
    	sb.append("  </HEAD>");
    	sb.append("  <FRAMESET cols=\"20%,80%\">");
    	sb.append("    <FRAMESET rows=\"100%\">");
    	sb.append("      <FRAME src=\"overview-frame.html\" name=\"pluginListFrame\" title=\"All ISAC Plugins\">");
    	sb.append("    </FRAMESET>");
    	sb.append("    <FRAME src=\"overview-summary.html\" name=\"pluginFrame\" title=\"ISAC plugins description\">");
    	sb.append("    <NOFRAMES>");
    	sb.append("      <H2>Frame Alert</H2>");
    	sb.append("      <P>");
    	sb.append("        This document is designed to be viewed using the frames feature. If you see this message, you are using a non-frame-capable web client.");
        sb.append("        <BR>");
        sb.append("        Link to<A HREF=\"overview-summary.html\">Non-frame version.</A>");
        sb.append("      </p>");
        sb.append("    </NOFRAMES>");
        sb.append("  </FRAMESET>");
        sb.append("</HTML>");
        
        FileWriter fw;
		try {
			fw = new FileWriter(helpPath+File.separator+"index.html");
			fw.write(sb.toString());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
    }

    /**
     * This method creates the overview-frame.html page.
     * This file display all the ISAC plugin name with a link to their description. 
     */       
    private static void generateOverviewFrame() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\">");
    	sb.append("<HTML>");
    	sb.append("  <HEAD>");
    	sb.append("    <TITLE>All ISAC plugins</TITLE>");
    	sb.append("    <LINK REL =\"stylesheet\" TYPE=\"text/css\" HREF=\"styles.css\" TITLE=\"Style\">");
    	sb.append("  </HEAD>");
    	sb.append("  <BODY BGCOLOR=\"white\">");
    	sb.append("    <FONT size=\"+1\" CLASS=\"FrameHeadingFont\">");
    	sb.append("      <B>All ISAC plugins</B>");
    	sb.append("    </FONT>");
    	sb.append("    <BR>");
    	for (Map.Entry<String,String> i : pluginXMLRelatifPathList.entrySet())
    	{
    		sb.append("    <DIV>");
    		sb.append("    <A HREF=\"" + i.getValue() + "\" title=\"" + i.getKey() + " plugin\" target=\"pluginFrame\">" + i.getKey()+"</A>");
    		sb.append("    </DIV>");
    		sb.append("    <BR>");
    	}
    	sb.append("  </BODY>");
        sb.append("</HTML>");
        
        FileWriter fw;
		try {
			fw = new FileWriter(helpPath+File.separator+"overview-frame.html");
			fw.write(sb.toString());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }    

    /**
     * This method creates the overview-summary.html page.
     * This file display a summary of all the ISAC plugin 
     */    
    private static void generateOverviewSummary() {
    	StringBuilder summaryStringBuilder = new StringBuilder();
    	summaryStringBuilder.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\" \"http://www.w3.org/TR/html4/frameset.dtd\">");
    	summaryStringBuilder.append("<HTML>");
    	summaryStringBuilder.append("  <HEAD>");
    	summaryStringBuilder.append("    <TITLE>All ISAC plugins summary</TITLE>");
    	summaryStringBuilder.append("    <LINK REL =\"stylesheet\" TYPE=\"text/css\" HREF=\"styles.css\" TITLE=\"Style\">");
    	summaryStringBuilder.append("  </HEAD>");
    	summaryStringBuilder.append("  <BODY BGCOLOR=\"white\">");
    	
    	summaryStringBuilder.append("    <h1 CLASS=\"FrameHeadingFont\">");
    	summaryStringBuilder.append("      All ISAC plugins summary");
    	summaryStringBuilder.append("    </h1>");
    	summaryStringBuilder.append("    <BR>");
    	
    	summaryStringBuilder.append("    <TABLE BORDER=\"1\" WIDTH=\"100%\" CELLPADDING=\"3\" CELLSPACING=\"0\" SUMMARY=\"\">");
    	summaryStringBuilder.append("      <TR BGCOLOR=\"#CCCCFF\" CLASS=\"TableHeadingColor\">");
    	summaryStringBuilder.append("        <TH><FONT SIZE=\"+2\"><B>Plugin name</B></FONT></TH>");
    	summaryStringBuilder.append("        <TH><FONT SIZE=\"+2\"><B>Class</B></FONT></TH>");
    	summaryStringBuilder.append("        <TH><FONT SIZE=\"+2\"><B>Description</B></FONT></TH>");
    	summaryStringBuilder.append("      </TR>");
    	
    	summaryStringBuilder.append(getPluginSummary());
    	
    	summaryStringBuilder.append("    </TABLE>");
    	
    	summaryStringBuilder.append("  </BODY>");
    	summaryStringBuilder.append("</HTML>");
        
        FileWriter fw;
		try {
			fw = new FileWriter(helpPath+File.separator+"overview-summary.html");
			fw.write(summaryStringBuilder.toString());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }  
    
    /**
     * This method creates the style sheet that will be used to display the ISAC plugin help
     */
    private static void generateStyleFile() {
    	StringBuilder styleFileStringBuilder = new StringBuilder();
    	styleFileStringBuilder.append("/* IsacPluginHelp style sheet */\n");
    	styleFileStringBuilder.append("/* Define colors, fonts and other style attributes here to override the defaults  */\n");
    	styleFileStringBuilder.append("/* Page background color */\n");
    	styleFileStringBuilder.append("body                     {background-color: #FFFFFF;font-size: small;font-family: sans-serif;}\n");
    	styleFileStringBuilder.append(".TableHeadingColor       { background: #CCCCFF }\n");
    	styleFileStringBuilder.append(".TableSubHeadingColor    { background: #EEEEFF }\n");
    	styleFileStringBuilder.append("h2.titleBloc             { background: #CCCCFF;padding:5px;border:1px solid #000;}\n");
    	styleFileStringBuilder.append("table.parameter    { margin:10px 0 20px 50px;border:1px solid #000;border-collapse: collapse;}\n");
    	styleFileStringBuilder.append("table.parameter td { padding:2px 5px 2px 15px;border:1px solid #000;}\n");
    	styleFileStringBuilder.append("p.summary                { padding: 10px 20px 10px 20px;text-align: justify;}\n");
    	styleFileStringBuilder.append("span.controlName         { margin: 10px;font-weight: bold;font-size: large;}\n");
    	styleFileStringBuilder.append("div.noParam              { margin-left: 20px;margin-bottom: 10px;font-weight: bold;}\n");
    	styleFileStringBuilder.append("div.separate             { background-color: #000;height: 1px;margin-bottom: 10px;}\n");
    	styleFileStringBuilder.append("p.indexInPlugin		   { text-align:center; vertical-align:middle}\n");

        FileWriter fw;
		try {
			fw = new FileWriter(helpPath+File.separator+"styles.css");
			fw.write(styleFileStringBuilder.toString());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}    	
    	
    }

    /**
     * This method creates the xsl file that will be used to display the xml plugin file.
     */
    private static void generatePlugInXSLFile() {
    	
    	String[] baliseName = {"test","control","sample","timer"};
    	String[] fonctionName = {"Conditions","Controls","Samples","Timers"};
    	
    	StringBuilder pluginXSLFileStringBuilder = new StringBuilder();
    	
    	pluginXSLFileStringBuilder = generateBeginningXslFile(pluginXSLFileStringBuilder, fonctionName);
    	
    	for (int i = 0; i < fonctionName.length; i++) {
    		pluginXSLFileStringBuilder = generateBloc(pluginXSLFileStringBuilder, fonctionName[i], baliseName[i], fonctionName[i]);
		}
    	
    	pluginXSLFileStringBuilder = generateEndXslFile(pluginXSLFileStringBuilder);
    	
        FileWriter fw;
		try {
			fw = new FileWriter(helpPath+File.separator+"plugin.xsl");
			fw.write(pluginXSLFileStringBuilder.toString());
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}       	
    }

    /**
     * This method adds the beginning of the xsl file.
     * @param sb empty StringBuilder object whose content is to be initialized
     * @return the StringBuilder object provided as parameter, initialized with the beginning of the xsl file. 
     */  
    private static StringBuilder generateBeginningXslFile(StringBuilder sb, String[] fonctionName) {
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		sb.append("<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n");
		sb.append("  <xsl:template match=\"plugin\">\n");
		sb.append("    <html>\n");
		sb.append("      <HEAD>\n");
		sb.append("			<TITLE>Isac Plug-in: <xsl:value-of select=\"@name\"/></TITLE>\n");
		sb.append("			<xsl:element name=\"META\">\n");
		sb.append("				<xsl:attribute name=\"NAME\">keywords</xsl:attribute>\n");
		sb.append("				<xsl:attribute name=\"CONTENT\">\n");
		sb.append("					<xsl:value-of select=\"object/@class\"/>\n");
		sb.append("				</xsl:attribute>\n");
		sb.append("			</xsl:element>\n");
		sb.append("			<LINK REL =\"stylesheet\" TYPE=\"text/css\" HREF=\"../styles.css\" TITLE=\"Style\" />\n");
		sb.append("		  </HEAD>\n");
		sb.append("		  <BODY>\n");
		sb.append("		    <H1>Isac Plug-in: <xsl:value-of select=\"@name\"/></H1>\n");
		sb.append("			<P CLASS=\"summary\">\n");
		sb.append("				<xsl:value-of select=\"object/help\" />\n");
		sb.append("			</P>\n");
		sb.append("			<P CLASS=\"indexInPlugin\">\n");
		sb.append("     		<a href=\"#"+fonctionName[0]+"\" title=\""+fonctionName[0]+"\">"+fonctionName[0]+"</a> - \n");
		sb.append("     		<a href=\"#"+fonctionName[1]+"\" title=\""+fonctionName[1]+"\">"+fonctionName[1]+"</a> - \n");
		sb.append("     		<a href=\"#"+fonctionName[2]+"\" title=\""+fonctionName[2]+"\">"+fonctionName[2]+"</a> - \n");
		sb.append("     		<a href=\"#"+fonctionName[3]+"\" title=\""+fonctionName[3]+"\">"+fonctionName[3]+"</a>\n");
		sb.append("         </P>\n");
		sb.append("			<xsl:choose>\n");
		sb.append("				<xsl:when test=\"object/params/param\">\n");
		sb.append("					<TABLE CLASS=\"parameter\">\n");
		sb.append("						<TR CLASS=\"TableSubHeadingColor\">\n");
		sb.append("							<TH>Plug-in parameters</TH>\n");
		sb.append("						</TR>\n");
		sb.append("						<xsl:for-each select=\"object/params/param\">\n");
		sb.append("						<TR>\n");
		sb.append("							<TD><xsl:value-of select=\"@name\" /></TD>\n");
		sb.append("						</TR>\n");
		sb.append("						</xsl:for-each>\n");
		sb.append("					</TABLE>\n");
		sb.append("				</xsl:when>\n");
		sb.append("				<xsl:otherwise>\n");
		sb.append("					<DIV CLASS=\"noParam\">No global parameter for this ISAC plugin</DIV>\n");
		sb.append("				</xsl:otherwise>\n");
		sb.append("			</xsl:choose>\n");
    	
    	return sb;
    }    
    
    /**
     * This method adds a part containing information about Conditions, Controls, Samples, Timers.
     * 
     * @param StringBuilder containing the xsl file's begin.
     * 
     * @return StringBuilder containing the xsl file's begin with information about Conditions, Controls, Samples, Timers. 
     */    
    private static StringBuilder generateBloc(StringBuilder pluginXSLFileStringBuilder, String name, String baliseName, String fonctionName) {
		pluginXSLFileStringBuilder.append("  <H2 CLASS=\"titleBloc\">"+fonctionName+"</H2>\n");
		pluginXSLFileStringBuilder.append("  <xsl:choose>\n");
		pluginXSLFileStringBuilder.append("    <xsl:when test=\""+baliseName+"\">\n");
		pluginXSLFileStringBuilder.append("      <xsl:for-each select=\""+baliseName+"\">\n");
		pluginXSLFileStringBuilder.append("		  <SPAN id=\""+fonctionName+"\" CLASS=\"controlName\"><xsl:value-of select=\"@name\" /></SPAN>\n");
		pluginXSLFileStringBuilder.append("		  <P CLASS=\"summary\"><xsl:value-of select=\"help\" /></P>\n");
		pluginXSLFileStringBuilder.append("			<xsl:choose>\n");
		pluginXSLFileStringBuilder.append("				<xsl:when test=\"params/param\">\n");
		pluginXSLFileStringBuilder.append("					<TABLE CLASS=\"parameter\">\n");
		pluginXSLFileStringBuilder.append("						<TR CLASS=\"TableSubHeadingColor\">\n");
		pluginXSLFileStringBuilder.append("							<TH>Parameters</TH>\n");
		pluginXSLFileStringBuilder.append("						</TR>\n");
		pluginXSLFileStringBuilder.append("						<xsl:for-each select=\"params/param\">\n");
		pluginXSLFileStringBuilder.append("						<TR>\n");
		pluginXSLFileStringBuilder.append("							<TD><xsl:value-of select=\"@name\" /></TD>\n");
		pluginXSLFileStringBuilder.append("						</TR>\n");
		pluginXSLFileStringBuilder.append("						</xsl:for-each>\n");
		pluginXSLFileStringBuilder.append("					</TABLE>\n");
		pluginXSLFileStringBuilder.append("				</xsl:when>\n");
		pluginXSLFileStringBuilder.append("				<xsl:otherwise>\n");
		pluginXSLFileStringBuilder.append("					<DIV CLASS=\"noParam\">No parameter.</DIV>\n");
		pluginXSLFileStringBuilder.append("				</xsl:otherwise>\n");
		pluginXSLFileStringBuilder.append("			</xsl:choose>\n");
		pluginXSLFileStringBuilder.append("		  <div class=\"separate\"></div>\n");
		pluginXSLFileStringBuilder.append("		</xsl:for-each>\n");
		pluginXSLFileStringBuilder.append("	  </xsl:when>\n");
		pluginXSLFileStringBuilder.append("	  <xsl:otherwise>\n");
		pluginXSLFileStringBuilder.append("      <DIV CLASS=\"noParam\">No "+name+" for this ISAC plugin</DIV>\n");
		pluginXSLFileStringBuilder.append("    </xsl:otherwise>\n");
		pluginXSLFileStringBuilder.append("  </xsl:choose>\n");	
		
		return pluginXSLFileStringBuilder;
    }
    
    /**
     * This method adds a part containing the end of the xsl file.
     * 
     * @param StringBuilder containing the xsl file's begin.
     * 
     * @return StringBuilder containing the all xsl file. 
     */    
    private static StringBuilder generateEndXslFile(StringBuilder sb) {
    	sb.append("</BODY>\n");
    	sb.append("</html>\n");
    	sb.append("</xsl:template>\n");
    	sb.append("</xsl:stylesheet>\n");
    	
    	return sb;
    }
}