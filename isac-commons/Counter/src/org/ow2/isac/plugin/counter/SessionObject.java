/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005-2007, 2013 France Telecom
* Copyright (C) 2014, 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.counter;

import java.math.BigInteger;
import java.util.Map;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;


/**
 * ISAC plug-in providing a counter, i.e. basically holding an Integer value
 * and supporting arithmetic Integer operations.
 * @author Bruno Dillenseger
 */
public class SessionObject implements ControlAction, TestAction, SessionObjectAction, DataProvider
{
	static final String PLUGIN_RADIX_ARG = "radix_arg";
	static final String PLUGIN_SHARED_ARG = "shared_arg";
	static final String PLUGIN_VALUE_ARG = "value_arg";
	static final int TEST_EQUALS = 0;
	static final String TEST_EQUALS_VALUE_ARG = "value_arg";
	static final int TEST_NOT_EQUAL = 11;
	static final String TEST_NOT_EQUAL_VALUE_ARG = "value_arg";
	static final int TEST_GT = 1;
	static final String TEST_GT_VALUE_ARG = "value_arg";
	static final int TEST_GTE = 2;
	static final String TEST_GTE_VALUE_ARG = "value_arg";
	static final int TEST_LT = 3;
	static final String TEST_LT_VALUE_ARG = "value_arg";
	static final int TEST_LTE = 4;
	static final String TEST_LTE_VALUE_ARG = "value_arg";
	static final int TEST_IS_ZERO = 5;
	static final int TEST_IS_NOT_ZERO = 6;
	static final int TEST_GT0 = 7;
	static final int TEST_GTE0 = 8;
	static final int TEST_LT0 = 9;
	static final int TEST_LTE0 = 10;
	static final int TEST_TEST_AND_SET = 15;
	static final String TEST_TEST_AND_SET_NEW_VALUE = "new_value";
	static final String TEST_TEST_AND_SET_TEST_VALUE = "test_value";
	static final int TEST_NOT_TEST_AND_SET = 16;
	static final String TEST_NOT_TEST_AND_SET_NEW_VALUE = "new_value";
	static final String TEST_NOT_TEST_AND_SET_TEST_VALUE = "test_value";
	static final int CONTROL_RESET = 0;
	static final int CONTROL_SET = 1;
	static final String CONTROL_SET_VALUE_ARG = "value_arg";
	static final int CONTROL_INC = 2;
	static final int CONTROL_DEC = 3;
	static final int CONTROL_ADD = 4;
	static final String CONTROL_ADD_VALUE_ARG = "value_arg";
	static final int CONTROL_SUB = 5;
	static final String CONTROL_SUB_VALUE_ARG = "value_arg";
	static final int CONTROL_DIV = 12;
	static final String CONTROL_DIV_INTEGER = "integer";
	static final int CONTROL_MOD = 13;
	static final String CONTROL_MOD_INTEGER = "integer";
	static final int CONTROL_MULTIPLY = 14;
	static final String CONTROL_MULTIPLY_INTEGER = "integer";
	static private final String ENABLED = "enabled";
	static private final String GET_INC = "++";
	static private final String GET_DEC = "--";
	static private final String GET_SEP = "/";

	private BigInteger initialValue = BigInteger.ZERO;
	private volatile BigInteger value = BigInteger.ZERO;
	private boolean shared = false;
	private SessionObject specimen = null;
	private int radix = 10;


	/**
	 * Build a new SessionObject for this plugin
	 * @param params The table containing all the parameters
	 */
	public SessionObject(Map<String,String> params)
	{
		String radixStr = params.get(PLUGIN_RADIX_ARG);
		if (radixStr != null && ! radixStr.trim().isEmpty())
		{
			radix = Integer.parseInt(radixStr);
		}
		String valueStr = params.get(PLUGIN_VALUE_ARG);
		if (valueStr != null && valueStr.length() > 0)
		{
			initialValue = new BigInteger(valueStr, radix);
		}
		shared = ParameterParser.getCheckBox(params.get(PLUGIN_SHARED_ARG)).contains(ENABLED);
		if (shared)
		{
			value = initialValue;
		}
	}


	/**
	 * This constructor is used to clone the specified session object
	 *
	 * @param toClone
	 *            The session object to clone
	 */
	private SessionObject(SessionObject toClone)
	{
		initialValue = toClone.initialValue;
		shared = toClone.shared;
		radix = toClone.radix;
		specimen = toClone;
		if (!shared)
		{
			value = initialValue;
		}
	}


	///////////////////////////
	// TestAction interface //
	///////////////////////////


	public boolean doTest(int number, Map<String,String> params)
		throws IsacRuntimeException
	{
		if (shared)
		{
			value = specimen.value;
		}
		switch (number)
		{
			case TEST_NOT_TEST_AND_SET:
				if (shared)
				{
					synchronized(specimen)
					{
						if (specimen.value.equals(new BigInteger(params.get(TEST_NOT_TEST_AND_SET_TEST_VALUE), radix)))
						{
							specimen.value = new BigInteger(params.get(TEST_NOT_TEST_AND_SET_NEW_VALUE), radix);
							return false;
						}
					}
				}
				else
				{
					if (value.equals(new BigInteger(params.get(TEST_NOT_TEST_AND_SET_TEST_VALUE), radix)))
					{
						value = new BigInteger(params.get(TEST_NOT_TEST_AND_SET_NEW_VALUE), radix);
						return false;
					}
				}
				return true;
			case TEST_TEST_AND_SET:
				if (shared)
				{
					synchronized(specimen)
					{
						if (specimen.value.equals(new BigInteger(params.get(TEST_TEST_AND_SET_TEST_VALUE), radix)))
						{
							specimen.value = new BigInteger(params.get(TEST_TEST_AND_SET_NEW_VALUE), radix);
							return true;
						}
					}
				}
				else
				{
					if (value.equals(new BigInteger(params.get(TEST_TEST_AND_SET_TEST_VALUE), radix)))
					{
						value = new BigInteger(params.get(TEST_TEST_AND_SET_NEW_VALUE), radix);
						return true;
					}
				}
				return false;
			case TEST_EQUALS:
				return value.compareTo(new BigInteger(params.get(TEST_EQUALS_VALUE_ARG), radix)) == 0;
			case TEST_NOT_EQUAL:
				return value.compareTo(new BigInteger(params.get(TEST_NOT_EQUAL_VALUE_ARG), radix)) != 0;
			case TEST_GT:
				return value.compareTo(new BigInteger(params.get(TEST_GT_VALUE_ARG), radix)) > 0;
			case TEST_GTE:
				return value.compareTo(new BigInteger(params.get(TEST_GTE_VALUE_ARG), radix)) >= 0;
			case TEST_LT:
				return value.compareTo(new BigInteger(params.get(TEST_LT_VALUE_ARG), radix)) < 0;
			case TEST_LTE:
				return value.compareTo(new BigInteger(params.get(TEST_LTE_VALUE_ARG), radix)) <= 0;
			case TEST_IS_ZERO:
				return value.equals(BigInteger.ZERO);
			case TEST_IS_NOT_ZERO:
				return ! value.equals(BigInteger.ZERO);
			case TEST_GT0:
				return value.compareTo(BigInteger.ZERO) > 0;
			case TEST_GTE0:
				return value.compareTo(BigInteger.ZERO) >= 0;
			case TEST_LT0:
				return value.compareTo(BigInteger.ZERO) < 0;
			case TEST_LTE0:
				return value.compareTo(BigInteger.ZERO) <= 0;
			default:
				throw new Error("Fatal error in ISAC's Counter plug-in: unknown test identifier " + number);
		}
	}


	////////////////////////////
	// ControlAction interface //
	////////////////////////////


	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_MULTIPLY:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = specimen.value.multiply(new BigInteger(params.get(CONTROL_MULTIPLY_INTEGER), radix));
					}
				}
				else
				{
					value = value.multiply(new BigInteger(params.get(CONTROL_MULTIPLY_INTEGER), radix));
				}
				return;
			case CONTROL_MOD:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = specimen.value.remainder(new BigInteger(params.get(CONTROL_MOD_INTEGER), radix));
					}
				}
				else
				{
					value = value.remainder(new BigInteger(params.get(CONTROL_MOD_INTEGER), radix));
				}
				return;
			case CONTROL_DIV:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = specimen.value.divide(new BigInteger(params.get(CONTROL_DIV_INTEGER), radix));
					}
				}
				else
				{
					value = value.divide(new BigInteger(params.get(CONTROL_DIV_INTEGER), radix));
				}
				return;
			case CONTROL_RESET:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = specimen.initialValue;
					}
				}
				else
				{
					value = initialValue;
				}
				return;
			case CONTROL_SET:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = new BigInteger(params.get(CONTROL_SET_VALUE_ARG), radix);
					}
				}
				else
				{
					value = new BigInteger(params.get(CONTROL_SET_VALUE_ARG), radix);
				}
				return;
			case CONTROL_INC:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = specimen.value.add(BigInteger.ONE);
					}
				}
				else
				{
					value = value.add(BigInteger.ONE);
				}
				return;
			case CONTROL_DEC:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = specimen.value.subtract(BigInteger.ONE);
					}
				}
				else
				{
					value = value.subtract(BigInteger.ONE);
				}
				return;
			case CONTROL_ADD:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = specimen.value.add(new BigInteger(params.get(CONTROL_ADD_VALUE_ARG), radix));
					}
				}
				else
				{
					value = value.add(new BigInteger(params.get(CONTROL_ADD_VALUE_ARG), radix));
				}
				return;
			case CONTROL_SUB:
				if (shared)
				{
					synchronized(specimen)
					{
						specimen.value = specimen.value.subtract(new BigInteger(params.get(CONTROL_SUB_VALUE_ARG), radix));
					}
				}
				else
				{
					value = value.subtract(new BigInteger(params.get(CONTROL_SUB_VALUE_ARG), radix));
				}
				return;
			default:
				throw new Error("Fatal error in ISAC's Counter plug-in: unknown control identifier " + number);
		}
	}


	////////////////////////////
	// DataProvider interface //
	////////////////////////////


	/**
	 * Gets the counter value as a String, with an optional string length
	 * @param lengthStr an integer value specifying the minimum length of the returned string.
	 * If the counter value has less digits than the given length, white spaces are added at the
	 * beginning of the string. The string is returned as is otherwise.
	 * @return a string representing current counter value, possibly with heading white spaces
	 * to fit the specified size.  
	 */
	public String doGet(String lengthStr)
	{
		String result;

		lengthStr= lengthStr.trim();
		if (lengthStr.startsWith(GET_INC))
		{
			result = getAndSet(CONTROL_INC);
			lengthStr = lengthStr.substring(GET_INC.length());
		}
		else if (lengthStr.startsWith(GET_DEC))
		{
			result = getAndSet(CONTROL_DEC);
			lengthStr = lengthStr.substring(GET_DEC.length());
		}
		else
		{
			if (shared)
			{
				synchronized (specimen)
				{
					result = specimen.value.toString(radix);
				}
			}
			else
			{
				result = value.toString(radix);
			}
		}
		if (lengthStr.length() > 1 && lengthStr.startsWith(GET_SEP))
		{
			lengthStr = lengthStr.substring(GET_SEP.length());
		}
		if (lengthStr.length() != 0)
		{
			try
			{
				int length = Integer.parseInt(lengthStr);
				while (result.length() < length)
				{
					result = " ".concat(result);
				}
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("Bad number format for Counter value: " + lengthStr, ex);
			}
		}
		return result;
	}


	private String getAndSet(int control)
	{
		String result;
		if (shared)
		{
			synchronized (specimen)
			{
				result = specimen.value.toString(radix);
				doControl(control, null);
			}
		}
		else
		{
			result = value.toString(radix);
			doControl(control, null);
		}
		return result;
	}


	///////////////////////////////////
	// SessionObjectAction interface //
	///////////////////////////////////


	public Object createNewSessionObject()
	{
		return new SessionObject(this);
	}


	public void close()
	{
	}


	public void reset()
	{
		if (! shared)
		{
			value = initialValue;
		}
	}
}
