<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plugin SYSTEM "classpath:org/ow2/clif/scenario/isac/dtd/plugin.dtd">

<plugin name="Counter">
  <object class="org.ow2.isac.plugin.counter.SessionObject">
    <help>Provides an integer counter, with associated primitives for changing and testing its value. Each virtual user may manage its own counter instance, or share a single counter, which can be useful to handle unique identifiers, for example.
Since the counter value is held by a multi-precision integer, it has no limit value. When setting, retrieving or comparing the counter value, the value is formatted accordingly to the radix set on import.

The counter value can be obtained through the variable expression ${counter_id:}, or with a format specification ${counter_id:n} where n is the size of the resulting string (white spaces are added at the head of the string to reach the specified number of characters).
Examples: ${counter_id:8} will be substituted by the string " 1234567" if the counter value is 1234567, or "123456789" if the value is 123456789 (no truncation).

It is also possible to perform atomic "get-and-set" operations, through variable expressions ${counter_id:++} and ${counter_id:--}. These variables are replaced by the current counter value, and the counter value is incremented (respectively decremented) in an atomic manner (i.e. when the counter is shared, no other virtual user can concurrently get or change the counter value). This is just a convenience for an unshared counter, while it is a necessary construct for managing unique identifiers through all vUsers sharing a counter.

A Counter default value is 0 (zero), unless another default value is specified on import.</help>
    <params>
      <param name="value_arg" type="String"></param>
      <param name="shared_arg" type="String"></param>
      <param name="radix_arg" type="String"></param>
    </params>
  </object>
  <test name="equals" number="0">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>True when the counter equals the given value, false otherwise.</help>
  </test>
  <test name="not_equal" number="11">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>True when the counter is different from the given value, false otherwise.</help>
  </test>
  <test name="gt" number="1">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>True when the counter is strictly greater than the given value, false otherwise.</help>
  </test>
  <test name="gte" number="2">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>True when the counter is greater than, or equal to the given value, false otherwise.</help>
  </test>
  <test name="lt" number="3">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>True when the counter is strictly less than the given value, false otherwise.</help>
  </test>
  <test name="lte" number="4">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>True when the counter is less than, or equal to the given value, false otherwise.</help>
  </test>
  <test name="is_zero" number="5">
    <help>True when the counter equals zero, false otherwise.</help>
  </test>
  <test name="is_not_zero" number="6">
    <help>True when the counter is different from zero, false otherwise.</help>
  </test>
  <test name="gt0" number="7">
    <help>True when the counter is strictly greater than zero, false otherwise.</help>
  </test>
  <test name="gte0" number="8">
    <help>True when the counter is greater than, or equal to zero, false otherwise.</help>
  </test>
  <test name="lt0" number="9">
    <help>True when the counter is strictly less than zero, false otherwise.</help>
  </test>
  <test name="lte0" number="10">
    <help>True when the counter is less than, or equal to zero, false otherwise.</help>
  </test>
  <test name="test_and_set" number="15">
    <params>
      <param name="test_value" type="String"></param>
      <param name="new_value" type="String"></param>
    </params>
    <help>True when the counter's value equals the given value, false otherwise. Atomic operation, setting the counter to a new value when it equals a given value. This operation is mostly useful when the counter is used in shared mode.</help>
  </test>
  <test name="not_test_and_set" number="16">
    <params>
      <param name="test_value" type="String"></param>
      <param name="new_value" type="String"></param>
    </params>
    <help>True when the counter's value differs from the given value, false otherwise. Atomic operation, setting the counter to a new value when it equals a given value. This operation is mostly useful when the counter is used in shared mode.</help>
  </test>
  <control name="reset" number="0">
    <help>Sets this Counter's value to the default value (as set on plug-in import).</help>
  </control>
  <control name="set" number="1">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>Sets this Counter's value to the provided Integer.</help>
  </control>
  <control name="inc" number="2">
    <help>Increments this Counter's value.</help>
  </control>
  <control name="dec" number="3">
    <help>Decrements this Counter's value.</help>
  </control>
  <control name="add" number="4">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>Adds the provided Integer to this Counter's value.</help>
  </control>
  <control name="sub" number="5">
    <params>
      <param name="value_arg" type="String"></param>
    </params>
    <help>Subtracts the provided Integer to the current Counter's value.</help>
  </control>
  <control name="multiply" number="14">
    <params>
      <param name="integer" type="String"></param>
    </params>
    <help>Multiplies this Counter's value by the provided Integer.</help>
  </control>
  <control name="div" number="12">
    <params>
      <param name="integer" type="String"></param>
    </params>
    <help>Divides this Counter's value by the provided Integer. This is an "integer division", which means the result is the integer part of the division result.</help>
  </control>
  <control name="mod" number="13">
    <params>
      <param name="integer" type="String"></param>
    </params>
    <help>Sets this Counter's value to the remainder of the integer division of current value by the provided integer.</help>
  </control>
</plugin>

