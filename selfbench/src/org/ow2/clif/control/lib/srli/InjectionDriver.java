/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.control.lib.srli;

import java.util.ArrayList;
import java.util.List;
import org.ow2.clif.control.lib.srli.util.Matrix;
import org.ow2.clif.control.lib.srli.util.StatUtil;
import org.ow2.clif.control.lib.srli.util.SquareMatrix;
import org.ow2.clif.control.lib.srli.util.Vector;


/**
 * @author Colin PUY
 * @author Nabila Salmi
 * @author Bruno Dillenseger
 */
public abstract class InjectionDriver
{
	/**
	 * Computes the necessary number of measures to get for current step.
	 * It depends about the stability of measures depicted by the sample
	 * variance compared to the sample mean value, according to a fixed
	 * confidence interval.
	 * @param mean mean value of current sample
	 * @param variance variance of current sample
	 * @return the number of measures to get for current step
	 * @see SelfbenchConstants#SAMPLING_DURATION_EPSILON
	 * @see SelfbenchConstants#SAMPLING_DURATION_Z
	 */
	static public int calculateSamplingMeasuresNumber(
		double mean,
		double variance)
	{
		int measuresNumber = 0;
		
		if (mean != 0)
		{
			double coeff =
				Math.pow(SelfbenchConstants.SAMPLING_DURATION_Z, 2) 
				/ Math.pow(SelfbenchConstants.SAMPLING_DURATION_EPSILON, 2);
			measuresNumber = (int)(coeff *  variance / Math.pow(mean, 2));
		}
		return measuresNumber;
	}
	
	/**
	 * Compute stabilization time for current load step
	 * @param current_lambda injection rate in current step
	 * @param previousVUserNumber number of virtual users in previous step
	 * @param currentVUserNumber number of virtual users in current step
	 * @param meanR average response time in current step
	 * @return stabilization time for current step
	 * @throws InjectionDriverException invalid parameters
	 */
	static public float calculateStabilizationTime(
		double current_lambda,
		int previousVUserNumber, 
		int currentVUserNumber,
		double meanR)
	throws InjectionDriverException
	{
		double lambda = current_lambda;
		double mu = lambda + 1 / (meanR * 1000);
		int length = currentVUserNumber;
		
		// build initial_pi: vector of length 'length',
		// whose value at index previous_lambda is 1,
		// and values at other indexes are 0
		Vector initial_pi = new Vector(length);
		initial_pi.fill(new Double(0));
		initial_pi.setData(previousVUserNumber, new Double(1));

		// build generating matrix Q of size length*length, initialized to 0
		SquareMatrix Q = new SquareMatrix(length);
		Q.fill(0L);
		Q.setData(0, 0, 1 - lambda / (lambda + mu));
		for (int i = 1; i < length; i++)
		{
			Q.setData(i, i-1, mu / (lambda + mu));
			Q.setData(i-1, i, lambda / (lambda + mu));
			Q.setData(i, i, -1L);
		}
		Q.setData(length - 1, length - 1, 1 - mu/(lambda+mu));

		// compute the number of necessary iterations nb_iter to reach the stationary 
		// state, by resolving the equations system pi*Q=0 with pi= initial_pi as 
		// initial vector, with Gauss-Seidel's iterative algorithm
		SquareMatrix A  = new SquareMatrix(Q.transpose());

		// build matrices D, L, U such that A=D-L-U initialized to 0
		SquareMatrix D = new SquareMatrix(Q.getOrder());
		D.fill(0);
		SquareMatrix L = new SquareMatrix(D);
		SquareMatrix U = new SquareMatrix(D);
		for (int i = 0; i < length; i++)
		{
			D.setData(i, i, A.getData(i,i));
			for (int j = 0; j < length; j++)
			{
				if (i > j)
				{
					L.setData(i, j, - A.getData(i,j));
				}
				if (i < j)
				{
					U.setData(i, j, - A.getData(i,j));
				}
			}
		}
		
		int nb_iter = 0;
		int max_it = 500;
		Vector pi_k = new Vector(initial_pi);
		
		// first GAUSS-SEIDEL iteration
		SquareMatrix inverseDMinusL = new SquareMatrix(Matrix.sub(D, L)).inverse();
		
		// pi_kplus1 = inverse(D-L) * U * pi_k
		Vector pi_kplus1 = new Vector(Matrix.product(Matrix.product(inverseDMinusL, U), pi_k));

		// next iterations
		while (
			(new Vector(Matrix.sub(pi_kplus1, pi_k)).getNorm() / pi_k.getNorm()) > SelfbenchConstants.STABILISATION_TIME_EPSILON
			&& nb_iter < max_it)
		{
			nb_iter++;
			pi_k = new Vector(pi_kplus1);
			pi_kplus1 = new Vector(Matrix.product(Matrix.product(inverseDMinusL, U), pi_k));
		}
		return (float)(nb_iter / (lambda + mu)) * 2;
	}
	
	/**
	 * Compute empirical mean of response times for a given sample of average response times
	 * for one injector. Caution: input statistical values related to response time are supposed
	 * to be in microseconds, while the returned value is in milliseconds.
	 * @param samplingInjectorStats sample of the injector's moving statistical values (microseconds)
	 * @param initialActionNumber	initial number of performed actions (requests) by the injector
	 * before this sample
	 * @return the empirical mean of the sample (milliseconds)
	 */
	static private double calculateMeanResponseTime(
		List<long[]> samplingInjectorStats,
		long initialActionNumber)
	{
		long measureNumberAction =  samplingInjectorStats.get(0)[4] - initialActionNumber;
		long measureMeanResponseTime = samplingInjectorStats.get(0)[1];
		long totalMeanResponseTime = measureMeanResponseTime * measureNumberAction;
		long totalNumberAction = measureNumberAction;
		
		for (int i = 1; i < samplingInjectorStats.size(); i++)
		{
			measureNumberAction = samplingInjectorStats.get(i)[4] - samplingInjectorStats.get(i-1)[4];
			measureMeanResponseTime =  samplingInjectorStats.get(i)[1];
			totalMeanResponseTime += measureNumberAction * measureMeanResponseTime;
			totalNumberAction += measureNumberAction;
		}
		double meanR = 0;
		if (totalNumberAction != 0)
		{
			meanR = (double)totalMeanResponseTime / (double)totalNumberAction;
		}
		return meanR / 1000d;
	}

	/**
	 * Compute empirical mean of response times for a given sample of average response times
	 * for several injectors. Caution: input statistical values related to response time are supposed
	 * to be in microseconds, while the returned value is in milliseconds.
	 * @param samplingInjectorStats all injectors' statistical samples (microseconds)
	 * @param statsBeforeSampling latest statistical values for each injector
	 * before these samples
	 * @return the empirical mean of all injectors' samples (milliseconds)
	 */
	public static double calculateMeanResponseTime(
		List<long[][]> samplingInjectorStats, 
		long[][] statsBeforeSampling)
	{
		// compute mean and number of actions for the first injector
		List<long[]> firstInjSamplingStats = new ArrayList<long[]>();
		long firstInjNbAction = 0;
		for (long[][] stats : samplingInjectorStats)
		{
			firstInjSamplingStats.add(stats[0]);
			firstInjNbAction = stats[0][4];
		}
		firstInjNbAction -= statsBeforeSampling[0][4];
		double meanR = calculateMeanResponseTime(firstInjSamplingStats, statsBeforeSampling[0][4]);
		
		// compute mean and number of actions for other injectors
		if (statsBeforeSampling.length > 1)
		{
			double[] average = new double[statsBeforeSampling.length];
			average[0] = meanR;
			long[] actions = new long[statsBeforeSampling.length];
			actions[0] = firstInjNbAction;
			
			for (int i = 1; i < statsBeforeSampling.length; i++)
			{
				List<long[]> injSamplingStats = new ArrayList<long[]>();
				long injNbAction = 0;
				for (long[][] stats : samplingInjectorStats)
				{
					injSamplingStats.add(stats[i]);
					injNbAction = stats[i][4];
				}
				average[i] = calculateMeanResponseTime(injSamplingStats, statsBeforeSampling[i][4]);
				actions[i] = injNbAction - statsBeforeSampling[i][4];
			}
			meanR = StatUtil.weightedMean(average, actions);
		}
		return meanR;
	}


	/**
	 * Compute the global variance of a sample of variances of one injector
	 * @param samplingInjectorStats statistical sample for the injector
	 * @param meanR average response time
	 * @param initialActionNumber	initial number of performed actions (requests)
	 * by the injector before this sample
	 * @return the global variance
	 */
	static private double calculateSampleVariance(
		List<long[]> samplingInjectorStats,
		double meanR, 
		long initialActionNumber)
	{
		long ni =  samplingInjectorStats.get(0)[4] - initialActionNumber;
		double varRi = Math.pow((double)samplingInjectorStats.get(0)[9] / 1000d, 2);
		double meanRi = (double)samplingInjectorStats.get(0)[1] / 1000d;
		double leftPart = (double)ni * varRi;
		double rightPart = (double)ni * Math.pow(meanRi - meanR, 2);
			
		for (int i = 1; i < samplingInjectorStats.size(); i++)
		{
			ni = samplingInjectorStats.get(i)[4] - samplingInjectorStats.get(i-1)[4];
			varRi = Math.pow((double)samplingInjectorStats.get(i)[9] / 1000d, 2);
			meanRi = (double)samplingInjectorStats.get(i)[1] / 1000d; 
			leftPart += (double)ni * varRi;
			rightPart += (double)ni * Math.pow((double)meanRi - meanR, 2);
		}
		long n =
			samplingInjectorStats.get(samplingInjectorStats.size() - 1)[4]
			- initialActionNumber;
		double varR = 0;
		if (n != 0)
		{
			varR = ((double)1 / (double)n) * leftPart + ((double)1 / (double)n) * rightPart;
		}
		return varR;
	}


	/**
	 * Compute the global variance of a sample of variances of several injectors
	 * @param samplingInjectorStats statistical samples for all the injectors
	 * @param p_InitialActionNumber latest statistical values of the injectors
	 * before these samples
	 * @return global variance for all the injectors
	 */
	static public double calculateSampleVariance(
		List<long[][]> samplingInjectorStats, 
		long[][] statsBeforeSampling)
	{
		// compute variance and number of actions for the first injector
		List<long[]> firstInjSamplingStats = new ArrayList<long[]>();
		long firstInjNbAction = 0;
		for (long[][] stats : samplingInjectorStats)
		{
			firstInjSamplingStats.add(stats[0]);
			firstInjNbAction = stats[0][4];
		}
		double meanR = calculateMeanResponseTime(firstInjSamplingStats, statsBeforeSampling[0][4]);
		double varR = calculateSampleVariance(firstInjSamplingStats, meanR, statsBeforeSampling[0][4]);

		// compute variance and number of actions for the other injectors
		if (statsBeforeSampling.length > 1)
		{
			double[] variance = new double[statsBeforeSampling.length];
			variance[0] = varR;
			long[] actions = new long[statsBeforeSampling.length];
			actions[0] = firstInjNbAction - statsBeforeSampling[0][4];
			double[] averages = new double[statsBeforeSampling.length];
			averages[0] = calculateMeanResponseTime(firstInjSamplingStats, statsBeforeSampling[0][4]);
			
			for (int i = 1; i < statsBeforeSampling.length; i++)
			{
				List<long[]> injSamplingStats = new ArrayList<long[]>();
				long injNbAction = 0;
				for (long[][] stats : samplingInjectorStats)
				{
					injSamplingStats.add(stats[i]);
					injNbAction = stats[i][4];
				}
				averages[i] = calculateMeanResponseTime(
					injSamplingStats,
					statsBeforeSampling[i][4]);
				variance[i] = calculateSampleVariance(
					injSamplingStats,
					averages[i],
					statsBeforeSampling[i][4]);
				actions[i] = injNbAction - statsBeforeSampling[i][4];
			}
			varR = StatUtil.weightedVariance(variance, averages, actions);
		}
		return varR;
	}


	/**
	 * Compute average per-vUser throughput for a statistical sample of one injector.
	 * @param samplingInjectorStats the statistical sample for the injector
	 * @param initialActionNumber the initial number of actions (requests) performed
	 * by the injector before this sample
	 * @param nbVusers the number of active virtual users
	 * @return the per-virtual user average request throughput
	 */
	static private double calculateActionThroughputPerUser(
		List<long[]> samplingInjectorStats, 
		long initialActionNumber,
		int nbVusers)
	{
		long measureNumberAction =  samplingInjectorStats.get(0)[4] - initialActionNumber;
		long measureTimeFrame = samplingInjectorStats.get(0)[0];
		double measureActionThroughput = (double)measureNumberAction / (double)measureTimeFrame;
		double totalActionThroughput = (double)measureNumberAction * measureActionThroughput;
		long totalAction = measureNumberAction;
		
		for (int i = 1; i < samplingInjectorStats.size(); i++)
		{
			measureNumberAction = samplingInjectorStats.get(i)[4] - samplingInjectorStats.get(i-1)[4];
			measureTimeFrame = samplingInjectorStats.get(i)[0];
			measureActionThroughput = (double)measureNumberAction / (double)measureTimeFrame;	
			totalActionThroughput += (double)measureNumberAction * measureActionThroughput;
			totalAction += measureNumberAction;
		}
		return (totalActionThroughput / (double)totalAction) / (double)nbVusers;
	}

	/**
	 * Compute average per-vUser throughput for a statistical sample of several injectors.
	 * @param samplingInjectorStats the statistical samples for the injectors
	 * @param p_InitialActionNumber the initial numbers of actions (requests) performed
	 * by the injectors before these samples
	 * @param nbVusers the total number of active virtual users
	 * @return the per-virtual user average request throughput
	 */
	static public double calculateActionThroughputPerUser(
		List<long[][]> samplingInjectorStats, 
		long[][] statsBeforeSampling,
		int nbVusers)
	{
		List<long[]> firstInjSamplingStats = new ArrayList<long[]>();
		for (long[][] stats : samplingInjectorStats)
		{
			firstInjSamplingStats.add(stats[0]);
		}
		double throughput = 
			calculateActionThroughputPerUser(firstInjSamplingStats, statsBeforeSampling[0][4], nbVusers);	
		return throughput;
	}

	/**
	 * Compute the number of virtual users for next step
	 * @param cMax currently assumed maximum global sustainable throughput
	 * @param perVuserThroughput per-virtual user throughput as assumed from the scenario
	 * @param nbSteps number of steps to test before trying to reach cMax
	 * @return increment of virtual users number for next step
	 */
	static public int calculateVUserIncrement(
		double cMax,
		double perVuserThroughput,
		int nbSteps)
	{
		double loadRiseRate = cMax / (double)nbSteps;
		return (int)Math.round(loadRiseRate / perVuserThroughput);
	}
}
