/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.control.lib.srli.util;

import org.ow2.clif.control.lib.srli.InjectionDriverException;


/**
 * Specialization of the Matrix class for square matrices
 * @author Colin PUY
 */
public class SquareMatrix extends Matrix
{
	/**
	 * Generate an identity matrix
	 * @param the matrix dimension
	 * @return the identity matrix
	 */
	static public SquareMatrix getIdentity(int dimension)
	{
		SquareMatrix identity = new SquareMatrix(dimension);
		for (int i = 0; i < dimension; i++)
		{
			for (int j = 0; j < dimension; j++)
			{
				try
				{
					identity.setData(i, j, (i == j) ?  1 : 0);
				}
				catch (InjectionDriverException ex)
				{
					// should never occur
					throw new Error("Algorithm error in identity matrix calculus.", ex);
				}
			}
		}
		return identity;
	}

	/** dimension of this square matrix */
	private int m_Order;

	/**
	 * Creates a new square matrix with the given dimension
	 * @param dimension the square matrix dimension
	 */
	public SquareMatrix(int dimension)
	{
		super(dimension, dimension);
		m_Order = dimension;
	} 
	
	/**
	 * Copy constructor
	 */
	public SquareMatrix(SquareMatrix matrix)
	{
		super(matrix);
		m_Order = matrix.m_Order;
	}

	/**
	 * Copy an ordinary Matrix object, presumed to be square,
	 * into a new SquareMatrix object
	 * @param the Matrix object to copy, which should be a square matrix
	 * @throws InjectionDriverException the matrix was not a square matrix
	 */
	public SquareMatrix(Matrix matrix)
	throws InjectionDriverException
	{
		this(matrix.getLines());
		setData(matrix.getData());
	}

	/**
	 * Get the dimension of this square matrix
	 * @return the dimension of this square matrix
	 */
	public int getOrder() 
	{
		return m_Order;
	}

	/**
	 * Compute the inverse matrix of this matrix. Method: product of the transpose matrix
	 * with the inverse of the determinant of the matrix
	 * @return the inverse matrix
	 * @throws InjectionDriverException this matrix is not inversible
	 */
	public SquareMatrix inverse() throws InjectionDriverException
	{
		SquareMatrix MM = new SquareMatrix(this);
		SquareMatrix M1 = new SquareMatrix(this);
		SquareMatrix Mi1 = getIdentity(m_Order);
		SquareMatrix Minv = getIdentity(m_Order);

		for (int i = 0; i < this.m_Order; i++)
		{
			if (MM.getData(i, i) == 0)
			{
				throw new InjectionDriverException(
						InjectionDriverException.Type.ILLEGAL_GAUSS_PIVOT_INVERSION);
			}			
			for (int j = 0; j < this.m_Order; j++)
			{
				M1.setData(i, j, MM.getData(i,j) / MM.getData(i,i));
				Mi1.setData(i, j, Minv.getData(i,j) / MM.getData(i,i));
			}
			MM.setData(M1.getData());
			Minv.setData(Mi1.getData());
			for (int k = 0; k < this.m_Order; k++)
			{
				if (k != i)
					for (int j = 0;j < this.m_Order; j++)
					{
						M1.setData(k, j, 
								MM.getData(k,j) - MM.getData(i,j) * MM.getData(k,i));

						Mi1.setData(k, j, 
								Minv.getData(k,j) - Minv.getData(i,j) * MM.getData(k,i));
					}
			}
			MM.setData(M1.getData());
			Minv.setData(Mi1.getData());
		}
		return Minv;
	}
}
