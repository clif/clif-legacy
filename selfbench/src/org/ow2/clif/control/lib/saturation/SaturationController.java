/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.control.lib.saturation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;
import java.util.Properties;
import java.util.Set;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import org.ow2.clif.console.lib.batch.StatPublisher;
import org.ow2.clif.control.lib.AbstractControllerImpl;
import org.ow2.clif.control.lib.srli.SelfbenchConstants;
import org.ow2.clif.supervisor.api.TestControl;

/**
 * Controller component for checking saturation of some blades in a CLIF test.
 * Its acts as a saturation control loop factory, instantiating a new control
 * loop on each reception of a Properties object on JMS topic "saturation".
 * This Properties object defines the saturation detection policy. Saturation is
 * checked on each message reception on topic "stats".
 * When saturation is detected, a SaturationException object is published on JMS
 * topic "saturation". This exception holds a key, originally given by the policy
 * through the value of "saturation.key" property, which enables identifying which
 * policy triggered the exception. 
 * @see SaturationChecker
 * @see SaturationException
 * @see StatPublisher
 * 
 * @author Bruno Dillenseger
 */
public class SaturationController
	extends AbstractControllerImpl
{
	protected TopicSubscriber saturationSub;
	protected TopicSubscriber configurationSub;
	protected TopicSubscriber statsSub;
	protected TopicSession sessionSub;
	protected Topic satTopic;
	protected Topic confTopic;
	protected Topic statsTopic;
	protected TopicConnection connection;
	protected Set<SaturationChecker> checkers = new HashSet<SaturationChecker>();
	protected ConfigurationListener confListener;
	protected StatsListener statsListener;

	/**
	 * Creates a new saturation controller component attached to a given
	 * deployed CLIF test. It subscribes to topics "configuration" and "saturation"
	 * to react on configuration updates (blades added or removed) and to create
	 * saturation control loops applying saturation policies
	 * @param tcItf the TestControl interface of the target CLIF test's
	 * supervisor component
	 * @throws Exception the saturation controller component could not be created
	 */
	public SaturationController(TestControl tcItf) throws Exception
	{
		super(tcItf);
		satTopic = (Topic) jndi.lookup(SelfbenchConstants.SATURATION_TOPIC);
		confTopic = (Topic) jndi.lookup(SelfbenchConstants.CONFIGURATION_TOPIC);
		statsTopic = (Topic) jndi.lookup(SelfbenchConstants.STATS_TOPIC);
		connection = tcf.createTopicConnection(SelfbenchConstants.USERNAME, SelfbenchConstants.PASSWORD);
	    sessionSub = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
		saturationSub = sessionSub.createSubscriber(satTopic, null, true);
	    saturationSub.setMessageListener(new SaturationListener());
	    configurationSub = sessionSub.createSubscriber(confTopic);
	    configurationSub.setMessageListener(confListener = new ConfigurationListener());
	    statsSub = sessionSub.createSubscriber(statsTopic, null, true);
	    statsSub.setMessageListener(statsListener = new StatsListener());
	    connection.start();
	}


	/**
	 * Class for getting messages from topic "configuration":
	 * on any message received, update the SaturationChecker
	 * instances to give the new blades identifiers.
	 */
	class ConfigurationListener extends Observable implements MessageListener
	{
		public void onMessage(Message msg)
		{
			try
			{
   				setChanged();
   				notifyObservers(tcItf.getBladesIds());
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}


	/**
	 * Class for getting messages from topic "saturation":
	 * create a new SaturationChecker instance to process
	 * each saturation policy received.
	 */
	class SaturationListener implements MessageListener
	{
		public synchronized void onMessage(Message msg)
		{
			try
			{
				if (msg instanceof TextMessage)
				{
					System.out.println(((TextMessage)msg).getText());
				}
				else if (
					msg instanceof ObjectMessage
					&& ((ObjectMessage)msg).getObject() instanceof Properties)
				{
					Properties policy = (Properties)((ObjectMessage)msg).getObject();
					synchronized (this)
					{
						new Thread(
							new SaturationChecker(
								tcItf,
								policy,
								satTopic,
								connection,
								confListener,
								statsListener)).start();
					}
				}
				else
				{
					System.out.println("Unexpected message: " + msg);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}

	/**
	 * Class for getting messages from topic "saturation":
	 * create a new SaturationChecker instance to process
	 * each saturation policy received.
	 */
	class StatsListener extends Observable implements MessageListener
	{
		public void onMessage(Message msg)
		{
			try
			{
				if (msg instanceof ObjectMessage
					&& ((ObjectMessage)msg).getObject() instanceof HashMap<?,?>)
				{
	   				setChanged();
	   				notifyObservers(((ObjectMessage)msg).getObject());
				}
				else
				{
					System.out.println("Unexpected message: " + msg);
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}
}
