/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2012 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import java.util.Properties;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import org.ow2.clif.control.lib.srli.SelfbenchConstants;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.clif.util.ExecutionContext;

/**
 * This command publishes a selfbench policy on the "benchmark" topic
 * and waits for the result (the maximum number of virtual users reached
 * before saturation).
 * The policy is loaded from file selfbench.props, located in current
 * directory or downloaded from the code server. It contains the global
 * policy for both saturation detection and load injection.
 * The JMS server and both the saturation controller and the load injection
 * controller must be run prior to run this command.
 * @author Bruno Dillenseger
 */
public class SelfbenchCmd implements MessageListener
{
	static protected Context jndi;
	static protected TopicConnectionFactory tcf;

	static {
		jndi = JNDI.init();
		try
		{
			tcf = (TopicConnectionFactory) jndi.lookup("tcf");
		}
		catch (Exception ex)
		{
			throw new Error("Can't get topic connection factory.", ex);
		}
	}

	/**
	 * @param args args[0] is the deployment name of the target test plan
	 */
	public static void main(String[] args)
	throws Exception
	{
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		ExecutionContext.init("./");
		Properties policies = new Properties();
		policies.load(ClifClassLoader.getClassLoader().getResourceAsStream(SelfbenchConstants.SELFBENCH_PROPS_FILE));
		SelfbenchCmd cmd = new SelfbenchCmd(policies);
		System.exit(cmd.run());
	}

	protected Properties policies;
	protected volatile Object result;
	TopicSession pubSession;
    TopicSession subSession;
    TopicPublisher benchmarkPub;
    TopicConnection connection;

	/**
	 * @param testPlanName the name of the deployed test plan name to use
	 * @return execution status code
	 */
	public SelfbenchCmd(Properties policies)
	throws Exception
	{
		this.policies = policies;
		connection = tcf.createTopicConnection(SelfbenchConstants.USERNAME, SelfbenchConstants.PASSWORD);
		pubSession = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
	    subSession = connection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
		Topic benchTopic = (Topic)jndi.lookup(SelfbenchConstants.BENCHMARK_TOPIC);
	    benchmarkPub = pubSession.createPublisher(benchTopic);
	    TopicSubscriber benchmarkSub = subSession.createSubscriber(benchTopic, null, true);
	    benchmarkSub.setMessageListener(this);
	    connection.start();
	}

	public int run()
	{
		try
		{
		    benchmarkPub.publish(pubSession.createObjectMessage(policies));
			synchronized(this)
			{
				while (result == null)
				{
					wait();
				}
			}
			System.out.println(result);
			return BatchUtil.SUCCESS;
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}

	///////////////////////////////
	// interface MessageListener //
	///////////////////////////////

	public synchronized void onMessage(Message msg)
	{
		try
		{
			if (msg instanceof TextMessage)
			{
				System.out.println(((TextMessage)msg).getText());
			}
			else if (
				msg instanceof ObjectMessage
				&& ((ObjectMessage)msg).getObject() instanceof Integer)
			{
				result = ((ObjectMessage)msg).getObject();
			}
			else
			{
				System.out.println("Unexpected message: " + msg);
			}
		}
		catch (Exception ex)
		{
			result = ex;
		}
		notifyAll();
	}
}
