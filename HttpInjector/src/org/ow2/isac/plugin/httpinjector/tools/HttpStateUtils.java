/*
 *
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2005, 2006 France Telecom
 * Copyright (C) 2013 Orange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.httpinjector.tools;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Map;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.isac.plugin.httpinjector.SessionObject;

/**
 * Class that deals with the HttpState of a connection Created on august 17 2004
 *
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public class HttpStateUtils
{
	/**
	 * Function which return the CookiePolicy (an int) corresponding to the
	 * String cookiePolicy
	 *
	 * @param cookiePolicy
	 *            String representing what sort of policy
	 * @return the CookiePolicy corresponding
	 */
	private static String returnCookiePolicy(String cookiePolicy)
	{
		if (cookiePolicy.equals(ParameterConstants.RFC2109))
		{
			return CookiePolicy.RFC_2109;
		}
		else if (cookiePolicy.equals(ParameterConstants.RFC2965))
		{
			return CookiePolicy.RFC_2965;
		}
		else if (cookiePolicy.equals(ParameterConstants.NETSCAPE))
		{
			return CookiePolicy.NETSCAPE;
		}
		else if (cookiePolicy.equals(ParameterConstants.NOCOOKIE))
		{
			return CookiePolicy.IGNORE_COOKIES;
		}
		else
		{
			return CookiePolicy.BROWSER_COMPATIBILITY;
		}
	}

	/**
	 * build an httpState that is a container for HTTP attributes that may
	 * persist from request to request, such as cookies and authentication
	 * credentials.
	 *
	 * @param params
	 *            Hashtable containing all the variable
	 * @return an HttpState containing all Http attributes
	 */
	public static HttpState setHttpState(SessionObject sessionObject, Map<String,String> params)
	{
		HttpState httpState = (sessionObject.getHttpClient().getState() != null)
			? sessionObject.getHttpClient().getState()
			: new HttpState();
		// configure the credentials if they are defined
		if (params.containsKey(ParameterConstants.PROXYUSERNAME))
		{
			String proxyLogin = (String) params.get(ParameterConstants.PROXYUSERNAME);
			String proxyPassword = (String) params.get(ParameterConstants.PROXYUSERPASS);
			if (proxyLogin != null
				&& ! proxyLogin.isEmpty()
				&& proxyPassword != null
				&& ! proxyPassword.isEmpty())
			{
				httpState.setProxyCredentials(
					new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
					new UsernamePasswordCredentials(proxyLogin, proxyPassword));
			}
		}
		// build the cookies defined and add them to the HttpState
		if (params.containsKey(ParameterConstants.COOKIES))
		{
			Iterator<List<String>> cookieIter =
				ParameterParser.getTable(
					(String)params.get(ParameterConstants.COOKIES)).iterator();
			while (cookieIter.hasNext())
			{
				List<String> cookieParameters = cookieIter.next();
				Cookie cookie = new Cookie();
				String domain = cookieParameters.get(ParameterConstants.DOMAIN);
				String name = cookieParameters.get(ParameterConstants.NAME);
				String value = cookieParameters.get(ParameterConstants.VALUE);
				String path = cookieParameters.get(ParameterConstants.PATH);
				String expires = cookieParameters.get(ParameterConstants.EXPIRES);
				String maxAge = cookieParameters.get(ParameterConstants.MAXAGE);
				String secure = cookieParameters.get(ParameterConstants.SECURE);
				secure = (secure != null) ? secure : "";
				if (expires != null && !expires.equals(""))
				{
					StringTokenizer st = new StringTokenizer(expires, "/");
					Calendar expiresDate = Calendar.getInstance();
					if (st.countTokens() == 5)
					{
						// year / month / date / hour / minute / second
						expiresDate.setTime(new Date());
						expiresDate.set(
							new Integer(st.nextToken()).intValue(),
							new Integer(st.nextToken()).intValue(),
							new Integer(st.nextToken()).intValue(),
							new Integer(st.nextToken()).intValue(),
							new Integer(st.nextToken()).intValue(),
							new Integer(st.nextToken()).intValue());
					}
					else
					{
						throw new IsacRuntimeException("Invalid expiry date for cookie " + cookie);
					}
					cookie = new Cookie(
						domain,
						name,
						value,
						path,
						expiresDate.getTime(),
						new Boolean(secure).booleanValue());
				}
				else if (maxAge != null && !maxAge.equals(""))
				{
					cookie = new Cookie(
						domain,
						name,
						value,
						path,
						new Integer(maxAge).intValue(),
						new Boolean(secure).booleanValue());
				}
				else
				{
					cookie = new Cookie(domain, name, value);
				}
				cookie.setDomainAttributeSpecified(domain != null && !domain.equals(""));
				cookie.setPathAttributeSpecified(path != null && !path.equals(""));
				httpState.addCookie(cookie);
			}
		}
		// configure or reconfigure the general policy for the cookies
		if (params.containsKey(ParameterConstants.COOKIEPOLICY))
		{
			sessionObject.getHttpClient().getParams().setCookiePolicy(
				returnCookiePolicy(ParameterParser.getCombo(ParameterConstants.COOKIEPOLICY)));
		}
		return httpState;
	}
}
