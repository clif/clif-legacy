<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plugin SYSTEM "classpath:org/ow2/clif/scenario/isac/dtd/plugin.dtd">

<plugin name="HttpInjector">
  <object class="org.ow2.isac.plugin.httpinjector.SessionObject">
    <params>
      <param name="indepthload" type="String"></param>
      <param name="proxyhost" type="String"></param>
      <param name="proxyport" type="String"></param>
      <param name="proxyusername" type="String"></param>
      <param name="proxyuserpass" type="String"></param>
      <param name="preemptiveauthentication" type="String"></param>
      <param name="useragent" type="String"></param>
      <param name="unit" type="String"></param>
      <param name="cookieheader" type="String"></param>
    </params>
    <help>Support for HTTP protocol: get, post, multipart post, head, options, put, delete.
    By default, each of these methods just gets the given URL without analyzing its content.
    Optionally, the content may be analyzed to transparently include referenced external resources, such as frames, images, style sheets and scripts
    Provides conditions on response status code and headers.
    Provides a number of variables: user-defined variables to get response bodies,
    variables to get headers values ${HttpInj:#header_name},
    and a variable to get the response status code ${HttpInj:!}.</help>
  </object>
  <sample name="get" number="0">
    <params>
      <param name="uri" type="String"></param>
      <param name="redirect" type="String"></param>
      <param name="type" type="String"></param>
      <param name="comment" type="String"></param>
      <param name="headers" type="String"></param>
      <param name="parameters" type="String"></param>
      <param name="response-body" type="String"></param>
      <param name="username" type="String"></param>
      <param name="password" type="String"></param>
      <param name="hostauth" type="String"></param>
      <param name="realm" type="String"></param>
      <param name="proxylogin" type="String"></param>
      <param name="proxypassword" type="String"></param>
      <param name="cookies" type="String"></param>
      <param name="cookiepolicy" type="String"></param>
      <param name="proxyhost" type="String"></param>
      <param name="proxyport" type="String"></param>
      <param name="localaddress" type="String"></param>
      <help>The GET method means retrieve whatever information is identified by the requested URL
Parameters for the GET method :
-------------------------------
uri : an Uri beginning with http://. It define the URL and gets the document the URL points to
redirect : set to enabled  in order to follow redirection (3XX code)
parameters : set the parameters that were send with the request
response-body : the response body may be optionaly stored as a variable with the given name.
This variable's value can be retrieved with the usual expression ${plug-inId:variableName}.

Configuration of the authentication :
---------------------------------------
username/password : set the identifiers
realm : the authentication realm
host : the host the realm belongs to

Configuration of the Cookies :
------------------------------
cookies : a table of cookie definitions :
	name:   the cookie name
    value:  the cookie value
    domain: the domain this cookie can be sent to
    path:   the path prefix for which this cookie can be sent
    maxAge : the number of seconds for which this cookie is valid.
           maxAge is expected to be a non-negative number.
           (-1 signifies that the cookie should never expire)
    expires : a date in the format 'YY/MM/DD/hh/mm/ss'
    secure: if true this cookie can only be sent over secure
(define expires or maxAge or neither)
cookie policy : define the policy to follow (by default 'compatibility')

Configuration of the Proxy :
----------------------------
proxyhost : host of the proxy
proxyport : port of the proxy
proxylogin : login to be used if the proxy needs authentication
proxypassword : password corresponding
localadress : the virtual host name</help>
    </params>
  </sample>
  <sample name="post" number="1">
    <params>
      <param name="uri" type="String"></param>
      <param name="redirect" type="String"></param>
      <param name="type" type="String"></param>
      <param name="comment" type="String"></param>
      <param name="headers" type="String"></param>
      <param name="parameters" type="String"></param>
      <param name="bodyparameters" type="String"></param>
      <param name="request" type="String"></param>
      <param name="file" type="String"></param>
      <param name="response-body" type="String"></param>
      <param name="username" type="String"></param>
      <param name="password" type="String"></param>
      <param name="hostauth" type="String"></param>
      <param name="realm" type="String"></param>
      <param name="proxylogin" type="String"></param>
      <param name="proxypassword" type="String"></param>
      <param name="cookies" type="String"></param>
      <param name="cookiepolicy" type="String"></param>
      <param name="proxyhost" type="String"></param>
      <param name="proxyport" type="String"></param>
      <param name="localaddress" type="String"></param>
    </params>
    <help>The POST method is used to request that the origin server accept the data enclosed in the request as a new child of the request URL.
POST is designed to allow a uniform method to cover a variety of functions such as appending to a database, providing data to a data-handling process or posting to a message board.

Parameters for the POST method :
-------------------------------
uri: an Uri beginning with http://. It defines the target URL
redirect: set to enable automatic HTTP redirections (3XX code)
parameters: set the parameters to be included in the URI string
body alternatives: either a file, or a set of key-value pairs (typically for HTML forms), or a string. The specified Content-Type header is used for content type and character encoding.
response-body: the response body may be optionally stored in a variable with the given name. This variable's value can be retrieved with the usual expression ${plug-inId:variableName}.

Configuration of the authentication :
---------------------------------------
username/password : set the identifiers
realm : the authentication realm
host : the host the realm belongs to

Configuration of the Cookies :
------------------------------
cookies : a table of cookie definitions :
	name:   the cookie name
    value:  the cookie value
    domain: the domain this cookie can be sent to
    path:   the path prefix for which this cookie can be sent
    maxAge : the number of seconds for which this cookie is valid.
           maxAge is expected to be a non-negative number.
           (-1 signifies that the cookie should never expire)
    expires : a date in the format 'YY/MM/DD/hh/mm/ss'
    secure: if true this cookie can only be sent over secure
(define expires or maxAge or neither)
cookie policy : define the policy to follow (by default 'compatibility')

Configuration of the Proxy :
----------------------------
proxyhost : host of the proxy
proxyport : port of the proxy
proxylogin : login to be used if the proxy needs authentication
proxypassword : password corresponding
localadress : the virtual host name</help>
  </sample>
  <sample name="multipartpost" number="2">
    <params>
      <param name="uri" type="String"></param>
      <param name="redirect" type="String"></param>
      <param name="type" type="String"></param>
      <param name="comment" type="String"></param>
      <param name="headers" type="String"></param>
      <param name="parameters" type="String"></param>
      <param name="files" type="String"></param>
      <param name="response-body" type="String"></param>
      <param name="username" type="String"></param>
      <param name="password" type="String"></param>
      <param name="hostauth" type="String"></param>
      <param name="realm" type="String"></param>
      <param name="proxylogin" type="String"></param>
      <param name="proxypassword" type="String"></param>
      <param name="cookies" type="String"></param>
      <param name="cookiepolicy" type="String"></param>
      <param name="proxyhost" type="String"></param>
      <param name="proxyport" type="String"></param>
      <param name="localaddress" type="String"></param>
    </params>
    <help>The multipart post method is identical to the POST method, except that the request body is separated into multiple parts.
This method is generally used when uploading files to the server.


Parameters for the MULTIPARTPOST method :
-----------------------------------------
uri : an Uri beginning with http://. It define the URL and gets the document the URL points to
redirect : set to enabled  in order to follow redirection (3XX code)
parameters : set the parameters that were send with the request
files : a table of the file to multipost

Configuration of the authentication :
---------------------------------------
username/password : set the identifiers
realm : the authentication realm
host : the host the realm belongs to

Configuration of the Cookies :
------------------------------
cookies : a table of cookie definitions :
	name:   the cookie name
    value:  the cookie value
    domain: the domain this cookie can be sent to
    path:   the path prefix for which this cookie can be sent
    maxAge : the number of seconds for which this cookie is valid.
           maxAge is expected to be a non-negative number.
           (-1 signifies that the cookie should never expire)
    expires : a date in the format 'YY/MM/DD/hh/mm/ss'
    secure: if true this cookie can only be sent over secure
(define expires or maxAge or neither)
cookie policy : define the policy to follow (by default 'compatibility')

Configuration of the Proxy :
----------------------------
proxyhost : host of the proxy
proxyport : port of the proxy
proxylogin : login to be used if the proxy needs authentication
proxypassword : password corresponding
localadress : the virtual host name</help>
  </sample>
  <sample name="head" number="3">
    <params>
      <param name="uri" type="String"></param>
      <param name="redirect" type="String"></param>
      <param name="type" type="String"></param>
      <param name="comment" type="String"></param>
      <param name="headers" type="String"></param>
      <param name="parameters" type="String"></param>
      <param name="username" type="String"></param>
      <param name="password" type="String"></param>
      <param name="hostauth" type="String"></param>
      <param name="realm" type="String"></param>
      <param name="proxylogin" type="String"></param>
      <param name="proxypassword" type="String"></param>
      <param name="cookies" type="String"></param>
      <param name="cookiepolicy" type="String"></param>
      <param name="proxyhost" type="String"></param>
      <param name="proxyport" type="String"></param>
      <param name="localaddress" type="String"></param>
    </params>
    <help>The HEAD method is identical to GET except that the server must not return a message-body in the response.
This method can be used for obtaining metainformation about the document implied by the request without transferring the document itself.

Parameters for the HEAD method :
-------------------------------
uri : an Uri beginning with 'http://'.
redirect : set to enabled  in order to follow redirection (3XX code)
parameters : set the parameters that were send with the request

Configuration of the authentication :
---------------------------------------
username/password : set the identifiers
realm : the authentication realm
host : the host the realm belongs to

Configuration of the Cookies :
------------------------------
cookies : a table of cookie definitions :
	name:   the cookie name
    value:  the cookie value
    domain: the domain this cookie can be sent to
    path:   the path prefix for which this cookie can be sent
    maxAge : the number of seconds for which this cookie is valid.
           maxAge is expected to be a non-negative number.
           (-1 signifies that the cookie should never expire)
    expires : a date in the format 'YY/MM/DD/hh/mm/ss'
    secure: if true this cookie can only be sent over secure
(define expires or maxAge or neither)
cookie policy : define the policy to follow (by default 'compatibility')

Configuration of the Proxy :
----------------------------
proxyhost : host of the proxy
proxyport : port of the proxy
proxylogin : login to be used if the proxy needs authentication
proxypassword : password corresponding
localadress : the virtual host name</help>
  </sample>
  <sample name="options" number="4">
    <params>
      <param name="uri" type="String"></param>
      <param name="redirect" type="String"></param>
      <param name="type" type="String"></param>
      <param name="comment" type="String"></param>
      <param name="headers" type="String"></param>
      <param name="parameters" type="String"></param>
      <param name="response-body" type="String"></param>
      <param name="username" type="String"></param>
      <param name="password" type="String"></param>
      <param name="hostauth" type="String"></param>
      <param name="realm" type="String"></param>
      <param name="proxylogin" type="String"></param>
      <param name="proxypassword" type="String"></param>
      <param name="cookies" type="String"></param>
      <param name="cookiepolicy" type="String"></param>
      <param name="proxyhost" type="String"></param>
      <param name="proxyport" type="String"></param>
      <param name="localaddress" type="String"></param>
    </params>
    <help>The OPTIONS method represents a request for information about the communication options available on the request/response chain identified by the request URL.

Parameters for the OPTIONS method :
-------------------------------
uri : an Uri beginning with 'http://'.
redirect : set to enabled  in order to follow redirection (3XX code)
parameters : set the parameters that were send with the request

Configuration of the authentication :
---------------------------------------
username/password : set the identifiers
realm : the authentication realm
host : the host the realm belongs to

Configuration of the Cookies :
------------------------------
cookies : a table of cookie definitions :
	name:   the cookie name
    value:  the cookie value
    domain: the domain this cookie can be sent to
    path:   the path prefix for which this cookie can be sent
    maxAge : the number of seconds for which this cookie is valid.
           maxAge is expected to be a non-negative number.
           (-1 signifies that the cookie should never expire)
    expires : a date in the format 'YY/MM/DD/hh/mm/ss'
    secure: if true this cookie can only be sent over secure
(define expires or maxAge or neither)
cookie policy : define the policy to follow (by default 'compatibility')

Configuration of the Proxy :
----------------------------
proxyhost : host of the proxy
proxyport : port of the proxy
proxylogin : login to be used if the proxy needs authentication
proxypassword : password corresponding
localadress : the virtual host name</help>
  </sample>
  <sample name="put" number="5">
    <params>
      <param name="uri" type="String"></param>
      <param name="redirect" type="String"></param>
      <param name="type" type="String"></param>
      <param name="comment" type="String"></param>
      <param name="headers" type="String"></param>
      <param name="parameters" type="String"></param>
      <param name="request" type="String"></param>
      <param name="response-body" type="String"></param>
      <param name="file" type="String"></param>
      <param name="filetype" type="String"></param>
      <param name="username" type="String"></param>
      <param name="password" type="String"></param>
      <param name="hostauth" type="String"></param>
      <param name="realm" type="String"></param>
      <param name="proxylogin" type="String"></param>
      <param name="proxypassword" type="String"></param>
      <param name="cookies" type="String"></param>
      <param name="cookiepolicy" type="String"></param>
      <param name="proxyhost" type="String"></param>
      <param name="proxyport" type="String"></param>
      <param name="localaddress" type="String"></param>
    </params>
    <help>The PUT method requests that the enclosed document be stored under the supplied URL.
This method is generally disabled on publicly available servers because it is generally undesirable to allow clients to put new files on the server or to replace existing files.

Parameters for the PUT method :
-------------------------------
uri : an Uri beginning with 'http://'.
redirect : set to enabled  in order to follow redirection (3XX code)
parameters : set the parameters that were send with the request
file :  the name of the file to put
filetype : the type of the file to put. (optional)

Configuration of the authentication :
---------------------------------------
username/password : set the identifiers
realm : the authentication realm
host : the host the realm belongs to

Configuration of the Cookies :
------------------------------
cookies : a table of cookie definitions :
	name:   the cookie name
    value:  the cookie value
    domain: the domain this cookie can be sent to
    path:   the path prefix for which this cookie can be sent
    maxAge : the number of seconds for which this cookie is valid.
           maxAge is expected to be a non-negative number.
           (-1 signifies that the cookie should never expire)
    expires : a date in the format 'YY/MM/DD/hh/mm/ss'
    secure: if true this cookie can only be sent over secure
(define expires or maxAge or neither)
cookie policy : define the policy to follow (by default 'compatibility')

Configuration of the Proxy :
----------------------------
proxyhost : host of the proxy
proxyport : port of the proxy
proxylogin : login to be used if the proxy needs authentication
proxypassword : password corresponding
localadress : the virtual host name</help>
  </sample>
  <sample name="delete" number="6">
    <params>
      <param name="uri" type="String"></param>
      <param name="redirect" type="String"></param>
      <param name="type" type="String"></param>
      <param name="headers" type="String"></param>
      <param name="comment" type="String"></param>
      <param name="parameters" type="String"></param>
      <param name="response-body" type="String"></param>
      <param name="username" type="String"></param>
      <param name="password" type="String"></param>
      <param name="hostauth" type="String"></param>
      <param name="realm" type="String"></param>
      <param name="proxylogin" type="String"></param>
      <param name="proxypassword" type="String"></param>
      <param name="cookies" type="String"></param>
      <param name="cookiepolicy" type="String"></param>
      <param name="proxyhost" type="String"></param>
      <param name="proxyport" type="String"></param>
      <param name="localaddress" type="String"></param>
    </params>
    <help>The DELETE method requests that the server delete the resource identified by the request URL.
This method is generally disabled on publicly available servers because it is generally undesireable to allow clients to delete files on the server.

Parameters for the DELETE method :
-------------------------------
uri : an Uri beginning with 'http://'.
redirect : set to enabled  in order to follow redirection (3XX code)
parameters : set the parameters that were send with the request

Configuration of the authentication :
---------------------------------------
username/password : set the identifiers
realm : the authentication realm
host : the host the realm belongs to

Configuration of the Cookies :
------------------------------
cookies : a table of cookie definitions :
	name:   the cookie name
    value:  the cookie value
    domain: the domain this cookie can be sent to
    path:   the path prefix for which this cookie can be sent
    maxAge : the number of seconds for which this cookie is valid.
           maxAge is expected to be a non-negative number.
           (-1 signifies that the cookie should never expire)
    expires : a date in the format 'YY/MM/DD/hh/mm/ss'
    secure: if true this cookie can only be sent over secure
(define expires or maxAge or neither)
cookie policy : define the policy to follow (by default 'compatibility')

Configuration of the Proxy :
----------------------------
proxyhost : host of the proxy
proxyport : port of the proxy
proxylogin : login to be used if the proxy needs authentication
proxypassword : password corresponding
localadress : the virtual host name</help>
  </sample>
  <test name="is404Response" number="0">
    <help>True if the last response status was 404, false otherwise.</help>
  </test>
  <test name="isStatusCodeResponse" number="1">
    <params>
      <param name="statuscode" type="String"></param>
    </params>
    <help>True if the last response status code equals, or starts with, the given value or prefix; false otherwise.</help>
  </test>
  <test name="isHeaderValue" number="2">
    <params>
      <param name="headervalue" type="String"></param>
      <param name="headertype" type="String"></param>
      <param name="equality" type="String"></param>
    </params>
    <help>Checks a header value in the latest response.</help>
  </test>
  <test name="hasHeader" number="3">
    <params>
      <param name="headertype" type="String"></param>
    </params>
    <help>True if the given header is defined in the latest response, false otherwise.</help>
  </test>
  <test name="hasNoHeader" number="4">
    <params>
      <param name="headertype" type="String"></param>
    </params>
    <help>True if the given header is undefined in the latest response, false otherwise.</help>
  </test>
  <test name="statusIsOneOf" number="5">
    <params>
      <param name="statuscodes" type="String"></param>
    </params>
    <help>True if the last response status code equals one of the given values; false otherwise.</help>
  </test>
  <test name="statusIsNotOneOf" number="6">
    <params>
      <param name="statuscodes" type="String"></param>
    </params>
    <help>True if the last response status code differs from all the given values; false otherwise.</help>
  </test>
  <control name="setConnectionTimeOut" number="0">
    <params>
      <param name="timeout" type="String"></param>
    </params>
    <help>Sets the maximum delay (in milliseconds) waiting for the target server to accept the connection.
      When this delay is reached, the request is considered as a failure.
      A zero time-out value disables the time-out feature (infinite time-out).</help>
  </control>
  <control name="setResponseTimeOut" number="1">
    <params>
      <param name="timeout" type="String"></param>
    </params>
    <help>Sets the maximum delay (in milliseconds) waiting for a request response.
      When this delay is reached, the request is considered as a failure.
      A zero time-out value disables the time-out feature (infinite time-out).</help>
  </control>
  <control name="setErrorResponseCodes" number="2">
    <params>
      <param name="codes" type="String"></param>
    </params>
    <help>The HttpInjector states that an HTTP request is successful as long as it gets
      a response from the server, whatever the response code is.
      Here, you may specify that some chosen response codes must be considered as errors:
      you may provide either full 3-digits HTTP response codes, or just one or two digits prefixes.
      When a response code begins with one of the provided prefix, or equals one of the provided codes,
      the request is considered as failed.</help>
  </control>
</plugin>

