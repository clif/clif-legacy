/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012,2013 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.udpinjector;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.HashMap;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.storage.api.ActionEvent;

/**
 * Implementation of a session object for plugin ~UDPinjector~
 * @author Bruno Dillenseger
 */
public class SessionObject
	implements SessionObjectAction, DataProvider, ControlAction, SampleAction, TestAction
{
	static final String PLUGIN_UNIT = "unit";
	static final String PLUGIN_CHARSET = "charset";
	static final int TEST_ISCONNECTED = 4;
	static final int TEST_ISNOTCONNECTED = 5;
	static final int TEST_ISBOUND = 6;
	static final int TEST_ISNOTBOUND = 7;
	static final int TEST_ISCLOSED = 11;
	static final int TEST_ISNOTCLOSED = 12;
	static final int TEST_ISDEFINED = 17;
	static final String TEST_ISDEFINED_VARIABLE = "variable";
	static final int TEST_ISNOTDEFINED = 18;
	static final String TEST_ISNOTDEFINED_VARIABLE = "variable";
	static final int SAMPLE_CONNECT = 2;
	static final String SAMPLE_CONNECT_PORT = "port";
	static final String SAMPLE_CONNECT_HOST = "host";
	static final int SAMPLE_SEND = 13;
	static final String SAMPLE_SEND_COMMENT = "comment";
	static final String SAMPLE_SEND_DATA = "data";
	static final int SAMPLE_RECEIVE = 14;
	static final String SAMPLE_RECEIVE_COMMENT = "comment";
	static final String SAMPLE_RECEIVE_VARIABLE = "variable";
	static final int SAMPLE_STEAL = 15;
	static final String SAMPLE_STEAL_COMMENT = "comment";
	static final String SAMPLE_STEAL_BUFFER = "buffer";
	static final String SAMPLE_STEAL_PORT = "port";
	static final int SAMPLE_CONNECT2SENDER = 16;
	static final int CONTROL_BIND = 0;
	static final String CONTROL_BIND_PORT = "port";
	static final String CONTROL_BIND_HOST = "host";
	static final int CONTROL_CLOSE = 1;
	static final int CONTROL_DISCONNECT = 3;
	static final int CONTROL_SETRCVBUFSIZE = 8;
	static final String CONTROL_SETRCVBUFSIZE_SIZE = "size";
	static final int CONTROL_SETSNDBUFSIZE = 9;
	static final String CONTROL_SETSNDBUFSIZE_SIZE = "size";
	static final int CONTROL_SETTIMEOUT = 10;
	static final String CONTROL_SETTIMEOUT_TIMEOUT = "timeout";
	static final int CONTROL_CLEAR = 19;
	static final String CONTROL_CLEAR_VARIABLE = "variable";
	static final int CONTROL_CLEARALL = 20;
	static final String SEND_TYPE = "UDP SEND";
	static final String RECEIVE_TYPE = "UDP RECEIVE";
	static final String STEAL_TYPE = "UDP STEAL";
	static final String CONNECT_TYPE = "UDP CONNECT";

	static final SessionObject[] boundPorts = new SessionObject[65536];
	private Map<String,String> buffers;
	protected SocketAddress lastSenderAddress = null; 
	private DatagramSocket socket = null;
	private String charset;
	private boolean microsecond = false;

	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params) throws SocketException
	{
		charset = params.get(PLUGIN_CHARSET);
		if (charset == null || charset.trim().isEmpty())
		{
			charset = Charset.defaultCharset().name();
		}
		String unit = ParameterParser.getRadioGroup(params.get(PLUGIN_UNIT));
		microsecond = unit.equalsIgnoreCase("microsecond");
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so) throws SocketException
	{
		charset = so.charset;
		microsecond = so.microsecond;
		socket = new DatagramSocket((SocketAddress)null);
		buffers = new HashMap<String,String>();
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() throws IsacRuntimeException
	{
		try
		{
			return new SessionObject(this);
		}
		catch (SocketException ex)
		{
			throw new IsacRuntimeException("Can't create UDPinjector session object", ex);
		}
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		if (! socket.isClosed())
		{
			if (socket.isBound())
			{
				boundPorts[socket.getLocalPort()] = null;
			}
			socket.close();
		}
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	throws IsacRuntimeException
	{
		buffers.clear();
		lastSenderAddress = null;
		socket.disconnect();
		close();
		try
		{
			socket = new DatagramSocket((SocketAddress)null);
		}
		catch (SocketException ex)
		{
			throw new IsacRuntimeException("UDPInjector can't reset session object.", ex);
		}
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var)
	{
		return buffers.get(var);
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String,String> params)
	{
		switch (number)
		{
			case CONTROL_CLEARALL:
				buffers.clear();
				break;
			case CONTROL_CLEAR:
				String variable = params.get(CONTROL_CLEAR_VARIABLE);
				if (variable != null)
				{
					buffers.remove(variable);
				}
				break;
			case CONTROL_SETTIMEOUT:
				try
				{
					socket.setSoTimeout(Integer.parseInt(params.get(CONTROL_SETTIMEOUT_TIMEOUT)));
					break;
				}
				catch (SocketException ex)
				{
					throw new IsacRuntimeException("Can't set UDPinjector socket timeout", ex);
				}
			case CONTROL_SETSNDBUFSIZE:
				try
				{
					socket.setSendBufferSize(Integer.parseInt(params.get(CONTROL_SETSNDBUFSIZE_SIZE)));
					break;
				}
				catch (SocketException ex)
				{
					throw new IsacRuntimeException("Can't set UDPinjector socket send buffer size", ex);
				}
			case CONTROL_SETRCVBUFSIZE:
				try
				{
					socket.setReceiveBufferSize(Integer.parseInt(params.get(CONTROL_SETRCVBUFSIZE_SIZE)));
					break;
				}
				catch (SocketException ex)
				{
					throw new IsacRuntimeException("Can't set UDPinjector socket receive buffer size", ex);
				}
			case CONTROL_DISCONNECT:
				socket.disconnect();
				break;
			case CONTROL_CLOSE:
				close();
				break;
			case CONTROL_BIND:
				try
				{
					String hostname = params.get(CONTROL_BIND_HOST);
					InetAddress host = null;
					if (hostname != null)
					{
						hostname = hostname.trim();
						host = InetAddress.getByName(hostname);
					}
					int port = 0;
					String portString = params.get(CONTROL_BIND_PORT);
					try
					{
						if (portString != null && ! portString.isEmpty())
						{
							port = Integer.parseInt(portString);
						}
					}
					catch (NumberFormatException ex)
					{
						throw new IsacRuntimeException("Invalid port number for bind operation in UDPInjector:" + portString);
					}
					socket.bind(new InetSocketAddress(host, port));
					boundPorts[socket.getLocalPort()] = this;
				}
				catch (Exception ex)
				{
					throw new IsacRuntimeException("Can't bind UDPinjector", ex);
				}
				break;
			default:
				throw new Error("Unable to find this control in ~UDPinjector~ ISAC plugin: " + number);
		}
	}


	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report)
	{
		switch (number)
		{
			case SAMPLE_CONNECT2SENDER:
			{
				if (lastSenderAddress == null)
				{
					throw new IsacRuntimeException("Attempt to connect to a packet sender but no packet received.");
				}
				else
				{
					report.type = CONNECT_TYPE;
					return connectTo(lastSenderAddress, report);
				}
			}
			case SAMPLE_STEAL:
			{
				int port = Integer.parseInt(params.get(SAMPLE_STEAL_PORT));
				SessionObject target = boundPorts[port];
				if (target == null)
				{
					throw new IsacRuntimeException("Attempt to steal from unbound UDP port " + port);
				}
				else
				{
					report.type = STEAL_TYPE;
					report.comment = params.get(SAMPLE_STEAL_COMMENT);
					return receiveFrom(target, report, params.get(SAMPLE_STEAL_BUFFER).trim(), buffers);
				}
			}
			case SAMPLE_RECEIVE:
			{
				report.type = RECEIVE_TYPE;
				report.comment = params.get(SAMPLE_RECEIVE_COMMENT);
				return receiveFrom(this, report, params.get(SAMPLE_RECEIVE_VARIABLE), buffers);
			}
			case SAMPLE_SEND:
			{
				synchronized(this)
				{
					if (! socket.isConnected())
					{
						throw new IsacRuntimeException("UDPinjector must be connected before sending data.");
					}
					report.type = SEND_TYPE;
					report.comment = params.get(SAMPLE_SEND_COMMENT);
					report.setDate(System.currentTimeMillis());
					long nanoStart = 0;
					if (microsecond)
					{
						nanoStart = System.nanoTime();
					}
					try
					{
						byte[] data = params.get(SAMPLE_SEND_DATA).getBytes(charset);
						socket.send(new DatagramPacket(data, data.length));
						if (microsecond)
						{
							report.duration = (int)((System.nanoTime() - nanoStart) / 1000);
						}
						else
						{
							report.duration = (int)(System.currentTimeMillis() - report.getDate());
						}
						report.result = String.valueOf(data.length) + " bytes";
						report.successful = true;
					}
					catch (Exception ex)
					{
						report.successful = false;
						report.result = ex.toString();
					}
				}
				return report;
			}
			case SAMPLE_CONNECT:
			{
				String host = params.get(SAMPLE_CONNECT_HOST);
				int port = Integer.parseInt(params.get(SAMPLE_CONNECT_PORT));
				report.type = CONNECT_TYPE;
				return connectTo(new InetSocketAddress(host, port), report);
			}
			default:
				throw new Error("Unable to find this sample in ~UDPinjector~ ISAC plugin: " + number);
		}
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String,String> params)
	{
		String variable;

		switch (number)
		{
			case TEST_ISNOTDEFINED:
				variable = params.get(TEST_ISNOTDEFINED_VARIABLE);
				return
					(variable == null)
					|| ! buffers.containsKey(variable);
			case TEST_ISDEFINED:
				variable = params.get(TEST_ISDEFINED_VARIABLE);
				return
					(variable != null)
					&& buffers.containsKey(variable);
			case TEST_ISNOTCLOSED:
				return ! socket.isClosed();
			case TEST_ISCLOSED:
				return socket.isClosed();
			case TEST_ISNOTBOUND:
				return ! socket.isBound();
			case TEST_ISBOUND:
				return socket.isBound();
			case TEST_ISNOTCONNECTED:
				return ! socket.isConnected();
			case TEST_ISCONNECTED:
				return socket.isConnected();
			default:
				throw new Error("Unable to find this test in ~UDPinjector~ ISAC plugin: " + number);
		}
	}

	/////////////////////
	// utility methods //
	/////////////////////

	private ActionEvent connectTo(SocketAddress target, ActionEvent report)
	{
		report.setDate(System.currentTimeMillis());
		long nanoStart = 0;
		if (microsecond)
		{
			nanoStart = System.nanoTime();
		}
		try
		{
			socket.connect(target);
			if (socket.isConnected())
			{
				if (microsecond)
				{
					report.duration = (int)((System.nanoTime() - nanoStart) / 1000);
				}
				else
				{
					report.duration = (int)(System.currentTimeMillis() - report.getDate());
				}
				report.successful = true;
				report.result = "connected to " + target;
			}
			else
			{
				report.successful = false;
				report.comment = "Can't connect UDPinjector socket to " + target;
				report.result = "connection failed";
			}
		}
		catch (SocketException ex)
		{
			report.successful = false;
			report.comment = "Can't connect UDPinjector socket to " + target;
			report.result = ex.toString();
		}
		return report;
	}

	private static ActionEvent receiveFrom(
		SessionObject so,
		ActionEvent report,
		String bufferName,
		Map<String, String> buffers)
	throws IsacRuntimeException
	{
		synchronized (so)
		{
			if (! so.socket.isBound())
			{
				throw new IsacRuntimeException("UDPinjector must be bound before receiving data.");
			}
			byte[] buffer;
			try
			{
				buffer = new byte[so.socket.getReceiveBufferSize()];
			}
			catch (SocketException ex)
			{
				throw new IsacRuntimeException("Can't get size of socket receive buffer in UDPinjector.", ex);
			}
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			report.setDate(System.currentTimeMillis());
			long nanoStart = 0;
			if (so.microsecond)
			{
				nanoStart = System.nanoTime();
			}
			try
			{
				so.socket.receive(packet);
				if (so.microsecond)
				{
					report.duration = (int)((System.nanoTime() - nanoStart) / 1000);
				}
				else
				{
					report.duration = (int)(System.currentTimeMillis() - report.getDate());
				}
				report.successful = true;
				report.result = String.valueOf(packet.getLength()) + " bytes on port " + so.socket.getLocalPort();
				if (bufferName != null && !bufferName.equals(""))
				{
					buffers.put(bufferName, new String(buffer, 0, packet.getLength(), so.charset));
				}
				so.lastSenderAddress = packet.getSocketAddress();
			}
			catch (Exception ex)
			{
				report.successful = false;
				report.result = ex.toString();
				if (bufferName != null && !bufferName.equals(""))
				{
					buffers.remove(bufferName);
				}
			}
		}
		return report;
	}
}
