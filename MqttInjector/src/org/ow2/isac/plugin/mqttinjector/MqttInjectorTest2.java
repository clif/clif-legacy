/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.mqttinjector;


import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.storage.api.ActionEvent;


/**
 * This test class requires an MQTT server to be available.
 * Test class MqttInjectorTest must be run prior to running this
 * class, since its main goal is to check that the retained
 * last will message is available from the broker.
 * Each step prints the generated ActionEvent instance (aka action report).
 * No error nor exception should be reported.
 * Asserts should be enabled (see JVM option -ea)
 *
 * @author Bruno Dillenseger
 */
public class MqttInjectorTest2
{
	/**
	 * Runs the test.
	 * @param args
	 * <ul>
	 *   <li>args[0] MQTT server host,</li>
	 *   <li>args[1] MQTT server port,</li>
	 *   <li>args[2] MQTT topic to publish to and to subscribe to</li>
	 * </ul>
	 */
	public static void main(String[] args)
	{
		// time reference for pretty printing of action events (aka action reports)
		long startTime = System.currentTimeMillis();

		Map<String,String> params = new HashMap<String,String>();

		// create specimen object and session object
		params.put(SessionObject.PLUGIN_CLIENT_ID_SIZE, "20");
		SessionObject so = new SessionObject(params);
		SessionObject vu = (SessionObject)so.createNewSessionObject();

		// connection
		params.clear();
		params.put(SessionObject.SAMPLE_CONNECT_ACTION_TYPE, "MQTT connect");
		params.put(SessionObject.SAMPLE_CONNECT_CLIENTID, "");
		params.put(SessionObject.SAMPLE_CONNECT_HOST, args[0]);
		params.put(SessionObject.SAMPLE_CONNECT_PORT, args[1]);
		params.put(SessionObject.SAMPLE_CONNECT_PROTOCOL, "tcp");
		params.put(SessionObject.SAMPLE_CONNECT_TIMEOUT_S, "10");
		params.put(SessionObject.SAMPLE_CONNECT_ENABLE, "no");
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_CONNECT,
				params,
				new ActionEvent()
			).toString(startTime, ","));

		// subscribe
		params.clear();
		params.put(SessionObject.SAMPLE_SUBSCRIBE_ACTION_TYPE, "MQTT subscribe");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_POLICY, "old");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_QOS, "2");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_SIZE, "2");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_TOPIC, args[2]);
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_SUBSCRIBE,
				params, 
				new ActionEvent()
			).toString(startTime, ","));
		
		// get retained last will message from first test
		params.clear();
		params.put(SessionObject.SAMPLE_GETMESSAGE_ACTION_TYPE, "MQTT get message");
		params.put(SessionObject.SAMPLE_GETMESSAGE_TOPICS, args[2]);
		params.put(SessionObject.SAMPLE_GETMESSAGE_TIMEOUT, "2000");
		params.put(SessionObject.SAMPLE_GETMESSAGE_MESSAGE_VAR, "msg");
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_GETMESSAGE,
				params,
				new ActionEvent()
			).toString(startTime, ","));
		assert vu.doGet("msg").equals(MqttInjectorTest.LWT_MESSAGE);

		// clear LWT retained message
		params.clear();
		params.put(SessionObject.SAMPLE_PUBLISH_FLAGS, "retain");
		params.put(SessionObject.SAMPLE_PUBLISH_ACTION_TYPE, "MQTT publish");
		params.put(SessionObject.SAMPLE_PUBLISH_MESSAGE, "");
		params.put(SessionObject.SAMPLE_PUBLISH_QOS, "2");
		params.put(SessionObject.SAMPLE_PUBLISH_TOPIC, args[2]);
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_PUBLISH,
				params,
				new ActionEvent()
			).toString(startTime, ","));
		
		// disconnect
		params.clear();
		params.put(SessionObject.SAMPLE_DISCONNECT_ACTION_TYPE, "MQTT disconnect");
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_DISCONNECT,
				params,
				new ActionEvent()
			).toString(startTime, ","));
	}
}
