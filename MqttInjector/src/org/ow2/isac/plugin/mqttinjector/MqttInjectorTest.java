/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.isac.plugin.mqttinjector;


import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.storage.api.ActionEvent;


/**
 * This test class requires an MQTT server to be available.
 * It performs a sequence of MqttInjector actions and leaves a retained
 * last will message, to be received and cleared by MqttInjectorTest2.
 * Each step prints the generated ActionEvent instance (aka action report).
 * No error nor exception should be reported.
 * Asserts should be enabled (see JVM option -ea)
 *
 * @author Bruno Dillenseger
 */
public class MqttInjectorTest
{
	public static final String LWT_MESSAGE = "bye-bye";

	/**
	 * Runs the test.
	 * @param args
	 * <ul>
	 *   <li>args[0] MQTT server host,</li>
	 *   <li>args[1] MQTT server port,</li>
	 *   <li>args[2] MQTT topic to publish to and to subscribe to</li>
	 * </ul>
	 */
	public static void main(String[] args)
	{
		// time reference for pretty printing of action events (aka action reports)
		long startTime = System.currentTimeMillis();

		Map<String,String> params = new HashMap<String,String>();

		// create specimen object and session object
		params.put(SessionObject.PLUGIN_CLIENT_ID_SIZE, "20");
		SessionObject so = new SessionObject(params);
		SessionObject vu = (SessionObject)so.createNewSessionObject();

		// connect with "last will and testament" option
		params.clear();
		params.put(SessionObject.SAMPLE_CONNECT_ACTION_TYPE, "MQTT connect");
		params.put(SessionObject.SAMPLE_CONNECT_CLIENTID, "");
		params.put(SessionObject.SAMPLE_CONNECT_HOST, args[0]);
		params.put(SessionObject.SAMPLE_CONNECT_PORT, args[1]);
		params.put(SessionObject.SAMPLE_CONNECT_PROTOCOL, "tcp");
		params.put(SessionObject.SAMPLE_CONNECT_TIMEOUT_S, "10");
		params.put(SessionObject.SAMPLE_CONNECT_ENABLE, "yes");
		params.put(SessionObject.SAMPLE_CONNECT_MESSAGE, LWT_MESSAGE);
		params.put(SessionObject.SAMPLE_CONNECT_TOPIC, args[2]);
		params.put(SessionObject.SAMPLE_CONNECT_QOS, "0");
		params.put(SessionObject.SAMPLE_CONNECT_FLAGS, "retain");
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_CONNECT,
				params,
				new ActionEvent()
			).toString(startTime, ","));

		// subscribe with no topic => exception expected
		params.clear();
		params.put(SessionObject.SAMPLE_SUBSCRIBE_ACTION_TYPE, "MQTT subscribe");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_POLICY, "old");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_QOS, "2");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_SIZE, "10");
		try
		{
			vu.doSample(
				SessionObject.SAMPLE_SUBSCRIBE,
				params, 
				new ActionEvent());
			assert false : "Subscription with missing topic should raise an exception";
		}
		catch (IsacRuntimeException ex)
		{
			// expected
		}

		// subscribe with empty topic => exception expected
		params.put(SessionObject.SAMPLE_SUBSCRIBE_TOPIC, "");
		try
		{
			vu.doSample(
				SessionObject.SAMPLE_SUBSCRIBE,
				params, 
				new ActionEvent());
			assert false : "Subscription with empty topic should raise an exception";
		}
		catch (IsacRuntimeException ex)
		{
			// expected
		}

		// subscribe with topic
		params.put(SessionObject.SAMPLE_SUBSCRIBE_TOPIC, args[2]);
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_SUBSCRIBE,
				params, 
				new ActionEvent()
			).toString(startTime, ","));

		// publish
		params.clear();
		params.put(SessionObject.SAMPLE_PUBLISH_FLAGS, "");
		params.put(SessionObject.SAMPLE_PUBLISH_ACTION_TYPE, "MQTT publish");
		params.put(SessionObject.SAMPLE_PUBLISH_MESSAGE, "Hello from client " + vu.doGet("#"));
		params.put(SessionObject.SAMPLE_PUBLISH_QOS, "2 - Exactly once");
		params.put(SessionObject.SAMPLE_PUBLISH_TOPIC, args[2]);
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_PUBLISH,
				params,
				new ActionEvent()
			).toString(startTime, ","));

		// get a message on any topic
		params.clear();
		params.put(SessionObject.SAMPLE_GETMESSAGE_ACTION_TYPE, "MQTT get message");
		params.put(SessionObject.SAMPLE_GETMESSAGE_TOPICS, "");
		params.put(SessionObject.SAMPLE_GETMESSAGE_TIMEOUT, "2000");
		params.put(SessionObject.SAMPLE_GETMESSAGE_MESSAGE_VAR, "msg");
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_GETMESSAGE,
				params,
				new ActionEvent()
			).toString(startTime, ","));
		System.out.println(vu.doGet("msg"));

		// second attempt to get a message => time-out 
		assert !vu.doSample(
			SessionObject.SAMPLE_GETMESSAGE,
			params,
			new ActionEvent()).successful : "no second message should be received";
		try
		{
			vu.doGet("msg");
			assert false : "msg variable should not be defined";
		}
		catch (IsacRuntimeException ex)
		{
			// everything is all right
		}

		// subscribe with topic filter
		params.clear();
		params.put(SessionObject.SAMPLE_SUBSCRIBE_ACTION_TYPE, "MQTT subscribe");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_POLICY, "old");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_QOS, "2");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_SIZE, "10");
		params.put(SessionObject.SAMPLE_SUBSCRIBE_TOPIC, args[2] + "/#");
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_SUBSCRIBE,
				params, 
				new ActionEvent()
			).toString(startTime, ","));

		// publish to extended topic
		params.clear();
		params.put(SessionObject.SAMPLE_PUBLISH_FLAGS, "");
		params.put(SessionObject.SAMPLE_PUBLISH_ACTION_TYPE, "MQTT publish");
		params.put(SessionObject.SAMPLE_PUBLISH_MESSAGE, "Good bye from client " + vu.doGet("#"));
		params.put(SessionObject.SAMPLE_PUBLISH_QOS, "2 - Exactly once");
		params.put(SessionObject.SAMPLE_PUBLISH_TOPIC, args[2] + "/a/b");
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_PUBLISH,
				params,
				new ActionEvent()
			).toString(startTime, ","));

		// get message from plain topic => time-out
		params.clear();
		params.put(SessionObject.SAMPLE_GETMESSAGE_ACTION_TYPE, "MQTT get message");
		params.put(SessionObject.SAMPLE_GETMESSAGE_TOPICS, args[2]);
		params.put(SessionObject.SAMPLE_GETMESSAGE_TIMEOUT, "2000");
		params.put(SessionObject.SAMPLE_GETMESSAGE_MESSAGE_VAR, "msg");
		assert !vu.doSample(
			SessionObject.SAMPLE_GETMESSAGE,
			params,
			new ActionEvent()).successful : "This message should be received on topic " + args[2];

		// get message from topic filter
		params.put(SessionObject.SAMPLE_GETMESSAGE_TOPICS, args[2] + "/#");
		params.put(SessionObject.SAMPLE_GETMESSAGE_TOPIC_VAR, "topic");
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_GETMESSAGE,
				params,
				new ActionEvent()
			).toString(startTime, ","));
		System.out.println(vu.doGet("msg"));

		// check actual topic is args[2]/a/b
		assert vu.doGet("topic").equals(args[2] + "/a/b") : "Incorrect actual topic";

		// unsubscribe
		params.clear();
		params.put(SessionObject.SAMPLE_UNSUBSCRIBE_ACTION_TYPE, "MQTT unsubscribe");
		params.put(SessionObject.SAMPLE_UNSUBSCRIBE_TOPIC, args[2]);
		System.out.println(
			vu.doSample(
				SessionObject.SAMPLE_UNSUBSCRIBE,
				params, 
				new ActionEvent()
			).toString(startTime, ","));
	}
}
