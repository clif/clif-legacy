/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.plugins.gui;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.ow2.clif.scenario.isac.egui.plugins.ActionDescription;
import org.ow2.clif.scenario.isac.egui.plugins.ObjectDescription;
import org.ow2.clif.scenario.isac.egui.plugins.ParameterDescription;
import org.ow2.clif.scenario.isac.egui.plugins.PluginDescription;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.XMLEntityResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author JC Meillaud
 * @author A Peyrard
 * @author Thomas Escalle
 */
public class GUIDescriptionParser {
	/* Definition of the name of the node we manipulate	 */
	private static final String OBJECT = "object";
	private static final String TEST = org.ow2.clif.scenario.isac.egui.plugins.nodes.Node.TEST;
	private static final String SAMPLE = org.ow2.clif.scenario.isac.egui.plugins.nodes.Node.SAMPLE;
    private static final String TIMER = org.ow2.clif.scenario.isac.egui.plugins.nodes.Node.TIMER;
    private static final String CONTROL = org.ow2.clif.scenario.isac.egui.plugins.nodes.Node.CONTROL;
	private static final String GROUP = "group";
	private static final String NAME = "name";
	private static final String PARAM = "param";
	private static final String CHOICE = "choice";
	static final String SELECTED = "selected";
	private static final String ID = "id";
	static final String CHOICES = "choices";
	private static final String TEXT = "text";
	private static final String RADIOBUTTON = "radiobutton";
	private static final String CHECKBOX = "checkbox";
	private static final String COMBO = "combo";
	private static final String VALUE = "value";
	private static final String DEFAULT = "default";
	private static final String FIELD = "field";
	private static final String NFIELD = "nfield";
	private static final String TABLE = "table";
	private static final String COLS = "cols";
	private static final String TRUE = "true";
	private static final String SIZE = "size";
	private static final String PLEASE_EDIT = "";
	private static final int DEFAULT_SIZE = 10;
	private static final String LABEL = "label";

	/**
	 * Loads a GUI description file, trying to find description
	 * for the session object, samples, timers, tests and controls. If there
	 * is a description, a ParametersWidgetNode is built from it, otherwise,
	 * a default ParametersWidgetNode is built.
	 * @param ids 
	 * @param fileName Name of the GUI description file
	 * @param plugin the plugin description object
	 * @param panels an Hashtable of the ParametersWidgetNode
	 */
	public static void loadGUIDescriptionFile(
		Vector ids,
		InputStream fileName,
		PluginDescription plugin,
		Map<String,ParametersWidgetsNode> panels)
	{
		Document document = null;
		DocumentBuilderFactory factory = null;

		try
		{
			factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			builder.setEntityResolver(new XMLEntityResolver());
			// MODIFICATION THOMAS
			//document = builder.parse(fileName);
			document = builder.parse(fileName);
			// MODIFICATION THOMAS
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

        // plug-in parameters
		ParametersWidgetsNode parametersWidgetsNode = null;
		ObjectDescription obj = plugin.getObject();
		Map<String,ParameterDescription> parametersByName = new HashMap<String,ParameterDescription>();
		Iterator<ParameterDescription> iter = obj.getParams().iterator();
		while (iter.hasNext())
		{
			ParameterDescription desc = iter.next();
			parametersByName.put(desc.getName(), desc);
		}
		parametersWidgetsNode = visitDOM(document, OBJECT, parametersByName);
		if (parametersWidgetsNode != null)
		{
			int intKey = parametersWidgetsNode.hashCode();
			intKey = findNewIntKey(panels, intKey);
			panels.put(String.valueOf(intKey), parametersWidgetsNode);
			obj.setGUIKey(String.valueOf(intKey));
		}
		else
		{
			parametersWidgetsNode = ParametersWidgetsNode.createParametersWidgetsNode(
				ids,
				obj.getParams(),
				org.ow2.clif.scenario.isac.egui.plugins.nodes.Node.USE,
				plugin.getName());
			int intKey = parametersWidgetsNode.hashCode();
			intKey = findNewIntKey(panels, intKey);
			panels.put(String.valueOf(intKey), parametersWidgetsNode);
			obj.setGUIKey(String.valueOf(intKey));
		}
		// actions parameters
		loadActionGUIDescription(
			ids, document, plugin.getName(), panels, SAMPLE, plugin.getSamples());
		loadActionGUIDescription(
			ids, document, plugin.getName(), panels, TEST, plugin.getTests());
		loadActionGUIDescription(
			ids, document, plugin.getName(), panels, TIMER, plugin.getTimers());
		loadActionGUIDescription(
			ids, document, plugin.getName(), panels, CONTROL, plugin.getControls());
	}


	private static void loadActionGUIDescription(
		Vector ids,
		Document document,
		String pluginName,
		Map<String,ParametersWidgetsNode> panels,
		String actionType,
		Map<String,ActionDescription> actions)
	{
		Map<String,ActionDescription> remaining = new HashMap<String,ActionDescription>(actions);
		Map<String,ParametersWidgetsNode> hashtablePWN = new HashMap<String,ParametersWidgetsNode>();
		visitDOM2(hashtablePWN, document, actionType, actions);
		if (hashtablePWN.size() != 0)
		{
			Iterator<String> keys = hashtablePWN.keySet().iterator();
			while (keys.hasNext())
			{
				String key = keys.next();
				if (actions.containsKey(key))
				{
					ActionDescription temp = actions.get(key);
					int intKey = hashtablePWN.get(key).hashCode();
					intKey = findNewIntKey(panels, intKey);
					temp.setGUIKey(String.valueOf(intKey));
					panels.put(String.valueOf(intKey), hashtablePWN.get(key));
					remaining.remove(key);
				}
			}
		}
		if (remaining.size() != 0)
		{
			Iterator<String> actionsKeys = remaining.keySet().iterator();
			while (actionsKeys.hasNext())
			{
			    ActionDescription action = remaining.get(actionsKeys.next());
                Vector<ParameterDescription> params = action.getParams();
			    ParametersWidgetsNode tree = ParametersWidgetsNode.createParametersWidgetsNode(
			    	ids, params, actionType, pluginName);
			    int intKey = findNewIntKey(panels, tree.hashCode());
			    panels.put(String.valueOf(intKey), tree);
			    action.setGUIKey(String.valueOf(intKey));
			}
		}
	}


    private static int findNewIntKey(Map<String,ParametersWidgetsNode> panels, int intKey) {
        while (panels.containsKey(String.valueOf(intKey))) {
            intKey++;
        }
        return intKey;
    }
	/**
	 * Method to get the value of a certain attribut from a DOM's Node
	 * @param node The node
	 * @param attributName Name of the attribut
	 * @return An object representing the value
	 */
	private static Object returnAttributValue(Node node, String attributName) {
		String result = null;
		if (node.getAttributes() != null) {
			for (int i = 0; i < node.getAttributes().getLength(); i++) {
				if (node.getAttributes().item(i).getNodeName().equals(attributName)) {
					result = node.getAttributes().item(i).getNodeValue();
				}
			}
        }
		return result;
	}
	/**
	 * Method to get a Vector with all the value of the attribut from certain
	 * nodes
	 * @param node The node
	 * @param tagName
	 * @param attributName
	 * @return Vector of params
	 */
	private static Vector<Object> returnParams(
            Node node, String tagName, String attributName) {
		Vector<Object> parametersNames = new Vector<Object>();
		if (node.hasChildNodes()) {
			NodeList fils = node.getChildNodes();
			for (int i = 0; i < fils.getLength(); i++) {
				if (fils.item(i).getNodeName().equals(tagName)) {
					if (returnAttributValue(fils.item(i), attributName) != null) {
						parametersNames.add(returnAttributValue(fils.item(i), attributName));
					}
				}
			}
		}
		return parametersNames;
	}
	/**
	 * Method to get a Vector with all the value of the attribut from certain
	 * nodes which are set to 'true'
	 * @param node
	 * @param tagName
	 * @param attributName
	 * @return Vector of selected params
	 */
	private static Vector<Object> returnSelectedParams(Node node, String tagName,
			String attributName) {
		Vector<Object> parametersNames = new Vector<Object>();
		if (node.hasChildNodes()) {
			NodeList children = node.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {
				if (children.item(i).getNodeName().equals(tagName)) {
					String attribut = 
                        (String)returnAttributValue(children.item(i), attributName);
					if (attribut != null && attribut.equals(TRUE)) {
						parametersNames.add(returnAttributValue(children.item(i),
								VALUE));
					}
				}
			}
		}
		return parametersNames;
	}

	/**
	 * Method to complete the ParametersWidgetsNode description tree, if some of
	 * the parameters are not defined
	 * 
	 * @param parent The current description where we 
     * put the missing ParametersWidgetsNode
	 * @param parametersByName map of missing parameters containing
	 * parameter description objects indexed by the parameter name.
	 */
	public static void completeParamsWidgetDesc(
		ParametersWidgetsNode parent,
		Map<String,ParameterDescription> parametersByName)
	{
		Iterator<String> names = parametersByName.keySet().iterator();
		while (names.hasNext())
		{
			String currentName = names.next();
			if (!currentName.equals(ID))
			{
				ParametersWidgetsNode result = new ParametersWidgetsNode(
					new WidgetDescription(
						WidgetDescription.TEXT_FIELD,
						currentName,
						null,
						null));
				parent.addChild(result);
			}
		}
	}

	/**
	 * Return an Object describing the apparence for the GUI
	 * @param parent
	 * @param node
	 * @param parentName
	 * @param labelValue
	 * @param parametersNames
	 */
	private static void returnParamsWidgetDesc(
		ParametersWidgetsNode parent,
		Node node,
		String parentName,
		String labelValue,
		Map<String,ParameterDescription> parametersByName)
	{
		ParametersWidgetsNode result = null;
		String label = labelValue;
		// test if it is a group
		boolean isGroup = false;
		/* isFinalNode is set to true, if we found a node 
         * RADIOBUTTON | CHECKBOX | COMBO | FIELD (see gui.dtd) */
		boolean isFinalNode = false;
		Map<String,Object> params = new HashMap<String,Object>();
        
		String tagName = node.getNodeName();

		if (tagName.equals(GROUP)) {
			parentName = (String) returnAttributValue(node, NAME);
			isGroup = true;
			result = new ParametersWidgetsNode(new WidgetDescription(
					WidgetDescription.GROUP, parentName, label, params));
			result.setParent(parent);
			parent.addChild(result);
			if (node.hasChildNodes()) {
				NodeList fils = node.getChildNodes();
				for (int i = 0; i < fils.getLength(); i++) {
					returnParamsWidgetDesc(
						result,
						fils.item(i),
						parentName,
						label,
						parametersByName);
				}
			}
		} else if (tagName.equals(PARAM)) {
			parentName = (String) returnAttributValue(node, NAME);
			String labelTemp = (String) returnAttributValue(node, LABEL);
			/* only if this parameter is defined put it to the parameters vector */
			if (labelTemp != null) {
				label = labelTemp;
			}
			if (! parametersByName.containsKey(parentName)) {
				return;
			}
		} else if (tagName.equals(FIELD)) {
			params.put(SIZE, returnAttributValue(node, SIZE) != null
					? returnAttributValue(node, SIZE)
					: String.valueOf(DEFAULT_SIZE));
			Object defaultValue = returnAttributValue(node, TEXT) != null
					? returnAttributValue(node, TEXT)
					: PLEASE_EDIT;
			params.put(TEXT, defaultValue);
			parametersByName.get(parentName).setDefaultValue((String)defaultValue);
			parametersByName.remove(parentName);
			result = new ParametersWidgetsNode(new WidgetDescription(
					WidgetDescription.TEXT_FIELD, parentName, label, params));
			parentName = "";
			parent.addChild(result);
			isFinalNode = true;
		} else if (tagName.equals(NFIELD)) {
			result = new ParametersWidgetsNode(new WidgetDescription(
					WidgetDescription.NFIELD, parentName, label, params));
			parametersByName.remove(parentName);
			parentName = "";
			parent.addChild(result);
			isFinalNode = true;
		} else if (tagName.equals(TABLE)) {
			params.put(COLS, returnAttributValue(node, COLS) != null
					? returnAttributValue(node, COLS)
					: "");
			result = new ParametersWidgetsNode(new WidgetDescription(
					WidgetDescription.TABLE, parentName, label, params));
			parametersByName.remove(parentName);
			parentName = "";
			parent.addChild(result);
			isFinalNode = true;
		}
		else if (tagName.equals(RADIOBUTTON)
			|| tagName.equals(CHECKBOX)
			|| tagName.equals(COMBO))
		{
			params.put(CHOICES, returnParams(node, CHOICE, VALUE));
			Vector defaultValues = returnSelectedParams(node, CHOICE, DEFAULT);
			params.put(SELECTED, defaultValues);
			String value = "";
			Iterator values = defaultValues.iterator();
			while (values.hasNext())
			{
				value = value.concat((String)values.next()) + ";";
			}
			parametersByName.get(parentName).setDefaultValue(value);
			parametersByName.remove(parentName);
			int widgetType = WidgetDescription.NONE;
			if (tagName.equals(RADIOBUTTON))
			{
				widgetType = WidgetDescription.RADIO_GROUP;
			}
			else if (tagName.equals(CHECKBOX))
			{
				widgetType = WidgetDescription.CHECK_BOX;
			}
			else if (tagName.equals(COMBO))
			{
				widgetType = WidgetDescription.COMBO;
			}
			result = new ParametersWidgetsNode(
				new WidgetDescription(widgetType, parentName, label, params));
			parentName = "";
			parent.addChild(result);
			isFinalNode = true;
		}
		if (node.hasChildNodes() && !isGroup && !isFinalNode) {
			NodeList fils = node.getChildNodes();
			for (int i = 0; i < fils.getLength(); i++) {
				returnParamsWidgetDesc(
					parent,
					fils.item(i),
					parentName,
					label,
					parametersByName);
			}
		}
	}

	/**
	 * Method wich return the ParametersWidgetsNode representing the whole
	 * parameters gui descriptions
	 * @param node
	 * @param tagName
	 * @param paramsNames 
	 * @return the ParametersWidgetsNode
	 */
	private static ParametersWidgetsNode visitDOM(
		Node node,
		String tagName,
		Map<String,ParameterDescription> paramsByName)
	{
		boolean EndNode = false;
		if (node == null)
		{
			return null;
		}
		switch (node.getNodeType()) {
			case Node.ELEMENT_NODE :
				if (node.getNodeName().equals(tagName)) {
					ParametersWidgetsNode res = new ParametersWidgetsNode(null);
                    
					EndNode = true;
					if (tagName.equals(OBJECT)) {
						res.addChild(new ParametersWidgetsNode(
								new WidgetDescription(
										WidgetDescription.TEXT_FIELD, ID, null,
										null)));
					} else {
						res.addChild(new ParametersWidgetsNode(
								new WidgetDescription(WidgetDescription.COMBO,
										ID, null, null)));
					}

					returnParamsWidgetDesc(res, node, "", "", paramsByName);
					completeParamsWidgetDesc(res, paramsByName);

					if (res.getChildren().size() != 1) {
						return res;
					}

				}
				break;
			default :
		}
		if (node.hasChildNodes() && !EndNode) {
			NodeList fils = node.getChildNodes();
			for (int i = 0; i < fils.getLength(); i++) {
				ParametersWidgetsNode temp = 
                    visitDOM(fils.item(i), tagName,	paramsByName);
				if (temp != null) {
                    return temp;
                }
			}
		}
		return null;
	}
	/**
	 * Method that visit a tree in order to find every (tagName) node
	 * @param hashtable
	 * @param node
	 * @param tagName
	 * @param plugins
	 */
	private static void visitDOM2(Map<String,ParametersWidgetsNode> hashtable, Node node,
			String tagName, Map<String,ActionDescription> plugins) {
		if (node == null)
		{
			return;
		}
		boolean EndNode = false;
		Vector<ParameterDescription> descriptionParams = null;
		Map<String,ParameterDescription> parametersByName = new HashMap<String,ParameterDescription>();

		switch (node.getNodeType()) {
			case Node.ELEMENT_NODE :
				if (node.getNodeName().equals(tagName)) {
					Object name = returnAttributValue(node, NAME);
					ParametersWidgetsNode result = null;
					EndNode = true;

					if (plugins.containsKey(name)) {
						descriptionParams = plugins.get(name).getParams();
						if (descriptionParams != null)
						{
							Iterator<ParameterDescription> descs = descriptionParams.iterator();
							while (descs.hasNext())
							{
								ParameterDescription currentDesc = descs.next(); 
								parametersByName.put(currentDesc.getName(), currentDesc);
							}
						}
						result = visitDOM(node, tagName, parametersByName);
						if (result != null) {
							hashtable.put((String)name,	result);
						}
					}
				}
				break;
			default :
		}

		if (node.hasChildNodes() && !EndNode) {
			NodeList fils = node.getChildNodes();
			for (int i = 0; i < fils.getLength(); ++i) {
				visitDOM2(hashtable, fils.item(i), tagName, plugins);
			}
		}
	}
}
