/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2012 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.TransferData;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.ActionPlacement;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.BehaviorTreeViewer;
import org.ow2.clif.scenario.isac.egui.util.BehaviorUtil;
import org.w3c.dom.Element;
import java.util.List;

/**
 * Supports dropping ScenarioNode into a tree viewer.
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public class ElementDropAdapter extends ViewerDropAdapter {

    /**
     * Constructor
     * @param viewer
     */
    public ElementDropAdapter(TreeViewer viewer)
    {
        super(viewer);
    }

    /**
     * Method declared on ViewerDropAdapter
     */
    public boolean performDrop(Object data)
    {
        TreeViewer viewer = (TreeViewer) getViewer();
        Element target = (Element) getCurrentTarget();
        List<Element> toDrop = (List<Element>) data;
        ActionPlacement placement = getActionPlacement();
        if (target == null)
        {
       		target = ((BehaviorTreeViewer) viewer).getRootElement();
        	placement = ActionPlacement.END;
        }
        else if (placement == ActionPlacement.CHILD && ! BehaviorUtil.childrenAllowed(target, toDrop.get(0)))
        {
        	placement = ActionPlacement.BEFORE_NODE;
        }
        if (placement == ActionPlacement.AFTER_NODE)
    	{
    		for (int i=toDrop.size()-1 ; i>=0 ; --i)
    		{
    			if(! BehaviorUtil.insertElementInTree(toDrop.get(i), target, target, placement))
	            {
					viewer.refresh();
					return false;
	            }
    		}
    	}
        else
        {
	        for (Element element : toDrop)
	        {
	            if(! BehaviorUtil.insertElementInTree(element, target, target, placement))
	            {
					viewer.refresh();
					return false;
	            }
			}
        }
        viewer.expandToLevel(toDrop, 1);
        viewer.refresh();
        return true;
    }

	private ActionPlacement getActionPlacement() {
		switch (getCurrentLocation())
        {
            case LOCATION_BEFORE:
                return ActionPlacement.BEFORE_NODE;
            case LOCATION_ON:
                return ActionPlacement.CHILD;
            case LOCATION_AFTER:
                return ActionPlacement.AFTER_NODE;
            default:
                return ActionPlacement.AFTER_NODE;
        }
	}

	/**
     * Method declared on ViewerDropAdapter
     */
    public boolean validateDrop(Object target, int op, TransferData type)
    {
		return ElementTransfer.getInstance().isSupportedType(type);
    }
}