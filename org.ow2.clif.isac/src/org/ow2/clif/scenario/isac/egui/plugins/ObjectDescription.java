/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.NodeDescription;
/**
 * This class store an object description
 *
 * @author JC Meillaud
 * @author A Peyrard
 */
public class ObjectDescription implements ActionDescription {
    private String name;
    private String clazz;
    private Vector<ParameterDescription> params;
    private Vector<String> help;
    private String guiKey;

    /**
     * Build a new 'object description' object
     * @param n The name of the object
     * @param c The clazz name of the implementation of the object
     * @param p The parameters descriptions which are needed to construct the object
     * @param h The help of the object
     */
    public ObjectDescription(String n, String c, Vector<ParameterDescription> p, Vector<String> h) {
        this.name = n;
        this.clazz = c;
        this.params = p;
        this.help = h;
        this.guiKey = null;
    }

    public void createNodeDescription(NodeDescription desc) {
        desc.setActionName(this.name);
        Map<String,String> paramsValues = new HashMap<String,String>();
        for (int i = 0; i < this.params.size(); i++)
            paramsValues.put(params.elementAt(i).getName(), "");
        desc.setParams(paramsValues);
    }

    public String getGUIKey() {
        return guiKey ;
    }

    public void setGUIKey(String key) {
        this.guiKey = key ;
    }
    /**
     * Attribute clazz getter
     * @return The class name of the object
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Attribute help getter
     * @return The help of the object
     */
    public Vector<String> getHelp() {
        return help;
    }

    /**
     * Attribute name getter
     * @return The name of the object
     */
    public String getName() {
        return name;
    }

    /**
     * Attribute params getter
     * @return The parameters descriptions which are needed by the object to be build
     */
    public Vector<ParameterDescription> getParams() {
        return params;
    }
}
