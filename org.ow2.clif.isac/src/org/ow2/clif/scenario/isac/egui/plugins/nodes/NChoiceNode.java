/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004,2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins.nodes;

import java.util.Vector;


/**
 * This class embeds help information for nodes of type 'nchoice' 
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 */
public abstract class NChoiceNode {
     
    /**
     * This method return the help of an nchoice node
     * @return The help lines
     */
    public static Vector<String> getHelp() {
        Vector<String> help = new Vector<String>() ;
        help.add("nchoice random branching statement.");
        help.add("Must enclose at least two choice statements.");
        help.add("This statement defines a random execution branching to one behavior block among several branches.");
        help.add("Each branch is specified using a \"choice\" statement, which is associated to a weight and a behavior block.");
        help.add("The probability of executing one branch equals to its weight devided by the sum of all the branches' weights.");
        return help ;
    }
}
