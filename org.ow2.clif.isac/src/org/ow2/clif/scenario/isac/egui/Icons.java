/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.jface.resource.ImageRegistry;
import org.ow2.clif.scenario.isac.egui.FileName;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;

/**
 * Implementation of a image registry, which load the icons files
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class Icons {
	private static ImageRegistry imageRegistry;
	private static Map<String,String> imageDescriptorFileName;

	/**
	 * Create a new URL with the given string in parameters
	 * 
	 * @param url  The string which contains the URL
	 * @return The URL created
	 */
	public static URL newURL(String url) {
		try {
			return new URL("file:" + url);
		} catch (MalformedURLException e) {
			throw new RuntimeException("Malformed URL " + url, e);
		}
	}

    /**
     * Create an hashtable with images indexed by their name
     * @return HashTable with all images
     */
	public static Map<String,String> getImageDescriptorFileName() {
		if (imageDescriptorFileName == null) {
			imageDescriptorFileName = new HashMap<String,String>();
			
            /* ICONS FOR THE BEHAVIORS TREE */
			imageDescriptorFileName.put(Node.BEHAVIOR, FileName.BEHAVIOR_ICON);
			imageDescriptorFileName.put(Node.PLUGINS, FileName.PLUGINS_ICON);
			imageDescriptorFileName.put(Node.USE, FileName.USE_ICON);
			imageDescriptorFileName.put(Node.SAMPLE, FileName.SAMPLE_ICON);
			imageDescriptorFileName.put(Node.TIMER, FileName.TIMER_ICON);
            imageDescriptorFileName.put(Node.CONTROL, FileName.CONTROL_ICON);
			imageDescriptorFileName.put(Node.IF, FileName.IF_ICON);
			imageDescriptorFileName.put(Node.NCHOICE, FileName.NCHOICE_ICON);
            imageDescriptorFileName.put(Node.WHILE, FileName.WHILE_ICON);
            imageDescriptorFileName.put(Node.PREEMPTIVE, FileName.PREEMPTIVE_ICON);
		}
		return imageDescriptorFileName;
	}

	/**
     * Get the image registry
     * this function create it if the registry don't exist
	 * @return ImageRegistry
	 */
	public static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
            
			/* put all the icons file */
			imageRegistry.put(FileName.EXIT_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.EXIT_ICON));
			imageRegistry.put(FileName.ABOUT_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.ABOUT_ICON));
			imageRegistry.put(FileName.NEW_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.NEW_ICON));
			imageRegistry.put(FileName.OPEN_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.OPEN_ICON));
			imageRegistry.put(FileName.SAVE_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.SAVE_ICON));
			imageRegistry.put(FileName.SAVE_AS_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.SAVE_AS_ICON));
			imageRegistry.put(FileName.HELP_SHOW_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.HELP_SHOW_ICON));
			imageRegistry.put(FileName.HELP_HIDE_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.HELP_HIDE_ICON));
			imageRegistry.put(FileName.UP_ARROW_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.UP_ARROW_ICON));
			imageRegistry.put(FileName.DOWN_ARROW_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.DOWN_ARROW_ICON));
			imageRegistry.put(FileName.DELETE_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.DELETE_ICON));
			imageRegistry.put(FileName.COPY_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.COPY_ICON));
			imageRegistry.put(FileName.CUT_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.CUT_ICON));
			imageRegistry.put(FileName.PASTE_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.PASTE_ICON));
			imageRegistry.put(FileName.ADD_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.ADD_ICON));
			imageRegistry.put(FileName.EXPAND_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.EXPAND_ICON));
			imageRegistry.put(FileName.COLLAPSE_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.COLLAPSE_ICON));
			imageRegistry.put(FileName.GENERATE_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.GENERATE_ICON));
			imageRegistry.put(FileName.RENAME_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.RENAME_ICON));
			imageRegistry.put(FileName.ADD_GROUP_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.ADD_GROUP_ICON));
			imageRegistry.put(FileName.ADD_PARAM_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.ADD_PARAM_ICON));
			imageRegistry.put(FileName.GROUP_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.GROUP_ICON));
			imageRegistry.put(FileName.PARAM_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.PARAM_ICON));
			
            /* ICONS FOR THE LOAD PROFILE VIEW */
			imageRegistry.put(FileName.ADDPOINT_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.ADDPOINT_ICON));
			imageRegistry.put(FileName.DELPOINT_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.DELPOINT_ICON));
			imageRegistry.put(FileName.MAXIMIZEVIEW_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.MAXIMIZEVIEW_ICON));
			imageRegistry.put(FileName.NORMALVIEW_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.NORMALVIEW_ICON));
			imageRegistry.put(FileName.DRAW_NOTHING_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.DRAW_NOTHING_ICON));
            imageRegistry.put(FileName.LINE_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.LINE_ICON));
            imageRegistry.put(FileName.HV_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.HV_ICON));
            imageRegistry.put(FileName.VH_ICON, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.VH_ICON));
            
            /*  ICONS FOR THE BEHAVIORS TREE */
			imageRegistry.put(Node.BEHAVIOR, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.BEHAVIOR_ICON));
			imageRegistry.put(Node.PLUGINS, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.PLUGINS_ICON));
			imageRegistry.put(Node.USE, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.USE_ICON));
			imageRegistry.put(Node.SAMPLE, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.SAMPLE_ICON));
            imageRegistry.put(Node.CONTROL, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.CONTROL_ICON));
			imageRegistry.put(Node.TIMER, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.TIMER_ICON));
			imageRegistry.put(Node.IF, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.IF_ICON));
			imageRegistry.put(Node.NCHOICE, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.NCHOICE_ICON));	
            imageRegistry.put(Node.CHOICE, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.CHOICE_ICON));  
            imageRegistry.put(Node.WHILE, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.WHILE_ICON));  
            imageRegistry.put(Node.CONDITION, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.CONDITION_ICON));
            imageRegistry.put(Node.THEN, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.THEN_ICON)); 
            imageRegistry.put(Node.ELSE, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.THEN_ICON)); 
            imageRegistry.put(Node.PREEMPTIVE, IsacScenarioPlugin.imageDescriptorFromPlugin(
                    IsacScenarioPlugin.PLUGIN_ID, FileName.PREEMPTIVE_ICON)); 
		}
		return imageRegistry;
	}
}