/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright 2010 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.IConsoleConstants;

/**
 * An Eclipse perspective for the Isac scenario editor : 
 * Isac scenario perspective.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class IsacScenarioPerspective implements IPerspectiveFactory {
   
    /** Isac scenario perspective Id */
    public static final String ISAC_SCENARIO_PERSPECTIVE_ID = 
        "org.ow2.clif.isac.gui.IsacScenarioPerspective";
    
    /** Isac load profile view id */
    public static final String ISAC_SCENARIO_LOADPROFILE_ID = 
        "org.ow2.clif.isac.ProfileView";
    
    /**
     * Create the Isac Scenario Perspective with:
     * <ul>
     * <li>Navigator view on the top left</li>
     * <li>Editor on the center</li>
     * </ul>
     */
    public void createInitialLayout(IPageLayout layout) {
        /* Get the editor area. */
        String editorArea = layout.getEditorArea();
        
        /* Top left: Resource Navigator view */
        IFolderLayout topLeft = layout.createFolder("topLeft", 
                IPageLayout.LEFT, 0.20f, editorArea);
        topLeft.addView(IPageLayout.ID_RES_NAV);
        
        /* Bottom : LoadProfile view */
        IFolderLayout bottom = layout.createFolder("bottom", 
                IPageLayout.BOTTOM, 0.70f, editorArea);
        bottom.addView(ISAC_SCENARIO_LOADPROFILE_ID);
        bottom.addView(IConsoleConstants.ID_CONSOLE_VIEW);

        /* Add ISAC view in Show View menu */
        layout.addShowViewShortcut(IPageLayout.ID_RES_NAV);
        layout.addShowViewShortcut(ISAC_SCENARIO_LOADPROFILE_ID);
    }
}
