/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.NodeDescription;

/**
 * The class which store the description of a sample action
 *
 * @author JC Meillaud
 * @author A Peyrard
 */
public class SampleDescription implements ActionDescription {
    private String name;
    private String clazz;
    private String method;
    private Vector<ParameterDescription> params;
    private Vector<String> help;
    private String guiKey;

    /**
     * Build a new sample description object with specified values
     * @param n The name of the action
     * @param c The clazz where the action method is defined
     * @param m The method to be launch by the action
     * @param p The description of the parameters needed for this method
     * @param h The help of the action
     */
    public SampleDescription(String n, String c, String m, Vector<ParameterDescription> p, Vector<String> h) {
        this.name = n;
        this.clazz = c;
        this.method = m;
        this.params = p;
        this.help = h;
        this.guiKey = null;
    }

    public void createNodeDescription(NodeDescription desc) {
        desc.setActionName(this.name);
        Map<String,String> paramsValues = new HashMap<String,String>();
        for (int i = 0; i < this.params.size(); i++) {
            paramsValues.put(params.elementAt(i).getName(), "");
        }
        desc.setParams(paramsValues);
    }

    public String getGUIKey() {
        return this.guiKey;
    }

    public void setGUIKey(String key) {
        this.guiKey = key;
    }

    /**
     * Attribute clazz getter
     * @return The clazz name of the action
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Attribute help getter
     * @return The help in a vector, each element in the vector is a help line
     */
    public Vector<String> getHelp() {
        return help;
    }

    /**
     * Atribute method getter
     * @return The method name of the action
     */
    public String getMethod() {
        return method;
    }

    /**
     * Attribute name getter
     * @return The name of the action
     */
    public String getName() {
        return name;
    }

    /**
     * Attribute params getter
     * @return The parameters descriptions
     */
    public Vector<ParameterDescription> getParams() {
        return params;
    }
}