/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2008, 2009, 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins.gui;

import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Vector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.ow2.clif.scenario.isac.util.ParameterParser;


/**
 * Implementation of the object which included widgets to edit parameters
 * 
 * @author JC Meillaud
 * @author A Peyrard
 * @author Bruno Dillenseger
 * @author Florian Francheteau
 */
public class ParameterWidget implements SelectionListener
{
	private static final String EMPTY = "";

	/**
	 * Store the widget style
	 */
	private int style;

	private Composite composite;

	private String name;

	private String labelValue;
	private Text textField;

	private Group radioGroup;
	private Group checkBox;
	private Group group;
    private Group nfieldGroup;
    
	private Vector<Button> buttons;
    private Vector<Text> nfield;
    private Vector<Label> nfieldLabel;
    private Vector<TableEditor> cellEditors;
    private Vector<Text> cellText;
    private Vector<TableColumn> tableColumns;
    
	private Combo combo;

	private ModifyListener listener;

	private Button nfieldAdd;
	private Button nfieldRemove;

	private Composite parent;

	private Table table;

	private int cellYAxis;

	/**
	 * Constructor, build a new WidgetParam
	 * 
	 * @param style
	 *            The style of the widget for editing this param
	 * @param name
	 *            The name of the parameter
	 * @param labelValue
	 *            The value of the label for this widget, if it is not defined
	 *            use the name of the param
	 * @param params
	 *            The parameter to build the widget
	 * @param parent
	 *            The parent composite to add the new widget
	 * @param listener
	 *            The modify listener to set on the parameter widget
	 */
	public ParameterWidget(
		int style,
		String name,
		String labelValue,
		Map<String,Object> params,
		Composite parent,
		ModifyListener listener)
	{
		this.listener = listener;
		this.style = style;
		this.name = name;
		if (labelValue == null) {
			this.labelValue = name;
		} else {
			if (labelValue.equals("")) {
				this.labelValue = name;
			} else {
				this.labelValue = labelValue;
			}
		}
		// initialize the widgets
		this.textField = null;
		this.radioGroup = null;
		this.checkBox = null;
		this.group = null;
		this.buttons = null;
		this.nfield = null;
		this.nfieldLabel = null;
		this.nfieldAdd = null;
		this.nfieldRemove = null;
		this.nfieldGroup = null;
		this.table = null;
		this.tableColumns = null;
		// construct the main composite
		this.parent = parent;
		this.composite = new Composite(parent, SWT.FLAT);
		this.composite.setBackground(parent.getDisplay().getSystemColor(
				SWT.COLOR_WHITE));
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		this.composite.setLayout(gridLayout);
		// set grid data to the main composite
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		this.composite.setLayoutData(gridData);
		// construct the widget depending it style
		switch (style) {
		case WidgetDescription.TEXT_FIELD:
			initField(params, this.composite);
			break;
		case WidgetDescription.RADIO_GROUP:
			initRadioGroup(params, this.composite);
			break;
		case WidgetDescription.CHECK_BOX:
			initCheckBox(params, this.composite);
			break;
		case WidgetDescription.GROUP:
			initGroup(params, this.composite);
			break;
		case WidgetDescription.COMBO:
			initCombo(params, this.composite);
			break;
		case WidgetDescription.NFIELD:
			initNField(params, this.composite);
			break;
		case WidgetDescription.TABLE:
			initTable(params, this.composite);
			break;
		default:
			throw new Error("Unknown widget style in ISAC editor: " + style);
		}
	}

	/**
	 * Method which permit to get the parameter value
	 * @return String value
	 */
	public String getValue()
	{
		switch (this.style)
		{
			case WidgetDescription.TEXT_FIELD:
				return getValueField();
			case WidgetDescription.RADIO_GROUP:
				return getValueRadioGroup();
			case WidgetDescription.CHECK_BOX:
				return getValueCheckBox();
			case WidgetDescription.GROUP:
				return getValueGroup();
			case WidgetDescription.COMBO:
				return getValueCombo();
			case WidgetDescription.NFIELD:
				return getValueNField();
			case WidgetDescription.TABLE:
				return getValueTable();
			default:
				throw new Error("Unknown widget style in ISAC editor: " + style);
		}
	}

	/**
	 * Set the widget value
	 * 
	 * @param value
	 *            The value to set
	 */
	public void setValue(String value)
	{
		// remove the listeners because this changes are made by the system
		// and it do not need to be notify of this changes...
		removeListeners();
		switch (this.style)
		{
			case WidgetDescription.TEXT_FIELD:
				setValueField(value);
				break;
			case WidgetDescription.RADIO_GROUP:
				setValueRadioGroup(value);
				break;
			case WidgetDescription.CHECK_BOX:
				setValueCheckBox(value);
				break;
			case WidgetDescription.GROUP:
				setValueGroup(value);
				break;
			case WidgetDescription.COMBO:
				setValueCombo(value);
				break;
			case WidgetDescription.NFIELD:
				setValueNField(value);
				break;
			case WidgetDescription.TABLE:
				setValueTable(value);
				break;
			default:
				throw new Error("Unknown widget style in ISAC editor: " + style);
		}
		addListeners();
	}

	/**
	 * Set the values of a combo
	 * 
	 * @param values
	 *            The values to be set
	 */
	public void setComboValues(Vector values)
	{
		for (int i = 0; i < values.size(); i++) {
			this.combo.add((String) values.elementAt(i));
		}
	}

	/**
	 * Add a new field to the current nfield
	 * @param source The source of the event
	 * @return true if a new field has been added, false otherwise
	 * (i.e. when the event source is not the add new field button)
	 */
	public boolean addEmptyFieldForNField(Object source)
	{
		if (source == this.nfieldAdd)
		{
			// create a new label
			Label label = new Label(this.nfieldGroup, SWT.NONE);
			label.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
			// initialize the text label
			label.setText("field " + this.nfield.size() + " : ");
			// add it to the label vector
			this.nfieldLabel.add(label);
			// create the text field
			Text text = new Text(this.nfieldGroup, SWT.BORDER);
			text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			// add a modify listener
			text.addModifyListener(this.listener);
			// add the field to the fields vector
			this.nfield.add(text);
			// refresh the group
			this.nfieldGroup.layout();
			this.composite.layout();
			this.parent.layout();
			return true;
		}
		return false;
	}

	/**
	 * This method adds an entry on the table widget
	 * 
	 * @param source The source of the event
	 * @return true if the source is our add button, else false
	 */
	public boolean addEmptyEntryForTable(Object source)
	{
		if (source == this.nfieldAdd)
		{
			TableItem temp = new TableItem(table, SWT.NONE);
			// set the default values for each cell
			String[] values = new String[this.tableColumns.size()];
			for (int i = 0; i < this.tableColumns.size(); i++)
			{
				values[i] = new String(EMPTY);
			}
			temp.setText(values);
			return true;
		}
		return false;
	}

	/**
	 * This method removes the last entry of the table widget
	 * 
	 * @param source The source of the event
	 * @return true if the source is our remove button, else false
	 */
	public boolean removeLastEntryForTable(Object source)
	{
		if (source == this.nfieldRemove)
		{
			this.table.remove(this.table.getItemCount() - 1);
			return true;
		}
		return false;
	}

	/**
	 * Remove the last field of the nfield
	 * @param source The source of the event
	 * @return true if the last field has been removed, false otherwise
	 * (i.e. when the event source is not the remove field button)
	 */
	public boolean removeLastFieldForNField(Object source)
	{
		if (source == this.nfieldRemove)
		{
			if (!this.nfield.isEmpty())
			{
				// get the last label and last text
				Label lastLabel = this.nfieldLabel.elementAt(this.nfieldLabel.size() - 1);
				Text lastText = this.nfield.elementAt(this.nfield.size() - 1);
				// remove the vector entries
				this.nfield.remove(lastText);
				this.nfieldLabel.remove(lastLabel);
				// dispose the widgets
				lastLabel.dispose();
				lastText.dispose();
				// refresh the group
				this.nfieldGroup.layout();
				this.composite.layout();
				this.parent.layout();
			}
			return true;
		}
		return false;
	}

	///////////////////////////////////////////
	// Listeners methods
	//////////////////////////////////////////

	/**
	 * This method remove the listener of the current widget
	 */
	public void removeListeners()
	{
		switch (this.style)
		{
			case WidgetDescription.TEXT_FIELD:
				this.textField.removeModifyListener(listener);
				break;
			case WidgetDescription.RADIO_GROUP:
				this.removeButtonsListeners();
				break;
			case WidgetDescription.CHECK_BOX:
				this.removeButtonsListeners();
				break;
			case WidgetDescription.COMBO:
				this.combo.removeSelectionListener((SelectionListener) listener);
				break;
			case WidgetDescription.NFIELD:
				this.removeNFieldListeners();
				break;
			case WidgetDescription.TABLE:
				//			this.table.removeListener(SWT.Modify, (Listener)this.listener) ;
				break;
			default:
				throw new Error("Unknown widget style in ISAC editor: " + style);
		}
	}

	/**
	 * This method add a listener to the current widget
	 */
	public void addListeners()
	{
		switch (this.style)
		{
			case WidgetDescription.TEXT_FIELD:
				this.textField.addModifyListener(listener);
				break;
			case WidgetDescription.RADIO_GROUP:
				this.addButtonsListeners();
				break;
			case WidgetDescription.CHECK_BOX:
				this.addButtonsListeners();
				break;
			case WidgetDescription.COMBO:
				this.combo.addSelectionListener((SelectionListener) listener);
				break;
			case WidgetDescription.NFIELD:
				this.addNFieldListeners();
				break;
			case WidgetDescription.TABLE:
				//			this.table.addListener(SWT.Modify, (Listener)this.listener) ;
				break;
			default:
				throw new Error("Unknown widget style in ISAC editor: " + style);
		}
	}

	/**
	 * This method remove the listeners on the buttons (used for check box
	 * widgets or radio group...)
	 */
	private void removeButtonsListeners()
	{
		for (int i = 0; i < this.buttons.size(); i++)
		{
			this.buttons.elementAt(i).removeSelectionListener((SelectionListener) listener);
		}
	}

	private void addButtonsListeners()
	{
		for (int i = 0; i < this.buttons.size(); i++)
		{
			this.buttons.elementAt(i).addSelectionListener((SelectionListener) listener);
		}
	}

	/**
	 * Remove the listeners of each fields
	 */
	private void removeNFieldListeners() 
	{
		for (int i = 0; i < this.nfield.size(); i++)
		{
			this.nfield.elementAt(i).removeModifyListener(this.listener);
		}
	}

	/**
	 * Add the listeners of each fields
	 */
	private void addNFieldListeners()
	{
		for (int i = 0; i < this.nfield.size(); i++)
		{
			this.nfield.elementAt(i).addModifyListener(this.listener);
		}
	}

	///////////////////////////////////////////
	// Initialization widgets
	///////////////////////////////////////////

	/**
	 * Method which initialize a new table widget
	 */
	private void initTable(Map<String,Object> params, Composite parent)
	{
		// initialize a new group
		this.nfieldGroup = new Group(parent, SWT.BORDER);
		this.nfieldGroup.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		this.nfieldGroup.setText(this.labelValue + ": ");
		// set the grid data of the group
		this.nfieldGroup.setLayoutData(new GridData(GridData.FILL_BOTH));
		// initialize the layout of the group
		GridLayout gd = new GridLayout();
		gd.numColumns = 1;
		this.nfieldGroup.setLayout(gd);
		Composite cButtons = new Composite(this.nfieldGroup, SWT.FLAT);
		GridLayout gdc = new GridLayout();
		gdc.numColumns = 2;
		cButtons.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		cButtons.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		cButtons.setLayout(gdc);
		// initialize the two buttons
		this.nfieldAdd = new Button(cButtons, SWT.FLAT);
		// set the grid data
		this.nfieldAdd.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.nfieldAdd.setText("Add entry");
		this.nfieldAdd.addSelectionListener((SelectionListener) this.listener);
		this.nfieldRemove = new Button(cButtons, SWT.FLAT);
		this.nfieldRemove.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.nfieldRemove.setText("Remove entry");
		this.nfieldRemove.addSelectionListener((SelectionListener) this.listener);
		// initialize the new table
		this.table = new Table(this.nfieldGroup, SWT.BORDER);
		this.table.setLayoutData(new GridData(GridData.FILL_BOTH));
		// this.table.addListener(SWT.Modify, (Listener)this.listener) ;
		this.tableColumns = new Vector<TableColumn>();
		// initialize the columns
		String cols = (String) params.get("cols");
		List<String> colsNames = ParameterParser.getRealTokens(";",cols);
		for (int i=0;i<colsNames.size();i++)
		{
			TableColumn col = new TableColumn(this.table, SWT.LEFT);
			col.setText(colsNames.get(i));
			col.setWidth(60);
			this.tableColumns.add(col);
		}
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		// set the line editable
		table.addSelectionListener(this);
		//initialize vectors
		this.cellEditors = new Vector<TableEditor>();
		this.cellText = new Vector<Text>();
	}

	/**
	 * Initializes the label of the parameter
	 */
	private void initLabel()
	{
		Label label = new Label(this.composite, SWT.RIGHT);
		label.setBackground(composite.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		label.setText(this.labelValue + ": ");
	}

	/**
	 * Initialize a new text field
	 * 
	 * @param params The parameters to build the widget
	 * @param parent The parent composite to add the new widget
	 */
	private void initField(Map<String,Object> params, Composite parent)
	{
		// change the composite layout
		((GridLayout) this.composite.getLayout()).numColumns = 2;
		// initialize the label of the parameter
		this.initLabel();
		// initialize the text field
		this.textField = new Text(this.composite, SWT.SINGLE | SWT.BORDER);
		// set the background color
		this.textField.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		// set grid data to the text field
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		this.textField.setLayoutData(gridData);
		// set the default text to the field
		if (params != null)
		{
			if (params.containsKey("text"))
			{
				this.textField.setText((String) params.get("text"));
			}
		}
		// add the modify listener
		this.textField.addModifyListener(listener);
	}

	/**
	 * Initialize a new nField
	 * 
	 * @param params The parameters to build the widget
	 * @param parent The parent composite to add the new widget
	 */
	private void initNField(Map<String,Object> params, Composite parent)
	{
		// initialize a new group
		this.nfieldGroup = new Group(parent, SWT.BORDER);
		this.nfieldGroup.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		this.nfieldGroup.setText(this.labelValue + " : ");
		// set the grid data of the group
		this.nfieldGroup.setLayoutData(new GridData(GridData.FILL_BOTH));
		// initialize the layout of the group
		GridLayout gd = new GridLayout();
		gd.numColumns = 2;
		this.nfieldGroup.setLayout(gd);
		// initialize the two buttons
		this.nfieldAdd = new Button(this.nfieldGroup, SWT.FLAT);
		// set the grid data
		this.nfieldAdd.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.nfieldAdd.setText("Add field");
		this.nfieldAdd.addSelectionListener((SelectionListener) this.listener);
		this.nfieldRemove = new Button(this.nfieldGroup, SWT.FLAT);
		this.nfieldRemove.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.nfieldRemove.setText("Remove field");
		this.nfieldRemove.addSelectionListener((SelectionListener) this.listener);
		// initialize the fields vector
		this.nfield = new Vector<Text>();
		this.nfieldLabel = new Vector<Label>();
	}

	/**
	 * Initialize a new radio group
	 * 
	 * @param params The parameters to build the widget
	 * @param parent The parent composite to add the new widget
	 */
	private void initRadioGroup(Map<String,Object> params, Composite parent)
	{
		this.radioGroup = new Group(parent, SWT.BORDER);
		// set the background color
		this.radioGroup.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		this.radioGroup.setText(this.labelValue);
		// set grid data to the group
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		this.radioGroup.setLayoutData(gridData);
		this.radioGroup.setLayout(new GridLayout());

		this.buttons = new Vector<Button>();
		// get the different choices
		Vector choices = (Vector) params.get(GUIDescriptionParser.CHOICES);
		Vector selected = (Vector) params.get(GUIDescriptionParser.SELECTED);
		// add the button
		if (choices != null)
		{
			for (int i = 0; i < choices.size(); i++)
			{
				Button temp = new Button(this.radioGroup, SWT.RADIO);
				temp.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				String buttonName = (String) choices.elementAt(i);
				temp.setText(buttonName);
				// add a selection listener to get changes
				temp.addSelectionListener((SelectionListener) listener);
				this.buttons.add(temp);
				// if the button is in the selected vector, select it
				if (selected.contains(buttonName))
				{
					temp.setSelection(true);
				}
			}
		}
	}

	/**
	 * Initialize a new check box
	 * 
	 * @param params The parameters to build the widget
	 * @param parent The parent composite to add the new widget
	 */
	private void initCheckBox(Map<String,Object> params, Composite parent)
	{
		this.checkBox = new Group(parent, SWT.BORDER);
		// set the background color
		this.checkBox.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		this.checkBox.setText(this.labelValue);
		// set grid data to the group
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		this.checkBox.setLayoutData(gridData);
		FillLayout fill = new FillLayout();
		fill.type = SWT.VERTICAL;
		this.checkBox.setLayout(fill);

		this.buttons = new Vector<Button>();
		// get the different choices
		Vector choices = (Vector) params.get(GUIDescriptionParser.CHOICES);
		Vector selected = (Vector) params.get(GUIDescriptionParser.SELECTED);
		// add the button
		if (choices != null)
		{
			for (int i = 0; i < choices.size(); i++)
			{
				Button temp = new Button(this.checkBox, SWT.CHECK);
				temp.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				String buttonName = (String) choices.elementAt(i);
				temp.setText(buttonName);
				temp.addSelectionListener((SelectionListener) listener);
				this.buttons.add(temp);
				// if the button is in the selected vector, select it
				if (selected.contains(buttonName))
				{
					temp.setSelection(true);
				}
			}
		}
	}

	/**
	 * Initialize a new combo
	 * 
	 * @param params
	 *            The parameters to build the widget
	 * @param parent
	 *            The parent composite to add the new widget
	 */
	private void initCombo(Map<String,Object> params, Composite parent)
	{
		// change the composite layout
		((GridLayout) this.composite.getLayout()).numColumns = 2;
		// initialize the label of the parameter
		this.initLabel();
		this.combo = new Combo(parent, SWT.BORDER | SWT.READ_ONLY);
		// set the background color
		this.combo.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		// set grid data to the group
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		this.combo.setLayoutData(gridData);
		// get the different choices
		Vector choices = null;
		// if there is no parameter, we are done
		if (params != null)
		{
			choices = (Vector) params.get(GUIDescriptionParser.CHOICES);
			Vector selected = (Vector) params.get(GUIDescriptionParser.SELECTED);
			if (choices != null)
			{
				// add the values
				for (int i = 0; i < choices.size(); i++)
				{
					this.combo.add((String) choices.elementAt(i));
					if (selected.contains((String) choices.elementAt(i)))
					{
						this.combo.select(i);
					}	
				}
			}
			this.combo.addSelectionListener((SelectionListener) listener);
		}
	}

	/**
	 * Initialize a new group
	 * 
	 * @param params The parameters to build the widget
	 * @param parent The parent composite to add the new widget
	 */
	private void initGroup(Map<String,Object> params, Composite parent)
	{
		this.group = new Group(parent, SWT.BORDER);
		// set the background color
		this.group.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		this.group.setText(this.labelValue);
		// set grid data to the group
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		this.group.setLayoutData(gridData);
		GridLayout groupLayout = new GridLayout();
		groupLayout.numColumns = 1;
		this.group.setLayout(groupLayout);
	}

	/**
	 * get the value of a table; escaping reserved characters with '\' character
	 * 
	 * @return The table values as a single string, with the following format:
	 * "col1_label=col1raw1_value|col2_label=col2raw1_value...|;col1_label=col1raw2_value|...|;"
	 */
	private String getValueTable()
	{
		String result = "";
		TableItem[] items = this.table.getItems();
		for (int i = 0; i < items.length; i++)
		{
			for (int j = 0; j < this.tableColumns.size(); j++)
			{
				String colName = this.tableColumns.elementAt(j).getText();
				String colNameModified = ParameterParser.addEscapeCharacter("\\", colName);
				colNameModified = ParameterParser.addEscapeCharacter(";",colNameModified);
				colNameModified = ParameterParser.addEscapeCharacter("|",colNameModified);
				colNameModified = ParameterParser.addEscapeCharacter("=",colNameModified);
				String colValue = items[i].getText(j);
				String colValueModified = ParameterParser.addEscapeCharacter("\\",colValue);
				colValueModified = ParameterParser.addEscapeCharacter(";",colValueModified);
				colValueModified = ParameterParser.addEscapeCharacter("|",colValueModified);
				colValueModified = ParameterParser.addEscapeCharacter("=",colValueModified);
				result = result.concat(colNameModified + "=" + colValueModified + "|");
			}
			result = result.concat(";");
		}
		return result;
	}

	/**
	 * Get the value of the text field
	 * @return String value
	 */
	private String getValueField()
	{
		return this.textField.getText();
	}

	/**
	 * Get the value of a nfield widget,
	 * escaping reserved characters '\' and ';' with '\' character
	 * 
	 * @return The value of the fields as a single string: "value1;value2;...;"
	 */
	private String getValueNField()
	{
		String result = new String("");
		// get all the values of the fields and concatenate them
		for (int i = 0; i < this.nfield.size(); i++)
		{
			// get the field
			Text text = this.nfield.elementAt(i);
			// if the values already contains some reserved characters add escape character
			String value = ParameterParser.addEscapeCharacter("\\",text.getText());
			value = ParameterParser.addEscapeCharacter(";",value);
			// concatenate the value
			result = result.concat(value + ";");
		}
		// return the result
		return result;
	}

	/**
	 * Get the value of the radio group widget,
	 * escaping reserved characters '\' and ';' with '\' character
     * @return radio group selected value, or null if no value is selected (which should not occur)
	 */
	private String getValueRadioGroup()
	{
		String value = null;
		for (int i = 0; i < this.buttons.size() && value == null; i++)
		{
			Button temp = this.buttons.elementAt(i);
			if (temp.getSelection())
			{
				value = temp.getText();
				value = ParameterParser.addEscapeCharacter("\\", value);
				value = ParameterParser.addEscapeCharacter(";", value);
			}
		}
		if (value.equals(""))
		{
			value = null;
		}
		return value;
	}

	/**
	 * Get the value of the check box, escaping reserved characters '\' and ';' with '\' character
	 * @return the check box' selected values as a single string, with the following format:
	 * "sel_value1;sel_value2;...;"
	 */
	private String getValueCheckBox()
	{
		StringBuilder result = new StringBuilder();
		for (Button checkbox : buttons)
		{
			if (checkbox.getSelection())
			{
				String value = checkbox.getText();
				value = ParameterParser.addEscapeCharacter("\\", value);
				value = ParameterParser.addEscapeCharacter(";", value);
				value += ";";
				result.append(value);
			}
		}
		return result.length() == 0 ? null : result.toString();
	}

	/**
	 * Get the selected value of the combo widget,
	 * escaping reserved characters '\' and ';' with '\' character
	 * @return the selected combo value
	 */
	private String getValueCombo()
	{
		String value = this.combo.getText();
		value = ParameterParser.addEscapeCharacter("\\", value);
		value = ParameterParser.addEscapeCharacter(";", value);
		return value;
	}

	/**
	 * Get the value of the group
     * @return null (there is no value for this kind of widget)
	 */
	private String getValueGroup()
	{
		return null;
	}

	/**
	 * This method set the value of the table widget
	 * @param value The value to set
	 */
	private void setValueTable(String value)
	{
		List<List<String>> entries = null;
		try
		{
			entries = ParameterParser.getTable(value);
			for (int i = 0; i < entries.size(); i++)
			{
				List<String> values = entries.get(i);
				String[] entryValues = values.toArray(new String[values.size()]);
				TableItem item = new TableItem(this.table, SWT.NONE);
				item.setText(entryValues);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
		}
	}

	/**
	 * Set the value of a text field widget
	 * 
	 * @param value The value to set
	 */
	private void setValueField(String value)
	{
		if (value!=null){
			this.textField.setText(value);
		}
	}

	/**
	 * Set the values of a nfield widget, unescaping escape sequences
	 * 
	 * @param value The values of the fields as a single string
	 * @see #getValueNField()
	 */
	private void setValueNField(String value)
	{
		// remove all the fields
		for (int i = 0; i < nfield.size(); i++)
		{
			nfield.elementAt(i).dispose();
			nfieldLabel.elementAt(i).dispose();
		}
		List<String> fields = ParameterParser.getNField(value);
		// for all values add a field
		Iterator<String> iter = fields.iterator();
		for (int i=0 ; iter.hasNext() ; ++i)
		{
			// create a new field
			Label label = new Label(this.nfieldGroup, SWT.NONE);
			label.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
			label.setText("field " + i + ": ");
			this.nfieldLabel.add(label);
			Text text = new Text(this.nfieldGroup, SWT.BORDER);
			// set the grid data
			GridData data = new GridData(GridData.FILL_HORIZONTAL);
			text.setLayoutData(data);
			// set the value of the field
			text.setText(iter.next());
			// add a modify listener on the field
			text.addModifyListener(this.listener);
			// add the field in the vector
			this.nfield.add(text);
		}
		// refresh the group storing the fields
		this.nfieldGroup.layout();
	}

	/**
	 * set the value of a radio group, unescaping escape sequences
	 * 
	 * @param value The radio group selected value
	 * @see #getValueRadioGroup()
	 */
	private void setValueRadioGroup(String value)
	{
		String selected = ParameterParser.getRadioGroup(value);
		for (int i = 0; i < this.buttons.size(); ++i)
		{
			Button temp = this.buttons.elementAt(i);
			temp.setSelection(temp.getText().equals(selected));
		}
	}

	/**
	 * Set the selected values of a check box group
	 * 
	 * @param value The values to be selected (in an ISAC-serialized form, i.e.
	 * values separated by ; character, and some reserved characters escaped)
	 * @see #getValueCheckBox()
	 */
	private void setValueCheckBox(String values)
	{
		List<String> choices = ParameterParser.getCheckBox(values);
		for (Button checkbox : buttons)
		{
			checkbox.setSelection(choices.contains(checkbox.getText()));	
		}
	}

	/**
	 * Set the value of the combo
	 * 
	 * @param value The value to select
	 * @see #getValueCombo()
	 */
	private void setValueCombo(String value)
	{
		String choice = ParameterParser.getCombo(value);
		// if value is empty, just set it
		if (choice.equals(""))
		{
			this.combo.setText("");
		}
		else
		{
			// check if the value is an existing value and select it if necessary
			String[] values = this.combo.getItems();
			for (int i = 0; i < values.length; i++)
			{
				if (values[i].equals(choice))
				{
					this.combo.setText(choice);
				}
			}
		}
	}

	/**
	 * Do nothing
	 * 
	 * @param value (ignored)
	 */
	private void setValueGroup(String value)
	{
	}

	/**
	 * Get the widget to add new widget on it, used for group widget
	 * @return Composite
	 */
	public Composite getComposite()
	{
		switch (this.style)
		{
			case WidgetDescription.GROUP:
				return this.group;
			default:
				return this.composite;
		}
	}

	/**
	 * dispose the element used
	 */
	public void dispose()
	{
		switch (this.style)
		{
			case WidgetDescription.GROUP:
				this.group.dispose();
				break;
			case WidgetDescription.TEXT_FIELD:
				this.textField.dispose();
				break;
			case WidgetDescription.RADIO_GROUP:
				this.radioGroup.dispose();
				break;
			case WidgetDescription.CHECK_BOX:
				this.checkBox.dispose();
				break;
			case WidgetDescription.COMBO:
				this.combo.dispose();
				break;
		}
		this.composite.dispose();
	}

	/**
	 * Attribute name getter
	 * 
	 * @return The name of the parameter
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the widget style identifier
	 */
	public int getStyle()
	{
		return style;
	}
	

	//////////////////////////////////////////////
	// LISTENERS
	//////////////////////////////////////////////


	/**
	 * modify the text of a table entry
	 * @param source 
	 * @return boolean
	 */
	public boolean modifyText(Object source)
	{
		boolean isText = false;
		for (int i = 0; i < cellText.size(); i++)
		{
			if (cellText.elementAt(i) == source)
			{
				isText = true;
			}
		}
		if (isText)
		{
			// we change the value of an item on the table
			String[] colValues = new String[this.tableColumns.size()];
			for (int i = 0; i < cellText.size(); i++)
			{
				colValues[i] = cellText.elementAt(i).getText();
			}
			TableItem item = this.table.getItem(cellYAxis);
			item.setText(colValues);
		}
		return isText;
	}

	/**
	 * Do nothing
	 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	public void widgetDefaultSelected(SelectionEvent arg0)
	{	
	}

	/**
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	public void widgetSelected(SelectionEvent arg0)
	{
		// Clean up any previous editor control
		for (int i = 0; i < cellEditors.size(); i++)
		{
			Control oldEditor = cellEditors.elementAt(i).getEditor();
			oldEditor.dispose();
		}
		// clear the editors vector
		cellEditors.removeAllElements();
		cellText.removeAllElements();

		// Identify the selected row
		int index = table.getSelectionIndex();
		if (index != -1)
		{
			TableItem item = table.getItem(index);
			cellYAxis = index;
			for (int i = 0; i < tableColumns.size(); i++) {
				// The control that will be the editor must be a child of the Table
				Text text = new Text(table, SWT.NONE);
				String toSet = item.getText(i);
				if (toSet != null) {
					text.setText(toSet);
				} else {
					text.setText("");
				}
				text.addModifyListener(this.listener);
	
				TableEditor editor = new TableEditor(table);
				//The text editor must have the same size as the cell and must
				//not be any smaller than 50 pixels.
				editor.horizontalAlignment = SWT.LEFT;
				editor.grabHorizontal = true;
				editor.minimumWidth = 50;
	
				// Open the text editor in the second column of the selected row.
				editor.setEditor(text, item, i);
				cellEditors.add(editor);
				cellText.add(text);
			}
		}
	}

	public String toString()
	{
		String styleStr;
		switch (style)
		{
			case WidgetDescription.TEXT_FIELD:
				styleStr = "field";
				break;
			case WidgetDescription.RADIO_GROUP:
				styleStr = "radio";
				break;
			case WidgetDescription.CHECK_BOX:
				styleStr = "checkbox";
				break;
			case WidgetDescription.GROUP:
				styleStr = "group";
				break;
			case WidgetDescription.COMBO:
				styleStr = "combo";
				break;
			case WidgetDescription.NFIELD:
				styleStr = "nfield";
				break;
			case WidgetDescription.TABLE:
				styleStr = "table";
				break;
			default:
				throw new Error("Unknown widget style " + style);
		}
		return styleStr + " " + name + "#" + getValue();
	}
}