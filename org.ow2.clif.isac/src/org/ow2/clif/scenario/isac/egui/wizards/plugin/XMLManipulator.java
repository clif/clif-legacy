/*
 * CLIF is a Load Injection Framework Copyright (C) 2006, 2007 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF $Name$
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.wizards.plugin;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.xpath.XPath;
import org.ow2.clif.util.XMLEntityResolver;

/**
 * This class permits to manipulate XML model with the JDOM API
 * in order to construct valid gui, plugin and build XML files.
 * @author Fabrice Rivart
 * @author Bruno Dillenseger
 */
public class XMLManipulator {
	
	private Document guiDocument;
	private Document pluginDocument;
	private Document buildDocument;
	private XMLOutputter outputter;
	private SAXBuilder sb;
	
	
	/**
	 * Constructor.
	 */
	public XMLManipulator() {
		sb = new SAXBuilder();
		sb.setEntityResolver(new XMLEntityResolver());
		outputter = new XMLOutputter(Format.getPrettyFormat().setExpandEmptyElements(true));
	}

	/**
	 * This method loads gui file in memory.
	 * @param arg0 InputStream representing the gui file
	 */
	public void loadGui(InputStream arg0) throws Exception {
		guiDocument = sb.build(arg0);
	}
	
	/**
	 * This method loads plugin file in memory.
	 * @param arg0 InputStream representing the plugin file
	 */
	public void loadPlugin(InputStream arg0) throws Exception {
		pluginDocument = sb.build(arg0);
	}

	/**
	 * This method loads build file in memory.
	 * @param arg0 InputStream representing the build file
	 */
	public void loadBuild(InputStream arg0) throws Exception {
		buildDocument = sb.build(arg0);
	}

	/**
	 * This method resets memory.
	 */
	public void reset() {
		guiDocument = null;
		pluginDocument = null;
		buildDocument = null;
	}

	/**
	 * Returns a InputStream representing the gui XML file.
	 * @return InputStream representing the gui XML file.
	 */
	public InputStream getGuiContent() {
		return new ByteArrayInputStream(outputter.outputString(guiDocument).getBytes());
	}
	
	/**
	 * Returns a InputStream representing the plugin XML file.
	 * @return InputStream representing the plugin XML file.
	 */
	public InputStream getPluginContent() {
		return new ByteArrayInputStream(outputter.outputString(pluginDocument).getBytes());
	}

	/**
	 * Returns a InputStream representing the build XML file.
	 * @return input stream for the build XML file contents
	 */
	public InputStream getBuildContent()
	{
		return new ByteArrayInputStream(outputter.outputString(buildDocument).getBytes());
	}
	
	/**
	 * This method sets plugin name in plugin and build XML files.
	 * @param name String representing plugin name.
	 */
	public void setPluginName(String name) throws Exception {
		
		String xpath = "/plugin";
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		element.setAttribute("name", name);
		xpath = "/project/property[@name='plugin.dir']";
		element = (Element)XPath.selectSingleNode(buildDocument, xpath);
		element.setAttribute("value", "${isac.dir}/plugins/" + name);
		xpath = "/project/target[@name='compile']/copy";
		element = (Element)XPath.selectSingleNode(buildDocument, xpath);
		element.setAttribute("todir", "${build.dir}/" + name);
	}
	
	/**
	 * This method sets class name in plugin XML file.
	 * @param name String representing class name.
	 */
	public void setPluginClassName(String name) throws Exception {
		
		String xpath = "/plugin/object";
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		element.setAttribute("class", name);
	}
	
	/**
	 * This method sets object name in gui XML file.
	 * @param name String representing object name.
	 */
	public void setGuiObjectName(String name) throws Exception {
		
		String xpath = "/gui/object";
		Element element = (Element) XPath.selectSingleNode(guiDocument, xpath);
		element.setAttribute("name", name);
	}

	/**
	 * This method sets the source directory in the build XML file.
	 * @param dirname the directory name
	 */
	public void setSourceDir(String dirname) throws JDOMException
	{
		String xpath = "/project/target[@name='compile']/javac";
		Element element = (Element) XPath.selectSingleNode(buildDocument, xpath);
		element.setAttribute("srcdir", "${plugin.dir}" + dirname);
	}

	/**
	 * Sets the names of plugin and gui XML files associated to the plug-in
	 * @param pluginFile name of the plugin XML file
	 * @param guiFile name of the GUI XML file
	 */
	public void setXMLFiles(String pluginFile, String guiFile) throws JDOMException
	{
		String xpath = "/project/target[@name='compile']/copy/fileset";
		Element element = (Element) XPath.selectSingleNode(buildDocument, xpath);
		element.setAttribute("includes", "isac-plugin.properties," + pluginFile + "," + guiFile);
	}

	/**
	 * This method adds action element in gui and plugin XML file.
	 * @param action String representing the action to add (sample, timer, test, control)
	 * @param name String representing action name
	 * @param number int representing action number (generated)
	 * @throws Exception if there is an error
	 */
	public void addAction(String action, String name, int number) throws Exception {
		
		// Create gui elements
		Element eActionGui = new Element(action);
		eActionGui.addContent(new Element("params"));
		eActionGui.setAttribute("name", name);
		
		// Create plugin elements
		Element eActionPlugin = new Element(action);
		eActionPlugin.addContent(new Element("params"));
		eActionPlugin.setAttribute("name", name);
		eActionPlugin.setAttribute("number", String.valueOf(number));
		
		guiDocument.getRootElement().addContent(getIndexOfGuiLastAction(action) + 1, eActionGui);
		pluginDocument.getRootElement().addContent(getIndexOfPluginLastAction(action) + 1, eActionPlugin);
	}
	
	/**
	 * This method deletes action element in gui and plugin XML file.
	 * @param action String representing the action to delete (sample, timer, test, control)
	 * @param name String representing action name
	 * @throws Exception if there is an error
	 */
	public void deleteAction(String action, String name) throws Exception {
		
		guiDocument.getRootElement().removeContent(getGuiAction(action, name));
		pluginDocument.getRootElement().removeContent(getPluginAction(action, name));	
	}
	
	/**
	 * This method update action element in gui and plugin XML file.
	 * @param action String representing the action to update (sample, timer, test, control)
	 * @param oldName String representing old action name
	 * @param newName String representing new action name
	 * @throws Exception if there is an error
	 */
	public void updateAction(String action, String oldName, String newName) throws Exception {
		
		getGuiAction(action, oldName).setAttribute("name", newName);
		getPluginAction(action, oldName).setAttribute("name", newName);
	}
	
	/**
	 * This method move up action element in gui and plugin XML file.
	 * @param action String representing the action to move (sample, timer, test, control)
	 * @param name String representing action name
	 * @throws Exception if there is an error
	 */
	public void moveUpAction(String action, String name) throws Exception {
		
		Element actionGui = getGuiAction(action, name);
		Element actionPlugin = getPluginAction(action, name);
		List actionsGui = guiDocument.getRootElement().getChildren(action);
		List actionsPlugin = pluginDocument.getRootElement().getChildren(action);
		
		if (actionsGui.size() > 1) {
			for (int i = 1; i < actionsGui.size(); i++) {
				if (actionsGui.get(i).equals(actionGui)) {
					actionsGui.remove(i);
					actionsGui.add(i - 1, actionGui);
					break;
				}
			}
		}
		
		if (actionsPlugin.size() > 1) {
			for (int i = 1; i < actionsPlugin.size(); i++) {
				if (actionsPlugin.get(i).equals(actionPlugin)) {
					actionsPlugin.remove(i);
					actionsPlugin.add(i - 1, actionPlugin);
					break;
				}
			}
		}
	}
	
	/**
	 * This method move down action element in gui and plugin XML file.
	 * @param action String representing the action to move (sample, timer, test, control)
	 * @param name String representing action name
	 * @throws Exception if there is an error
	 */
	public void moveDownAction(String action, String name) throws Exception {
		
		Element actionGui = getGuiAction(action, name);
		Element actionPlugin = getPluginAction(action, name);
		List actionsGui = guiDocument.getRootElement().getChildren(action);
		List actionsPlugin = pluginDocument.getRootElement().getChildren(action);
		
		if (actionsGui.size() > 1) {
			for (int i = actionsGui.size() - 2; i >= 0; i--) {
				if (actionsGui.get(i).equals(actionGui)) {
					actionsGui.remove(i);
					actionsGui.add(i + 1, actionGui);
					break;
				}
			}
		}
		
		if (actionsPlugin.size() > 1) {
			for (int i = actionsPlugin.size() - 2; i >= 0; i--) {
				if (actionsPlugin.get(i).equals(actionPlugin)) {
					actionsPlugin.remove(i);
					actionsPlugin.add(i + 1, actionPlugin);
					break;
				}
			}
		}
	}
	
	/**
	 * Returns a ArrayList representing string actions.
	 * @param action String representing the action to get (sample, timer, test, control)
	 * @return ArrayList representing string actions
	 * @throws Exception if there is an error
	 */
	public ArrayList<String> getActions(String action) throws Exception {
		
		String xpath = "/plugin/" + action;
		List elements = XPath.selectNodes(pluginDocument, xpath);
		
		ArrayList<String> list = new ArrayList<String>();
		
		Iterator iter = elements.iterator();
		while (iter.hasNext()) {
			Element element = (Element) iter.next();
			list.add(element.getAttributeValue("name"));
		}
		
		return list;
	}
	
	/**
	 * Returns a String representing action help.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @return String representing action help
	 * @throws Exception if there is an error
	 */
	public String getHelp(String action, String name) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/plugin/" + action + "/help";
		}
		else {
			xpath = "/plugin/" + action + "[@name='" + name + "']/help";
		}
		
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		
		if (element == null) {
			return "";
		}
		else {
			return element.getText();
		}
	}
	
	/**
	 * Set action help in plugin XML file
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @param help String representing action help
	 * @throws Exception if there is an error
	 */
	public void setHelp(String action, String name, String help) throws Exception {
		
		Element element = getPluginAction(action, name);
		
		if (help == null || help.equalsIgnoreCase("")) {
			element.removeChild("help");
		}
		else {
			Element eHelp = element.getChild("help");
			if (eHelp == null) {
				eHelp = new Element("help");
				element.addContent(eHelp);
			}
			eHelp.setText(help);
		}
		
	}
	
	/**
	 * This method adds group element in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param parentPath String representing XPath parent expression 
	 * @param groupName String representing group name
	 * @throws Exception if there is an error
	 */
	public void addGroup(String action, 
						String actionName, 
						String parentPath,
						String groupName) throws Exception {
		
		// Create gui elements
		Element eGroup = new Element("group");
		eGroup.setAttribute("name", groupName);
		
		Element element = getGuiElement(action, actionName, parentPath);
		element.addContent(eGroup);
	}
	
	/**
	 * This method adds parameter element in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param pluginPath String representing XPath parent expression in plugin file
	 * @param paramName String representing parameter name
	 * @throws Exception if there is an error
	 */
	public void addParameter(String action, 
						String actionName, 
						String guiPath,
						String pluginPath,
						String paramName) throws Exception {
		
		// Create gui elements
		Element eParamGui = new Element("param");
		eParamGui.setAttribute("label", "");
		eParamGui.setAttribute("name", paramName);
		Element eField = new Element("field");
		eField.setAttribute("text", "");
		eField.setAttribute("size", "8");
		eParamGui.addContent(eField);
		
		// Create plugin elements
		Element eParamPlugin = new Element("param");
		eParamPlugin.setAttribute("name", paramName);
		eParamPlugin.setAttribute("type", "String");
		
		Element eParentGui = getGuiElement(action, actionName, guiPath);
		eParentGui.addContent(eParamGui);
		
		Element eParentPlugin = getPluginElement(action, actionName, pluginPath);
		eParentPlugin.addContent(eParamPlugin);
	}
	
	/**
	 * This method deletes group or parameter element in gui and plugin XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiParentPath String representing XPath parent expression in gui file
	 * @param pluginParentPath String representing XPath parent expression in plugin file
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param pluginElementPath String representing XPath element expression in plugin file
	 * @throws Exception if there is an error
	 */
	public void deleteElement(String action, 
						String actionName, 
						String guiParentPath,
						String pluginParentPath,
						String guiElementPath,
						String pluginElementPath) throws Exception {
				
		Element eParentGui = getGuiElement(action, actionName, guiParentPath);
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		eParentGui.removeContent(eGui);
		
		Element eParentPlugin = getPluginElement(action, actionName, pluginParentPath);
		Element ePlugin = getPluginElement(action, actionName, pluginElementPath);
		eParentPlugin.removeContent(ePlugin);
	}
	
	/**
	 * This method updates group or parameter element name in gui and plugin XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param pluginElementPath String representing XPath element expression in plugin file
	 * @param newName String representing the new element name
	 * @throws Exception if there is an error
	 */
	public void updateElementName(String action, 
						String actionName, 
						String guiElementPath,
						String pluginElementPath,
						String newName) throws Exception {
				
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		Element ePlugin = getPluginElement(action, actionName, pluginElementPath);
		
		if (eGui != null) {
			eGui.setAttribute("name", newName);
		}
		
		if (ePlugin != null) {
			ePlugin.setAttribute("name", newName);
		}
	}
	
	/**
	 * This method moves up parameter in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiParentPath String representing XPath parent element expression in gui file
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @throws Exception if there is an error
	 */
	public void moveUpParameter(String action, 
						String actionName, 
						String guiParentPath,
						String guiElementPath) throws Exception {
		
		Element parentGui = getGuiElement(action, actionName, guiParentPath);
		Element paramGui = getGuiElement(action, actionName, guiElementPath);
		
		if (parentGui != null) {
			List paramsGui = parentGui.getChildren();
			if (paramsGui.size() > 1) {
				for (int i = 1; i < paramsGui.size(); i++) {
					if (paramsGui.get(i).equals(paramGui)) {
						paramsGui.remove(i);
						paramsGui.add(i - 1, paramGui);
						break;
					}
				}
			}
		}
	}
	
	/**
	 * This method moves down parameter in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiParentPath String representing XPath parent element expression in gui file
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @throws Exception if there is an error
	 */
	public void moveDownParameter(String action, 
						String actionName, 
						String guiParentPath,
						String guiElementPath) throws Exception {
		
		Element parentGui = getGuiElement(action, actionName, guiParentPath);
		Element paramGui = getGuiElement(action, actionName, guiElementPath);
		
		if (parentGui != null) {
			List paramsGui = parentGui.getChildren();
			if (paramsGui.size() > 1) {
				for (int i = paramsGui.size() - 2; i >= 0; i--) {
					if (paramsGui.get(i).equals(paramGui)) {
						paramsGui.remove(i);
						paramsGui.add(i + 1, paramGui);
						break;
					}
				}
			}
		}
	}
	
	/**
	 * This method updates parameter label in gui file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param label String representing the new parameter label
	 * @throws Exception if there is an error
	 */
	public void setParameterLabel(String action, 
						String actionName, 
						String guiElementPath,
						String label) throws Exception {
				
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		
		if (eGui != null) {
			eGui.setAttribute("label", label);
		}
	}
	
	/**
	 * This method returns parameter label from gui file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @return parameter label from gui file.
	 * @throws Exception if there is an error
	 */
	public String getParameterLabel(String action, 
						String actionName, 
						String guiElementPath) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		
		return eGui.getAttributeValue("label");
	}
	
	/**
	 * This method updates parameter represention in gui file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param type String representing the new parameter representation
	 * @throws Exception if there is an error
	 */
	public void setParameterType(String action, 
						String actionName, 
						String guiElementPath,
						String type) throws Exception {
				
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		
		if (eGui != null) {
			eGui.removeContent();
			Element eType = new Element(type);
			eGui.addContent(eType);
		}
	}
	
	/**
	 * This method returns parameter represention from gui file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @return parameter represention from gui file
	 * @throws Exception if there is an error
	 */
	public String getParameterType(String action, 
						String actionName, 
						String guiElementPath) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		Element type = (Element) eGui.getChildren().get(0);
		
		if (type != null) {
			return type.getName();
		}
		else {
			return "field";
		}
	}
	
	/**
	 * This method updates parameter field text in gui file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param text String representing the new parameter field text
	 * @throws Exception if there is an error
	 */
	public void setParameterFieldText(String action, 
						String actionName, 
						String guiElementPath,
						String text) throws Exception {
				
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		
		if (eGui != null) {
			Element eField = (Element) eGui.getChild("field");
			if (eField == null){
				eField = new Element("field");
				eGui.addContent(eField);
			}
			eGui.getChild("field").setAttribute("text", text);
		}
	}
	
	/**
	 * This method returns parameter field text from gui file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @return parameter represention from gui file
	 * @throws Exception if there is an error
	 */
	public String getParameterFieldText(String action, 
						String actionName, 
						String guiElementPath) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		Element eField = (Element) eGui.getChild("field");
		
		if (eField != null) {
			return eField.getAttributeValue("text");
		}
		else {
			return "";
		}
	}
	
	/**
	 * This method updates parameter field size in gui file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @param size String representing the new parameter field size
	 * @throws Exception if there is an error
	 */
	public void setParameterFieldSize(String action, 
						String actionName, 
						String guiElementPath,
						String size) throws Exception {
				
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		
		if (eGui != null) {
			Element eField = (Element) eGui.getChild("field");
			if (eField == null){
				eField = new Element("field");
				eGui.addContent(eField);
			}
			eField.setAttribute("size", size);
		}
	}
	
	/**
	 * This method returns parameter field size from gui file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiElementPath String representing XPath element expression in gui file
	 * @return parameter represention from gui file
	 * @throws Exception if there is an error
	 */
	public String getParameterFieldSize(String action, 
						String actionName, 
						String guiElementPath) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiElementPath);
		Element eField = (Element) eGui.getChild("field");
		
		if (eField != null) {
			return eField.getAttributeValue("size");
		}
		else {
			return "";
		}
	}
	
	/**
	 * This method adds parameter element in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @throws Exception if there is an error
	 */
	public void addParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiPath);
		Element eType = (Element) eGui.getChild(type);
		
		if (type.equalsIgnoreCase("table")) {
			String cols = eType.getAttributeValue("cols");
			if (cols == null || cols.equalsIgnoreCase("")) {
				eType.setAttribute("cols", elementName);
			}
			else {
				eType.setAttribute("cols", cols + ";" + elementName);
			}
		}
		else {
			Element choice = new Element("choice");
			choice.setAttribute("value", elementName);
			choice.setAttribute("default", "false");
			eType.addContent(choice);
		}
	}
	
	/**
	 * This method deletes parameter element in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @throws Exception if there is an error
	 */
	public void deleteParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiPath);
		Element eType = (Element) eGui.getChild(type);
		
		if (type.equalsIgnoreCase("table")) {
			String cols = eType.getAttributeValue("cols");
			String newCols = cols.replaceAll(";" + elementName, "")
								.replaceAll(elementName + ";", "")
								.replaceAll(elementName, "");
			eType.setAttribute("cols", newCols);
		}
		else {
			Element choice = getGuiParameterElement(action, actionName, guiPath, type, elementName);
			eType.removeContent(choice);
		}
	}
	
	/**
	 * This method updates parameter element in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param oldElementName String representing old parameter element name
	 * @param newElementName String representing new parameter element name
	 * @throws Exception if there is an error
	 */
	public void updateParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String oldElementName,
						String newElementName) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiPath);
		Element eType = (Element) eGui.getChild(type);
		
		if (type.equalsIgnoreCase("table")) {
			String cols = eType.getAttributeValue("cols");
			String newCols = cols.replaceAll(oldElementName, newElementName);
			eType.setAttribute("cols", newCols);
		}
		else {
			Element choice = getGuiParameterElement(action, actionName, guiPath, type, oldElementName);
			choice.setAttribute("value", newElementName);
		}
	}
	
	/**
	 * This method moves up parameter element in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @throws Exception if there is an error
	 */
	public void moveUpParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiPath);
		Element eType = (Element) eGui.getChild(type);
		
		if (type.equalsIgnoreCase("table")) {
			String cols = eType.getAttributeValue("cols");
			String [] elements = cols.split(";");
			
			for (int i = 0; i < elements.length; i++) {
				if (elements[i].equalsIgnoreCase(elementName)) {
					if (i > 0) {
						String tmp = elements[i - 1];
						elements[i - 1] = elements[i];
						elements[i] = tmp;
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < elements.length; i++) {
				sb.append(elements[i]);
				if (i != elements.length - 1) {
					sb.append(";");
				}
			}
			
			eType.setAttribute("cols", sb.toString());
		}
		else {
			Element choice = getGuiParameterElement(action, actionName, guiPath, type, elementName);
			List choices = eType.getChildren();
			
			if (choices.size() > 1) {
				for (int i = 1; i < choices.size(); i++) {
					if (choices.get(i).equals(choice)) {
						choices.remove(i);
						choices.add(i - 1, choice);
						break;
					}
				}
			}
		}
	}
	
	/**
	 * This method moves down parameter element in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @throws Exception if there is an error
	 */
	public void moveDownParameterElement(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		
		Element eGui = getGuiElement(action, actionName, guiPath);
		Element eType = (Element) eGui.getChild(type);
		
		if (type.equalsIgnoreCase("table")) {
			String cols = eType.getAttributeValue("cols");
			String [] elements = cols.split(";");
			
			for (int i = 0; i < elements.length; i++) {
				if (elements[i].equalsIgnoreCase(elementName)) {
					if (i < elements.length - 1) {
						String tmp = elements[i + 1];
						elements[i + 1] = elements[i];
						elements[i] = tmp;
					}
				}
			}
			
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < elements.length; i++) {
				sb.append(elements[i]);
				if (i != elements.length - 1) {
					sb.append(";");
				}
			}
			
			eType.setAttribute("cols", sb.toString());
		}
		else {
			Element choice = getGuiParameterElement(action, actionName, guiPath, type, elementName);
			List choices = eType.getChildren();
			
			if (choices.size() > 1) {
				for (int i = choices.size() - 2; i >= 0 ; i--) {
					if (choices.get(i).equals(choice)) {
						choices.remove(i);
						choices.add(i + 1, choice);
						break;
					}
				}
			}
		}
	}
	
	/**
	 * Returns parameter element list in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @return ArrayList of all elements
	 * @throws Exception if there is an error
	 */
	public ArrayList<String> getParameterElements(String action, 
						String actionName, 
						String guiPath,
						String type) throws Exception {
		
		ArrayList<String> list = new ArrayList<String>();
		Element eGui = getGuiElement(action, actionName, guiPath);
		Element eType = (Element) eGui.getChild(type);
		
		if (type.equalsIgnoreCase("table")) {
			String cols = eType.getAttributeValue("cols");
			String [] elements = cols.split(";");
			
			for (int i = 0; i < elements.length; i++) {
				list.add(elements[i]);
			}
		}
		else {
			List choices = eType.getChildren();
			Iterator iter = choices.iterator();
			while (iter.hasNext()) {
				Element element = (Element) iter.next();
				list.add(element.getAttributeValue("value"));
			}
		}
		
		return list;
	}
	
	
	
	/**
	 * This method sets default parameter element value to true or false in gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @param value String representing the value to set
	 * @throws Exception if there is an error
	 */
	public void setParameterElementDefault(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName,
						String value) throws Exception {
		Element choice = getGuiParameterElement(action, actionName, guiPath, type, elementName);
		choice.setAttribute("default", value);
		
	}
	
	/**
	 * Returns default parameter element value from gui XML file.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param guiPath String representing XPath parent expression in gui file
	 * @param type String representing representation of element
	 * @param elementName String representing parameter element name
	 * @return String representing default parameter element value
	 * @throws Exception if there is an error
	 */
	public String getParameterElementDefault(String action, 
						String actionName, 
						String guiPath,
						String type,
						String elementName) throws Exception {
		Element choice = getGuiParameterElement(action, actionName, guiPath, type, elementName);
		String value = choice.getAttributeValue("default");
		
		if (value != null && (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("yes"))) {
			return "true";
		}
		else {
			return "false";
		}
	}
	
	/**
	 * Returns a Element representing action params element.
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @return Element representing action params element
	 * @throws Exception if there is an error
	 */
	public Element getParams(String action, String name) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/gui/" + action + "/params";
		}
		else {
			xpath = "/gui/" + action + "[@name='" + name + "']/params";
		}
		
		return (Element) XPath.selectSingleNode(guiDocument, xpath);
	}
	
	/**
	 * Returns String [] contening all parameter name
	 * @param action String representing an action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @return String [] contening all parameter name
	 * @throws Exception if there is an error
	 */
	public String [] getStringParams(String action, String name) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/plugin/" + action + "/params/param";
		}
		else {
			xpath = "/plugin/" + action + "[@name='" + name + "']/params/param";
		}
		
		List parameters = XPath.selectNodes(pluginDocument, xpath);
		
		String [] params = new String [parameters.size()];
		
		int i = 0;
		Iterator iter = parameters.iterator();
		while (iter.hasNext()) {
			Element element = (Element) iter.next();
			params[i] = element.getAttributeValue("name");
			i++;
		}
		
		return params;
	}
	
	/**
	 * Returns a Element representing the XML element in gui file.
	 * @param action String representing the action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @param elementPath String representing XPath expression of element
	 * @param type String representing parameter representation
	 * @param elementName String representing the choice name
	 * @return Element representing the action XML element in gui file
	 * @throws Exception if there is an error.
	 */
	private Element getGuiParameterElement(String action, 
										String name, 
										String elementPath,
										String type,
										String elementName) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/gui/" + action + 
			elementPath + "/" + type + "/choice[@value='" + elementName + "']";
		}
		else {
			xpath = "/gui/" + action + "[@name='" + name + "']" + 
			elementPath + "/" + type + "/choice[@value='" + elementName + "']";
		}
		
		Element element = (Element) XPath.selectSingleNode(guiDocument, xpath);
		return element;
	}
	
	/**
	 * Returns a Element representing the XML element in gui file.
	 * @param action String representing the action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @param elementPath String representing XPath expression of element
	 * @return Element representing the action XML element in gui file
	 * @throws Exception if there is an error.
	 */
	private Element getGuiElement(String action, String name, String elementPath) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/gui/" + action + elementPath;
		}
		else {
			xpath = "/gui/" + action + "[@name='" + name + "']" + elementPath;
		}
		
		Element element = (Element) XPath.selectSingleNode(guiDocument, xpath);
		return element;
	}
	
	/**
	 * Returns a Element representing the XML element in plugin file.
	 * @param action String representing the action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @param elementPath String representing XPath expression of element
	 * @return Element representing the action XML element in plugin file
	 * @throws Exception if there is an error.
	 */
	private Element getPluginElement(String action, String name, String elementPath) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/plugin/" + action + elementPath;
		}
		else {
			xpath = "/plugin/" + action + "[@name='" + name + "']" + elementPath;
		}
		
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		return element;
	}
	
	/**
	 * Returns a Element representing the action XML element in gui file.
	 * @param action String representing the action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @return Element representing the action XML element in gui file
	 * @throws Exception if there is an error.
	 */
	private Element getGuiAction(String action, String name) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/gui/" + action;
		}
		else {
			xpath = "/gui/" + action + "[@name='" + name + "']";
		}
		
		Element element = (Element) XPath.selectSingleNode(guiDocument, xpath);
		return element;
	}
	
	/**
	 * Returns a Element representing the action XML element in plugin file.
	 * @param action String representing the action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @return Element representing the action XML element in plugin file
	 * @throws Exception if there is an error.
	 */
	private Element getPluginAction(String action, String name) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/plugin/" + action;
		}
		else {
			xpath = "/plugin/" + action + "[@name='" + name + "']";
		}
		
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		return element;
	}
	
	/**
	 * Returns a int representing index of the last action element in gui file.
	 * @param action String representing the action (sample, timer, test, control, object)
	 * @return int representing index of the last action element in gui file
	 * @throws Exception if there is an error.
	 */
	private int getIndexOfGuiLastAction(String action) throws Exception {
		
		String xpath = "/gui/" + action + "[last()]";
		Element element = (Element) XPath.selectSingleNode(guiDocument, xpath);
		
		if (element == null) {
			String last = "/gui/*[last()]";
			element = (Element) XPath.selectSingleNode(guiDocument, last);
		}
		
		return guiDocument.getRootElement().indexOf(element);
	}
	
	/**
	 * Returns a int representing index of the last action element in plugin file.
	 * @param action String representing the action (sample, timer, test, control, object)
	 * @return int representing index of the last action element in plugin file
	 * @throws Exception if there is an error.
	 */
	private int getIndexOfPluginLastAction(String action) throws Exception {
		
		String xpath = "/plugin/" + action + "[last()]";
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		
		if (element == null) {
			String last = "/plugin/*[last()]";
			element = (Element) XPath.selectSingleNode(pluginDocument, last);
		}
		
		return pluginDocument.getRootElement().indexOf(element);
	}
	
	/**
	 * Returns a int representing the maximum number attribute in plugin XML file.
	 * @return int representing the maximum number attribute in plugin XML file
	 * @throws Exception if there is an error.
	 */
	public int getMaxNumber() throws Exception {
		
		int max = -1;
		String xpath = "/plugin/sample | /plugin/test | /plugin/timer | /plugin/control";
		List actions = XPath.selectNodes(pluginDocument, xpath);
		
		Iterator iter = actions.iterator();
		while (iter.hasNext()) {
			Element element = (Element) iter.next();
			int number = element.getAttribute("number").getIntValue();
			if (number > max) {
				max = number;
			}
		}
		
		return max;
	}
	
	/**
	 * Returns true if element exists
	 * @param action String representing the action (sample, timer, test, control, object)
	 * @param name String representing action name
	 * @param elementPath String representing XPath expression of element
	 * @return true if element exists
	 * @throws Exception if there is an error.
	 */
	public boolean isParameterExist(String action, String name, String elementPath) throws Exception {
		
		String xpath;
		if (action.equals("object")) {
			xpath = "/plugin/" + action + elementPath;
		}
		else {
			xpath = "/plugin/" + action + "[@name='" + name + "']" + elementPath;
		}
		
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		
		if (element != null) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	/**
	 * @return String representing class name
	 * @throws Exception if there is an error.
	 */
	public String getClassName() throws Exception {
		String xpath = "/plugin/object";
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		if (element !=  null) {
			String className = element.getAttributeValue("class");
			String [] tab = className.split("[.]");
			return tab[tab.length - 1];
		}
		return "";
	}
	
	/**
	 * Returns an int representing action number.
	 * @param action String representing the action (sample, timer, test, control)
	 * @param name String representing action name
	 * @return int representing action number
	 * @throws Exception if there is an error.
	 */
	public int getActionNumber(String action, String name) throws Exception {
		String xpath = "/plugin/" + action + "[@name='" + name + "']";
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		
		String number = element.getAttributeValue("number");
		
		return Integer.parseInt(number);
	}
	
	/**
	 * @return String representing package name
	 * @throws Exception if there is an error.
	 */
	public String getPackageName() throws Exception {
		String xpath = "/plugin/object";
		Element element = (Element) XPath.selectSingleNode(pluginDocument, xpath);
		StringBuffer sb = new StringBuffer();
		if (element !=  null) {
			String className = element.getAttributeValue("class");
			String [] tab = className.split("[.]");
			
			for (int i = 0; i < tab.length - 1; i++) {
				sb.append(tab[i]);
				if (i != tab.length - 2) {
					sb.append(".");
				}
			}
		}
		return sb.toString();
	}

}