/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The main plugin class to be used in the desktop.
 * 
 * @author Joan Chaumont
 */
public class IsacScenarioPlugin extends AbstractUIPlugin {
    
    /**
     * The shared instance.
     */
    private static IsacScenarioPlugin plugin;
    
    /**
     * Resource bundle.
     */
    private ResourceBundle resourceBundle;
    
    /**
     * The plugin id.
     */
    public static String PLUGIN_ID = "org.ow2.clif.isac.gui";
    
    /**
     * The constructor.
     */
    public IsacScenarioPlugin() {
        super();
        plugin = this;
        try {
            resourceBundle = ResourceBundle.getBundle(
            "org.ow2.clif.isac.gui.IsacScenarioPluginResources");
        } catch (MissingResourceException x) {
            resourceBundle = null;
        }
    }
    
    /**
     * This method is called upon plug-in activation
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
    }
    
    /**
     * This method is called when the plug-in is stopped
     */
    public void stop(BundleContext context) throws Exception {
        super.stop(context);
    }
    
    /**
     * Returns the shared instance.
     * @return IsacScenarioPlugin
     */
    public static IsacScenarioPlugin getDefault() {
        return plugin;
    }
    
    /**
     * Returns the string from the plugin's resource bundle,
     * or 'key' if not found.
     * @param key 
     * @return String
     */
    public static String getResourceString(String key) {
        ResourceBundle bundle = IsacScenarioPlugin.getDefault().getResourceBundle();
        try {
            return (bundle != null) ? bundle.getString(key) : key;
        } catch (MissingResourceException e) {
            return key;
        }
    }
    
    /**
     * Returns the plugin's resource bundle,
     * @return ResourceBundle
     */
    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }
}
