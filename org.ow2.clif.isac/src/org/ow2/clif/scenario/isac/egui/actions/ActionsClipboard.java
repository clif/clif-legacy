/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.actions;

import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.widgets.Display;

/**
 * This clipboard is used by all copy paste actions.
 * @author Joan Chaumont
 */
public class ActionsClipboard {
    
    private static Clipboard clipboard;
    
    /**
     * @return the current Clipboard
     */
    public static Clipboard getClipboard()
    {
      if (clipboard == null)
      {
        clipboard = new Clipboard(Display.getCurrent());
      }

      return clipboard;
    }
    
}
