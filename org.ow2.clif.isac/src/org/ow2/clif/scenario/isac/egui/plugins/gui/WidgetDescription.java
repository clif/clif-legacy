/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins.gui;

import java.util.Iterator;
import java.util.Map;

/**
 * This class is the implementation of a widget description object, it will be
 * used to create a widget with this description in order to edit some
 * parameters
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class WidgetDescription {

    /**
     * No widget should have this dummy type
     */
    public static final int NONE = -1;

    /** 
     * A simple text field 
     * */
    public static final int TEXT_FIELD = 0;
    /**
     * A radio button group
     */
    public static final int RADIO_GROUP = 1;
    /**
     * A check box group
     */
    public static final int CHECK_BOX = 2;
    /**
     * A group for grouping multiple widget
     */
    public static final int GROUP = 3;
    /**
     * A combo for grouping values
     */
    public static final int COMBO = 4;
    /**
     * A nfield for editing n values
     */
    public static final int NFIELD = 5;
    /**
     * A table widget
     */
    public static final int TABLE = 6 ;
    
    private int type;
    private String text;
    private String label;
    private Map<String,Object> params;
    
    /**
     * Build a new widget description element
     * @param t The type of the widget
     * @param text The text of the widget if label is not defined (the name of the param)
     * @param label The label of the widget
     * @param p The parameters to build the widget
     */
    public WidgetDescription(int t, String text, String label, Map<String,Object> p) {
        this.type = t;
        this.text = text;
        this.params = p;
        this.label = label ;
    }
    
    /**
     * @return Returns the params.
     */
    public Map<String,Object> getParams() {
        return params;
    }
    /**
     * @return Returns the text.
     */
    public String getText() {
        return text;
    }
    /**
     * @return Returns the type.
     */
    public int getType() {
        return type;
    }
    /**
     * @return Returns the label.
     */
    public String getLabel() {
        return label;
    }
    /**
     * @return Return a representation of the WidgetDescription
     */
    public String toString() {
        String result = "";
        switch (this.type) {
        case CHECK_BOX :
            result = result+"type = checkbox\n";
            break;
        case COMBO :
            result = result+"type = combo\n";
            break;
        case GROUP :
            result = result+"type = group\n";
            break;
        case RADIO_GROUP :
            result = result+"type = radio_group\n";
            break;
        case TEXT_FIELD :
            result = result+"type = text_field\n";
            break;
        case NFIELD :
            result = result+"type = nfield\n";
            break;
        default :
            result = result+"type = unknown\n";
        break;
        }
        result = result + "text of the widget = " + this.text + "\n";
        if (this.params != null) {
            Iterator<String> keys = this.params.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                result = result + "Parameters :\n";
                result = result + key + " : " + this.params.get(key) + "\n";
            }
        }
        return result;
    }
}