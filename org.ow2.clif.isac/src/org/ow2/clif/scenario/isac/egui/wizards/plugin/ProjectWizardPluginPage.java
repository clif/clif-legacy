/*
 * CLIF is a Load Injection Framework Copyright (C) 2006 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF $Name$
 * 
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.wizards.plugin;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.ui.wizards.NewTypeWizardPage;
import org.eclipse.swt.widgets.Composite;

/**
 * This class creates a delegate class (InteractionManager) in order to manage wizard 
 * pages during the 'New ISAC Project' wizard.
 * @author Fabrice Rivart
 */
public class ProjectWizardPluginPage extends NewTypeWizardPage {

	private InteractionManager interaction;
	private String actionType;
	
	
	/**
	 * Constructor.
	 * @param actionType String representing action type (plugin, object, test, control, sample, timer)
	 */
	public ProjectWizardPluginPage(String actionType) {
		
		super(NewTypeWizardPage.CLASS_TYPE, "");
		
		String title = null;
		String description = null;
		
		if (actionType.equals("plugin")) {
			title = "Plugin properties";
			description = "Complete properties for your ISAC Plugin.";
		}
		else if (actionType.equals("object")) {
			title = "Session Object";
			description = "Add parameters for your ISAC Plugin.";
		}
		else if (actionType.equals("test")) {
			title = "Conditions";
			description = "Add tests for your ISAC Plugin.";
		}
		else if (actionType.equals("control")) {
			title = "Controls";
			description = "Add controls for your ISAC Plugin.";	
				}
		else if (actionType.equals("sample")) {
			title = "Samples";
			description = "Add samples for your ISAC Plugin.";
		}
		else if (actionType.equals("timer")) {
			title = "Timers";
			description = "Add timers for your ISAC Plugin.";
		}
		
        setTitle(title);
        setDescription(description);
        
		this.actionType = actionType;
	}
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createControl(Composite arg0) {
				
		interaction = new InteractionManager(this, null, actionType);
		
		if (actionType.equals("plugin")) {
			setControl(interaction.createPluginContents(arg0));
		}
		else {
			setControl(interaction.createActionContents(arg0));
		}
    }

	
	/**
	 * Apply all modifications into model.
	 */
	public void finish() {
		try {
			interaction.apply();
		}
		catch (Exception e) {
			interaction.catchException(e);
		}
	}

	/**
	 * Cancel all modifications into model.
	 */
	public void cancel() {
		interaction.cancel();
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(IProject project) {
		interaction.setProject(project);
	}
	
	/**
	 * This method initializes the wizard page with model informations.
	 */
	public void initContents() {
		interaction.initContents();
	}
}
