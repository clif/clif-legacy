/*
 * CLIF is a Load Injection Framework Copyright (C) 2006, 2007 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF $Name$
 * 
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.wizards.plugin;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Hashtable;
import java.util.Properties;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.ui.wizards.JavaCapabilityConfigurationPage;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.internal.util.BundleUtility;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;
import org.ow2.clif.scenario.isac.dtd.Files;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.StringHelper;
import org.ow2.clif.util.XMLEntityResolver;
import org.osgi.framework.Bundle;
import org.ow2.clif.scenario.isac.egui.FileName;
import org.ow2.clif.scenario.isac.egui.IsacScenarioPlugin;

/**
 * Class creates a new ISAC Plugin project with appropriate files.
 * There are 4 files: <br>
 * - a java class file which implements SessionObjectAction <br>
 * - a gui xml file which defines all gui parameters <br>
 * - a plugin xml file which defines all plugin parameters <br>
 * - a isac-plugin.properties file which defines plugin project parameters<br>
 * <br>
 * During the wizard, a default project is created, users might modify it or finished the wizard.
 * 
 * @author Fabrice Rivart
 * @author Bruno Dillenseger
 * @author Thomas Escalle
 */
public class ProjectWizard extends Wizard implements IExecutableExtension, INewWizard {

	// project informations
	private IProject project;
	private boolean projectCreated = false;
	
	// Wizard pages
	private WizardNewProjectCreationPage fMainPage;
	private JavaCapabilityConfigurationPage fJavaPage;
	private ProjectWizardPluginPage pluginPage;
	private ProjectWizardPluginPage objectPage;
	private ProjectWizardPluginPage conditionsPage;
	private ProjectWizardPluginPage controlsPage;
	private ProjectWizardPluginPage samplesPage;
	private ProjectWizardPluginPage timersPage;
	
	// Initialization fields
	private IConfigurationElement fConfigElement;
	private IWorkbench fWorkbench;

	
	/**
	 * Constructor.
	 */
	public ProjectWizard() {
		setWindowTitle("New ISAC Plugin Project");
		setHelpAvailable(false);
		setNeedsProgressMonitor(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org.eclipse.core.runtime.IConfigurationElement,
	 *      java.lang.String, java.lang.Object)
	 */
	public void setInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
		// The config element will be used in <code>finishPage</code> to set the
		// result perspective.
		fConfigElement = cfig;
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 *      org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		fWorkbench = workbench;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Wizard#addPages
	 */
	public void addPages() {
		super.addPages();
		
		// Create first page
		fMainPage = new WizardNewProjectCreationPage("NewProjectCreationWizard") {
				// need to override to react to changes on first page
				public void setVisible(boolean visible) {

					getShell().setSize(600, 765);
					super.setVisible(visible);
				}
		};
		
		fMainPage.setTitle("New ISAC Plugin");
		fMainPage.setDescription("Create a new ISAC Plugin project.");

		addPage(fMainPage);

		// Create Java build path configuration page
		fJavaPage = new JavaCapabilityConfigurationPage() {
			// need to override to react to changes on second page
			public void setVisible(boolean visible) {

				updateJavaCapabilityPage();
				super.setVisible(visible);
			}
		};
		addPage(fJavaPage);

		//Create pages
		objectPage = new ProjectWizardPluginPage("object");
		conditionsPage = new ProjectWizardPluginPage("test");
		controlsPage = new ProjectWizardPluginPage("control");
		samplesPage = new ProjectWizardPluginPage("sample");
		timersPage = new ProjectWizardPluginPage("timer");
		pluginPage = new ProjectWizardPluginPage("plugin") {
			// need to override to react to changes on third page
			public void setVisible(boolean visible) {

				updatePluginPropertiesPage();
				super.setVisible(visible);
			}
		};
		
		addPage(pluginPage);
		addPage(objectPage);
		addPage(conditionsPage);
		addPage(controlsPage);
		addPage(samplesPage);
		addPage(timersPage);
	}

	/**
	 * This method initialize the Java capability page wizard before it appears.
	 */
	private void updateJavaCapabilityPage() {
		
		// Create ISAC Plugin project without files
		project = fMainPage.getProjectHandle();
		IPath locationPath = fMainPage.getLocationPath();

		try {
			if (!project.isOpen()) {
				IProjectDescription desc = project.getWorkspace().newProjectDescription(project.getName());
				if (!fMainPage.useDefaults()) {
					desc.setLocation(locationPath);
				}
				project.create(desc, null);
				project.open(null);
				
				IJavaProject jproject = JavaCore.create(project);
				
				if (!jproject.equals(fJavaPage.getJavaProject())) {
			//		IPath corePath = (jproject.getPath().append("lib").append("/clif-core.jar"));
			//		copyFile(ExecutionContext.getBaseDir() + "/lib/clif-core.jar",coreFile.getPath());					
					
					
					// add all classpath entry
					Path path = new Path(ExecutionContext.getBaseDir() + "/lib/clif-core.jar");
					
					IClasspathEntry[] buildPath = {
							JavaCore.newLibraryEntry(path, null, null),
							JavaCore.newSourceEntry(jproject.getPath().append("src")),
							JavaRuntime.getDefaultJREContainerEntry() };
					IPath outputLocation = jproject.getPath().append("bin");
					fJavaPage.init(jproject, outputLocation, buildPath, false);
				}
				
				fJavaPage.configureJavaProject(null);
				
				pluginPage.setProject(project);
				objectPage.setProject(project);
				conditionsPage.setProject(project);
				controlsPage.setProject(project);
				samplesPage.setProject(project);
				timersPage.setProject(project);
			}
		}
		catch (Exception e) {
			catchException(e);
		}
	}
	
	/**
	 * This method initialize the plugin properties page wizard before it appears.
	 */
	private void updatePluginPropertiesPage() {
		
		if (!projectCreated) {
			
			String source = null;
			String pluginName = project.getName();
			String packageName =
				"org.ow2.isac.plugin."
				+ StringHelper.convertJavaIdentifier(pluginName).toLowerCase();
			String className = "SessionObject";
			String guiFileName = "gui.xml";
			String pluginFileName = "plugin.xml";
			boolean dataProvider = false;
			
			try {
				// find first source folder
				IJavaProject jproject = JavaCore.create(fMainPage.getProjectHandle());
				IPackageFragmentRoot [] pfrTab = jproject.getAllPackageFragmentRoots();
				
				for (int i = 0; i < pfrTab.length; i++) {
					if (pfrTab[i].getKind() == IPackageFragmentRoot.K_SOURCE) {
						source = pfrTab[i].getPath().toString();
						break;
					}
				}
				
				//Create files
				createGuiXMLFile(className, guiFileName);
				createPluginXMLFile(pluginName, packageName, className, pluginFileName);
				createPropertiesFile(pluginName, guiFileName, pluginFileName);
				createBuildFile(pluginName, source);
				createJavaFile(pluginName, source, packageName, className, dataProvider);
				
				// init wizard pages
				pluginPage.initContents();
				objectPage.initContents();
				conditionsPage.initContents();
				controlsPage.initContents();
				samplesPage.initContents();
				timersPage.initContents();
				projectCreated = true;
			}
			catch (CoreException e) {
				catchException(e);
			}
		}
	}

	/**
	 * This method create a default gui XML file for the new ISAC Plugin project.
	 * @param className represents the java file name
	 * @param fileName represents the GUI file name
	 * @throws CoreException if there is an error.
	 */
	private void createGuiXMLFile(String className, String fileName) 
			throws CoreException {

		IFile iFile = project.getFile(fileName);
		
		String content = "";
		try {
			FileInputStream buildTemplate = new FileInputStream(
					ExecutionContext.getBaseDir() + 
					File.separator + 
					FileName.PLUGIN_TEMPLATES_DIR +
					FileName.PLUGIN_TEMPLATES_GUI_FILE);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(buildTemplate));
			
			String line = "";
			while ((line = br.readLine()) != null) {
				content += line;
				content += "\n";
			}
			
			content = content.replaceAll("@SET_CLASSPATH_SHEME@", XMLEntityResolver.CLASSPATH_SCHEME);
			content = content.replaceAll("@SET_PLUGIN_DTD@", Files.PLUGIN_DTD);
			content = content.replaceAll("@SET_CLASSNAME@", className);
		} catch (Exception e) {
			catchException(e);
		}		
		
		byte[] bytes = content.toString().getBytes();
	    InputStream source = new ByteArrayInputStream(bytes);
	    iFile.create(source, IResource.NONE, null);
	}
	
	/**
	 * This method create a default plugin XML file for the new ISAC Plugin project.
	 * @param pluginName represents the plugin name
	 * @param packageName represents the package where java file will be create
	 * @param className represents the java file name
	 * @param fileName represents the plugin file name
	 * @throws CoreException if there is an error.
	 */
	private void createPluginXMLFile(	String pluginName,
										String packageName, 
										String className,
										String fileName) throws CoreException {

		IFile iFile = project.getFile(fileName);

		String content = "";
		try {
			FileInputStream buildTemplate = new FileInputStream(
					ExecutionContext.getBaseDir() + 
					File.separator + 
					FileName.PLUGIN_TEMPLATES_DIR + 
					FileName.PLUGIN_TEMPLATES_PLUGIN_FILE);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(buildTemplate));
			
			String line = "";
			while ((line = br.readLine()) != null) {
				content += line;
				content += "\n";
			}
			
			content = content.replaceAll("@SET_PLUGIN_NAME@", pluginName);
			content = content.replaceAll("@SET_CLASSPATH_SHEME@", XMLEntityResolver.CLASSPATH_SCHEME);
			content = content.replaceAll("@SET_PLUGIN_DTD@", Files.PLUGIN_DTD);

			if (packageName.equals("")) {
				content = content.replaceAll("@SET_CLASSNAME@", className);
			}
			else {
				content = content.replaceAll("@SET_CLASSNAME@", packageName + "." + className);
			}		
		
		} catch (Exception e) {
			catchException(e);
		}		
		
		byte[] bytes = content.toString().getBytes();
	    InputStream source = new ByteArrayInputStream(bytes);
	    iFile.create(source, IResource.NONE, null);
	}
	
	/**
	 * This method create the properties file for the new ISAC Plugin project.
	 * @param pluginName represents the ISAC Plugin name
	 * @param guiFileName represents the GUI file name
	 * @param pluginFileName represents the plugin file name
	 * @throws CoreException if there is an error.
	 */
	private void createPropertiesFile(	String pluginName, 
										String guiFileName, 
										String pluginFileName) throws CoreException {

		IFile iFile = project.getFile("isac-plugin.properties");
		String content = "";
		try {
			FileInputStream buildTemplate = new FileInputStream(
					ExecutionContext.getBaseDir() + 
					File.separator + 
					FileName.PLUGIN_TEMPLATES_DIR + 
					FileName.PLUGIN_TEMPLATES_PROPERTIES_FILE);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(buildTemplate));
			
			String line = "";
			while ((line = br.readLine()) != null) {
				content += line;
				content += "\n";
			}
			
			content = content.replaceAll("@SET_PLUGIN_NAME@", pluginName);
		
			byte[] bytes = content.toString().getBytes();
			
		    InputStream source = new ByteArrayInputStream(bytes);
		    iFile.create(source, IResource.NONE, null);
		} catch (Exception e) {
			catchException(e);
		}		
	}
	
	/**
	 * This method create a default java file for the new ISAC Plugin project.
	 * @param pluginName represents the ISAC Plugin name
	 * @param sourceFolder represents the java source folder
	 * @param packageName represents the package where java file will be create
	 * @param className represents the java file name
	 * @param dataProvider True if java class must implements DataProvider interface
	 * @throws CoreException if there is an error.
	 */
	private void createJavaFile(String pluginName, 
								String sourceFolder, 
								String packageName, 
								String className,
								boolean dataProvider) throws CoreException {
		
		IJavaProject jproject = JavaCore.create(project);
		Path sourcePath = new Path(sourceFolder);
		IPackageFragmentRoot pFragmentRoot = jproject.findPackageFragmentRoot(sourcePath);
		IPackageFragment pFragment =  pFragmentRoot.createPackageFragment(packageName, false, null);
		ICompilationUnit cUnit = pFragment.createCompilationUnit(className + ".java", "", false, null);
		
		cUnit.createPackageDeclaration(packageName, null);
		
		String importSession = "org.ow2.clif.scenario.isac.plugin.SessionObjectAction";
		cUnit.createImport(importSession, null, Flags.AccDefault, null);
		cUnit.createImport("java.util.Map", null, Flags.AccDefault, null);
		
		// Create the body class
		String content = "";
		try {
			FileInputStream buildTemplate = new FileInputStream(
					ExecutionContext.getBaseDir() + 
					File.separator + 
					FileName.PLUGIN_TEMPLATES_DIR + 
					FileName.PLUGIN_TEMPLATES_SESSIONOBJECT_FILE);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(buildTemplate));
			
			String line, contenu = "";
			while ((line = br.readLine()) != null) {
				contenu += line;
				contenu += "\n";
			}
			
			content = contenu.replaceAll("@SET_PLUGIN_NAME@", pluginName);
		} catch (Exception e) {
			catchException(e);
		}

		cUnit.createType(content, null, true, null);
	}


	private void createBuildFile(String pluginName, String sourceDir) throws CoreException
	{
		IFile iFile = project.getFile("build.xml");
		String content = "";
		
		try {
			FileInputStream buildTemplate = new FileInputStream(
					ExecutionContext.getBaseDir() + 
					File.separator + 
					FileName.PLUGIN_TEMPLATES_DIR + 
					FileName.PLUGIN_TEMPLATES_BUILD_FILE);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(buildTemplate));
			
			String line, contenu = "";
			while ((line = br.readLine()) != null) {
				contenu += line;
			}
			contenu = contenu.replaceAll("@CLIF_CORE_LIB@", ExecutionContext.getBaseDir() + "lib/clif-core.jar");
			content = contenu.replaceAll("@SET_PLUGIN_NAME@", pluginName);
			byte[] bytes = content.getBytes();
		    InputStream source = new ByteArrayInputStream(bytes);
		    iFile.create(source, IResource.NONE, null);
	    
		} catch (Exception e) {
			catchException(e);
		} 	    
	}


	/**
	 * This method create the ISAC Plugin project with customs parameters and
	 * files used to produce a plugin.
	 * @param monitor IProgressMonitor to show creating progress.
	 */
	private void finishPage(IProgressMonitor monitor)
			throws InterruptedException, CoreException {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}
		try {
			
			pluginPage.finish();
			
			// change to the perspective specified
			BasicNewProjectResourceWizard.updatePerspective(fConfigElement);
			BasicNewResourceWizard.selectAndReveal(project, fWorkbench.getActiveWorkbenchWindow());
			
		}
		catch (Exception e) {
			catchException(e);
		}
		finally {
			monitor.done();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see Wizard#performFinish
	 */
	@Override
	public boolean performFinish() {
		WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
			protected void execute(IProgressMonitor monitor)
					throws CoreException, InvocationTargetException,
					InterruptedException {
				finishPage(monitor);
			}
		};
		try {
			getContainer().run(false, true, op);
		}
		catch (InvocationTargetException e) {
			e.printStackTrace();
			performCancel();
			return false;
		}
		catch (InterruptedException e) {
			e.printStackTrace();
			performCancel();
			return false; // canceled
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#performCancel()
	 */
	@Override
	public boolean performCancel() {
		
		try {
			if (fMainPage.isPageComplete() && fMainPage.getProjectHandle().isOpen()) {
				fMainPage.getProjectHandle().delete(true, true, null);
				
				if (projectCreated) {
					pluginPage.cancel();
				}
			}
		}
		catch (CoreException e) {
			e.printStackTrace();
			return false;
		}
		return super.performCancel();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#canFinish()
	 */
	@Override
	public boolean canFinish() {
		
		return projectCreated;
	}
	
	/**
	 * This method open a message error dialog to user and block plugin activity
	 * in setting valide page to false.
	 * @param e Exception to catch
	 */
	private void catchException(Exception e) {
		
		Status status = new Status(Status.ERROR, 
									IsacScenarioPlugin.PLUGIN_ID, 
									Status.OK, 
									"Error during manipulation.",
									e);
		
		ErrorDialog diag = new ErrorDialog(getShell(), 
											"Plugin error", 
											"Problem detects", 
											status, 
											IStatus.ERROR);	
		diag.open();
		e.printStackTrace();
		performCancel();
		getShell().close();
	}
	
	
	private void copyFile(String inFilePath,String outFilePath) {
	
		FileChannel in = null; // canal d'entrée
		FileChannel out = null; // canal de sortie
		 
		try {
			// Init
		    in = new FileInputStream(inFilePath).getChannel();
			out = new FileOutputStream(outFilePath).getChannel();
		 
		    // Copie depuis le in vers le out
		    in.transferTo(0, in.size(), out);
		} catch (Exception e) {
		    e.printStackTrace(); // n'importe quelle exception
		} finally { // finalement on ferme
		    if(in != null) {
		  	  try {
			    in.close();
			  } catch (IOException e) {}
		    }
		    if(out != null) {
		  	  try {
			    out.close();
			  } catch (IOException e) {}
		    }
		}
	}
	
	
}