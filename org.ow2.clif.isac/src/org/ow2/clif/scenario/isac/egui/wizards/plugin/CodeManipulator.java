/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2006, 2010 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.wizards.plugin;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.ParameterizedType;
import org.eclipse.jdt.core.dom.PrimitiveType;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;
import org.eclipse.jdt.ui.refactoring.RenameSupport;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.text.Document;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.text.edits.TextEdit;


/**
 * This class permits to manipulate Java code with the Eclipse JDT API in order to construct a valid 
 * Java class for ISAC plugin.
 * A AST Tree is loaded in memory and modified at each user action.
 * @author Fabrice Rivart
 */
public class CodeManipulator {
	
	private ASTParser parser;
	private CompilationUnit astRoot;
	private Document document;
	private ICompilationUnit cu;
	private String pluginName;
	private RenameSupport renameRefactoring;
	private Shell s;
	private CountVisitor visitor;
	
	/**
	 * Constructor.
	 */
	public CodeManipulator() {
		parser = ASTParser.newParser(AST.JLS3);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		visitor = new CountVisitor();
	}
	
	/**
	 * This method loads information and contructs a AST Tree.
	 * @param arg0 ICompilationUnit representing java class
	 * @param arg1 String representing plugin name
	 * @param arg2 Shell representing the parent shell
	 */
	public void load(ICompilationUnit arg0, String arg1, Shell arg2) throws Exception {
		
		cu = arg0;
		pluginName = arg1;
		s = arg2;
		
		// creation of document
		String source = cu.getBuffer().getContents();
		document= new Document(source);
		
		// creation of AST
		parser.setSource(cu);
		astRoot = (CompilationUnit) parser.createAST(null);
		
		// start record of the modifications
		astRoot.recordModifications();
	}

	/**
	 * This method resets memory.
	 */
	public void reset() {
		astRoot = null;
		visitor = null;
	}
	
	/**
	 * This method updates plugin name in class (Javadoc, exception, ...).
	 * Plugin name must have this syntax ~name~ to be replaced.
	 * @param arg0 String representing new plugin name
	 */
	public void updatePluginName(String arg0) throws Exception {
		
		// computation of the text edits
		TextEdit edits = astRoot.rewrite(document, cu.getJavaProject().getOptions(true));

		// computation of the new source code
		edits.apply(document);
		String source = document.get();
		
		// update plugin name
		String newSource = source.replaceAll("~" + pluginName + "~", "~" + arg0 + "~");
		
		// creation of AST
		document= new Document(newSource);
		parser.setSource(newSource.toCharArray());
		astRoot = (CompilationUnit) parser.createAST(null);
		
		// start record of the modifications
		astRoot.recordModifications();
	}
	
	/**
	 * This method updates package name declaration.
	 * @param packageName String representing new package name declaration
	 */
	public void updatePackage(String packageName) throws Exception {
		astRoot.getPackage().setName(astRoot.getAST().newName(packageName));
		
	}
	
	/**
	 * This method updates class name by doing a rename refactoring.
	 * @param className String representing new class name
	 */
	public void updateClassName(String className) throws Exception {		
		
		// do refactoring into project on all references and textual matches
		renameRefactoring = RenameSupport.create(cu, className, 
				RenameSupport.UPDATE_REFERENCES);
		renameRefactoring.perform(s, new ProgressMonitorDialog(s));
	}
	
	/**
	 * This method adds or removes informations about DataProvider interface.
	 * @param provider true if class must implements DataProvider interface
	 */
	public void setDataProvider(boolean provider) throws Exception {
		
		if (provider) {
			addImport("org.ow2.clif.scenario.isac.plugin.DataProvider");
			addImport("org.ow2.clif.scenario.isac.exception.IsacRuntimeException");
			addInterfaceType("provider");
			addMethod("provider");
		}
		else {
			removeImport("org.ow2.clif.scenario.isac.plugin.DataProvider");
			removeInterfaceType("provider");
			commentMethod("provider");
		}
	}
			
	
	/**
	 * This method adds action in java code (interface type + field + method + switch case)
	 * @param action String representing action (sample, timer, test, control, provider)
	 * @param name String representing action name
	 * @param value int representing action value
	 */
	public void addAction(String action, String name, int value) throws Exception {
		if (action.equals("sample")) {
			addImport("java.util.Map");
			addImport("org.ow2.clif.storage.api.ActionEvent");
			addImport("org.ow2.clif.scenario.isac.plugin.SampleAction");
		}
		else if (action.equals("timer")) {
			addImport("java.util.Map");
			addImport("org.ow2.clif.scenario.isac.plugin.TimerAction");
		}
		else if (action.equals("test")) {
			addImport("java.util.Map");
			addImport("org.ow2.clif.scenario.isac.plugin.TestAction");
		}
		else if (action.equals("control")) {
			addImport("java.util.Map");
			addImport("org.ow2.clif.scenario.isac.plugin.ControlAction");
		}
		
		addImport("org.ow2.clif.scenario.isac.exception.IsacRuntimeException");
		
		// replace all non valid Java identifier character
		String javaName = convertJavaIdentifier(name);
		
		// add implement type
		addInterfaceType(action);
		
		// add field
		addFieldAction(action, javaName, value);
		
		// add method
		addMethod(action);
		
		// add switch case
		addSwitchCase(action, javaName);
		
	}
	
	/**
	 * This method removes action in java code if user does not use the field.
	 * If field is used, it will be renamed by refactoring with new name 'OLDi_', 
	 * i is an incremental number, and all switch case will be commented.
	 * If field is not used, field will be deleted and all relation too
	 * (interface type, method, import)
	 * @param action String representing action (sample, timer, test, control)
	 * @param name String representing action name
	 * @param params String [] representing all action parameters name
	 */
	public void removeAction(String action, String name, String [] params) throws Exception {
		
		// replace all non valid Java identifier character
		String javaName = convertJavaIdentifier(name);
	
		String fieldName = null;
		
		if (action.equals("sample")) {
			fieldName = "SAMPLE_" + javaName.toUpperCase();
		}
		else if (action.equals("timer")) {
			fieldName = "TIMER_" + javaName.toUpperCase();
		}
		else if (action.equals("test")) {
			fieldName = "TEST_" + javaName.toUpperCase();
		}
		else if (action.equals("control")) {
			fieldName = "CONTROL_" + javaName.toUpperCase();
		}
		
		// remove switch cases
		removeSwitchCase(action, javaName);
		
		// search how many references there are in Java Code
		// if there are more than one occurence, field is used by user.
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		visitor.init(action, fieldName);
		td.accept(visitor);
		
		if (visitor.getFieldNumber() > 1) {
			applyAST();
			
			// find old field in compilation unit
			IField field = cu.getTypes()[0].getField(fieldName);
			
			String newJavaName = "OLD" + (getMaxOldField() + 1) + "_" + fieldName;
			
			// do refactoring into project on all references and textual matches
			renameRefactoring = RenameSupport.create(field, newJavaName, 
					RenameSupport.UPDATE_REFERENCES | RenameSupport.UPDATE_TEXTUAL_MATCHES);
			renameRefactoring.perform(s, new ProgressMonitorDialog(s));
			cu.makeConsistent(null);
			cu.save(null, true);
			
			reloadAST();
			
			// comment switch case
			commentSwitchCase(action, newJavaName);
			applyAST();
			reloadAST();
		}
		else {
			// remove field
			removeFieldAction(action, javaName);
			
			// remove import + interface type + method if field was the last
			visitor.init(action, fieldName);
			td.accept(visitor);
			if (visitor.getActionNumber() == 0) {
				
				if (action.equals("sample")) {
					removeImport("org.ow2.clif.storage.api.ActionEvent");
					removeImport("org.ow2.clif.scenario.isac.plugin.SampleAction");
				}
				else if (action.equals("timer")) {
					removeImport("org.ow2.clif.scenario.isac.plugin.TimerAction");
				}
				else if (action.equals("test")) {
					removeImport("org.ow2.clif.scenario.isac.plugin.TestAction");
				}
				else if (action.equals("control")) {
					removeImport("org.ow2.clif.scenario.isac.plugin.ControlAction");
				}
				
				removeInterfaceType(action);
				commentMethod(action);
				applyAST();
				reloadAST();
			}
		}
		
		// remove all parameters
		for (int i = 0; i < params.length; i++) {
			removeActionParameter(action, name, params[i]);
		}
	}
	
	/**
	 * This method updates action and action parameters in project by doing a rename refactoring.
	 * @param action String representing action (sample, timer, test, control)
	 * @param oldName String representing old action name
	 * @param newName String representing new action name
	 * @param params String [] representing all action parameters name
	 */
	public void updateAction(String action, 
							 String oldName, 
							 String newName,
							 String [] params) throws Exception {
		
		String oldFieldName = null;
		String newFieldName = null;
		
		if (action.equals("sample")) {
			oldFieldName = "SAMPLE_" + oldName.toUpperCase();
			newFieldName = "SAMPLE_" + newName.toUpperCase();
		}
		else if (action.equals("timer")) {
			oldFieldName = "TIMER_" + oldName.toUpperCase();
			newFieldName = "TIMER_" + newName.toUpperCase();
		}
		else if (action.equals("test")) {
			oldFieldName = "TEST_" + oldName.toUpperCase();
			newFieldName = "TEST_" + newName.toUpperCase();
		}
		else if (action.equals("control")) {
			oldFieldName = "CONTROL_" + oldName.toUpperCase();
			newFieldName = "CONTROL_" + newName.toUpperCase();
		}
		
		//replace all non valid Java identifier character
		String oldJavaName = convertJavaIdentifier(oldFieldName);
		String newJavaName = convertJavaIdentifier(newFieldName);
		
		applyAST();
		
		// find old field in compilation unit
		IField field = cu.getTypes()[0].getField(oldJavaName);
		
		// do refactoring into project on all references and textual matches
		renameRefactoring = RenameSupport.create(field, newJavaName, 
				RenameSupport.UPDATE_REFERENCES | RenameSupport.UPDATE_TEXTUAL_MATCHES);
		renameRefactoring.perform(s, new ProgressMonitorDialog(s));
		cu.makeConsistent(null);
		cu.save(null, true);
		
		// do refactoring into project on all action parameters
		for (int i = 0; i < params.length; i++) {
			String oldParamName = convertJavaIdentifier(oldFieldName + "_" + params[i].toUpperCase());
			String newParamName = convertJavaIdentifier(newFieldName + "_" + params[i].toUpperCase());
			field = cu.getTypes()[0].getField(oldParamName);
			renameRefactoring = RenameSupport.create(field, newParamName, 
					RenameSupport.UPDATE_REFERENCES | RenameSupport.UPDATE_TEXTUAL_MATCHES);
			renameRefactoring.perform(s, new ProgressMonitorDialog(s));
			cu.makeConsistent(null);
			cu.save(null, true);
		}
		
		reloadAST();
		
		
	}
	
	/**
	 * This method adds action parameter in java code (field)
	 * (SAMPLE_ACTION_, TIMER_ACTION_, TEST_ACTION_, CONTROL_ACTION_) 
	 * @param action String representing action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param parameterName String representing parameter name
	 */
	public void addActionParameter(String action, String actionName, String parameterName) {
		
		boolean exist = false;
		String fieldActionName = null;
		String fieldParameterName = null;
		
		if (action.equals("sample")) {
			fieldActionName = "SAMPLE_" + actionName.toUpperCase();
		}
		else if (action.equals("timer")) {
			fieldActionName = "TIMER_" + actionName.toUpperCase();
		}
		else if (action.equals("test")) {
			fieldActionName = "TEST_" + actionName.toUpperCase();
		}
		else if (action.equals("control")) {
			fieldActionName = "CONTROL_" + actionName.toUpperCase();
		}
		else if (action.equals("object")) {
			fieldActionName = "PLUGIN";
		}
		
		fieldParameterName = fieldActionName + "_" + parameterName.toUpperCase();
		
		//replace all non valid Java identifier character
		String JavaName = convertJavaIdentifier(fieldParameterName);
		
		// create orphelin FieldDeclaration node
		// create SAMPLE_ACTION_NAME = value
		VariableDeclarationFragment vd = astRoot.getAST().newVariableDeclarationFragment();
		vd.setName(astRoot.getAST().newSimpleName(JavaName));
		StringLiteral sl = astRoot.getAST().newStringLiteral();
		sl.setLiteralValue(parameterName);
		vd.setInitializer(sl);
		//create String SAMPLE_ACTION_NAME = value
		FieldDeclaration fd = astRoot.getAST().newFieldDeclaration(vd);
		SimpleType pt = astRoot.getAST().newSimpleType(astRoot.getAST().newName("String"));
		fd.setType(pt);
		//create static final String SAMPLE_ACTION_NAME = value
		List modifiers = fd.modifiers();
		modifiers.add(astRoot.getAST().newModifier(Modifier.ModifierKeyword.STATIC_KEYWORD));
		modifiers.add(astRoot.getAST().newModifier(Modifier.ModifierKeyword.FINAL_KEYWORD));
		
		//verify if filed already exists
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List bodies = td.bodyDeclarations();
		Iterator iter = bodies.iterator();
		int index = -1;
		while (iter.hasNext()) {
			BodyDeclaration bd = (BodyDeclaration) iter.next();
			
			if (bd instanceof FieldDeclaration) {
				FieldDeclaration element = (FieldDeclaration) bd;
				VariableDeclarationFragment vdElement = 
					(VariableDeclarationFragment) element.fragments().get(0);
				
				String qualifiedName = vdElement.getName().getFullyQualifiedName();
				if (qualifiedName.equals(fieldActionName)) {
					index = bodies.indexOf(bd);
				}
				if (qualifiedName.equals(JavaName)) {
					exist = true;
				}
			}
		}
		
		if(!exist) {
			bodies.add(index + 1, fd);
		}
	}
	
	/**
	 * This method removes action parameter in java code (field)
	 * (SAMPLE_ACTION_, TIMER_ACTION_, TEST_ACTION_, CONTROL_ACTION_) 
	 * @param action String representing action (sample, timer, test, control, provider)
	 * @param actionName String representing action name
	 * @param parameterName String representing parameter name
	 */
	public void removeActionParameter(String action, 
									  String actionName, 
									  String parameterName) throws Exception {
		
		String fieldActionName = null;
		String fieldParameterName = null;
		
		if (action.equals("sample")) {
			fieldActionName = "SAMPLE_" + actionName.toUpperCase();
		}
		else if (action.equals("timer")) {
			fieldActionName = "TIMER_" + actionName.toUpperCase();
		}
		else if (action.equals("test")) {
			fieldActionName = "TEST_" + actionName.toUpperCase();
		}
		else if (action.equals("control")) {
			fieldActionName = "CONTROL_" + actionName.toUpperCase();
		}
		else if (action.equals("object")) {
			fieldActionName = "PLUGIN";
		}
		
		fieldParameterName = fieldActionName + "_" + parameterName.toUpperCase();
		
		//replace all non valid Java identifier character
		String JavaName = convertJavaIdentifier(fieldParameterName);
				
		// search how many references there are in Java Code
		// if there are more than one occurence, field is used by user.
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		visitor.init(action, JavaName);
		td.accept(visitor);

		if (visitor.getFieldNumber() > 1) {
			
			applyAST();
			
			// find old field in compilation unit
			IField field = cu.getTypes()[0].getField(JavaName);
			
			String newJavaName = "OLD" + (getMaxOldField() + 1) + "_" + JavaName;
			
			// do refactoring into project on all references and textual matches
			renameRefactoring = RenameSupport.create(field, newJavaName, 
					RenameSupport.UPDATE_REFERENCES | RenameSupport.UPDATE_TEXTUAL_MATCHES);
			renameRefactoring.perform(s, new ProgressMonitorDialog(s));
			cu.makeConsistent(null);
			cu.save(null, true);
			
			reloadAST();
		}
		else {
			//verify if filed already exists
			List bodies = td.bodyDeclarations();
			Iterator iter = bodies.iterator();
			while (iter.hasNext()) {
				BodyDeclaration bd = (BodyDeclaration) iter.next();
				
				if (bd instanceof FieldDeclaration) {
					FieldDeclaration element = (FieldDeclaration) bd;
					VariableDeclarationFragment vdElement = 
						(VariableDeclarationFragment) element.fragments().get(0);
					
					String qualifiedName = vdElement.getName().getFullyQualifiedName();
					if (qualifiedName.equals(JavaName)) {
						iter.remove();
					}
				}
			}
		}
	}

	/**
	 * This method updates action parameter in java code by doing rename refactoring
	 * @param action String representing action (sample, timer, test, control, object)
	 * @param actionName String representing action name
	 * @param oldParameterName String representing old parameter name
	 * @param newParameterName String representing new parameter name
	 */
	public void updateActionParameter(String action, String actionName, String oldParameterName, 
											String newParameterName) throws Exception{
		
		String oldFieldParameterName = null;
		String newFieldParameterName = null;
		
		String fieldActionName = null;
		
		if (action.equals("sample")) {
			fieldActionName = "SAMPLE_" + actionName.toUpperCase();
		}
		else if (action.equals("timer")) {
			fieldActionName = "TIMER_" + actionName.toUpperCase();
		}
		else if (action.equals("test")) {
			fieldActionName = "TEST_" + actionName.toUpperCase();
		}
		else if (action.equals("control")) {
			fieldActionName = "CONTROL_" + actionName.toUpperCase();
		}
		else if (action.equals("object")) {
			fieldActionName = "PLUGIN";
		}
		
		oldFieldParameterName = fieldActionName + "_" + oldParameterName.toUpperCase();
		newFieldParameterName = fieldActionName + "_" + newParameterName.toUpperCase();
		
		//replace all non valid Java identifier character
		String oldJavaName = convertJavaIdentifier(oldFieldParameterName);
		String newJavaName = convertJavaIdentifier(newFieldParameterName);
		
		//update parameter value
		updateActionParameterValue(oldJavaName, newParameterName);
		
		applyAST();
		
		// find old field in compilation unit
		IField field = cu.getTypes()[0].getField(oldJavaName);
		
		// do refactoring into project on all references and textual matches
		renameRefactoring = RenameSupport.create(field, newJavaName, 
				RenameSupport.UPDATE_REFERENCES | RenameSupport.UPDATE_TEXTUAL_MATCHES);
		renameRefactoring.perform(s, new ProgressMonitorDialog(s));
		cu.makeConsistent(null);
		cu.save(null, true);
		
		reloadAST();
	}
	
	/**
	 * This method updates parameter value in java code (field updated)
	 * @param parameter String representing parameter name
	 * @param value String representing parameter value
	 */
	private void updateActionParameterValue(String parameter, String value) {
		
		//find field element
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List bodies = td.bodyDeclarations();
		Iterator iter = bodies.iterator();
		while (iter.hasNext()) {
			BodyDeclaration bd = (BodyDeclaration) iter.next();
			
			if (bd instanceof FieldDeclaration) {
				FieldDeclaration element = (FieldDeclaration) bd;
				VariableDeclarationFragment vdElement = 
					(VariableDeclarationFragment) element.fragments().get(0);
				
				String qualifiedName = vdElement.getName().getFullyQualifiedName();
				if (qualifiedName.equals(parameter)) {
					StringLiteral sl = astRoot.getAST().newStringLiteral();
					sl.setLiteralValue(value);
					vdElement.setInitializer(sl);
				}
			}
		}
	}
	
	/**
	 * Apply all modifications from AST to compilation unit.
	 */
	private void applyAST() throws Exception {
		
		// computation of the text edits
		TextEdit edits = astRoot.rewrite(document, cu.getJavaProject().getOptions(true));

		// computation of the new source code
		edits.apply(document);
		String newSource = document.get();
		cu.getBuffer().setContents(newSource);
		cu.makeConsistent(null);
		cu.save(null, true);
	}
	
	/**
	 * Load AST from compilation unit.
	 */
	private void reloadAST() throws Exception {
		
		// creation of document
		String source = cu.getBuffer().getContents();
		document= new Document(source);
		// creation of AST
		parser.setSource(cu);
		astRoot = (CompilationUnit) parser.createAST(null);
		
		// start record of the modifications
		astRoot.recordModifications();
	}
	
	/**
	 * This method adds import
	 * @param importName String representing import name (package.classname)
	 */
	private void addImport(String importName) {
		
		boolean exist = false;
		
		// create unparented import node
		ImportDeclaration id = astRoot.getAST().newImportDeclaration();
		id.setOnDemand(false);
		id.setName(astRoot.getAST().newName(importName));
		
		
		//verify if import already exists
		List imports = astRoot.imports();
		Iterator iter = imports.iterator();
		while (iter.hasNext()) {
			ImportDeclaration element = (ImportDeclaration) iter.next();
			String qualifiedName = element.getName().getFullyQualifiedName();
			
			if (qualifiedName.equals(importName)) {
				exist = true;
			}
		}
		
		if(!exist) {
			imports.add(id);
		}
		
	}
	
	/**
	 * This method removes import importName if exists
	 * @param importName String representing import to remove
	 */
	private void removeImport(String importName) {
				
		//remove import if exists
		List imports = astRoot.imports();
		Iterator iter = imports.iterator();
		while (iter.hasNext()) {
			ImportDeclaration element = (ImportDeclaration) iter.next();
			String qualifiedName = element.getName().getFullyQualifiedName();
			if (qualifiedName.equals(importName)) {
				iter.remove();
			}
		}
	}
	
	/**
	 * This method adds interface type corresponding to action 
	 * (sample, timer, test, control, provider)
	 * @param action String representing action (sample, timer, test, control, provider)
	 */
	private void addInterfaceType(String action) {
		
		boolean exist = false;
		String interfaceName = null;
		
		if (action.equals("sample")) {
			interfaceName = "SampleAction";
		}
		else if (action.equals("timer")) {
			interfaceName = "TimerAction";
		}
		else if (action.equals("test")) {
			interfaceName = "TestAction";
		}
		else if (action.equals("control")) {
			interfaceName = "ControlAction";
		}
		else if (action.equals("provider")) {
			interfaceName = "DataProvider";
		}
		
		// create orphelin interface type node
		SimpleType st = astRoot.getAST().newSimpleType(astRoot.getAST().newName(interfaceName));		
		
		//verify if interface type already exists
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List interfaces = td.superInterfaceTypes();
		Iterator iter = interfaces.iterator();
		while (iter.hasNext()) {
			SimpleType type = (SimpleType) iter.next();
			String qualifiedName = type.getName().getFullyQualifiedName();
			
			if (qualifiedName.equals(interfaceName)) {
				exist = true;
			}
		}
		
		if(!exist) {
			interfaces.add(st);
		}
	}
	
	/**
	 * This method removes interface type corresponding to action 
	 * (sample, timer, test, control, provider)
	 * @param action String representing action (sample, timer, test, control, provider)
	 */
	private void removeInterfaceType(String action) {
		
		String interfaceName = null;
		
		if (action.equals("sample")) {
			interfaceName = "SampleAction";
		}
		else if (action.equals("timer")) {
			interfaceName = "TimerAction";
		}
		else if (action.equals("test")) {
			interfaceName = "TestAction";
		}
		else if (action.equals("control")) {
			interfaceName = "ControlAction";
		}
		else if (action.equals("provider")) {
			interfaceName = "DataProvider";
		}
		
		//remove interface type if exists
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List interfaces = td.superInterfaceTypes();
		Iterator iter = interfaces.iterator();

		while (iter.hasNext()) {
			SimpleType type = (SimpleType) iter.next();
			String qualifiedName = type.getName().getFullyQualifiedName();
			
			if (qualifiedName.equals(interfaceName)) {
				iter.remove();
			}
		}
	}
	
	/**
	 * This method adds action field (SAMPLE_, TIMER_, TEST_, CONTROL_) 
	 * @param action String representing action (sample, timer, test, control, provider)
	 * @param name String representing action name
	 * @param value int representing action value
	 */
	private void addFieldAction(String action, String name, int value) {
		
		boolean exist = false;
		String fieldName = null;
		
		if (action.equals("sample")) {
			fieldName = "SAMPLE_" + name.toUpperCase();
		}
		else if (action.equals("timer")) {
			fieldName = "TIMER_" + name.toUpperCase();
		}
		else if (action.equals("test")) {
			fieldName = "TEST_" + name.toUpperCase();
		}
		else if (action.equals("control")) {
			fieldName = "CONTROL_" + name.toUpperCase();
		}
		
		// create unparented FieldDeclaration node
		// create SAMPLE_NAME = value
		VariableDeclarationFragment vd = astRoot.getAST().newVariableDeclarationFragment();
		vd.setName(astRoot.getAST().newSimpleName(fieldName));
		NumberLiteral nl = astRoot.getAST().newNumberLiteral();
		nl.setToken(String.valueOf(value));
		vd.setInitializer(nl);
		//create int SAMPLE_NAME = value
		FieldDeclaration fd = astRoot.getAST().newFieldDeclaration(vd);
		PrimitiveType pt = astRoot.getAST().newPrimitiveType(PrimitiveType.INT);
		fd.setType(pt);
		//create static final int SAMPLE_NAME = value
		List modifiers = fd.modifiers();
		modifiers.add(astRoot.getAST().newModifier(Modifier.ModifierKeyword.STATIC_KEYWORD));
		modifiers.add(astRoot.getAST().newModifier(Modifier.ModifierKeyword.FINAL_KEYWORD));
		
		//verify if field type already exists
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List bodies = td.bodyDeclarations();
		Iterator iter = bodies.iterator();
		int index = -1;
		CharSequence cs = action.toUpperCase().subSequence(0, action.length());
		while (iter.hasNext()) {
			BodyDeclaration bd = (BodyDeclaration) iter.next();
			
			if (bd instanceof FieldDeclaration) {
				FieldDeclaration element = (FieldDeclaration) bd;
				VariableDeclarationFragment vdElement = 
					(VariableDeclarationFragment) element.fragments().get(0);
				
				String qualifiedName = vdElement.getName().getFullyQualifiedName();
				if (qualifiedName.contains(cs)) {
					index = bodies.indexOf(element);
				}
				if (qualifiedName.equals(fieldName)) {
					exist = true;
				}
			}
		}
		
		if(!exist) {
			bodies.add(index + 1, fd);
		}
	}
	
	/**
	 * This method removes action field (SAMPLE_, TIMER_, TEST_, CONTROL_) 
	 * @param action String representing action (sample, timer, test, control, provider)
	 * @param name String representing action name
	 */
	private void removeFieldAction(String action, String name) {
		
		String fieldName = null;
		
		if (action.equals("sample")) {
			fieldName = "SAMPLE_" + name.toUpperCase();
		}
		else if (action.equals("timer")) {
			fieldName = "TIMER_" + name.toUpperCase();
		}
		else if (action.equals("test")) {
			fieldName = "TEST_" + name.toUpperCase();
		}
		else if (action.equals("control")) {
			fieldName = "CONTROL_" + name.toUpperCase();
		}
		
		//verify if field type already exists
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List bodies = td.bodyDeclarations();
		Iterator iter = bodies.iterator();
		while (iter.hasNext()) {
			BodyDeclaration bd = (BodyDeclaration) iter.next();
			
			if (bd instanceof FieldDeclaration) {
				FieldDeclaration element = (FieldDeclaration) bd;
				VariableDeclarationFragment vdElement = 
					(VariableDeclarationFragment) element.fragments().get(0);
				
				String qualifiedName = vdElement.getName().getFullyQualifiedName();
				if (qualifiedName.equals(fieldName)) {
					iter.remove();
				}
			}
		}
	}
	
	/**
	 * This method adds action method which must override.
	 * @param action String representing action (sample, timer, test, control, provider)
	 */
	private void addMethod(String action) throws Exception{
		
		// create unparented MethodDeclaration node
		MethodDeclaration md = astRoot.getAST().newMethodDeclaration();
		md.setConstructor(false);
				
		// create modifier 'public'
		md.modifiers().add(astRoot.getAST().newModifier(Modifier.ModifierKeyword.PUBLIC_KEYWORD));
		
		// create argument0 : int number 
		SingleVariableDeclaration arg0 = astRoot.getAST().newSingleVariableDeclaration();
		arg0.setType(astRoot.getAST().newPrimitiveType(PrimitiveType.INT));
		arg0.setName(astRoot.getAST().newSimpleName("number"));
		
		// create argument1 : Map<String,String> params
		SingleVariableDeclaration arg1 = astRoot.getAST().newSingleVariableDeclaration();
		ParameterizedType mapStringString = astRoot.getAST().newParameterizedType(
			astRoot.getAST().newSimpleType(astRoot.getAST().newName("Map")));
		mapStringString.typeArguments().add(
			astRoot.getAST().newSimpleType(astRoot.getAST().newName("String")));
		mapStringString.typeArguments().add(
			astRoot.getAST().newSimpleType(astRoot.getAST().newName("String")));
		arg1.setType(mapStringString);
		arg1.setName(astRoot.getAST().newSimpleName("params"));

		// create argument2 : ActionEvent report
		SingleVariableDeclaration arg2 = astRoot.getAST().newSingleVariableDeclaration();
		arg2.setType(astRoot.getAST().newSimpleType(astRoot.getAST().newName("ActionEvent")));
		arg2.setName(astRoot.getAST().newSimpleName("report"));
		
		// create argument3 : String var
		SingleVariableDeclaration arg3 = astRoot.getAST().newSingleVariableDeclaration();
		arg3.setType(astRoot.getAST().newSimpleType(astRoot.getAST().newName("String")));
		arg3.setName(astRoot.getAST().newSimpleName("var"));
		
		// create return type method, method name and paramaters
		Type type = null;
		SimpleName name = null;
		String javadoc = null;
		StringBuffer comment = new StringBuffer();
		List parameters = md.parameters();
		if (action.equals("sample")) {
			type = astRoot.getAST().newSimpleType(astRoot.getAST().newName("ActionEvent"));
			name = astRoot.getAST().newSimpleName("doSample");
			javadoc = "org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()";
			comment.append("\n/////////////////////////////////\n");
			comment.append("// SampleAction implementation //\n");
			comment.append("/////////////////////////////////");
			parameters.add(arg0);
			parameters.add(arg1);
			parameters.add(arg2);
		}
		else if (action.equals("timer")) {
			type  = astRoot.getAST().newPrimitiveType(PrimitiveType.LONG);
			name = astRoot.getAST().newSimpleName("doTimer");
			javadoc = "org.ow2.clif.scenario.isac.plugin.TimerAction#doTimer()";
			comment.append("\n////////////////////////////////\n");
			comment.append("// TimerAction implementation //\n");
			comment.append("////////////////////////////////");
			parameters.add(arg0);
			parameters.add(arg1);
		}
		else if (action.equals("test")) {
			type = astRoot.getAST().newPrimitiveType(PrimitiveType.BOOLEAN);
			name = astRoot.getAST().newSimpleName("doTest");
			javadoc = "org.ow2.clif.scenario.isac.plugin.TestAction#doTest()";
			comment.append("\n///////////////////////////////\n");
			comment.append("// TestAction implementation //\n");
			comment.append("///////////////////////////////");
			parameters.add(arg0);
			parameters.add(arg1);
		}
		else if (action.equals("control")) {
			type = astRoot.getAST().newPrimitiveType(PrimitiveType.VOID);
			name = astRoot.getAST().newSimpleName("doControl");
			javadoc = "org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()";
			comment.append("\n//////////////////////////////////\n");
			comment.append("// ControlAction implementation //\n");
			comment.append("//////////////////////////////////");
			parameters.add(arg0);
			parameters.add(arg1);
		}
		else if (action.equals("provider")) {
			type = astRoot.getAST().newSimpleType(astRoot.getAST().newName("String"));
			name = astRoot.getAST().newSimpleName("doGet");
			javadoc = "org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()";
			comment.append("\n/////////////////////////////////\n");
			comment.append("// DataProvider implementation //\n");
			comment.append("/////////////////////////////////");
			parameters.add(arg3);
		}
		md.setReturnType2(type);
		md.setName(name);
		
		// create javadoc statement
		Javadoc doc = astRoot.getAST().newJavadoc();
		TagElement tag = astRoot.getAST().newTagElement();
		tag.setTagName("@see");
		TextElement text = astRoot.getAST().newTextElement();
		text.setText(javadoc);
		tag.fragments().add(text);
		doc.tags().add(tag);
		md.setJavadoc(doc);
		
		// create method body
		Block block = astRoot.getAST().newBlock();
		// switch block
		SwitchStatement st = astRoot.getAST().newSwitchStatement();		
		st.setExpression(astRoot.getAST().newName("number"));
		
		// default statement
		SwitchCase sc = astRoot.getAST().newSwitchCase();
		sc.setExpression(null);
		
		// exception in default statement
		ThrowStatement ts = astRoot.getAST().newThrowStatement();
		ClassInstanceCreation cicError = astRoot.getAST().newClassInstanceCreation();
		cicError.setType(astRoot.getAST().newSimpleType(astRoot.getAST().newName("Error")));
		StringLiteral slErrorDefault = astRoot.getAST().newStringLiteral();
		String errorDefault = "Unable to find this " + action + " in ~" + pluginName + "~ ISAC plugin: ";
		slErrorDefault.setLiteralValue(errorDefault);
		
		InfixExpression ieDefault = astRoot.getAST().newInfixExpression();
		ieDefault.setOperator(InfixExpression.Operator.PLUS);
		ieDefault.setLeftOperand(slErrorDefault);
		ieDefault.setRightOperand(astRoot.getAST().newName("number"));
		cicError.arguments().add(ieDefault);
		ts.setExpression(cicError);
		st.statements().add(sc);
		st.statements().add(ts);
		
		// exception at end
		ThrowStatement ts2 = astRoot.getAST().newThrowStatement();
		ClassInstanceCreation cicEnd = astRoot.getAST().newClassInstanceCreation();
		cicEnd.setType(astRoot.getAST().newSimpleType(astRoot.getAST().newName("IsacRuntimeException")));
		StringLiteral slErrorEnd = astRoot.getAST().newStringLiteral();
		String errorEnd;
		
		if (!action.equals("provider")) {
			errorEnd = "No action defined for this " + action + " in ~" + pluginName + "~ ISAC plugin: ";
		}
		else {
			errorEnd = "Unknown parameter value in ~" + pluginName + "~ ISAC plugin: ";
		}
		
		slErrorEnd.setLiteralValue(errorEnd);
		
		InfixExpression ieEnd = astRoot.getAST().newInfixExpression();
		ieEnd.setOperator(InfixExpression.Operator.PLUS);
		ieEnd.setLeftOperand(slErrorEnd);
		
		if (!action.equals("provider")) {
			
			ieEnd.setRightOperand(astRoot.getAST().newName("number"));
			cicEnd.arguments().add(ieEnd);
			ts2.setExpression(cicEnd);
			block.statements().add(st);
		}
		else {
			ieEnd.setRightOperand(astRoot.getAST().newName("var"));
			cicEnd.arguments().add(ieEnd);
			ts2.setExpression(cicEnd);
		}
		
		block.statements().add(ts2);
		md.setBody(block);
		
		
		MethodDeclaration method = findMethod(name.getFullyQualifiedName());
		
		if(method == null) {
			TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
			td.bodyDeclarations().add(td.bodyDeclarations().size(), md);
				
			// apply modifications
			TextEdit edits = astRoot.rewrite(document, cu.getJavaProject().getOptions(true));
			edits.apply(document);

			// create a new ast with modifications
			parser.setSource(document.get().toCharArray());
			astRoot = (CompilationUnit) parser.createAST(null);
			
			// insert comment before javadoc method
			ASTRewrite rewrite= ASTRewrite.create(astRoot.getAST());
			ASTNode placeHolder= rewrite.createStringPlaceholder(comment.toString(), ASTNode.LINE_COMMENT);
			
			// search method where comment will be inserted
			td = (TypeDeclaration) astRoot.types().get(0);
			method = findMethod(name.getFullyQualifiedName());
			ListRewrite list = rewrite.getListRewrite(td, TypeDeclaration.BODY_DECLARATIONS_PROPERTY);
			list.insertBefore(placeHolder, method ,null);
			
			// apply modifications
			edits = rewrite.rewriteAST(document, cu.getJavaProject().getOptions(true));
			edits.apply(document);
			parser.setSource(document.get().toCharArray());
			astRoot = (CompilationUnit) parser.createAST(null);
			astRoot.recordModifications();
		}
	}
	
	/**
	 * This method comments action method declaration
	 * @param action String representing action (sample, timer, test, control, provider)
	 * @throws Exception if there is an error.
	 */
	private void commentMethod(String action) throws Exception{
		
		String methodName = null;
		
		if (action.equals("sample")) {
			methodName = "doSample";
		}
		else if (action.equals("timer")) {
			methodName = "doTimer";
		}
		else if (action.equals("test")) {
			methodName = "doTest";
		}
		else if (action.equals("control")) {
			methodName = "doControl";
		}
		else if (action.equals("provider")) {
			methodName = "doGet";
		}
		
		// apply modifications
		TextEdit edits = astRoot.rewrite(document, cu.getJavaProject().getOptions(true));
		edits.apply(document);

		// create a new ast with modifications
		parser.setSource(document.get().toCharArray());
		astRoot = (CompilationUnit) parser.createAST(null);
		ASTRewrite rewrite= ASTRewrite.create(astRoot.getAST());
		
		// find method
		MethodDeclaration method = findMethod(methodName);
		
		// replace all block comment
		StringBuffer sb = new StringBuffer();
		sb.append("/*");
		sb.append(method.toString().replaceAll("[/][*]", "#*").replaceAll("[*][/]", "*#"));
		sb.append("*/");
		
		// insert comment before javadoc method
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		ASTNode placeHolder= rewrite.createStringPlaceholder(sb.toString(), ASTNode.LINE_COMMENT);
		ListRewrite list = rewrite.getListRewrite(td, TypeDeclaration.BODY_DECLARATIONS_PROPERTY);
		list.insertBefore(placeHolder, method ,null);
		rewrite.remove(method, null);
		
		// apply modifications
		edits = rewrite.rewriteAST(document, cu.getJavaProject().getOptions(true));
		edits.apply(document);
		parser.setSource(document.get().toCharArray());
		astRoot = (CompilationUnit) parser.createAST(null);
		astRoot.recordModifications();
	}
	
	/**
	 * This method adds switch case statement in appropriate method.
	 * @param action String representing action (sample, timer, test, control)
	 * @param name String representing action name
	 */
	private void addSwitchCase(String action, String name) {
		
		boolean exist = false;
		String fieldName = null;
		String methodName = null;
		
		if (action.equals("sample")) {
			fieldName = "SAMPLE_" + name.toUpperCase();
			methodName = "doSample";
		}
		else if (action.equals("timer")) {
			fieldName = "TIMER_" + name.toUpperCase();
			methodName = "doTimer";
		}
		else if (action.equals("test")) {
			fieldName = "TEST_" + name.toUpperCase();
			methodName = "doTest";
		}
		else if (action.equals("control")) {
			fieldName = "CONTROL_" + name.toUpperCase();
			methodName = "doControl";
		}
		
		// find method where switch case statements are
		MethodDeclaration method = findMethod(methodName);
		
		//Find parameter that switch statement use
		String paramName = findMethodParameter(method);
				
		//search switch statements
		List statements = method.getBody().statements();
		Iterator iter = statements.iterator();
		while (iter.hasNext()) {
			exist = false;
			Statement statement = (Statement) iter.next();
			if (statement instanceof SwitchStatement) {
				SwitchStatement switchStatement = (SwitchStatement) statement;
				// find switch statement with parameter method as expression
				if (switchStatement.getExpression() instanceof Name &&
				((Name) switchStatement.getExpression()).getFullyQualifiedName().equals(paramName)) {
					//search if switch case already exists
					List caseStatements = switchStatement.statements();
					Iterator iter2 = caseStatements.iterator();
					while (iter2.hasNext()) {
						Statement caseStatement = (Statement) iter2.next();
						if (caseStatement instanceof SwitchCase) {
							SwitchCase sCase = (SwitchCase) caseStatement;
							//test if switch case has field name as expression
							if (sCase.getExpression() instanceof Name &&
							((Name) sCase.getExpression()).getFullyQualifiedName().equals(fieldName)) {
								exist = true;
							}
						}
					}
					
					if(!exist) {
						// create orphelin switch case node
						SwitchCase sc = astRoot.getAST().newSwitchCase();
						sc.setExpression(astRoot.getAST().newName(fieldName));
						BreakStatement bs = astRoot.getAST().newBreakStatement();
						bs.setLabel(null);
						switchStatement.statements().add(0, sc);
						switchStatement.statements().add(1, bs);
					}
				}
			}
		}
	}
	
	/**
	 * This method removes switch case statements in appropriate method.
	 * Case Statement is only remove if there are no statements into or
	 * if next statement is a break statement.
	 * @param action String representing action (sample, timer, test, control)
	 * @param name String representing action name
	 */
	private void removeSwitchCase(String action, String name) {
		
		String fieldName = null;
		String methodName = null;
		
		if (action.equals("sample")) {
			fieldName = "SAMPLE_" + name.toUpperCase();
			methodName = "doSample";
		}
		else if (action.equals("timer")) {
			fieldName = "TIMER_" + name.toUpperCase();
			methodName = "doTimer";
		}
		else if (action.equals("test")) {
			fieldName = "TEST_" + name.toUpperCase();
			methodName = "doTest";
		}
		else if (action.equals("control")) {
			fieldName = "CONTROL_" + name.toUpperCase();
			methodName = "doControl";
		}
		
		
		// find method where switch case statements are
		MethodDeclaration method = findMethod(methodName);
		
		//Find parameter that switch statement use
		String paramName = findMethodParameter(method);
		
		//search switch statements
		List statements = method.getBody().statements();
		Iterator iter = statements.iterator();
		while (iter.hasNext()) {
			Statement statement = (Statement) iter.next();
			if (statement instanceof SwitchStatement) {
				SwitchStatement switchStatement = (SwitchStatement) statement;
				
				// find switch statement with parameter method as expression
				if (switchStatement.getExpression() instanceof Name &&
				((Name) switchStatement.getExpression()).getFullyQualifiedName().equals(paramName)) {
					
					//search if switch case already exists
					List caseStatements = switchStatement.statements();
					ListIterator iter2 = caseStatements.listIterator();
					while (iter2.hasNext()) {
						Statement caseStatement = (Statement) iter2.next();
						if (caseStatement instanceof SwitchCase) {
							SwitchCase sCase = (SwitchCase) caseStatement;
							
							//test if switch case has field name as expression
							if (sCase.getExpression() instanceof Name &&
							((Name) sCase.getExpression()).getFullyQualifiedName().equals(fieldName)) {
								Statement statement2 = (Statement) iter2.next();
								
								// remove switch case if next statement is a switch case statement
								// or break statement
								if (statement2 instanceof SwitchCase) {
									sCase.delete();
								}
								if (statement2 instanceof BreakStatement) {
									sCase.delete();
									statement2.delete();
								}
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * This method comments switch case statements in appropriate method.
	 * @param action String representing action (sample, timer, test, control, provider)
	 * @param name String representing action name
	 * @throws Exception if there is an error.
	 */
	private void commentSwitchCase(String action, String name) throws Exception {
		
		String methodName = null;
		
		if (action.equals("sample")) {
			methodName = "doSample";
		}
		else if (action.equals("timer")) {
			methodName = "doTimer";
		}
		else if (action.equals("test")) {
			methodName = "doTest";
		}
		else if (action.equals("control")) {
			methodName = "doControl";
		}
		
		// apply modifications
		TextEdit edits = astRoot.rewrite(document, cu.getJavaProject().getOptions(true));
		edits.apply(document);
		String source = document.get();
		StringBuffer sb = new StringBuffer(source);
		
		// find method where switch case statements are
		MethodDeclaration method = findMethod(methodName);
		
		//Find parameter that switch statement use
		String paramName = findMethodParameter(method);
		
		//search switch statements
		List statements = method.getBody().statements();
		Iterator iter = statements.iterator();
		while (iter.hasNext()) {
			Statement statement = (Statement) iter.next();
			if (statement instanceof SwitchStatement) {
				SwitchStatement switchStatement = (SwitchStatement) statement;
				// find switch statement with parameter method as expression
				if (switchStatement.getExpression() instanceof Name &&
				((Name) switchStatement.getExpression()).getFullyQualifiedName().equals(paramName)) {
					//search if switch case already exists
					List caseStatements = switchStatement.statements();
					Iterator iter2 = caseStatements.iterator();
					while (iter2.hasNext()) {
						Statement caseStatement = (Statement) iter2.next();
						if (caseStatement instanceof SwitchCase) {
							SwitchCase sCase = (SwitchCase) caseStatement;
							//test if switch case has field name as expression
							if (sCase.getExpression() instanceof Name &&
							((Name) sCase.getExpression()).getFullyQualifiedName().equals(name)) {
								
								int start = sCase.getStartPosition();
								int end = start + sCase.getLength();
								
								while(iter2.hasNext()) {
									Statement next = (Statement) iter2.next();
									if (next instanceof SwitchCase) {
										end = next.getStartPosition();
										break;
									}
								}
								
								sb.insert(start, "/*");
								sb.insert(end, "*/\n");
								String block = sb.substring(start + 2, end);
								String comment = block.replaceAll("[/][*]", "#*").replaceAll("[*][/]", "*#");
								sb.replace(start + 2, end, comment);
								
								// only one switch case exists
								break;
							}
						}
					}				
				}
			}
		}
		
		document= new Document(sb.toString());
		parser.setSource(sb.toString().toCharArray());
		astRoot = (CompilationUnit) parser.createAST(null);
		astRoot.recordModifications();
	}
	
	/**
	 * Returns found method in current AST tree.
	 * @param methodName String representing method name to find
	 * @return MethodDeclaration if method found or null
	 */
	private MethodDeclaration findMethod(String methodName) {
		
		MethodDeclaration method = null;
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List bodies = td.bodyDeclarations();
		Iterator iter = bodies.iterator();
		//search method
		while (iter.hasNext()) {
			BodyDeclaration bd = (BodyDeclaration) iter.next();
			if (bd instanceof MethodDeclaration) {
				method = (MethodDeclaration) bd;
				String qualifiedName = method.getName().getFullyQualifiedName();
				if (qualifiedName.equals(methodName)) {
					return method;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Returns a string representing the first parameter name with 'int' type in current AST tree.
	 * @param methodName String representing method name to find
	 * @return String representing the first parameter name with 'int' type or null;
	 */
	private String findMethodParameter(MethodDeclaration method) {
		
		List parameters = method.parameters();
		Iterator iter = parameters.iterator();
		while (iter.hasNext()) {
			SingleVariableDeclaration sdv = (SingleVariableDeclaration) iter.next();
			if (sdv.getType() instanceof PrimitiveType) {
				PrimitiveType pt = (PrimitiveType) sdv.getType();
				if (pt.getPrimitiveTypeCode() == PrimitiveType.INT) {
					return sdv.getName().getFullyQualifiedName();
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns a valid Java identifier by removing non valid character.
	 * All whitespace characters are replaced by '_' character.
	 * @param arg0 String representing identifier to convert
	 * @return string representing a valid Java identifier
	 */
	private String convertJavaIdentifier(String arg0) {
		
		StringBuffer sb = new StringBuffer();
		boolean first = true;
		char [] tabs = arg0.toCharArray();
		
		for (int i = 0; i < tabs.length; i++) {
			
			if (first && Character.isJavaIdentifierStart(tabs[i])) {
				sb.append(tabs[i]);
				first = false;
			}
			else if (!first) {
				if (tabs[i] == ' ') {
					tabs[i] = '_';
				}
				if (Character.isJavaIdentifierPart(tabs[i])) {
					sb.append(tabs[i]);
				}
			}
		}
		
		return sb.toString();
	}
	
	/**
	 * @return int representing maximum number of old field.
	 */
	private int getMaxOldField() {
		
		int max = -1;
		
		//verify if field type already exists
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List bodies = td.bodyDeclarations();
		Iterator iter = bodies.iterator();
		while (iter.hasNext()) {
			BodyDeclaration bd = (BodyDeclaration) iter.next();
			
			if (bd instanceof FieldDeclaration) {
				FieldDeclaration element = (FieldDeclaration) bd;
				VariableDeclarationFragment vdElement = 
					(VariableDeclarationFragment) element.fragments().get(0);
				
				String qualifiedName = vdElement.getName().getFullyQualifiedName();
				if (qualifiedName.startsWith("OLD")) {
					String [] values = qualifiedName.split("_");
					int number = Integer.parseInt(values[0].substring(3));
					if (max < number) {
						max = number;
					}
				}
			}
		}
		
		return max;
	}
	
	/**
	 * @return true if class implements DataProvider interface.
	 */
	public boolean isDataProvider() {
		
		boolean provider = false;
		
		//verify if interface type already exists
		TypeDeclaration td = (TypeDeclaration) astRoot.types().get(0);
		List interfaces = td.superInterfaceTypes();
		Iterator iter = interfaces.iterator();
		while (iter.hasNext()) {
			SimpleType type = (SimpleType) iter.next();
			String qualifiedName = type.getName().getFullyQualifiedName();
			
			if (qualifiedName.equals("DataProvider")) {
				provider = true;
			}
		}
		
		return provider;
	}
	
	/**
	 * Applies all modifications and returns compilation unit.
	 * @return the CompilationUnit
	 */
	public ICompilationUnit getCompilationUnit() throws Exception{
		applyAST();
		return cu;
	}

	/**
	 * @param cu the CompilationUnit to set
	 */
	public void setCompilationUnit(ICompilationUnit cu) {
		this.cu = cu;
	}

}
