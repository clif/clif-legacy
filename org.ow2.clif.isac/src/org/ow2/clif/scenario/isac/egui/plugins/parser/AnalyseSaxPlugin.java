/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF 
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.plugins.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import org.ow2.clif.scenario.isac.egui.plugins.ActionDescription;
import org.ow2.clif.scenario.isac.egui.plugins.ControlDescription;
import org.ow2.clif.scenario.isac.egui.plugins.ObjectDescription;
import org.ow2.clif.scenario.isac.egui.plugins.ParameterDescription;
import org.ow2.clif.scenario.isac.egui.plugins.PluginDescription;
import org.ow2.clif.scenario.isac.egui.plugins.SampleDescription;
import org.ow2.clif.scenario.isac.egui.plugins.TestDescription;
import org.ow2.clif.scenario.isac.egui.plugins.TimerDescription;
import org.ow2.clif.scenario.isac.egui.plugins.nodes.Node;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
/**
 * This class build a new sax handler, which analyse a plugin xml definition file
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class AnalyseSaxPlugin extends DefaultHandler {
    private String pluginName;
    private Map<String,ActionDescription> samples;
    private Map<String,ActionDescription> timers;
    private Map<String,ActionDescription> tests;
    private Map<String,ActionDescription> controls;
    private ObjectDescription object;
    private String clazz;
    private String method;
    private String name;
    private String number;
    private Vector<ParameterDescription> params;
    private boolean inHelp;
    private Vector<String> help;
    private PluginDescription plugin ;
    
    /**
     * Build a new handler object, whic object is 
     * made to analyse a plugin definition file
     */
    public AnalyseSaxPlugin() {
        super();
        plugin = null ;
        pluginName = null;
        samples = new HashMap<String,ActionDescription>();
        timers = new HashMap<String,ActionDescription>();
        tests = new HashMap<String,ActionDescription>();
        controls = new HashMap<String,ActionDescription>();
        object = null;
        name = null;
        clazz = null;
        method = null;
        number = null;
        params = new Vector<ParameterDescription>();
        inHelp = false;
        help = null;
    }
    
    public void startElement(String namespaceURI, String localName,
            String qName, Attributes atts) {
        if (qName.equals("sample")) {
            name = atts.getValue(0);
            clazz = atts.getValue(1);
            method = atts.getValue(2);
        } 
        else if (qName.equals("object")) {
            /* initialise the name for the object */
            name = "SessionObject";
            clazz = atts.getValue(0);
        } 
        else if (qName.equals("timer")) {
            name = atts.getValue(0);
            number = atts.getValue(1);
        } 
        else if (qName.equals("test")) {
            name = atts.getValue(0);
            number = atts.getValue(1);
        } 
        else if (qName.equals("control")) {
            name = atts.getValue(0);
            number = atts.getValue(1);
        } 
        else if (qName.equals("params")) {
            // do nothing
        } 
        else if (qName.equals("param")) {
            ParameterDescription param =
                new ParameterDescription(atts.getValue(0), atts.getValue(1));
            params.add(param);
        } 
        else if (qName.equals("plugin")) {
            pluginName = atts.getValue(0);
        } 
        else if (qName.equals("help")) {
            inHelp = true;
            help = new Vector<String>();
        }
    }

    public void endElement(
            String namespaceURI, String localName, String qName) {
        /* if the tag closed is a plugin node, add the id parameter to it */
        if (Node.isPluginNode(qName) || qName.equals("object")) {
            params.add(new ParameterDescription("id", "String")) ;
        }
        if (qName.equals("sample")) {
            SampleDescription temp =
                new SampleDescription(name, clazz, method, new Vector<ParameterDescription>(params), help);
            samples.put(name, temp);
            params.clear() ;
            help = null;
        } else if (qName.equals("timer")) {
            TimerDescription temp =
                new TimerDescription(name, number, new Vector<ParameterDescription>(params), help);
            timers.put(name, temp);
            params.clear();
            help = null;
        } else if (qName.equals("test")) {
            TestDescription temp =
                new TestDescription(name, number, new Vector<ParameterDescription>(params), help);
            tests.put(name, temp);
            params.clear();
            help = null;
        } else if(qName.equals("control")) {
            ControlDescription temp = 
                new ControlDescription(name, number, new Vector<ParameterDescription>(params), help);
            controls.put(name, temp);
            params.clear();
            help = null;
        }
        else if (qName.equals("object")) {
            object = new ObjectDescription(name, clazz, new Vector<ParameterDescription>(params), help);
            params.clear();
            help = null;
        } else if (qName.equals("help")) {
            inHelp = false;
        } else if (qName.equals("plugin")) {
        	plugin = new PluginDescription(pluginName, samples, tests, timers, controls, object, help);
        }
    }

    public void characters(char[] ch, int begin, int l) {
        /* if we are not between tag help, we go away of the function */
        if (!inHelp) {
            return;
        }
        /* add help lines */
        String sg;
        sg = new String(ch, begin, l);
		/* search the caracter which show the break lines */
        StringTokenizer st = new StringTokenizer(sg, "\\");
        while (st.hasMoreTokens()) {
            String line = st.nextToken();
            help.add(line);
        }
    }

    /**
     * @return PluginDescription
     */
    public PluginDescription getPluginDescription() {
        return plugin ;
    }
}
