/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2009 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.pages.pageBehavior;

import org.eclipse.jface.dialogs.IInputValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Input validator
 * @author Anthonin Bonnefoy
 */
public class InputValidator implements IInputValidator {
    private String[] ids;

    public InputValidator(String[] ids)
    {
        this.ids = ids;
    }

    public String getStringValid(String seed)
    {
        String validation = this.isValid(seed);
        if (validation == null)
        {
            return seed;
        }
        if (validation.equals("This behavior already exists!"))
        {
            Pattern p = Pattern.compile("(.*)([0-9]+)");
            Matcher m = p.matcher(seed);
            if (m.matches())
            {
                String base = m.group(1);
                Integer num = Integer.valueOf(m.group(2)) + 1;
                return getStringValid(base + num);
            }
            return getStringValid(seed + "0");
        }
        return seed;
    }

    public String isValid(String newText)
    {
        if (newText.equals(""))
        {
            return "";
        }
        for (String id : ids)
        {
            if (id.equals(newText))
            {
                return "This behavior already exists!";
            }
        }
        return null;
    }

}
