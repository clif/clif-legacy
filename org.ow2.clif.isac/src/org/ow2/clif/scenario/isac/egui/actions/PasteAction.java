/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2009, 2012 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.ow2.clif.scenario.isac.egui.FileName;
import org.ow2.clif.scenario.isac.egui.Icons;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.ActionPlacement;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.BehaviorTreeViewer;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd.ElementTransfer;
import org.ow2.clif.scenario.isac.egui.pages.pageImport.ImportTableViewer;
import org.ow2.clif.scenario.isac.egui.util.BehaviorUtil;
import static org.ow2.clif.scenario.isac.egui.util.BehaviorUtil.insertElementInTree;
import org.w3c.dom.Element;
import java.util.List;

/**
 * Paste action used in editors : import and behavior
 * @author Joan Chaumont
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class PasteAction extends Action {

    private TreeViewer viewer;

    /**
     * Constructor
     * @param v this viewer in which we have to paste clipboard
     */
    public PasteAction(TreeViewer v)
    {
        super();

        viewer = v;

        setAccelerator(SWT.CTRL | 'V');
        setText("Paste");
        try
        {
            ImageRegistry reg = Icons.getImageRegistry();
            this.setImageDescriptor(reg.getDescriptor(FileName.PASTE_ICON));
        } catch (Exception e)
        {
        }
    }

    public void run()
    {
        /* get the ElementWithId to copy */
        Clipboard clipboard = ActionsClipboard.getClipboard();
        ElementTransfer transfer = ElementTransfer.getInstance();

        List<Element> elementsToCopy = (List<Element>) clipboard.getContents(transfer);
        if (elementsToCopy == null)
        {
            return;
        }

        Element target;

        /* get the target in which we have to copy */
        IStructuredSelection sel = (IStructuredSelection) viewer.getSelection();
        if (viewer instanceof BehaviorTreeViewer)
        {
            if (sel.isEmpty())
            {
                target = ((BehaviorTreeViewer) viewer).getRootElement();
            } else
            {
                target = (Element) sel.getFirstElement();
            }
        } else if (viewer instanceof ImportTableViewer)
        {
            target = ((ImportTableViewer) viewer).getRootElement();
        } else
        {
            return;
        }
        ActionPlacement placement = ActionPlacement.BEFORE_NODE;
        if (BehaviorUtil.childrenAllowed(target, elementsToCopy.get(0)))
        {
        	placement = ActionPlacement.CHILD;
        }
        for (Element element : elementsToCopy)
        {
            insertElementInTree(element, target, target, placement);
        }
        viewer.refresh();
    }
}
