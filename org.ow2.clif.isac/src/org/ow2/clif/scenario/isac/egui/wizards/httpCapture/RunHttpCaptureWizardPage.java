/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.wizards.httpCapture;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JTextArea;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.bitmechanic.maxq.Config;
import com.bitmechanic.maxq.JTextAreaTestScriptAdapter;
import com.bitmechanic.maxq.Test;
import com.bitmechanic.maxq.ProxyServer;
import com.bitmechanic.maxq.Utils.UserException;


/**
 * This is the second page of Http Capture wizard. Its role is to run maxq.
 * MaxQ properties file will be initialize before starting a record 
 * (with initialize button). After initialization, user cannot change
 * the properties but he can start and stop recording 
 *
 * @author Florian Francheteau
 */
public class RunHttpCaptureWizardPage extends WizardPage {
	
	private Button initializeButton;
	private Label timeLabelFix;
	private Label timeLabelChange;
	private Label requestLabelFix;
	private Label requestLabelChange;
	private Button startButton;
	private Button stopButton;
	
	private Config config;

	private int localPort;

	private Test currentTest;
	private JTextArea textArea;
	private ProxyServer proxy;
	private File resultFile;
	
	private Timer timer;
	private long beginTime;
	private long recordedTime = 0;
	private long currentTime;
	private int numberOfRequests;
	private int numberOfRequestNotRecorded;
	private int numberOfRequestRecorded;
	private boolean firstTime;
	

	/**
	 * Constructor for RunHttpCaptureWizardPage.
	 * @param selection
	 */
	public RunHttpCaptureWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("Run HTTP Capture");
		setDescription("Start and Stop the HTTP capture");
		setPageComplete(false);
		textArea = new JTextArea();
	}

	/**
	 * Creates all controls for RunHttpCaptureWizardPage
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		layout.verticalSpacing = 9;
		layout.horizontalSpacing = 20;

		initializeButton = new Button(container, SWT.PUSH);
		initializeButton.setText("Validate Configuration");
		initializeButton.setEnabled(true);
		GridData gd = new GridData(GridData.BEGINNING);
		gd.horizontalSpan = 2;
		initializeButton.setLayoutData(gd);
		initializeButton.addSelectionListener(initialize);
		
		timeLabelFix = new Label(container, SWT.NULL);
		timeLabelFix.setText("Time elapsed (seconds) :");
		gd = new GridData(GridData.BEGINNING);
		timeLabelFix.setLayoutData(gd);
		
		timeLabelChange = new Label(container, SWT.NULL);
		timeLabelChange.setText("0");
		gd = new GridData(GridData.BEGINNING);
		gd.widthHint=100;
		timeLabelChange.setLayoutData(gd);
		
		requestLabelFix = new Label(container, SWT.NULL);
		requestLabelFix.setText("Number of requests :");
		gd = new GridData(GridData.BEGINNING);
		requestLabelFix.setLayoutData(gd);
		
		requestLabelChange = new Label(container, SWT.NULL);
		requestLabelChange.setText("0");
		gd = new GridData(GridData.BEGINNING);
		gd.widthHint=100;
		requestLabelChange.setLayoutData(gd);

		startButton = new Button(container, SWT.PUSH);
		startButton.setText("Start Recording");
		startButton.setEnabled(false);
		startButton.addSelectionListener(startCapture);

		stopButton = new Button(container, SWT.PUSH);
		stopButton.setText("Stop Recording");
		stopButton.setEnabled(false);
		stopButton.addSelectionListener(stopCapture);

		setControl(container);
	}

	/**
	 * Initializes maxq configuration, creates test and starts proxy
	 */
	private SelectionListener initialize = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			writeProperties();
			config=ConfigIsac.getConfig();
			boolean erase=true;
			numberOfRequestRecorded=0;
			numberOfRequestNotRecorded=0;
			try {
				config.completeInit();
				createResultFile();
				if (resultFile.exists()){
					erase = MessageDialog.openQuestion(getShell(), 
							"CLIF ISAC Plug_in", 
							"File "+ resultFile.getName()+" already exists, erase it ?");
				}
				if (erase){
					proxy = new ProxyServer(localPort);
					proxy.start();
					((MainHttpCaptureWizardPage)getPreviousPage()).disableFields();
					setCurrentTest(new Test(proxy,
							new JTextAreaTestScriptAdapter(textArea),
					"com.bitmechanic.maxq.generator.IsacCodeGenerator"));
					initializeButton.setEnabled(false);
					startButton.setEnabled(true);
				}else{
					initializeButton.setEnabled(true);
					startButton.setEnabled(false);
				}
			}
			catch (UserException e) {
				MessageDialog.openError(
						getShell(),
						"CLIF ISAC Plug_in",
				"Error When initializing Configuration");
			} catch (IOException e){
				MessageDialog.openError(
						getShell(),
						"CLIF ISAC Plug_in",
				"Cannot start Proxy (port number may be already used)");
			}
			setPageComplete(false);
			stopButton.setEnabled(false);
		}
	}; 

	/**
	 * Start Capture
	 */
	private SelectionListener startCapture = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			setPageComplete(false);
			firstTime=true;
			beginTime = System.currentTimeMillis()/1000;
			currentTest.startRecording();
			startTimer();
			startButton.setEnabled(false);
			stopButton.setEnabled(true);
		}
	};	

	/**
	 * Stop Capture and save result file
	 */
	private SelectionListener stopCapture = new SelectionAdapter() {
		public void widgetSelected(SelectionEvent event) {
			currentTest.stopRecording();
			recordedTime=currentTime;
			numberOfRequestRecorded=numberOfRequests;
			
			timer.cancel();
			try {
				if (currentTest.getTestFile() == null){
					resultFile.createNewFile();
					currentTest.setTestFile(resultFile);
				}
				currentTest.save();
			} catch (IOException e) {
				MessageDialog.openError(
						getShell(),
						"CLIF ISAC Plug_in",
				"Cannot save result file)");
			}
			startButton.setEnabled(true);
			stopButton.setEnabled(false);
			setPageComplete(true);
		}
	};

	/**
	 * Start Timer
	 */
	private void startTimer(){
		timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				try{
					getShell().getDisplay().syncExec(new Runnable() {
						public void run () {
							try{
								displayTimer();
							} catch (Throwable t) {
								timer.cancel();
							}
						}
					});
				}catch (Throwable t){
					timer.cancel();
				}
			}
		}, new Date(), 1000);
	}

	/**
	 * Display time elapsed and number of requests recorded
	 */
	private void displayTimer(){
		currentTime = recordedTime + (System.currentTimeMillis()/1000)-beginTime;
		timeLabelChange.setText(String.valueOf(currentTime));
		if (firstTime){
			numberOfRequestNotRecorded = proxy.getNumberOfRequests();
			firstTime=false;
		}
		numberOfRequests = proxy.getNumberOfRequests() - numberOfRequestNotRecorded + numberOfRequestRecorded;
		requestLabelChange.setText(String.valueOf(numberOfRequests));
	}

	/**
	 * sets current test
	 */
	private void setCurrentTest(Test test)
	{
		if (currentTest != null)
			currentTest.close();
		currentTest = test;
	}

	/**
	 * Updates properties file by reading properties filled in previous page.
	 * Unused properties are removed
	 */
	private void writeProperties(){
		Properties properties = new Properties();
		FileInputStream propInputFile;
		FileOutputStream propOutputFile;
		try {
			propInputFile = new FileInputStream(((MainHttpCaptureWizardPage)getPreviousPage()).getPropertiesFile());
			properties.load(propInputFile);

			String portNumber = ((MainHttpCaptureWizardPage)getPreviousPage()).getPortNumber();
			properties.setProperty("local.proxy.port", portNumber);
			localPort = Integer.parseInt(portNumber);

			if (((MainHttpCaptureWizardPage)getPreviousPage()).getUseRemoteProxy()){
				String proxyHost = ((MainHttpCaptureWizardPage)getPreviousPage()).getProxyHost();
				properties.setProperty("remote.proxy.host", proxyHost);
				String proxyPortNumber = ((MainHttpCaptureWizardPage)getPreviousPage()).getProxyPortNumber();
				properties.setProperty("remote.proxy.port", proxyPortNumber);
			}else{
				properties.remove("remote.proxy.host");
				properties.remove("remote.proxy.port");
			}

			String timer = ((MainHttpCaptureWizardPage)getPreviousPage()).getTimer();
			properties.setProperty("generator.isac.timer", timer);
			if (timer.equals("Random")){
				String randomDist = ((MainHttpCaptureWizardPage)getPreviousPage()).getRandomDist();
				properties.setProperty("generator.isac.timer.random.dist", randomDist);
				if ((randomDist.equals("uniform"))
						||(randomDist.equals("gaussian"))
						||(randomDist.equals("negexpo"))){
					String randomDelta = ((MainHttpCaptureWizardPage)getPreviousPage()).getRandomDistDelta();
					properties.setProperty("generator.isac.timer.random.delta", randomDelta);
					properties.remove("generator.isac.timer.random.unit");
					if (randomDist.equals("gaussian")){
						String randomDeviation = ((MainHttpCaptureWizardPage)getPreviousPage()).getRandomDistDeviation();
						properties.setProperty("generator.isac.timer.random.deviation", randomDeviation);
					}else{
						properties.remove("generator.isac.timer.random.deviation");
					}
				}else{
					String randomUnit = ((MainHttpCaptureWizardPage)getPreviousPage()).getRandomDistDelta();
					properties.setProperty("generator.isac.timer.random.unit", randomUnit);
					properties.remove("generator.isac.timer.random.delta");
					properties.remove("generator.isac.timer.random.deviation");
				}
			}else{
				properties.remove("generator.isac.timer.random.dist");
				properties.remove("generator.isac.timer.random.delta");
				properties.remove("generator.isac.timer.random.deviation");
				properties.remove("generator.isac.timer.random.unit");
			}

			propOutputFile = new FileOutputStream(((MainHttpCaptureWizardPage)getPreviousPage()).getPropertiesFile());
			properties.store(propOutputFile,null);
		} catch (FileNotFoundException e) {
			MessageDialog.openError(
					getShell(),
					"CLIF ISAC Plug_in",
			"Cannot read file maxq.properties");
		} catch (IOException e) {
			MessageDialog.openError(
					getShell(),
					"CLIF ISAC Plug_in",
			"Cannot read file maxq.properties");
		}
	}

	/** 
	 * creates result file located in current CLIF project
	 */
	private void createResultFile(){
		String contPath = ((MainHttpCaptureWizardPage)getPreviousPage()).getContainerPath();
		String resultFilePath = contPath + File.separator +((MainHttpCaptureWizardPage)getPreviousPage()).getFileName();
		resultFile = new File(resultFilePath);
	}

	/** 
	 * Gets proxy server
	 * @return ProxyServer the proxy
	 */
	public ProxyServer getProxy(){
		return proxy;
	}
	
	/** 
	 * Gets Timer
	 * @return Timer the timer
	 */
	public Timer getTimer(){
		return timer;
	}
}