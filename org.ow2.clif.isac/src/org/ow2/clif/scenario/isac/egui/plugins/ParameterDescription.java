/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.egui.plugins;

/**
 * This class is the description of an action parameter 
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public class ParameterDescription {
	private String name ;
	private String type ;
	private String defaultValue;

	/**
	 * Build a new parameter description object with specified values
	 * @param n The name of the parameter
	 * @param t The type of the parameter
	 */
	public ParameterDescription(String n, String t) {
	    this.name = n ;
		this.type = t ;
	}
    
    /**
     * Attribute name getter
     * @return The name of the parameter
     */
    public String getName() {
        return name;
    }

    /**
     * Attribute type getter
     * @return The type of the parameter
     */
    public String getType() {
        return type;
    }
    
    public void setDefaultValue(String value)
    {
    	defaultValue = value;
    }

    public String getDefaultValue()
    {
    	return defaultValue;
    }

    public String toString()
    {
    	return "parameter name=" + name + ", type=" + type + ", default value=" + defaultValue;
    }
}
