/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2009 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageBehavior;

import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.StructuredModelManager;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.ow2.clif.scenario.isac.egui.Icons;
import org.ow2.clif.scenario.isac.egui.ScenarioManager;
import org.ow2.clif.scenario.isac.egui.actions.ActionsClipboard;
import org.ow2.clif.scenario.isac.egui.actions.CopyAction;
import org.ow2.clif.scenario.isac.egui.actions.CutAction;
import org.ow2.clif.scenario.isac.egui.actions.PasteAction;
import org.ow2.clif.scenario.isac.egui.model.ModelReaderXIS;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd.ElementDragListener;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd.ElementDropAdapter;
import org.ow2.clif.scenario.isac.egui.pages.pageBehavior.dnd.ElementTransfer;
import org.ow2.clif.scenario.isac.egui.plugins.ActionDescription;
import org.ow2.clif.scenario.isac.egui.plugins.PluginDescription;
import org.ow2.clif.scenario.isac.egui.util.BehaviorUtil;
import static org.ow2.clif.scenario.isac.egui.util.BehaviorUtil.getAddText;
import static org.ow2.clif.scenario.isac.egui.util.BehaviorUtil.insertElementInTree;
import org.w3c.dom.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class is used for displaying a behavior
 * A popup menu is set with this view for adding plug-ins
 * or for copy-cut-paste actions.
 * @author Joan Chaumont
 * @author Florian Francheteau
 */
public class BehaviorTreeViewer extends TreeViewer implements ISelectionChangedListener {

	private CopyAction copyAction;
	private CutAction cutAction;
	private PasteAction pasteAction;
	private Element root;

	BehaviorTreeContentProvider provider;
	ScenarioManager scenario;

	/**
	 * Provides the selection listener which filters the selection to the elements on the same level
	 */
	public void selectionChanged(SelectionChangedEvent selectionChangedEvent) {
		StructuredSelection selection = (StructuredSelection) selectionChangedEvent.getSelection();
		List<Element> elementList = BehaviorUtil.getSameLevelElement((List<Element>) selection.toList());
		if (selection.size()!=elementList.size()) {
			if (elementList.isEmpty()) {
				this.setSelection(StructuredSelection.EMPTY);
			} else {
				this.setSelection(new StructuredSelection(elementList));
			}
		}
	}

	class BehaviorTreeContentProvider implements ITreeContentProvider, ILabelProvider {

		private int behaviorPos;

		BehaviorTreeContentProvider(int behaviorPos) {
			this.behaviorPos = behaviorPos;
		}

		public Object[] getChildren(Object element) {
			NodeList nodes = ((Element) element).getChildNodes();
			List<Node> wantedNodes = new ArrayList<Node>();
			int nbNodes = nodes.getLength();

			for (int i = 0; i < nbNodes; i++) {
				Node node = nodes.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE
						&& isCorrectNode(node)) {
					wantedNodes.add(node);
				}
			}
			return wantedNodes.toArray();
		}

		private boolean isCorrectNode(Node node) {
			String n = node.getNodeName();
			return n.equals("if") || n.equals("while") || n.equals("nchoice")
					|| n.equals("else") || n.equals("sample") || n.equals("timer")
					|| n.equals("action") || n.equals("then") || n.equals("preemptive")
					|| n.equals("choice") || n.equals("control");
		}

		public Object getParent(Object o) {
			Object result = null;
			if (o instanceof Node) {
				Node node = (Node) o;
				if (node.getNodeType() == Node.ATTRIBUTE_NODE) {
					result = ((Attr) node).getOwnerElement();
				} else {
					result = node.getParentNode();
				}
			}
			return result;
		}

		public boolean hasChildren(Object element) {
			return getChildren(element).length > 0
					&& !((Node) element).getFirstChild().getNodeName().equals("params");
		}

		public Object[] getElements(Object element) {
			Element elt = getRootElement((Document) element);
			return (elt == null) ? new Object[0] : getChildren(elt);
		}

		private Element getRootElement(Document document) {
			NodeList list = document.getElementsByTagName("behavior");
			root = (Element) list.item(behaviorPos);
			return root;
		}

		public void dispose() {
		}

		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		public Image getImage(Object element) {
			Image image = null;
			try {
				String nodeType = ((Element) element).getNodeName();
				image = Icons.getImageRegistry().get(nodeType);
			}
			catch (Exception e) {
				e.printStackTrace(System.out);
			}

			return image;
		}

		public String getText(Object element) {
			String result;
			Element node = (Element) element;
			result = node.getNodeName();

			if (result.equals("sample") || result.equals("timer")
					|| result.equals("control")) {
				String use = node.getAttribute("use");
				String name = node.getAttribute("name");
				if (use != null && name != null
						&& !use.equals("") && !name.equals("")) {
					result = use + "." + name;
				} else {
					result = "";
				}
			}
			return result; //$NON-NLS-1$
		}

		public void addListener(ILabelProviderListener listener) {
		}

		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		public void removeListener(ILabelProviderListener listener) {
		}

	}

	/**
	 * This class is used for adding and inserting actions with the context menu
	 * @author Joan Chaumont
	 * @author Florian Francheteau
	 */
	class AddInsertAction extends Action {
		String actionPlugin;
		String actionName;
		String actionId;
		String type;
		/* indicates if it is an "add action" */
		Boolean addAction;

		Element selectedElem;
		ActionPlacement actionPlacement;

		/**
		 * Constructor for simple element ("choice", "then", "else")
		 * @param elt			 selected element
		 * @param name			name of element to add
		 * @param actionPlacement
		 */
		public AddInsertAction(Element elt, String name, ActionPlacement actionPlacement) {
			this.actionName = name;
			this.actionId = "";
			this.selectedElem = elt;
			this.actionPlacement = actionPlacement;

			setText(name);

			try {
				ImageRegistry reg = Icons.getImageRegistry();
				this.setImageDescriptor(reg.getDescriptor(name));
			} catch (Exception e) {
			}
		}

		/**
		 * Constructor for complex action (sample, control...)
		 * @param elt			 selected element
		 * @param plugin		  name of plugin
		 * @param actionName	  name of action
		 * @param actionId		id of action
		 * @param type			type of action
		 * @param actionPlacement
		 */
		public AddInsertAction(Element elt, String plugin, String actionName,
							   String actionId, String type, ActionPlacement actionPlacement) {
			this.actionPlugin = plugin;
			this.actionName = actionName;
			this.actionId = actionId;
			this.type = type;
			this.selectedElem = elt;

			this.actionPlacement = actionPlacement;

			setText(actionPlugin + "." + actionName);

			try {
				ImageRegistry reg = Icons.getImageRegistry();
				this.setImageDescriptor(reg.getDescriptor(type));
			} catch (Exception e) {
			}
		}

		public void run() {
			Document doc = (Document) getInput();
			Element newElement = doc.createElement(actionName);

			/* if it is a simple action */
			if (actionId.equals(""))
			{
				if (
					actionName.equals("if")
					|| actionName.equals("preemptive")
					|| actionName.equals("while"))
				{
					Element condition = doc.createElement("condition");
					condition.setAttribute("use", "");
					condition.setAttribute("name", "");
					newElement.appendChild(condition);
					if (actionName.equals("if")) {
						newElement.appendChild(doc.createElement("then"));
					}
				}
			} else {
				/* if it is a complex action */
				newElement = doc.createElement(type);
				newElement.setAttribute("use", actionId);
				newElement.setAttribute("name", actionName);
			}
			
			insertElementInTree(newElement, selectedElem, getRootElement(), actionPlacement);
			BehaviorTreeViewer.this.refresh();
		}

	}

	/**
	 * Constructor
	 * @param parent
	 * @param style
	 * @param scenario
	 * @param behaviorPos
	 */
	public BehaviorTreeViewer(Composite parent, int style,
							  ScenarioManager scenario, int behaviorPos) {
		super(parent, style);

		this.scenario = scenario;
		provider =
				new BehaviorTreeContentProvider(behaviorPos);

		this.setContentProvider(provider);
		this.setLabelProvider(provider);

		createActions();
		hookContextMenu();
		this.addSelectionChangedListener(this);
		/* Initialize drag and drop functions */
		Transfer[] types = new Transfer[]{ElementTransfer.getInstance()};
		int operations = DND.DROP_MOVE | DND.DROP_COPY;
		addDragSupport(operations, types,
				new ElementDragListener(this));
		addDropSupport(operations, types,
				new ElementDropAdapter(this));
	}


	/* These functions are for the popup menu */
	private void createActions() {
		copyAction = new CopyAction(this);
		cutAction = new CutAction(this);
		pasteAction = new PasteAction(this);
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(getControl());
		getControl().setMenu(menu);
	}

	protected void fillContextMenu(IMenuManager contextMenu) {
		MenuManager addMenuPopup = new MenuManager("Add child");
		IStructuredSelection sel = (IStructuredSelection) getSelection();
		Element elt = (Element) sel.getFirstElement();

		String addText = getAddText(sel);

		if (addText.equals(BehaviorMasterPage.ADD_THEN)) {
			addMenuPopup.add(new AddInsertAction(elt, "then", ActionPlacement.CHILD));
		} else if (addText.equals(BehaviorMasterPage.ADD_CHOICE)) {
			addMenuPopup.add(new AddInsertAction(elt, "choice", ActionPlacement.CHILD));
		} else if (addText.equals(BehaviorMasterPage.ADD_ELSE)) {
			addMenuPopup.add(new AddInsertAction(elt, "else", ActionPlacement.CHILD));
		} else if (!addText.equals("")) {
			makeSubMenu(addMenuPopup, elt, ActionPlacement.CHILD);
			addMenuPopup.add(new AddInsertAction(elt, "while", ActionPlacement.CHILD));
			addMenuPopup.add(new AddInsertAction(elt, "if", ActionPlacement.CHILD));
			addMenuPopup.add(new AddInsertAction(elt, "preemptive", ActionPlacement.CHILD));
			addMenuPopup.add(new AddInsertAction(elt, "nchoice", ActionPlacement.CHILD));
		}

		MenuManager insertBeforeMenuPopup = new MenuManager("Insert before");
		makeSubMenu(insertBeforeMenuPopup, elt, ActionPlacement.BEFORE_NODE);
		insertBeforeMenuPopup.add(new AddInsertAction(elt, "while", ActionPlacement.BEFORE_NODE));
		insertBeforeMenuPopup.add(new AddInsertAction(elt, "if", ActionPlacement.BEFORE_NODE));
		insertBeforeMenuPopup.add(new AddInsertAction(elt, "preemptive", ActionPlacement.BEFORE_NODE));
		insertBeforeMenuPopup.add(new AddInsertAction(elt, "nchoice", ActionPlacement.BEFORE_NODE));


		MenuManager insertAfterMenuPopup = new MenuManager("Insert after");
		makeSubMenu(insertAfterMenuPopup, elt, ActionPlacement.AFTER_NODE);
		insertAfterMenuPopup.add(new AddInsertAction(elt, "while", ActionPlacement.AFTER_NODE));
		insertAfterMenuPopup.add(new AddInsertAction(elt, "if", ActionPlacement.AFTER_NODE));
		insertAfterMenuPopup.add(new AddInsertAction(elt, "preemptive", ActionPlacement.AFTER_NODE));
		insertAfterMenuPopup.add(new AddInsertAction(elt, "nchoice", ActionPlacement.AFTER_NODE));

		contextMenu.add(addMenuPopup);
		contextMenu.add(insertBeforeMenuPopup);
		contextMenu.add(insertAfterMenuPopup);
		contextMenu.add(new Separator()); //$NON-NLS-1$
		contextMenu.add(copyAction);
		contextMenu.add(cutAction);
		contextMenu.add(pasteAction);

		Clipboard clipboard = ActionsClipboard.getClipboard();
		ElementTransfer transfer = ElementTransfer.getInstance();

		List<Element> toCopy = (List<Element>) clipboard.getContents(transfer);
		pasteAction.setEnabled(toCopy != null);
		copyAction.setEnabled(BehaviorUtil.isAllowedToMove(elt));
		cutAction.setEnabled(BehaviorUtil.isAllowedToMove(elt));
	}

	private void makeSubMenu(MenuManager addInsertMenuPopup, Element elt, ActionPlacement actionPlacement) {
		Map<String, String> usedPlugins = ModelReaderXIS.getPlugins((Document) getInput());

		for (Map.Entry<String, String> entryIdName : usedPlugins.entrySet()) {
			String id = entryIdName.getKey();
			String name = entryIdName.getValue();
			MenuManager menu = new MenuManager(id);
			PluginDescription description =
					scenario.getPluginManager().getDescription(name);

			if (description == null) {
				continue;
			}
			for (ActionDescription actionDescription : description.getSamples().values()) {
				menu.add(new AddInsertAction(elt, name, actionDescription.getName(), id, "sample", actionPlacement));
			}
			for (ActionDescription actionDescription : description.getTimers().values()) {
				menu.add(new AddInsertAction(elt, name, actionDescription.getName(), id, "timer", actionPlacement));
			}
			for (ActionDescription actionDescription : description.getControls().values()) {
				menu.add(new AddInsertAction(elt, name, actionDescription.getName(), id, "control", actionPlacement));
			}
			addInsertMenuPopup.add(menu);
		}
	}

	/* Setters */
	/**
	 * Set the behavior position
	 * @param behaviorPos
	 */
	public void setBehaviorPos(int behaviorPos) {
		provider =
				new BehaviorTreeContentProvider(behaviorPos);
		this.setContentProvider(provider);
		this.setLabelProvider(provider);
		this.refresh();
	}

	/**
	 * Set document in our tree view.
	 * The IDocument is transform to IStructuredModel
	 * @param doc
	 */
	public void setDocument(IDocument doc) {
		IStructuredModel model = null;
		try {
			model = StructuredModelManager.getModelManager().getExistingModelForRead(doc);

			if (model != null && model instanceof IDOMModel) {
				Document domDoc;
				domDoc = ((IDOMModel) model).getDocument();
				setInput(domDoc);
			}
		}
		finally {
			if (model != null) {
				model.releaseFromRead();
			}
		}
	}


	/**
	 * Root element getter
	 * @return Element root
	 */
	public Element getRootElement() {
		return root;
	}

	/* These functions are used by the tool tip displayer */
	/**
	 * @param x
	 * @param y
	 * @return Point
	 */
	public Point toDisplay(int x, int y) {
		return getTree().toDisplay(x, y);
	}

	/**
	 * Get item under the point
	 * @param point
	 * @return TreeItem
	 */
	public TreeItem getItem(Point point) {
		return getTree().getItem(point);
	}

	/**
	 * Force the focus on this composite
	 * @return boolean
	 */
	public boolean forceFocus() {
		return getTree().forceFocus();
	}

	/**
	 * Add listener to our tree
	 * @param eventType
	 * @param listener
	 */
	public void addListener(int eventType, Listener listener) {
		getTree().addListener(eventType, listener);
	}

	/**
	 * Remove a listener from our tree
	 * @param event
	 * @param listener
	 */
	public void removeListener(int event, Listener listener) {
		getTree().removeListener(event, listener);
	}
}
