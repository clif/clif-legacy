/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.wizards;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.*;
import org.eclipse.ui.ide.IDE;
import org.ow2.clif.scenario.isac.dtd.Files;
import org.ow2.clif.util.XMLEntityResolver;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;

/**
 * This is a sample new wizard. Its role is to create a new file
 * resource in the provided container. If the container resource
 * (a folder or a project) is selected in the workspace
 * when the wizard is opened, it will accept it as the target
 * container. The wizard creates one file with the extension
 * "xis". If a sample multi-page editor (also available
 * as a template) is registered for the same extension, it will
 * be able to open it.
 */
public class IsacScenarioWizard extends Wizard implements INewWizard {
    private IsacScenarioWizardPage page;
    private ISelection selection;

    /**
     * Constructor for IsacScenarioWizard.
     */
    public IsacScenarioWizard()
    {
        super();
        setNeedsProgressMonitor(true);
    }

    /**
     * Adding the page to the wizard.
     */
    public void addPages()
    {
        page = new IsacScenarioWizardPage(selection);
        addPage(page);
    }

    /**
     * This method is called when 'Finish' button is pressed in
     * the wizard. We will create an operation and run it
     * using wizard as execution context.
     */
    public boolean performFinish()
    {
        final String containerName = page.getContainerName();
        final String fileName = page.getFileName();
        IRunnableWithProgress op = new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor) throws InvocationTargetException
            {
                try
                {
                    doFinish(containerName, fileName, monitor);
                } catch (CoreException e)
                {
                    throw new InvocationTargetException(e);
                } finally
                {
                    monitor.done();
                }
            }
        };
        try
        {
            getContainer().run(true, false, op);
        } catch (InterruptedException e)
        {
            return false;
        } catch (InvocationTargetException e)
        {
            Throwable realException = e.getTargetException();
            MessageDialog.openError(getShell(), "Error", realException.getMessage());
            return false;
        }
        return true;
    }

    /*
* The worker method. It will find the container, create the
* file if missing or just replace its contents, and open
* the editor on the newly created file. */
    private void doFinish(String containerName, String fileName,
                          IProgressMonitor monitor)
            throws CoreException
    {
        // create a sample file
        monitor.beginTask("Creating " + fileName, 2);
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        IResource resource = root.findMember(new Path(containerName));
        if (!resource.exists() || !(resource instanceof IContainer))
        {
            throwCoreException("Container \"" + containerName + "\" does not exist.");
        }
        IContainer container = (IContainer) resource;
        final IFile file = container.getFile(new Path(fileName));
        try
        {
            InputStream stream = openContentStream();
            if (file.exists())
            {
                file.setContents(stream, true, true, monitor);
            } else
            {
                file.create(stream, true, monitor);
            }
            stream.close();
        } catch (IOException e)
        {
        }
        monitor.worked(1);
        monitor.setTaskName("Opening file for editing...");
        getShell().getDisplay().asyncExec(new Runnable() {
            public void run()
            {
                IWorkbenchPage page =
                        PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
                try
                {
                    IDE.openEditor(page, file, true);
                } catch (PartInitException e)
                {
                }
            }
        });
        monitor.worked(1);
    }

    /* We will initialize file contents with a sample text. */
    private InputStream openContentStream()
    {
        String head = "<?xml version=\"1.0\"?>\n";
        String dtd = "<!DOCTYPE scenario SYSTEM \"" + XMLEntityResolver.CLASSPATH_SCHEME + Files.SCENARIO_DTD + "\">\n";
        String contents =
                "<scenario>\n<behaviors>\n<plugins></plugins><behavior id=\"B0\"></behavior>\n</behaviors>\n<loadprofile>\n</loadprofile>\n</scenario>";
        String file = head + dtd + contents;

        return new ByteArrayInputStream(file.getBytes());
    }

    private void throwCoreException(String message) throws CoreException
    {
        IStatus status =
                new Status(IStatus.ERROR, "org.ow2.clif.isac.gui", IStatus.OK, message, null);
        throw new CoreException(status);
    }

    /**
     * We will accept the selection in the workbench to see if
     * we can initialize from it.
     * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
     */
    public void init(IWorkbench workbench, IStructuredSelection selection)
    {
        this.selection = selection;
    }
}