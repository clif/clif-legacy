/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005,2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.scenario.isac.egui.loadprofile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

/**
 * Extends canvas for drawing the curve of blade's stats.
 * @author Joan Chaumont
 * @author Florian Francheteau
 *
 */
public class Graph extends Canvas {
    private Map<String,LoadProfile> profiles;
    private int maxValue;
    
    private int maxTime;
    
    private int marginH;
    private int marginW;
    
    private String priority;
    
    /* Coords of mouse pointer click*/
    private int coordsX;
    private int coordsY;

    /* These values are used for scale draw.
     * Used with the calculateScale function, this returns
     * correct scale values depending on the max value 
     * of the graph. */
    private Object[] scaleValues = new Object[11];
    {
        scaleValues[0] = new int []{4,8};
        scaleValues[1] = new int []{4,8,12};
        scaleValues[2] = new int []{4,8,12,16};
        scaleValues[3] = new int []{10,20};
        scaleValues[4] = new int []{10,20,30};
        scaleValues[5] = new int []{10,20,30,40};
        scaleValues[6] = new int []{20,40};
        scaleValues[7] = new int []{20,40,60};
        scaleValues[8] = new int []{20,40,60};
        scaleValues[9] = new int []{20,40,60,80};
        scaleValues[10] = new int []{20,40,60,80};
    }
    
    /**
     * Create a new canvas for drawing curve
     * @param parent
     * @param style
     */
    public Graph(Composite parent, int style) {
        super(parent, style);
        
        profiles = new HashMap<String,LoadProfile>();
        maxValue = 0;
        
        priority = "";
        
        /* Calculate margin (sufficient space for scale values)*/
        GC gc = new GC(this);
        marginH = gc.getFontMetrics().getHeight()*2 + 4;
        marginW = gc.getFontMetrics().getAverageCharWidth()*10 + 4;
        gc.dispose();
        
        /* Mouse listener for getting coords under the mouse */
        coordsX = coordsY = -1;
        this.addMouseTrackListener(new MouseTrackListener() {
            public void mouseEnter(MouseEvent e) {}
            public void mouseExit(MouseEvent e) {}
            
            public void mouseHover(MouseEvent e) {
                Rectangle rec = getClientArea();
                if(e.x >= marginW && e.x <= rec.width - marginW 
                        && e.y >= marginH && e.y <= rec.height - marginH) {
                    coordsX = e.x;
                    coordsY = e.y;
                }
                else {
                    coordsX = coordsY = -1;
                }
                drawCoords(maxValue);
            }
        });
    }
    
    /**
     * Draw the graph for each id in ids
     * @param ids the blades id to draw
     * @param colorsMap a color for each id
     */
    public void drawGraph(Object[] ids, Map<String,Integer> colorsMap) {        
        Rectangle rec = getClientArea();
        Rectangle graph = new Rectangle(marginW, marginH, rec.width-2*marginW, rec.height-2*marginH);
        
        getMaxs(ids);
        
        drawXAxis();
        drawYAxis();
        
        drawYScale();
        drawXScale();
        
        int max = maxValue;
        int localMax = rec.height;
        
        boolean drawPriority = false;
        
        for (int i = 0; i < ids.length; i++) {
            String id = (String)ids[i];
            if(!id.equals(priority)) {
                Color color = getDisplay().getSystemColor(colorsMap.get(id).intValue());
                localMax = drawCurve(id, graph, max, localMax, color);
            }
            else {
                drawPriority = true;
            }
        }
        if(drawPriority) {
            Color color = getDisplay().getSystemColor(colorsMap.get(priority).intValue());
            localMax = drawCurve(priority, graph, max, localMax, color);
        }
        drawCoords(max);
    }
    
    private void getMaxs(Object[] ids) {
        maxTime = 0;
        maxValue = 0;
        for (int i = 0; i < ids.length; i++) {
            LoadProfile prof = profiles.get(ids[i]);
            int tmp = prof.getMaxTime();
            if(tmp > maxTime) {
                maxTime = tmp;
            }
            tmp = prof.getMaxPopulation();
            if(tmp > maxValue) {
                maxValue = tmp;
            }
        }
    }

    /**
     * Set the blade id which will be draw in first plan.
     * @param id
     */
    public void setPriority(String id) {
        priority = id;
    }
    
    /* All draw function : axis, coords, time, curve and scale */
    
    private void drawXAxis () {
        Rectangle rec = getClientArea();
        GC gc = new GC(this);
        gc.setLineWidth(2);
        gc.drawLine(marginW,rec.height-marginH,rec.width-marginW,rec.height-marginH);
        
        gc.dispose();
    }
    
    private void drawYAxis () {
        Rectangle rec = getClientArea();
        GC gc = new GC(this);
        gc.setLineWidth(2);
        gc.drawLine(marginW,rec.height-marginH,marginW,marginH);
        
        gc.dispose();
    }
    
    private void drawCoords(int max) {
        GC gc = new GC(this);
        Rectangle graph = new Rectangle(marginW, marginH, 
                getClientArea().width-2*marginW, getClientArea().height-2*marginH);
        
        gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_WHITE));
        gc.fillRectangle(0,0,getClientArea().width,marginH-1);
        gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_BLACK));
        
        if(coordsX != -1) {
            int time = (coordsX-marginW)*maxTime/graph.width;
            int val = (int)((long)(graph.height + marginH - coordsY)*(long)max/(long)graph.height);
            
            gc.drawString("Time : " + time + " Value : " + val, 0, 0);
        }
        else {
            gc.drawText("Time : 0 Value : 0", 0, 0);
        }
        gc.dispose();
    }
    
    /**
     * Draw the curve for this blade.
     * @param id 
     * @param graph 
     * @param max 
     * @param localMax 
     * @param color 
     * @return int
     */
    private int drawCurve(String id, Rectangle graph, int max, 
            int localMax, Color color) {
        GC gc = new GC(this);
        LoadProfile profile = profiles.get(id);
        List<ProfilePoint> points = profile.getProfilesPoints();
        int nbPoints = points.size();
        
        /* Check if we need additional points for crenel */
        int supPoints = profile.getAdditionalPoints();
        int[] intPoints = new int[(nbPoints+supPoints)*2 + 2];
        
        ProfilePoint p = points.get(0);
        intPoints[0] = 0 + marginW;
        if(max == 0) {
            intPoints[1] = graph.height + marginH;
        }
        else {
            intPoints[1] = graph.height - (int)((long)p.population * (long)graph.height / (long)max)+marginH;
        }
        
        for (int i = 1, j = 0; i < nbPoints + 1; i++) {
            p = points.get(i-1);
            
            /* in crenel_vh case the x is the same has x of previous point */
            if(p.rampStyle.toLowerCase().equals("crenel_vh")) {
                intPoints[(i+j)*2] = intPoints[(i-1+j)*2];
                if(max == 0) {
                    intPoints[(i+j)*2+1] = graph.height + marginH;
                }
                else {
                    intPoints[(i+j)*2+1] = graph.height - 
                    (int)((long)p.population * (long)graph.height 
                            / (long)max)+marginH;
                }
                j++;
            }
            
            /* in crenel_vh case the y is the same has y of previous point */
            else if(p.rampStyle.toLowerCase().equals("crenel_hv")) {
                intPoints[(i+j)*2] = p.time * graph.width / maxTime + marginW;
                if(max == 0) {
                    intPoints[(i+j)*2+1] = graph.height + marginH;
                }
                else {
                    intPoints[(i+j)*2+1] = intPoints[(i+j-1)*2+1];
                }
                j++;
            }
            
            /* line case */
            intPoints[(i+j)*2] = p.time * graph.width / maxTime + marginW;
            
            if(max == 0) {
                intPoints[(i+j)*2+1] = graph.height + marginH;
            }
            else {
                intPoints[(i+j)*2+1] = graph.height - (int)((long)p.population * (long)graph.height / (long)max)+marginH;
            }
        }
        localMax = (intPoints[intPoints.length-1]<localMax)?intPoints[intPoints.length-1]:localMax;
        
        gc.setForeground(color);
        gc.setLineWidth(2);
        gc.drawPolyline(intPoints);
        
        gc.dispose();
        return localMax;
    }
    
    
    /**
     * Function used for drawing scale and grid of the Y axis 
     */
    private void drawYScale(){
        Rectangle rec = getClientArea();
        GC gc = new GC(this);
        Rectangle graph = new Rectangle(0, marginH, rec.width-2*marginW, rec.height-2*marginH);
        gc.setLineStyle(SWT.LINE_DASH);
        
        int max = maxValue;
        int dec = getDecimals(max);
        int [] values = null;
        int []tmp = (int[])scaleValues[calculateScale(max)];
        
        //Special case if max<10
        if(max < 10 && max > 1) {
            values = new int[tmp.length];
            for (int i = 0; i < values.length; i++) {
                values[i]=tmp[i]/10;
            }
        }
        else {
            //Set values nbVal*2 if size is sufficient
            if(graph.height/50 > tmp.length*2) {
                int middle = tmp[0]/2;
                if(tmp[tmp.length-1] < max-middle*dec) {
                    values = new int[tmp.length*2+1];
                }
                else {
                    values = new int[tmp.length*2];
                }
                
                for (int i = 1; i <= values.length; i++) {
                    values[i-1] = middle*i;
                }
            }
            else {
                values = tmp;
            }
        }
        
        if(max != 0) {
            int maxWidth = rec.width-marginW;
            int maxPos = marginH;    
            //Draw scaleValues
            for (int i = 0; i < values.length; i++) {
                int val = values[values.length-i-1]*dec;
                int pos = (int)((long)(max-val) * (long)graph.height/(long)max) + marginH;
                gc.drawLine(marginW,pos,maxWidth,pos);
                if(Math.abs(pos - maxPos) > gc.getFontMetrics().getHeight()) {  
                    gc.drawText(String.valueOf(val), 0, pos);
                }
            }
            
            //Draw max
            gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_RED));
            gc.drawText(String.valueOf(max), 0, maxPos);
            gc.drawLine(marginW, maxPos, maxWidth, maxPos);
        }
        gc.dispose();
    }
    
    /**
     * Function used for drawing scale and grid of the X axis 
     */ 
    private void drawXScale(){
        Rectangle rec = getClientArea();
        GC gc = new GC(this);
        Rectangle graph = new Rectangle(0, marginH, rec.width-2*marginW, rec.height-2*marginH);
        gc.setLineStyle(SWT.LINE_DASH);
        
        int max = maxTime;
        int dec = getDecimals(max);
        int [] values = null;
        int []tmp = (int[])scaleValues[calculateScale(max)];
        
        //Special case if max<10
        if(max < 10 && max > 1) {
            values = new int[tmp.length];
            for (int i = 0; i < values.length; i++) {
                values[i]=tmp[i]/10;
            }
        }
        else {
            //Set values nbVal*2 if size is sufficient
            if(graph.width/50 > tmp.length*2) {
                int middle = tmp[0]/2;
                if(tmp[tmp.length-1] < max-middle*dec) {
                    values = new int[tmp.length*2+1];
                }
                else {
                    values = new int[tmp.length*2];
                }
                
                for (int i = 1; i <= values.length; i++) {
                    values[i-1] = middle*i;
                }
            }
            else {
                values = tmp;
            }
        }
        
        if(max != 0) {
            int maxHeight = rec.height-marginH;
            int maxPos = rec.width-marginW;    
            //Draw scaleValues
            for (int i = 1; i <= values.length; i++) {
                int val = values[i-1]*dec;
                int pos = (int)((long)(val) * (long)graph.width/(long)max) + marginW;
                gc.drawLine(pos,marginH,pos,maxHeight);
                if(Math.abs(pos - maxPos) > gc.getFontMetrics().getAverageCharWidth()) {  
                    gc.drawText(String.valueOf(val), pos, maxHeight+(marginH/2));
                }
            }
            
            //Draw max
            gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_RED));
            gc.drawText(String.valueOf(max), maxPos, rec.height-(marginH/2));
            gc.drawLine(maxPos, marginH, maxPos, maxHeight);
        }
        gc.dispose();
    }
    
    
    /**
     * Calculate scale value
     * @param val 	maximum axis value
     * @return int	scale value calculated
     */
    private int calculateScale(int val) {       
        if(val < 10 && val > 1) {
            return val+1;
        }
        while(val/100 >= 1) {
            val = val/10;
        }
        if(val < 20) {
            if(val<12) {
                return 0;
            }
            return (val < 16)?1:2;
        }
        return val/10+1;
    }
    
    /**
     * Returns number of digits (minus 1) for the given argument
     *
     * <ul><li>If the argument is between 0 and 9, getDecimal returns 1 
     * <li>If the argument is between 10 and 99, getDecimal returns 1 
     * <li>If the argument is between 100 and 999, getDecimal returns 2  
     * <li>If the argument is between 1000 and 9999, getDecimal returns 3 
     * </ul> 
     * 
     * @param val 	input number
     * @return int	number of tenses
     */
    private int getDecimals(int val) {
        int dec = 0;
        while((val = val/10) >= 1) {dec++;}
        return (dec==0)?1:(int)Math.pow(10,dec-1);
    }

    /**
     * Set profiles
     * @param profiles
     */
    public void setProfiles(Map<String,LoadProfile> profiles) {
        this.profiles = profiles;
    }
}
