/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.egui.pages.pageImport;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.sse.core.internal.provisional.StructuredModelManager;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.ow2.clif.scenario.isac.egui.Icons;
import org.ow2.clif.scenario.isac.egui.IsacScenarioPlugin;
import org.ow2.clif.scenario.isac.egui.ScenarioManager;
import org.ow2.clif.scenario.isac.egui.actions.CopyAction;
import org.ow2.clif.scenario.isac.egui.actions.CutAction;
import org.ow2.clif.scenario.isac.egui.actions.PasteAction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class is used for displaying the plug-ins section
 * A popup menu is set with this view for adding plug-ins
 * or for copy-cut-paste actions.
 * 
 * @author Joan Chaumont
 */
public class ImportTableViewer extends TreeViewer {

    private CopyAction copyAction;
    private CutAction cutAction;
    private PasteAction pasteAction;
    private ScenarioManager scenario; 
    
    class ImportTableContentProvider implements ITreeContentProvider, ILabelProvider {

        public Object[] getChildren(Object parentElement) {
            return null;
        }

        public Object getParent(Object element) {
            return null;
        }

        public boolean hasChildren(Object element) {
            return !((Element)element).getNodeName().equals("use") 
            && getChildren(element).length > 0;
        }

        public Object[] getElements(Object element) {
            if(((Document)element).getElementsByTagName("plugins").item(0) == null) {
                return new Object[0];
            }
            
            NodeList uses = ((Document)element).getElementsByTagName("use");
            int nbUses = uses.getLength();
            
            Object[] elts = new Object[nbUses];
            for (int i = 0; i < nbUses; i++) {
                elts[i] = uses.item(i);
            }
            return elts;
        }

        public void dispose() {}

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}

        public Image getImage(Object element) {
            Image image = null;
            try {
                String nodeType = ((Element)element).getNodeName();
                image = Icons.getImageRegistry().get(nodeType);
            }
            catch (Exception e) {
                e.printStackTrace(System.out) ;
            }
            return image;
        }

        public String getText(Object element) {
            String result = "";
            Element plugin = (Element) element;
            String id = plugin.getAttribute("id");
            String name = plugin.getAttribute("name");
            if(id != null && name != null 
                    && !id.equals("")) {
                result =  id + " : " + name;
            }
            return result != null ? result : ""; //$NON-NLS-1$
        }

        public void addListener(ILabelProviderListener listener) {}

        public boolean isLabelProperty(Object element, String property) {
            return false;
        }

        public void removeListener(ILabelProviderListener listener) {}

    }
    
    /**
     * This class is used for adding plug-ins with the context menu
     * @author Joan Chaumont
     */
    class AddPluginAction extends Action {
        
        String name;
        
        /**
         * Constructor
         * @param name
         */
        public AddPluginAction(String name) {
            this.name = name;
            
            setText(name);
            setToolTipText("Add a new " + name + " plugin to the tree");
            
            String fileName = Icons.getImageDescriptorFileName().get("use") ;
            if (fileName != null) {
                try {
                    setImageDescriptor(IsacScenarioPlugin.imageDescriptorFromPlugin(
                            IsacScenarioPlugin.PLUGIN_ID,fileName));
                } catch (Exception e) {}
            }
        }
        
        public void run() {
            Document doc = (Document)getInput();
            Element plugins = (Element)doc.getElementsByTagName("plugins").item(0);
            Element plugin = doc.createElement("use");
            plugin.setAttribute("id", ImportUtil.generatePluginId(doc, name));
            plugin.setAttribute("name", name);
            plugins.appendChild(plugin);
            ImportTableViewer.this.refresh();
        }
    }
    
    /**
     * Constructor
     * @param parent
     * @param style
     * @param scenario
     */
    public ImportTableViewer(Composite parent, int style, ScenarioManager scenario) {
        super(parent, style);

        this.scenario = scenario;
        
        ImportTableContentProvider provider = new ImportTableContentProvider();
        setContentProvider(provider);
        setLabelProvider(provider);
        
        this.setContentProvider(provider);
        this.setLabelProvider(provider);
        
        createActions();
        hookContextMenu();
    }
    
    /* These functions are for the popup menu */
    private void createActions() {
        copyAction = new CopyAction(this);
        cutAction = new CutAction(this);
        pasteAction = new PasteAction(this);
    }

    private void hookContextMenu() {
        MenuManager menuMgr = new MenuManager("#PopupMenu");
        menuMgr.setRemoveAllWhenShown(true);
        menuMgr.addMenuListener(new IMenuListener() {
            public void menuAboutToShow(IMenuManager manager) {
                fillContextMenu(manager);
            }
        });
        Menu menu = menuMgr.createContextMenu(getControl());
        getControl().setMenu(menu);
    }
    
    protected void fillContextMenu(IMenuManager contextMenu) {
        MenuManager addMenuPopup = new MenuManager("Add");
        String[] plugs = scenario.getPluginManager().getPluginsName();
        for (int i = 0; i < plugs.length; i++) {
            addMenuPopup.add(new AddPluginAction(plugs[i]));
        }
        
        contextMenu.add(addMenuPopup);
        contextMenu.add(new Separator()); //$NON-NLS-1$
        contextMenu.add(copyAction);
        contextMenu.add(cutAction);
        contextMenu.add(pasteAction);
    }
    
    /**
     * Set document in our tree view.
     * The IDocument is transform to IStructuredModel
     * @param doc
     */
    public void setDocument(IDocument doc) {
        IStructuredModel model = null;
        try {
            model = StructuredModelManager.getModelManager().getExistingModelForRead(doc);

            if (model != null && model instanceof IDOMModel) {
                Document domDoc = null;
                domDoc = ((IDOMModel) model).getDocument();
                setInput(domDoc);
            }
        }
        finally {
            if (model != null) {
                model.releaseFromRead();
            }
        }
    }
    
    /**
     * Root element getter
     * @return Element root
     */
    public Element getRootElement() {
        Document doc = (Document)getInput();
        return (Element)doc.getElementsByTagName("plugins").item(0);        
    }

    /* These functions are used by the tool tip displayer */
    /**
     * @param x 
     * @param y 
     * @return Point
     * 
     */
    public Point toDisplay(int x, int y) {
        return getTree().toDisplay(x, y);
    }
    
    /**
     * Get item under the point 
     * @param point
     * @return TreeItem
     */
    public TreeItem getItem(Point point) {
        return getTree().getItem(point);
    }
    
    /**
     * Force the focus on this composite
     * @return boolean
     */
    public boolean forceFocus() {
        return getTree().forceFocus();
    }
    
    /**
     * Add listener to our tree
     * @param eventType
     * @param listener
     */
    public void addListener(int eventType, Listener listener) {
        getTree().addListener(eventType, listener);
    }
    
    /**
     * Remove a listener from our tree
     * @param event
     * @param listener
     */
    public void removeListener(int event, Listener listener) {
        getTree().removeListener(event, listener);
    }
}
