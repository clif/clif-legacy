/*
 * CLIF is a Load Injection Framework Copyright (C) 2006 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF $Name$
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.egui.wizards.plugin;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.SimpleName;

/**
 * A visitor class which count field and action type number in AST Tree.
 * This class permits to find how many references there are in AST Tree.
 * @author Fabrice Rivart
 */
public class CountVisitor extends ASTVisitor {

	private int fieldNumber = 0;
	private int actionNumber = 0;
	private String searchName;
	private String searchAction;
	
	/**
	 * Constructor.
	 */
	public CountVisitor() {
		super();
	}

	/**
	 * @param name String representing name to search in Java Code
	 */
	public void init(String action, String name) {
		searchAction = action;
		searchName = name;
		fieldNumber = 0;
		actionNumber = 0;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.SimpleName)
	 */
	@Override
	public boolean visit(SimpleName arg0) {
		
		CharSequence cs = searchAction.toUpperCase().subSequence(0, searchAction.length());
		if (arg0.getFullyQualifiedName().contains(cs)) {
			actionNumber++;
		}
		
		if (arg0.getFullyQualifiedName().equals(searchName)) {
			fieldNumber++;
		}
		
		return super.visit(arg0);
	}

	/**
	 * Returns int a number of references in Java code.
	 * @return the number of references in Java code
	 */
	public int getFieldNumber() {
		return fieldNumber;
	}
	
	/**
	 * Returns int a number of same action in Java code.
	 * @return the number of same action in Java code
	 */
	public int getActionNumber() {
		return actionNumber;
	}

	
}
