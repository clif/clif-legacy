/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2013 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * See the "license.txt" provided along with this library or
 * the web site http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html for more details 
 * 
 * Contact : clif@ow2.org
 */

package org.ow2.isac.plugin.dnsinjector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.scenario.isac.plugin.DataProvider;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.util.ClifClassLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.AAAARecord;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.CNAMERecord;
import org.xbill.DNS.DClass;
import org.xbill.DNS.DNAMERecord;
import org.xbill.DNS.DNSKEYRecord;
import org.xbill.DNS.DNSSEC;
import org.xbill.DNS.DNSSEC.DNSSECException;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Header;
import org.xbill.DNS.MXRecord;
import org.xbill.DNS.Message;
import org.xbill.DNS.NSRecord;
import org.xbill.DNS.Name;
import org.xbill.DNS.OPTRecord;
import org.xbill.DNS.PTRRecord;
import org.xbill.DNS.RRSIGRecord;
import org.xbill.DNS.RRset;
import org.xbill.DNS.Rcode;
import org.xbill.DNS.Record;
import org.xbill.DNS.SOARecord;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.TSIG;
import org.xbill.DNS.TXTRecord;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;

/**
 * Implementation of a session object for plugin ~DnsInjector~ based on dnsjava library
 * 
 * @author Philippe Lemordant
 * 
 */
public class SessionObject implements SessionObjectAction, DataProvider, ControlAction, SampleAction, TestAction {
	static final String PLUGIN_DNSSEC_VERIFY_SIG_ARG = "dnssec_verify_sig_arg";
	static final String PLUGIN_DNSSEC_ZONE_FILE_ARG = "dnssec_zone_file_arg";
	static final String PLUGIN_SERVER_IPV6_ARG = "server_ipv6_arg";
	static final String PLUGIN_IP_SRC_ARG = "ip_src_arg";
	private static String PRODUCT = "DnsInjector";
	private static String VERSION = "1.1";
	private static String REVISION = "$Revision: 52 $";
	private static String DATE = "$Date: 2012-10-04 15:15:55 +0200 (jeu., 04 oct. 2012) $";
	private static String AUTHOR = "$Author: yplo6403 $";

	static final int TEST_DNS_ISQUERYANSWEROK = 3;
	static final int SAMPLE_DNS_QUERY = 4;
	static final String SAMPLE_DNS_QUERY_UDP_ARG = "udp_arg";
	static final String SAMPLE_DNS_QUERY_CHECKING_DISABLE_ARG = "checking_disable_arg";
	static final String SAMPLE_DNS_QUERY_DNSSEC_ARG = "dnssec_arg";
	static final String SAMPLE_DNS_QUERY_RECURSION_DESIRED_ARG = "recursion_desired_arg";
	static final String SAMPLE_DNS_QUERY_EXPECTED_RESPONSE_ARG = "expected_response_arg";
	static final String SAMPLE_DNS_QUERY_SRC_ADDRESS_ARG = "src_address_arg";
	static final String SAMPLE_DNS_QUERY_QUERY_TYPE_ARG = "query_type_arg";
	static final String SAMPLE_DNS_QUERY_TARGET_ARG = "target_arg";
	static final String PLUGIN_DELAIL_REPORT_ARG = "delail_report_arg";
	static final String PLUGIN_SIG_KEY_VALUE_ARG = "sig_key_value_arg";
	static final String PLUGIN_SIG_KEY_NAME_ARG = "sig_key_name_arg";
	static final String PLUGIN_SIG_ALGO_ARG = "sig_algo_arg";
	static final String PLUGIN_DELAY_ARG = "delay_arg";
	static final String PLUGIN_SERVER_IPV4_ARG = "server_ipv4_arg";
	static final String PLUGIN_SOURCE_ADDRESS_BASE = "Source address base";
	static final String PLUGIN_PROTOCOL = "protocol";
	static final String PLUGIN_SERVER_PORT_ARG = "server_port_arg";

	static final String DNSSEC_PROPS_DIR = "dnssec.keys.dir";

	private static final String DATAPROVIDER_RESULT = "result";

	private final static Logger logger = LoggerFactory.getLogger(SessionObject.class.getSimpleName());

	private static int totalInstanceNb = 0;
	private static boolean detailedTrace = false;
	private static Name tsig_algo = null;
	private static String tsig_key_name = null;
	private static String tsig_key_value = null;
	private static TSIG tsig_key = null;
	private static InetAddress ipV4AddressDst = null;
	private static InetAddress ipV6AddressDst = null;
	private static int delayForAnswer = 30;
	private static int serverPort = SimpleResolver.DEFAULT_PORT;
	private static OPTRecord queryOPT = null;
	private int myInstanceNb;
	private boolean success = false;
	private String result = null;

	private static Object syncObject = new Object();
	private static HashSet<InetAddress> ipSrcSet = null;
	private static HashMap<Integer, DNSKEYRecord> dnskeyMap = null;
	private static boolean verifySignature = false;

	/**
	 * Constructor for specimen object.
	 * 
	 * @param params
	 *            key-value pairs for plugin parameters
	 */
	@SuppressWarnings("unchecked")
	public SessionObject(Map<String, String> params) {
		String getParamStr;
		ipSrcSet = new HashSet<InetAddress>();
		totalInstanceNb = 0;
		logger.warn("Starting {} program at {}", PRODUCT, new Date());
		logger.warn("{} Version: {} ", PRODUCT, VERSION);
		logger.warn(PRODUCT + " {} at {}", REVISION, DATE);
		logger.debug("{} {}", PRODUCT, AUTHOR);

		getParamStr = (String) params.get(PLUGIN_DELAIL_REPORT_ARG);
		if (getParamStr != null) {
			detailedTrace = !getParamStr.equals("true");
		}
		logger.warn("Detailed report: {}", detailedTrace);

		getParamStr = (String) params.get(PLUGIN_SIG_ALGO_ARG);
		if ((getParamStr != null) && getParamStr.equals("none")) {
		} else if ((getParamStr != null) && getParamStr.equals("hmac-md5")) {
			tsig_algo = TSIG.HMAC_MD5;
		} else if ((getParamStr != null) && getParamStr.equals("hmac-sha1")) {
			tsig_algo = TSIG.HMAC_SHA1;
		} else if ((getParamStr != null) && getParamStr.equals("hmac-sha224")) {
			tsig_algo = TSIG.HMAC_SHA224;
		} else if ((getParamStr != null) && getParamStr.equals("hmac-sha256")) {
			tsig_algo = TSIG.HMAC_SHA256;
		} else if ((getParamStr != null) && getParamStr.equals("hmac-sha384")) {
			tsig_algo = TSIG.HMAC_SHA384;
		} else if ((getParamStr != null) && getParamStr.equals("hmac-sha512")) {
			tsig_algo = TSIG.HMAC_SHA512;
		}
		if (tsig_algo != null) {
			getParamStr = (String) params.get(PLUGIN_SIG_ALGO_ARG);
			if ((getParamStr != null) && (getParamStr.length() > 0)) {
				tsig_key_name = getParamStr;
				logger.warn("TSIG shared key name: {}", tsig_key_name);
				getParamStr = (String) params.get(PLUGIN_SIG_KEY_VALUE_ARG);
				if ((getParamStr != null) && (getParamStr.length() > 0)) {
					tsig_key_value = getParamStr;
					logger.warn("TSIG shared key value: {}", tsig_key_value);
					tsig_key = new TSIG(tsig_algo, tsig_key_name, tsig_key_value);
					logger.warn("DNS transactions are signed with algorithm {}", tsig_algo);
				}
			}
		}
		if (tsig_key == null) {
			logger.warn("No TSIG Signature of DNS queries");
		}

		getParamStr = (String) params.get(PLUGIN_SERVER_IPV4_ARG);
		if (getParamStr != null && getParamStr.length() != 0) {
			try {
				ipV4AddressDst = InetAddress.getByName(getParamStr);
				if (ipV4AddressDst instanceof Inet4Address == false) {
					logger.error("Bad server IPv4 address: {}", getParamStr);
					throw new IsacRuntimeException("Could not instantiate DnsInjector session object: bad server IPv4 address "
							+ getParamStr);
				}
			} catch (UnknownHostException e) {
				logger.error("Bad server IP address: {}", getParamStr);
				throw new IsacRuntimeException("Could not instantiate DnsInjector session object: bad server IP address "
						+ getParamStr, e);
			}
		}
		logger.warn("Server IPv4 address: {}", ipV4AddressDst);

		getParamStr = (String) params.get(PLUGIN_SERVER_IPV6_ARG);
		if (getParamStr != null && getParamStr.length() != 0) {
			try {
				ipV6AddressDst = InetAddress.getByName(getParamStr);
				if (ipV6AddressDst instanceof Inet6Address == false) {
					logger.error("Bad server IPv6 address: {}", getParamStr);
					throw new IsacRuntimeException("Could not instantiate DnsInjector session object: bad server IPv6 address "
							+ getParamStr);
				}
			} catch (UnknownHostException e) {
				logger.error("Bad server IP address: {}", getParamStr);
				throw new IsacRuntimeException("Could not instantiate DnsInjector session object: bad server IP address "
						+ getParamStr, e);
			}
		}
		logger.warn("Server IPv6 address: {}", ipV6AddressDst);

		if (ipV4AddressDst == null && ipV6AddressDst == null) {
			logger.error("Cannot instantiate DnsInjector: no server IP address");
			throw new IsacRuntimeException("Cannot instantiate DnsInjector: no server IP address");
		}

		getParamStr = (String) params.get(PLUGIN_SERVER_PORT_ARG);
		if (getParamStr != null) {
			try {
				serverPort = Integer.valueOf(getParamStr);
			} catch (NumberFormatException e) {
				logger.error("Bad number format for server IP port: {}", getParamStr);
				throw new IsacRuntimeException(
						"Could not instantiate DnsInjector session object: Bad number format for server IP port " + getParamStr,
						e);
			}
		}
		logger.warn("DNS server IP port: {}", serverPort);

		getParamStr = (String) params.get(PLUGIN_IP_SRC_ARG);
		if (getParamStr != null && !getParamStr.equals("")) {
			InputStream fileInputStream;
			if ((fileInputStream = ClifClassLoader.getClassLoader().getResourceAsStream(getParamStr)) != null) {
				BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));
				String strLine;
				// Read File Line By Line
				try {
					while ((strLine = br.readLine()) != null) {
						try {
							InetAddress addr = InetAddress.getByName(strLine);
							// Check if the address is defined on any interface
							if (NetworkInterface.getByInetAddress(addr) == null) {
								logger.error("Source IP address {} is not local", strLine);
								throw new IsacRuntimeException(
										"Could not instantiate DnsInjector session object: source IP address is not local "
												+ strLine);
							} else {
								if (addr.getClass() == Inet4Address.class && ipV4AddressDst != null)
									ipSrcSet.add(addr);
								else {
									if (addr.getClass() == Inet6Address.class && ipV6AddressDst != null)
										ipSrcSet.add(addr);
									else {
										logger.error("Source IP address {} is not compatible with server address", strLine);
									}
								}
							}
						} catch (SocketException e) {
							logger.error("Bad source IP address: {}", strLine);
							throw new IsacRuntimeException(
									"Could not instantiate DnsInjector session object: bad source IP address " + strLine, e);
						}
						if (ipSrcSet.size() == 0) {
							logger.error("No valid IP sources in file: {}", getParamStr);
							throw new IsacRuntimeException(
									"Could not instantiate DnsInjector session object: No valid source IP address !");
						}
					}
					fileInputStream.close();
				} catch (IOException e) {
					logger.error("Cannot load IP sources file: {}", getParamStr);
					throw new IsacRuntimeException(
							"Could not instantiate DnsInjector session object: cannot load IP sources file " + getParamStr, e);
				}
			} else {
				logger.error("Cannot locate IP sources file: {}", getParamStr);
				throw new IsacRuntimeException("Could not instantiate DnsInjector session object:cannot locate IP sources file "
						+ getParamStr);
			}
		} else {
			logger.error("No IP sources file: {}", getParamStr);
			throw new IsacRuntimeException("Could not instantiate DnsInjector session object: no IP sources file " + getParamStr);
		}

		dnskeyMap = new HashMap<Integer, DNSKEYRecord>();

		getParamStr = (String) params.get(PLUGIN_DNSSEC_VERIFY_SIG_ARG);
		if (getParamStr != null && getParamStr.equals("Verify;")) {
			verifySignature = true;
		}

		getParamStr = (String) params.get(PLUGIN_DNSSEC_ZONE_FILE_ARG);
		if (getParamStr != null && !getParamStr.equals("")) {
			InputStream fileInputStream;
			if ((fileInputStream = ClifClassLoader.getClassLoader().getResourceAsStream(getParamStr)) != null) {
				try {
					BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));
					String strLine;
					// Read File Line By Line
					while ((strLine = br.readLine()) != null) {
						if (strLine.charAt(strLine.length() - 1) != '.') {
							strLine += ".";
						}
						Name zone = null;
						try {
							zone = new Name(strLine);
						} catch (Exception e) {
							logger.error("DNSSEC zone '{}' is not valid", strLine);
							continue;
						}
						logger.warn("DNSSEC signature for zone '{}' must be verified", strLine);
						InetAddress server = ipV4AddressDst;
						if (server == null)
							server = ipV6AddressDst;
						SimpleResolver resolver = new SimpleResolver(server.getHostName());
						resolver.setPort(serverPort);
						resolver.setEDNS(0);
						if (tsig_key != null)
							resolver.setTSIGKey(tsig_key);
						Record question = Record.newRecord(zone, Type.DNSKEY, DClass.IN);
						Message query = Message.newQuery(question);
						Message response = null;
						try {
							response = resolver.send(query);
							Header header = response.getHeader();
							if (header.getRcode() == Rcode.NOERROR) {
								if (header.getCount(Section.ANSWER) > 0) {
									boolean found = false;
									RRset[] rrSets = response.getSectionRRsets(Section.ANSWER);
									for (RRset rrSet : rrSets) {
										if (rrSet.getType() == Type.DNSKEY) {
											Iterator<Record> it = rrSet.rrs();
											while (it.hasNext()) {
												Record record = it.next();
												if (record instanceof DNSKEYRecord) {
													DNSKEYRecord dnskey = (DNSKEYRecord) record;
													if ((dnskey.getFlags() & DNSKEYRecord.Flags.ZONE_KEY) == DNSKEYRecord.Flags.ZONE_KEY) {
														if ((dnskey.getFlags() & DNSKEYRecord.Flags.SEP_KEY) == 0) {
															logger.info("Zone DNSKEY RRset found for zone '{}'", dnskey.getName());
															dnskeyMap.put(dnskey.getFootprint(), dnskey);
															found = true;
														}
													}
												}
											}
										}
									}
									if (found == false) {
										logger.warn("No ZSK for zone '{}'", zone);
									}
								} else {
									logger.warn("No DNSKEY for zone '{}'", zone);
								}
							} else {
								int rcode = header.getRcode();
								logger.warn("Cannot retreive DNSKEY for zone '{}', Rcode: '{}'", zone, Rcode.string(rcode));
							}
						} catch (IOException e) {
							logger.error("Error occurred while sending or receiving query to retreive DNSKEY for zone: " + zone);
							e.printStackTrace();
						}
					}
					fileInputStream.close();
				} catch (IOException e) {
					logger.error("Cannot load DNSSEC zone file: {}", getParamStr);
					throw new IsacRuntimeException(
							"Could not instantiate DnsInjector session object, cannot load DNSSEC zone file: " + getParamStr, e);
				}
			} else {
				logger.error("Cannot locate DNSSEC zone file: {}", getParamStr);
			}
		}

		getParamStr = (String) params.get(PLUGIN_DELAY_ARG);
		if (getParamStr != null) {
			try {
				delayForAnswer = Integer.valueOf(getParamStr);
			} catch (NumberFormatException e) {
				logger.error("Bad number format for response delay: {}", getParamStr);
				throw new IsacRuntimeException(
						"Could not instantiate DnsInjector session object:Bad number format for response delay " + getParamStr, e);
			}
		}
		logger.warn("Response delay: {}", delayForAnswer);

		queryOPT = new OPTRecord(4096, 0, 0, Flags.DO, null);
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so
	 *            specimen object to clone
	 */
	private SessionObject(SessionObject so) {
		// 1. Initial the Instance
		synchronized (syncObject) {
			totalInstanceNb++;
			this.myInstanceNb = totalInstanceNb;
		}
		if (logger.isInfoEnabled()) {
			logger.info("DNS SessionObject created {}", this.myInstanceNb);
		}

	}

	@Override
	public void close() {
	}

	@Override
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	@Override
	public void reset() {
		// logger.debug( "Reset DnsInjector");
	}

	// ////////////////////////////////
	// ControlAction implementation //
	// ////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String, String> params) {
		switch (number) {
		default:
			throw new Error("Unable to find this control in ~DnsInjector~ ISAC plugin: " + number);
		}
	}

	// ///////////////////////////////
	// SampleAction implementation //
	// ///////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String, String> params, ActionEvent report) {
		try {
			switch (number) {
			case SAMPLE_DNS_QUERY:
				return sample_dns_query(params, report);
			default:
				throw new Error("Unable to find this sample in ~DnsInjector~ ISAC plugin: " + number);
			}
		} catch (Exception e) {
			report.successful = false;
			report.comment = e.getMessage();
			logger.error("Exception occurred ", e);
			return (report);
		}
	}

	/**
	 * sample_dns_query a sample to query a DNS server
	 * 
	 * @param params
	 * @param report
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private ActionEvent sample_dns_query(Map<String, String> params, ActionEvent report) {
		Name target = null;
		int queryType = Type.A;
		String expectedAnswer = null;
		boolean flagRD = true;
		boolean flagCD = false;
		boolean dnssec = false;
		boolean udp = true;
		InetAddress srcIp = null;
		String queryTypeStr = null;
		String targetStr = null;
		SimpleResolver resolver = null;
		InetAddress destIp = null;

		targetStr = (String) params.get(SAMPLE_DNS_QUERY_TARGET_ARG);
		if ((targetStr != null) && (targetStr.length() > 0)) {
			try {
				if (targetStr.charAt(targetStr.length() - 1) != '.') {
					targetStr += ".";
				}
				target = Name.fromString(targetStr);
			} catch (TextParseException e) {
				logger.error("TextParseException, bad target name {}", targetStr);
				throw new IsacRuntimeException("DnsInjector ISAC plug-in: TextParseException, bad target name " + targetStr);
			}
		} else {
			logger.error("Invalid target name {}", targetStr);
			throw new IsacRuntimeException("DnsInjector ISAC plug-in: Invalid target name " + targetStr);
		}

		queryTypeStr = (String) params.get(SAMPLE_DNS_QUERY_QUERY_TYPE_ARG);
		if (queryTypeStr != null) {
			queryType = Type.value(queryTypeStr);
			if (queryType == -1) {
				logger.error("Invalid query type: {}", queryTypeStr);
				throw new IsacRuntimeException("DnsInjector ISAC plug-in: Invalid query type " + queryTypeStr);
			}
		} else {
			logger.error("TextParseException, null query type");
			throw new IsacRuntimeException("DnsInjector ISAC plug-in: TextParseException, null query type");
		}

		String getParamStr = (String) params.get(SAMPLE_DNS_QUERY_SRC_ADDRESS_ARG);
		if (getParamStr != null) {
			try {
				srcIp = InetAddress.getByName(getParamStr);
				if (ipSrcSet.contains(srcIp)) {
					if (srcIp instanceof Inet4Address) {
						destIp = ipV4AddressDst;
						resolver = new SimpleResolver(ipV4AddressDst.getHostName());
					} else {
						destIp = ipV6AddressDst;
						resolver = new SimpleResolver(ipV6AddressDst.getHostName());
					}
					resolver.setTimeout(delayForAnswer);
					resolver.setLocalAddress(srcIp);
					resolver.setPort(serverPort);
					if (tsig_key != null)
						resolver.setTSIGKey(tsig_key);
				} else {
					logger.error("source IP address {} is not within IP allowed range", getParamStr);
					throw new IsacRuntimeException("DnsInjector ISAC plug-in: source IP address is not within IP allowed range "
							+ getParamStr);
				}
			} catch (UnknownHostException e) {
				logger.error("UnknownHostException, bad source IP address {}", getParamStr);
				throw new IsacRuntimeException("DnsInjector ISAC plug-in: UnknownHostException, bad source IP address "
						+ getParamStr);
			}
		} else {
			logger.error("IP source address is null");
			throw new IsacRuntimeException("DnsInjector ISAC plug-in: IP source address is null");
		}

		getParamStr = (String) params.get(SAMPLE_DNS_QUERY_EXPECTED_RESPONSE_ARG);
		if ((getParamStr != null) && (getParamStr.length() > 0)) {
			expectedAnswer = getParamStr;
		}

		getParamStr = (String) params.get(SAMPLE_DNS_QUERY_RECURSION_DESIRED_ARG);
		if ((getParamStr != null) && getParamStr.equals("0")) {
			flagRD = false;
		}

		getParamStr = (String) params.get(SAMPLE_DNS_QUERY_DNSSEC_ARG);
		if ((getParamStr != null) && getParamStr.equals("1")) {
			dnssec = true;
		}

		getParamStr = (String) params.get(SAMPLE_DNS_QUERY_CHECKING_DISABLE_ARG);
		if ((getParamStr != null) && getParamStr.equals("1")) {
			flagCD = true;
		}

		getParamStr = (String) params.get(SAMPLE_DNS_QUERY_UDP_ARG);
		if ((getParamStr != null) && getParamStr.equals("0")) {
			udp = false;
		}

		if (logger.isInfoEnabled()) {
			logger.info("Send DNS Query type '" + Type.string(queryType) + "' via '" + destIp.getHostAddress() + "' for '"
					+ target + "' RD=" + flagRD + " CD=" + flagCD + " D0=" + dnssec + " UDP=" + udp);
		}
		if (expectedAnswer != null)
			logger.debug("Expected answer: '{}'", expectedAnswer);
		Record question = Record.newRecord(target, queryType, DClass.IN);
		Message query = Message.newQuery(question);
		if (dnssec) {
			query.addRecord(queryOPT, Section.ADDITIONAL);
		}
		if (flagCD) {
			query.getHeader().setFlag(Flags.CD);
		}
		if (flagRD == false) {
			query.getHeader().unsetFlag(Flags.RD);
		}
		if (udp == false) {
			resolver.setTCP(true);
		}
		StringBuffer sb = new StringBuffer(targetStr);
		sb.append(" -t ");
		sb.append(queryTypeStr);
		sb.append(" via ");
		sb.append(destIp.getHostAddress());
		if (detailedTrace) {
			report.comment = sb.toString();
		}
		report.type = "dnsQuery";
		report.setDate(System.currentTimeMillis());

		Message msg = null;
		try {
			msg = resolver.send(query);
		} catch (IOException e) {
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			logger.warn("Timeout for query: '{}' after {}", sb.toString(), report.duration);
			report.result = "timeout";
			report.successful = false;
			return report;
		}
		Header header = msg.getHeader();

		report.duration = (int) (System.currentTimeMillis() - report.getDate());
		// logger.debug("difference duration: {}", (report.duration - response.getDuration()));

		report.successful = true;
		this.success = true;
		Record respRecord = null;
		RRset respRRset = null;
		if (header.getRcode() == Rcode.NOERROR) {
			if (header.getCount(Section.ANSWER) > 0) {
				RRset[] rrSets = msg.getSectionRRsets(Section.ANSWER);
				for (RRset rrSet : rrSets) {
					if (rrSet.getType() == queryType) {
						respRRset = rrSet;
						Iterator<Record> it = rrSet.rrs();
						while (it.hasNext()) {
							respRecord = it.next();
							break;
						}
						break;
					}
				}
			} else {
				if (logger.isInfoEnabled())
					logger.info("Answer to query '{}' with NOERROR and no record after '{}'", sb.toString(), report.duration);
			}
		} else {
			int rcode = header.getRcode();
			logger.warn("Answer to query '{}' with Rcode: '{}' after " + report.duration, sb.toString(), Rcode.string(rcode));
		}

		if (verifySignature && respRRset != null) {
			@SuppressWarnings("rawtypes")
			Iterator it = respRRset.sigs();
			boolean signatureIsAvailable = false;
			boolean dnskeyFound = false;
			if (it.hasNext()) {
				signatureIsAvailable = true;
			}
			while (it.hasNext()) {
				Record record = (Record) it.next();
				if (record instanceof RRSIGRecord) {
					RRSIGRecord rrsig = (RRSIGRecord) record;
					if (dnskeyMap.containsKey(rrsig.getFootprint())) {
						dnskeyFound = true;
						try {
							DNSSEC.verify(respRRset, rrsig, dnskeyMap.get(rrsig.getFootprint()));
							logger.info("DNSSEC Signature is verified OK for query '{}'", sb.toString());
						} catch (DNSSECException e) {
							logger.error("DNSSEC Signature '{}' is wrong for query '{}'", rrsig, sb.toString());
						}
						break;
					}
				}
			}
			if (signatureIsAvailable && dnskeyFound == false) {
				logger.warn("Response is signed but no DNSSEC key was found for query '{}'", sb.toString());
			}
		}

		this.result = null;
		if (respRecord != null) {
			switch (queryType) {
			case Type.A: {
				ARecord record = (ARecord) respRecord;
				this.result = record.getAddress().getHostAddress();
				if (detailedTrace) {
					report.result = this.result;
				}
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for '{}': '" + this.result + "' instead of '{}' after " + report.duration,
							sb.toString(), expectedAnswer);

				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}
			case Type.AAAA: {
				AAAARecord record = (AAAARecord) respRecord;
				this.result = record.getAddress().getHostAddress();
				if (detailedTrace) {
					report.result = this.result;
				}
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for query '{}': '" + this.result + "' instead of '{}' after "
							+ report.duration, sb.toString(), expectedAnswer);
				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}
			case Type.NS: {
				NSRecord record = (NSRecord) respRecord;
				this.result = record.getTarget().toString();
				if (detailedTrace)
					report.result = this.result;
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for query '{}': '" + this.result + "' instead of '{}' after "
							+ report.duration, sb.toString(), expectedAnswer);
				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}
			case Type.CNAME: {
				CNAMERecord record = (CNAMERecord) respRecord;
				this.result = record.getTarget().toString();
				if (detailedTrace)
					report.result = this.result;
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for query '{}': '" + this.result + "' instead of '{}' after "
							+ report.duration, sb.toString(), expectedAnswer);
				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}
			case Type.DNAME: {
				DNAMERecord record = (DNAMERecord) respRecord;
				this.result = record.getTarget().toString();
				if (detailedTrace)
					report.result = this.result;
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for query '{}': '" + this.result + "' instead of '{}' after "
							+ report.duration, sb.toString(), expectedAnswer);
				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}
			case Type.SOA: {
				SOARecord record = (SOARecord) respRecord;
				this.result = record.rdataToString();
				if (detailedTrace)
					report.result = this.result;
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for query '{}': '" + this.result + "' instead of '{}' after "
							+ report.duration, sb.toString(), expectedAnswer);
				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}
			case Type.TXT: {
				TXTRecord record = (TXTRecord) respRecord;
				ArrayList<String> list = (ArrayList<String>) record.getStrings();
				if ((list != null) && (list.size() > 0))
					this.result = list.get(0);

				if (detailedTrace)
					report.result = this.result;
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for query '{}': '" + this.result + "' instead of '{}' after "
							+ report.duration, sb.toString(), expectedAnswer);
				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}

			case Type.PTR: {
				PTRRecord record = (PTRRecord) respRecord;
				this.result = record.getTarget().toString();

				if (detailedTrace)
					report.result = this.result;
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for query '{}': '" + this.result + "' instead of '{}' after "
							+ report.duration, sb.toString(), expectedAnswer);
				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}
			case Type.MX: {
				MXRecord record = (MXRecord) respRecord;
				this.result = record.getTarget().toString();

				if (detailedTrace)
					report.result = this.result;
				if ((expectedAnswer != null) && (expectedAnswer.equals(this.result)) == false) {
					report.successful = false;
					this.success = false;
					logger.warn("Unexpected answer for query '{}': '" + this.result + "' instead of '{}' after "
							+ report.duration, sb.toString(), expectedAnswer);
				} else {
					if (logger.isInfoEnabled())
						logger.info("Answer to query '{}' is '{}' after " + report.duration, sb.toString(), this.result);
				}
				break;
			}
			default:
				if (detailedTrace)
					report.result = respRecord;
				if (logger.isInfoEnabled())
					logger.info("Answer to query '{}' is {} after " + report.duration, sb.toString(), respRecord);
				break;
			}

		} else {
			if (expectedAnswer != null) {
				report.successful = false;
				this.success = false;
				logger.warn("No matching answer for query '{}' / '{}' after " + report.duration, sb.toString(), expectedAnswer);
			}
		}
		return report;
	}

	// /////////////////////////////
	// TestAction implementation //
	// /////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String, String> params) {
		switch (number) {
		case TEST_DNS_ISQUERYANSWEROK:
			return this.success;
		default:
			throw new Error("Unable to find this test in ~DnsInjector~ ISAC plugin: " + number);
		}
	}

	@Override
	public String doGet(String var) {
		if (var.equals(DATAPROVIDER_RESULT)) {
			String doGet = "";
			if (this.result != null)
				doGet = this.result;
			return doGet;
		}
		throw new IsacRuntimeException("Unknown parameter value in ~DnsInjector~ ISAC plugin: " + var);
	}

}
