/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2013 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * See the "license.txt" provided along with this library or
 * the web site http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html for more details 
 * 
 * Contact : clif@ow2.org
 */
package org.ow2.isac.plugin.dnsprovider;

public class GreatestCommonDivisor {

	/**
	 * Calculates the Greatest Common Divisor (GCD) of several integer.
	 * 
	 * @param tab_arg
	 *            array of integers arguments.
	 * */
	public static final int greatestCommonDivisor(int[] tab_arg) {
		if (tab_arg.length < 2) {
			throw new IllegalArgumentException("There must be at least 2 arguments");
		}
		int tmp = greatestCommonDivisor(tab_arg[tab_arg.length - 1], tab_arg[tab_arg.length - 2]);
		for (int i = tab_arg.length - 3; i >= 0; i--) {
			if (tab_arg[i] < 0) {
				throw new IllegalArgumentException("Arguments must be positive or null.");
			}
			tmp = greatestCommonDivisor(tmp, tab_arg[i]);
		}
		return tmp;
	}

	/**
	 * Greatest Common Divisor (GCD) of two integers.
	 * */
	public static final int greatestCommonDivisor(int a, int b) {
		if (a < 0 || b < 0) {
			throw new IllegalArgumentException("Arguments must be positive or null.");
		}
		if (b == 0) {
			return a;
		}
		return greatestCommonDivisor(b, a % b);
	}

}
