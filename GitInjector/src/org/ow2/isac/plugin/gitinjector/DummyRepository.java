/*
* CLIF is a Load Injection Framework
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.isac.plugin.gitinjector;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import org.eclipse.jgit.internal.storage.dfs.DfsObjDatabase;
import org.eclipse.jgit.internal.storage.dfs.DfsOutputStream;
import org.eclipse.jgit.internal.storage.dfs.DfsPackDescription;
import org.eclipse.jgit.internal.storage.dfs.DfsReaderOptions;
import org.eclipse.jgit.internal.storage.dfs.DfsRefDatabase;
import org.eclipse.jgit.internal.storage.dfs.DfsRepository;
import org.eclipse.jgit.internal.storage.dfs.DfsRepositoryBuilder;
import org.eclipse.jgit.internal.storage.dfs.DfsRepositoryDescription;
import org.eclipse.jgit.internal.storage.dfs.ReadableChannel;
import org.eclipse.jgit.internal.storage.pack.PackExt;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Ref.Storage;
import org.eclipse.jgit.util.RefList;

/**
 * Git repository stored entirely in the local process memory.
 * <p>
 * This implementation builds on the DFS repository by storing all reference and
 * object data in the local process. It is not very efficient and exists only
 * for unit testing and small experiments.
 * <p>
 * The repository is thread-safe. Memory used is released only when this object
 * is garbage collected. Closing the repository has no impact on its memory.
 * @author Bruno Dillenseger
 */
public class DummyRepository extends DfsRepository
{
	private static final AtomicInteger packId = new AtomicInteger();
	private final DfsObjDatabase objdb;
	private final DfsRefDatabase refdb;
	private BigInteger dataSize = BigInteger.ZERO;

	public DummyRepository()
	{
		this(new DfsRepositoryDescription("low memory usage repository"));
	}

	/**
	 * Initialize a new in-memory repository.
	 *
	 * @param repoDesc
	 *            description of the repository.
	 * @since 2.0
	 */
	public DummyRepository(DfsRepositoryDescription repoDesc)
	{
		super(new DfsRepositoryBuilder<DfsRepositoryBuilder, DummyRepository>() {
			@Override
			public DummyRepository build() throws IOException {
				throw new UnsupportedOperationException();
			}
		}.setRepositoryDescription(repoDesc));

		objdb = new MemObjDatabase(this);
		refdb = new MemRefDatabase();
	}

	BigInteger getDataSize()
	{
		return dataSize; 
	}

	private void addDataSize(int size)
	{
		dataSize = dataSize.add(BigInteger.valueOf(size));
	}

	@Override
	public DfsObjDatabase getObjectDatabase() {
		return objdb;
	}

	@Override
	public DfsRefDatabase getRefDatabase() {
		return refdb;
	}

	private class MemObjDatabase extends DfsObjDatabase
	{
		private List<DfsPackDescription> packs = new ArrayList<DfsPackDescription>();
		private DummyRepository repo;

		MemObjDatabase(DfsRepository repo) {
			super(repo, new DfsReaderOptions());
			this.repo = (DummyRepository) repo;
		}

		@Override
		protected synchronized List<DfsPackDescription> listPacks() {
			return packs;
		}

		@Override
		protected DfsPackDescription newPack(PackSource source) {
			int id = packId.incrementAndGet();
			DfsPackDescription desc = new MemPack(
					"pack-" + id + "-" + source.name(), //$NON-NLS-1$ //$NON-NLS-2$
					getRepository().getDescription());
			return desc.setPackSource(source);
		}

		@Override
		protected synchronized void commitPackImpl(
			Collection<DfsPackDescription> desc,
			Collection<DfsPackDescription> replace)
		{
			List<DfsPackDescription> n;
			n = new ArrayList<DfsPackDescription>(desc.size() + packs.size());
			n.addAll(desc);
			n.addAll(packs);
			if (replace != null)
			{
				n.removeAll(replace);
			}
			packs = n;
			for (DfsPackDescription onePack : packs)
			{
				for (Integer size : ((MemPack)onePack).fileMap.values())
				{
					repo.addDataSize(size);
				}
			}
		}

		@Override
		protected void rollbackPack(Collection<DfsPackDescription> desc) {
			// Do nothing. Pack is not recorded until commitPack.
		}

		@Override
		protected ReadableChannel openFile(DfsPackDescription desc, PackExt ext)
		throws FileNotFoundException, IOException
		{
			MemPack memPack = (MemPack) desc;
			Integer fileSize = memPack.fileMap.get(ext);
			if (fileSize == null)
			{
				throw new FileNotFoundException(desc.getFileName(ext));
			}
			return new DummyReadableChannel(fileSize);
		}

		@Override
		protected DfsOutputStream writeFile(DfsPackDescription desc, final PackExt ext)
		throws IOException
		{
			final MemPack memPack = (MemPack) desc;
			return new Out(ext, memPack.fileMap);
		}
	}

	private static class MemPack extends DfsPackDescription
	{
		private final Map<PackExt,Integer> fileMap = new HashMap<PackExt,Integer>();

		MemPack(String name, DfsRepositoryDescription repoDesc)
		{
			super(repoDesc, name);
		}
	}

	private static class Out extends DfsOutputStream
	{
		private int dataLength = 0;
		private Map<PackExt, Integer> fileMap;
		private PackExt ext;

		Out(PackExt ext, Map<PackExt, Integer> fileMap)
		{
			this.ext = ext;
			this.fileMap = fileMap;
		}

		@Override
		public void write(byte[] buf, int off, int len)
		{
			dataLength += len;
		}

		@Override
		public int read(long position, ByteBuffer buf)
		throws IOException
		{
			throw new IOException("Read is not supported.");
		}

		@Override
		public void flush()
		{
			fileMap.put(ext, dataLength);
		}

		@Override
		public void close() {
			flush();
		}

	}

	private static class DummyReadableChannel implements ReadableChannel
	{
		private final long size;
		private long position = 0;
		private boolean closed = false;

		DummyReadableChannel(long size)
		{
			this.size = size;
		}

		@Override
		public int read(ByteBuffer dst)
		throws IOException
		{
			if (closed)
			{
				throw new ClosedChannelException();
			}
			if (position == size)
			{
				return -1;
			}
			long initialPosition = position;
			while (dst.hasRemaining() && position < size)
			{
				dst.put((byte)position++);
			}
			return (int)(position - initialPosition);
		}

		@Override
		public void close()
		{
			closed = true;
		}

		@Override
		public boolean isOpen()
		{
			return !closed;
		}

		@Override
		public long position()
		{
			return position;
		}

		@Override
		public void position(long newPosition)
		{
			position = newPosition;
		}

		@Override
		public long size()
		{
			return size;
		}

		@Override
		public int blockSize()
		{
			return 0;
		}
	}

	private class MemRefDatabase extends DfsRefDatabase {
		private final ConcurrentMap<String, Ref> refs = new ConcurrentHashMap<String, Ref>();

		MemRefDatabase() {
			super(DummyRepository.this);
		}

		@Override
		protected RefCache scanAllRefs() throws IOException {
			RefList.Builder<Ref> ids = new RefList.Builder<Ref>();
			RefList.Builder<Ref> sym = new RefList.Builder<Ref>();
			for (Ref ref : refs.values()) {
				if (ref.isSymbolic())
					sym.add(ref);
				ids.add(ref);
			}
			ids.sort();
			sym.sort();
			return new RefCache(ids.toRefList(), sym.toRefList());
		}

		@Override
		protected boolean compareAndPut(Ref oldRef, Ref newRef)
				throws IOException {
			String name = newRef.getName();
			if (oldRef == null || oldRef.getStorage() == Storage.NEW)
				return refs.putIfAbsent(name, newRef) == null;
			Ref cur = refs.get(name);
			if (cur != null && eq(cur, oldRef))
				return refs.replace(name, cur, newRef);
			else
				return false;

		}

		@Override
		protected boolean compareAndRemove(Ref oldRef) throws IOException {
			String name = oldRef.getName();
			Ref cur = refs.get(name);
			if (cur != null && eq(cur, oldRef))
				return refs.remove(name, cur);
			else
				return false;
		}

		private boolean eq(Ref a, Ref b) {
			if (a.getObjectId() == null && b.getObjectId() == null)
				return true;
			if (a.getObjectId() != null)
				return a.getObjectId().equals(b.getObjectId());
			return false;
		}
	}
}
