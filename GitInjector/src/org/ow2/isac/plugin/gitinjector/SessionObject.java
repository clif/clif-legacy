/*
* CLIF is a Load Injection Framework
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.isac.plugin.gitinjector;

import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.FetchResult;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.Map;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;

/**
 * Implementation of a session object for plugin ~GitInjector~
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, SampleAction
{
	static final int SAMPLE_FETCH = 0;
	static final String SAMPLE_FETCH_TYPE = "type";
	static final String SAMPLE_FETCH_COMMENT = "comment";
	static final String SAMPLE_FETCH_REFSPEC = "refspec";
	static final String SAMPLE_FETCH_PASSWORD = "password";
	static final String SAMPLE_FETCH_LOGIN = "login";
	static final String SAMPLE_FETCH_REMOTE = "remote";

	static final String DEFAULT_FETCH_TYPE = "GIT FETCH";
	private Git git;

	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Map<String,String> params)
	{
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
		git = new Git(new DummyRepository());
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		git.close();
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		git.close();
		git = new Git(new DummyRepository());
	}

	
	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String, String> params, ActionEvent report)
	{
		switch (number)
		{
		case SAMPLE_FETCH:
			try
			{
				report.type = params.get(SAMPLE_FETCH_TYPE);
				if (report.type == null || report.type.isEmpty())
				{
					report.type = DEFAULT_FETCH_TYPE;
				}
				report.comment = params.get(SAMPLE_FETCH_COMMENT);
				FetchCommand fetchCmd = git.fetch();
				fetchCmd.setRemote(params.get(SAMPLE_FETCH_REMOTE));
				fetchCmd.setCredentialsProvider(
					new UsernamePasswordCredentialsProvider(
						params.get(SAMPLE_FETCH_LOGIN),
						params.get(SAMPLE_FETCH_PASSWORD)));
				fetchCmd.setRefSpecs(new RefSpec(params.get(SAMPLE_FETCH_REFSPEC)));
				report.setDate(System.currentTimeMillis());
				FetchResult result = fetchCmd.call();
				report.successful = true;
				if (report.comment == null || report.comment.isEmpty())
				{
					report.comment = result.getAdvertisedRef(params.get(SAMPLE_FETCH_REFSPEC)).toString();
				}
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
				report.result = ((DummyRepository)git.getRepository()).getDataSize().toString() + " bytes read";
			}
			catch (Exception ex)
			{
				report.successful = false;
				if (report.comment == null || report.comment.isEmpty())
				{
					report.comment = params.get(SAMPLE_FETCH_REFSPEC);
				}
				report.duration = (int)(System.currentTimeMillis() - report.getDate());
				report.result = ex.getMessage();
			}
			break;
		default:
			throw new Error("No sample " + number + " in ~GitInjector~ ISAC plugin");
		}
		return report;
	}
}
