/*
* CLIF is a Load Injection Framework
* Copyright (C) 2009,2012-2013 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.isac.plugin.ftpinjector;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import java.util.Hashtable;
import java.util.List;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Error;
import java.net.InetAddress;
import java.util.Map;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.scenario.isac.plugin.SampleAction;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.scenario.isac.plugin.TestAction;
import org.ow2.clif.scenario.isac.util.ParameterParser;
import org.ow2.clif.scenario.isac.plugin.ControlAction;
import org.ow2.clif.util.Random;
import org.ow2.clif.scenario.isac.plugin.DataProvider;

/**
 * Implementation of a session object for plugin ~FtpInjector~
 * @author Bruno Dillenseger
 */
public class SessionObject implements SessionObjectAction, SampleAction, TestAction, ControlAction, DataProvider
{
	protected enum LimitPolicy { NONE, SIZE, TIME, ANY, BOTH};
	static final int CONTROL_LCD = 4;
	static final String CONTROL_LCD_PATH = "path";
	static final int TEST_CONNECTED = 3;
	static final String TEST_CONNECTED_OPTIONS = "options";
	static final int TEST_LOGGEDIN = 7;
	static final String TEST_LOGGEDIN_OPTIONS = "options";
	static final int TEST_REPLYCODEIS = 9;
	static final String TEST_REPLYCODEIS_OPTIONS = "options";
	static final String TEST_REPLYCODEIS_CODE = "code";
	static final int SAMPLE_CONNECT = 0;
	static final String SAMPLE_CONNECT_COMMENT = "comment";
	static final String SAMPLE_CONNECT_LOCALADDRESS = "localaddress";
	static final String SAMPLE_CONNECT_PORT = "port";
	static final String SAMPLE_CONNECT_HOST = "host";
	static final int SAMPLE_STORE = 1;
	static final String SAMPLE_STORE_REMOTE_FILE = "remote file";
	static final String SAMPLE_STORE_TIME = "time";
	static final String SAMPLE_STORE_SIZE = "size";
	static final String SAMPLE_STORE_LIMIT = "limit";
	static final String SAMPLE_STORE_FILE = "file";
	static final String SAMPLE_STORE_CONTENT = "content";
	static final int SAMPLE_DISCONNECT = 2;
	static final String SAMPLE_DISCONNECT_COMMENT = "comment";
	static final int SAMPLE_CD = 5;
	static final String SAMPLE_CD_PATH = "path";
	static final int SAMPLE_LOGIN = 6;
	static final String SAMPLE_LOGIN_COMMENT = "comment";
	static final String SAMPLE_LOGIN_PASSWORD = "password";
	static final String SAMPLE_LOGIN_LOGIN = "login";
	static final int SAMPLE_LOGOUT = 8;
	static final String SAMPLE_LOGOUT_COMMENT = "comment";
	static final int SAMPLE_NOOP = 10;
	static final String SAMPLE_NOOP_COMMENT = "comment";
	static final int SAMPLE_SETFILETYPE = 11;
	static final String SAMPLE_SETFILETYPE_FORMAT = "format";
	static final String SAMPLE_SETFILETYPE_BYTE_SIZE = "byte size";
	static final String SAMPLE_SETFILETYPE_TYPE = "type";
	static final int SAMPLE_RETRIEVE = 12;
	static final String SAMPLE_RETRIEVE_TIME = "time";
	static final String SAMPLE_RETRIEVE_SIZE = "size";
	static final String SAMPLE_RETRIEVE_POLICY = "policy";
	static final String SAMPLE_RETRIEVE_LOCAL_FILE = "local file";
	static final String SAMPLE_RETRIEVE_DESTINATION = "destination";
	static final String SAMPLE_RETRIEVE_REMOTE_FILE = "remote file";


	static final int SAMPLE_DELETE = 13;
	static final String SAMPLE_DELETE_FILE = "file";


	/**
	 * Format a global reply string from a set of replies received by the given FTP client.
	 * @param ftp the FTP client from which to get the FTP reply strings
	 * @return a single string possibly containing several replies,
	 * separated by a "|" character.
	 */
	static protected String replyString(FTP ftp)
	{
		String result = ftp.getReplyString();
		if (result == null)
		{
			result = String.valueOf(ftp.getReplyCode());
		}
		else
		{
			result = result.replace("\r\n", "|");
			if (result.endsWith("|"))
			{
				result = result.substring(0, result.length()-1);
			}
		}
		return result;
	}


	protected FTPClient ftp = null; // null also means not connected
	protected boolean loggedIn = false;
	protected int lastReplyCode = -1;
	protected String lastReplyString = "";
	protected File currentDirectory = new File(".");


	//////////////////
	// Constructors //
	//////////////////


	/**
	 * Constructor for specimen object.
	 *
	 * @param params key-value pairs for plugin parameters
	 */
	public SessionObject(Hashtable<String, String> params)
	{
	}

	/**
	 * Copy constructor (clone specimen object to get session object).
	 *
	 * @param so specimen object to clone
	 */
	private SessionObject(SessionObject so)
	{
	}


	////////////////////////////////////////
	// SessionObjectAction implementation //
	////////////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#createNewSessionObject()
	 */
	public Object createNewSessionObject() {
		return new SessionObject(this);
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#close()
	 */
	public void close()
	{
		if (ftp != null && ftp.isConnected())
		{
			try
			{
				ftp.disconnect();
			}
			catch (Exception ex)
			{
				throw new IsacRuntimeException("Warning: cannot disconnect FTP client.", ex);
			}
		}
		ftp = null;
		loggedIn = false;
		lastReplyCode = -1;
		lastReplyString = "";
		currentDirectory = new File(".");
	}

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SessionObjectAction#reset()
	 */
	public void reset()
	{
		ftp = null;
		loggedIn = false;
		lastReplyCode = -1;
		lastReplyString = "";
		currentDirectory = new File(".");
	}


	/////////////////////////////////
	// SampleAction implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.SampleAction#doSample()
	 */
	public ActionEvent doSample(int number, Map<String,String> params, ActionEvent report) {
		switch (number) {
			case SAMPLE_DELETE:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP delete is not possible when not connected.");
				}
				sampleDelete(params, report);
				break;
			case SAMPLE_RETRIEVE:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP retrieve is not possible when not connected.");
				}
				sampleRetrieve(params, report);
				break;
			case SAMPLE_SETFILETYPE:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP setFileType is not possible when not connected.");
				}
				sampleSetFileType(params, report);
				break;
			case SAMPLE_NOOP:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP noop is not possible when not connected.");
				}
				sampleNoOp(params, report);
				break;
			case SAMPLE_LOGOUT:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP logout is not possible when not connected.");
				}
				sampleLogout(params, report);
				break;
			case SAMPLE_LOGIN:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP login is not possible when not connected.");
				}
				sampleLogin(params, report);
				break;
			case SAMPLE_CD:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP cd is not possible when not connected.");
				}
				sampleCd(params, report);
				break;
			case SAMPLE_DISCONNECT:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP disconnect is not possible when not connected.");
				}
				sampleDisconnect(params, report);
				break;
			case SAMPLE_STORE:
				if (ftp == null || !ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP store is not possible when not connected.");
				}
				sampleStore(params, report);
				break;
			case SAMPLE_CONNECT:
				if (ftp != null && ftp.isConnected())
				{
					throw new IsacRuntimeException("FTP connect is not possible when already connected.");
				}
				sampleConnect(params, report);
				break;
			default:
				throw new Error("Unable to find this sample in ~FtpInjector~ ISAC plugin: " + number);
		}
		return report;
	}


	/**
	 * Actual implementation of sample delete
	 */
	private void sampleDelete(Map<String,String> params, ActionEvent report)
	{
		String file = params.get(SAMPLE_DELETE_FILE);
		report.setDate(System.currentTimeMillis());
		try
		{
			ftp.deleteFile(file);
			report.duration = (int)(System.currentTimeMillis() - report.getDate());
			report.successful = true;
			lastReplyCode = ftp.getReplyCode();
			lastReplyString = replyString(ftp);
			report.result = lastReplyString;
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.comment = file;
		report.type = "FTP delete";
	}


	/**
	 * Actual implementation of sample connect
	 */
	private void sampleConnect(Map<String,String> params, ActionEvent report)
	{
		String host = params.get(SAMPLE_CONNECT_HOST);
		String portStr = params.get(SAMPLE_CONNECT_PORT);
		String localAddr = params.get(SAMPLE_CONNECT_LOCALADDRESS);
		ftp = new FTPClient();
		report.type = "FTP connection";
		report.setDate(System.currentTimeMillis());
		try
		{
			if (localAddr == null || localAddr.length() == 0)
			{
				ftp.connect(host, Integer.parseInt(portStr));
			}
			else
			{
				ftp.connect(
					host,
					Integer.parseInt(portStr),
					InetAddress.getByAddress(localAddr, new byte[4]),
					0);
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			lastReplyCode = ftp.getReplyCode();
			lastReplyString = replyString(ftp);
			if (! FTPReply.isPositiveCompletion(lastReplyCode))
			{
		        ftp.disconnect();
			}
			report.successful = true;
			report.result = lastReplyString;
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.comment = params.get(SAMPLE_CONNECT_COMMENT);		
	}


	/**
	 * Actual implementation of sample store
	 */
	private void sampleStore(Map<String,String> params, ActionEvent report)
	{
		InputStream input;
		if (ParameterParser.getRadioGroup(params.get(SAMPLE_STORE_CONTENT)).equals("generated"))
		{
			input = new BufferedInputStream(new Random().getInputStream());
		}
		else
		{
			String inputFileStr = params.get(SAMPLE_STORE_FILE);
			File inputFile = new File(currentDirectory, inputFileStr);
			try
			{
				input = new BufferedInputStream(new FileInputStream(inputFile));
			}
			catch(IOException ex)
			{
				throw new IsacRuntimeException("FTP store can not find local file " + inputFile);
			}
		}
		LimitPolicy policy = LimitPolicy.NONE;
		String policyStr = ParameterParser.getRadioGroup(params.get(SAMPLE_STORE_LIMIT));
		if (policyStr.equals("size"))
		{
			policy = LimitPolicy.SIZE;
		}
		else if (policyStr.equals("time"))
		{
			policy = LimitPolicy.TIME;
		}
		else if (policyStr.equals("size or time"))
		{
			policy = LimitPolicy.ANY;
		}
		else if (policyStr.equals("size and time"))
		{
			policy = LimitPolicy.BOTH;
		}
		long maxBytes = -1;
		if (policy != LimitPolicy.NONE && policy != LimitPolicy.TIME)
		{
			maxBytes = Long.parseLong(params.get(SAMPLE_STORE_SIZE));
		}
		int timeOut = -1;
		if (policy != LimitPolicy.NONE && policy != LimitPolicy.SIZE)
		{
			timeOut = Integer.parseInt(params.get(SAMPLE_STORE_TIME));
		}
		String remoteFile = params.get(SAMPLE_STORE_REMOTE_FILE);
		long startTime = System.currentTimeMillis();
		report.setDate(startTime);
		try
		{
			OutputStream output = ftp.storeFileStream(remoteFile);
			lastReplyCode = ftp.getReplyCode();
			if (output != null && (FTPReply.isPositiveIntermediate(lastReplyCode) || FTPReply.isPositivePreliminary(lastReplyCode)))
			{
				output = new BufferedOutputStream(output);
				long bytes = 0l;
				int val = input.read();
				while (val != -1 && evalStoreLimitPolicy(bytes, policy, startTime, maxBytes, timeOut))
				{
					output.write(val);
					++bytes;
					val = input.read();
				}
				output.close();
				input.close();
				if(ftp.completePendingCommand())
				{
					report.duration = (int) (System.currentTimeMillis() - startTime); 
					report.successful = true;
					lastReplyCode = ftp.getReplyCode();
					lastReplyString = replyString(ftp);
					report.result = lastReplyString;
					report.comment = bytes + " bytes sent";
				}
				else
				{
					report.successful = false;
					lastReplyCode = ftp.getReplyCode();
					lastReplyString = replyString(ftp);
					report.result = lastReplyString;
					report.comment = remoteFile;
				}
			}
			else
			{
				report.successful = false;
				lastReplyCode = ftp.getReplyCode();
				lastReplyString = replyString(ftp);
				report.result = lastReplyString;
				report.comment = remoteFile;
				input.close();
				if (output != null)
				{
					try
					{
						output.close();
					}
					catch (Exception ex)
					{
						// never mind
					}
				}
			}
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.type = "FTP store";		
	}


	/**
	 * Actual implementation of sample disconnect
	 */
	private void sampleDisconnect(Map<String,String> params, ActionEvent report)
	{
		report.setDate(System.currentTimeMillis());
		try
		{
			ftp.disconnect();
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			lastReplyCode = ftp.getReplyCode();
			lastReplyString = replyString(ftp);
			report.successful = true;
			report.result = lastReplyString;
			ftp = null;
		}
		catch (IOException ex)
		{
			report.result = ex.toString();
			report.successful = false;
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.type = "FTP disconnect";
		report.comment = params.get(SAMPLE_DISCONNECT_COMMENT);		
	}


	/**
	 * Actual implementation of sample cd
	 */
	private void sampleCd(Map<String,String> params, ActionEvent report)
	{
		String newDir = params.get(SAMPLE_CD_PATH);
		report.setDate(System.currentTimeMillis());
		try
		{
			ftp.changeWorkingDirectory(newDir);
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			lastReplyCode = ftp.getReplyCode();
			lastReplyString = replyString(ftp);
			report.successful = true;
			report.result = lastReplyString;
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.type = "FTP cd";
		report.comment = newDir;		
	}


	/**
	 * Actual implementation of sample login
	 */
	private void sampleLogin(Map<String,String> params, ActionEvent report)
	{
		String login = params.get(SAMPLE_LOGIN_LOGIN);
		String passwd = params.get(SAMPLE_LOGIN_PASSWORD);
		report.setDate(System.currentTimeMillis());
		try
		{
			if (! loggedIn)
			{
				loggedIn = ftp.login(login, passwd);
			}
			else
			{
				ftp.login(login, passwd);
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			lastReplyCode = ftp.getReplyCode();
			lastReplyString = replyString(ftp);
			report.result = lastReplyString;
			report.successful = true;
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.type = "FTP login";
		report.comment = params.get(SAMPLE_LOGIN_COMMENT);	
	}


	/**
	 * Actual implementation of sample logout
	 */
	private void sampleLogout(Map<String,String> params, ActionEvent report)
	{
		report.setDate(System.currentTimeMillis());
		try
		{
			if (loggedIn)
			{
				loggedIn = ! ftp.logout();
			}
			else
			{
				ftp.logout();
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			lastReplyCode = ftp.getReplyCode();
			lastReplyString = replyString(ftp);
			report.result = lastReplyString;
			report.successful = true;
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.type = "FTP logout";
		report.comment = params.get(SAMPLE_LOGOUT_COMMENT);		
	}


	/**
	 * Actual implementation of sample noop
	 */
	private void sampleNoOp(Map<String,String> params, ActionEvent report)
	{		
		report.setDate(System.currentTimeMillis());
		try
		{
			ftp.sendNoOp();
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			lastReplyCode = ftp.getReplyCode();
			lastReplyString = replyString(ftp);
			report.result = lastReplyString;
			report.successful = true;
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.type = "FTP noop";
		report.comment = params.get(SAMPLE_NOOP_COMMENT);	
	}


	/**
	 * Actual implementation of sample setFileType
	 */
	private void sampleSetFileType(Map<String,String> params, ActionEvent report)
	{
		// process file type parameter
		String fileTypeStr = ParameterParser.getCombo(params.get(SAMPLE_SETFILETYPE_TYPE));
		int fileType = -1;
		if (fileTypeStr.equals("ASCII"))
		{
			fileType = FTP.ASCII_FILE_TYPE;
		}
		else if (fileTypeStr.equals("binary"))
		{
			fileType = FTP.BINARY_FILE_TYPE;
		}
		else if (fileTypeStr.equals("EBCDIC"))
		{
			fileType = FTP.EBCDIC_FILE_TYPE;
		}
		else if (fileTypeStr.equals("local"))
		{
			fileType = FTP.LOCAL_FILE_TYPE;
		}
		// process text format parameter
		String textFormatStr = ParameterParser.getCombo(params.get(SAMPLE_SETFILETYPE_FORMAT));
		int textFormat = -1;
		if (textFormatStr.equals("non print"))
		{
			textFormat = FTP.NON_PRINT_TEXT_FORMAT;
		}
		else if (textFormatStr.equals("telnet"))
		{
			textFormat = FTP.TELNET_TEXT_FORMAT;
		}
		else if (textFormatStr.equals("carriage control"))
		{
			textFormat = FTP.CARRIAGE_CONTROL_TEXT_FORMAT;
		}
		// process byte size parameter
		String byteSizeStr = params.get(SAMPLE_SETFILETYPE_BYTE_SIZE);
		int byteSize = -1;
		if (byteSizeStr.length() > 0)
		{
			byteSize = Integer.parseInt(byteSizeStr);
		}
		// sample execution
		report.setDate(System.currentTimeMillis());
		try
		{
			switch (fileType)
			{
				case FTP.BINARY_FILE_TYPE:
					ftp.setFileType(fileType);
					break;
				case FTP.LOCAL_FILE_TYPE:
					ftp.setFileType(fileType, byteSize);
					break;
				default:
					ftp.setFileType(fileType, textFormat);
			}
			report.duration = (int) (System.currentTimeMillis() - report.getDate());
			lastReplyCode = ftp.getReplyCode();
			lastReplyString = replyString(ftp);
			report.type = "FTP type";
			report.result = lastReplyString;
			report.successful = true;
			report.comment = fileTypeStr + "/" + textFormatStr;
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
	}


	/**
	 * Actual implementation of sample retrieve
	 */
	private void sampleRetrieve(Map<String,String> params, ActionEvent report)
	{
		OutputStream output = null;
		String remoteFile = params.get(SAMPLE_RETRIEVE_REMOTE_FILE);
		if (ParameterParser.getRadioGroup(params.get(SAMPLE_RETRIEVE_DESTINATION)).equals("write to local file"))
		{
			String localFileStr = params.get(SAMPLE_RETRIEVE_LOCAL_FILE);
			if (localFileStr.length() == 0)
			{
				localFileStr = remoteFile;
			}
			File localFile = new File(currentDirectory, localFileStr);
			try
			{
				output = new BufferedOutputStream(new FileOutputStream(localFile));
			}
			catch(IOException ex)
			{
				throw new IsacRuntimeException("FTP retrieve can not write to file " + localFile);
			}
		}
		LimitPolicy policy = LimitPolicy.NONE;
		String policyStr = ParameterParser.getRadioGroup(params.get(SAMPLE_RETRIEVE_POLICY));
		if (policyStr.equals("size"))
		{
			policy = LimitPolicy.SIZE;
		}
		else if (policyStr.equals("time"))
		{
			policy = LimitPolicy.TIME;
		}
		else if (policyStr.equals("size or time"))
		{
			policy = LimitPolicy.ANY;
		}
		else if (policyStr.equals("size and time"))
		{
			policy = LimitPolicy.BOTH;
		}
		long maxBytes = -1;
		if (policy != LimitPolicy.NONE && policy != LimitPolicy.TIME)
		{
			maxBytes = Long.parseLong(params.get(SAMPLE_RETRIEVE_SIZE));
		}
		int timeOut = -1;
		if (policy != LimitPolicy.NONE && policy != LimitPolicy.SIZE)
		{
			timeOut = Integer.parseInt(params.get(SAMPLE_RETRIEVE_TIME));
		}
		long startTime = System.currentTimeMillis();
		report.setDate(startTime);
		try
		{
			InputStream input = ftp.retrieveFileStream(remoteFile);
			if (input == null)
			{
				report.successful = false;
				report.result = replyString(ftp);
				report.comment = remoteFile;
			}
			else
			{
				BufferedInputStream bufInput = new BufferedInputStream(input);
				long bytes = 0l;
				int val = bufInput.read();
				while (val != -1 && evalStoreLimitPolicy(bytes, policy, startTime, maxBytes, timeOut))
				{
					if (output != null)
					{
						output.write(val);
					}
					++bytes;
					val = bufInput.read();
				}
				bufInput.close();
				ftp.completePendingCommand();
				report.duration = (int) (System.currentTimeMillis() - startTime);
				report.successful = true;
				lastReplyCode = ftp.getReplyCode();
				lastReplyString = replyString(ftp);
				report.result = lastReplyString;
				report.comment = bytes + " bytes received";
			}
			if (output != null)
			{
				output.close();
			}
		}
		catch (IOException ex)
		{
			report.successful = false;
			report.result = ex.toString();
			lastReplyCode = -1;
			lastReplyString = "";
		}
		report.type = "FTP retrieve";		
	}

	/**
	 * Evaluates if the FTP store termination condition is met. 
	 * @param bytes number of bytes already sent
	 * @param policy limit policy (NONE, SIZE, TIME, ANY, BOTH)
	 * @param startTime time when the transfer started (ignored if not relevant to the policy)
	 * @param maxBytes maximum number of bytes to transfer (ignored if not relevant to the policy)
	 * @param timeOut maximum transfer duration (ignored if not relevant to the policy)
	 * @return true if the transfer should go on, false if it should stop.
	 */
	private boolean evalStoreLimitPolicy(
		long bytes,
		LimitPolicy policy,
		long startTime,
		long maxBytes,
		int timeOut)
	{
		switch (policy)
		{
			case SIZE:
				return bytes < maxBytes;
			case TIME:
				return System.currentTimeMillis() < startTime + timeOut;
			case ANY:
				return bytes < maxBytes && System.currentTimeMillis() < startTime + timeOut;
			case BOTH:
				return bytes < maxBytes || System.currentTimeMillis() < startTime + timeOut;
			default:
				return true;
		}
	}


	///////////////////////////////
	// TestAction implementation //
	///////////////////////////////


	/**
	 * @see org.ow2.clif.scenario.isac.plugin.TestAction#doTest()
	 */
	public boolean doTest(int number, Map<String,String> params) {
		switch (number) {
			case TEST_REPLYCODEIS:
			{
				List<String> options = ParameterParser.getCheckBox(params.get(TEST_REPLYCODEIS_OPTIONS));
				boolean equals;
				try
				{
					equals = Integer.parseInt(params.get(TEST_REPLYCODEIS_CODE)) == lastReplyCode;
				}
				catch (NumberFormatException ex)
				{
					equals = String.valueOf(lastReplyCode).startsWith(
						(params.get(TEST_REPLYCODEIS_CODE)).substring(0, 1));
					
				}
				return options.contains("not") ? !equals : equals;
			}
			case TEST_LOGGEDIN:
			{
				List<String> options = ParameterParser.getCheckBox(params.get(TEST_LOGGEDIN_OPTIONS));
				return options.contains("not") ? !loggedIn : loggedIn;
			}
			case TEST_CONNECTED:
			{
				List<String> options = ParameterParser.getCheckBox(params.get(TEST_CONNECTED_OPTIONS));
				return options.contains("not") ? ftp == null : ftp != null;
			}
			default:
				throw new Error(
					"Unable to find this test in ~FtpInjector~ ISAC plugin: " + number);
		}
	}

	
	//////////////////////////////////
	// ControlAction implementation //
	//////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.ControlAction#doControl()
	 */
	public void doControl(int number, Map<String,String> params)
	{
		switch (number) {
			case CONTROL_LCD:
				String pathStr = params.get(CONTROL_LCD_PATH);
				File path = new File(pathStr);
				try
				{
					if (path.isAbsolute())
					{
						currentDirectory = path;
					}
					else
					{
						currentDirectory = new File(currentDirectory, pathStr);
					}
					currentDirectory = currentDirectory.getCanonicalFile();
				}
				catch (IOException ex)
				{
					throw new IsacRuntimeException("Problem with " + CONTROL_LCD_PATH + " in FTP lcd: " + path, ex);
				}
				break;
			default:
				throw new Error("Unable to find this control in ~FtpInjector~ ISAC plugin: " + number);
		}
	}


	/////////////////////////////////
	// DataProvider implementation //
	/////////////////////////////////

	/**
	 * @see org.ow2.clif.scenario.isac.plugin.DataProvider#doGet()
	 */
	public String doGet(String var)
	{
		if (var.equalsIgnoreCase("reply code"))
		{
			return String.valueOf(lastReplyCode);
		}
		else if (var.equalsIgnoreCase("reply string"))
		{
			return lastReplyString;
		}
		throw new IsacRuntimeException(
			"Unknown parameter value in ~FtpInjector~ ISAC plugin: " + var);
	}
}
