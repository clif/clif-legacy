#CLIF test plan
#Mon Feb 26 19:11:30 CET 2007
blade.4.id=1
blade.1.argument=1000 120 sda
blade.4.comment=for Windows, Linux or MacOSX
blade.2.comment=generic JVM probe
blade.1.comment=check disk name
blade.4.argument=1000 120
blade.4.probe=org.ow2.clif.probe.memory.Insert
blade.3.server=local host
blade.1.server=local host
blade.0.id=3
blade.2.argument=1000 120
blade.0.probe=org.ow2.clif.probe.network.Insert
blade.1.id=5
blade.3.comment=for Windows, Linux or MacOSX
blade.0.argument=1000 120 eth0
blade.2.id=2
blade.0.comment=check network adapter name
blade.3.argument=1000 120
blade.4.server=local host
blade.3.probe=org.ow2.clif.probe.cpu.Insert
blade.3.id=0
blade.2.server=local host
blade.2.probe=org.ow2.clif.probe.jvm.Insert
blade.0.server=local host
blade.1.probe=org.ow2.clif.probe.disk.Insert
