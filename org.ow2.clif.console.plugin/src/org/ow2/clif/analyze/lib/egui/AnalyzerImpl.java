/*
* CLIF is a Load Injection Framework
* Copyright (C) 2007 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF Jordan BRUNIER Gr�gory CALONNIER
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.egui;


import java.io.Serializable;
import java.util.Properties;
import org.ow2.clif.analyze.api.AnalyzerExternalAccess;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.BladeFilter;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;
import org.ow2.clif.supervisor.api.ClifException;
import org.objectweb.fractal.api.control.BindingController;



/**
 * Implementation of Analyzer for Eclipse GUI.
 * @author Grégory CALONNIER
 * @author Jordan BRUNIER
 */
public class AnalyzerImpl implements BindingController, AnalyzerExternalAccess{

	// list of client interfaces names
	static private String[] itfList = new String[] { StorageRead.STORAGE_READ };
	
	// field for client binding (access to stored data)
	private StorageRead srItf;
	
	
	/**
	 * Constructor
	 */
	public AnalyzerImpl()
	{
	}
	
	
	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(StorageRead.STORAGE_READ))
		{
			return srItf;
		}
		else
		{
			return null;
		}
	}


	public synchronized void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(StorageRead.STORAGE_READ))
		{
			srItf = (StorageRead) serverItf;
		}
	}


	public synchronized void unbindFc(String clientItfName)
	{
		if (clientItfName.equals(StorageRead.STORAGE_READ))
		{
			srItf = null;
		}
	}


	public String[] listFc()
	{
		return itfList;
	}
	    
    
	//////////////////////////////////////
	// interface AnalyzerExternalAccess //
	//////////////////////////////////////     
    
    /**
	 * Gets a list of test runs matching a given filter.
	 * @param filter the test run filter. If null, all available test runs are returned.
	 * @return an array of test run descriptors
	 */
	public TestDescriptor[] getTests(TestFilter filter) throws ClifException
    {
	    return srItf.getTests(filter);
    }
    
	/**
	 * Gets the java system properties for the given blade from the given test run
	 * @param testName the test run name
	 * @param bladeId the blade identifier
	 * @return the Java system properties for the given blade
	 * @see TestDescriptor#getName()
	 * @see BladeDescriptor#getId()
	 */
	public Properties getBladeProperties(String testName, String bladeId) throws ClifException
	{ 
		return srItf.getBladeProperties(testName, bladeId);
	}
	/**
	 * Gets the labels of fields hold by a given event type.
	 * @param testName the test run name
	 * @param bladeId the blade identifier from the given test run
	 * @param eventTypeLabel the label of the event type
	 * @return an array of event field labels provided by the given event type
	 */
	public String[] getEventFieldLabels(String testName, String bladeId, String eventTypeLabel)
	{ 
		return srItf.getEventFieldLabels(testName, bladeId, eventTypeLabel);
	}

	/**
	 * Creates an event iterator.
	 * @param testName the test name to retrieve events from
	 * @param bladeId the blade identifier in this test to retrieve events from
	 * @param eventTypeLabel the type label of the retrieved events
	 * @param filter the filter object to be used for event selection. If null, the iterator
	 * will return all events. The filter object may throw a NoMoreEvent exception, in which
	 * case current iterator step stops and currently selected events are returned.
	 * @return the iterator key to be used to iterate on getting events, and then to discard
	 * the iterator when done
	 * @throws ClifException the given test, blade, or event type could not be found
	 * @see #getNextEvents(Serializable, int)
	 * @see #closeEventIterator(Serializable)
	 */
	public Serializable getEventIterator(String testName, String bladeId, String eventTypeLabel, EventFilter filter) throws ClifException
	{ 
		return srItf.getEventIterator(testName, bladeId, eventTypeLabel, filter);
	}

	/**
	 * Gets next events from the given event iterator.
	 * @param iteratorKey the key for the target event iterator
	 * @param count the number of event to get (at most)
	 * @return selected events
	 * @throws ClifException
	 * @see #getEventIterator(String, String, String, EventFilter)
	 * @see #closeEventIterator(Serializable)
	 */
	public BladeEvent[] getNextEvents(Serializable iteratorKey, int count) throws ClifException
	{ 
		return srItf.getNextEvents(iteratorKey, count);
	}
	
	/**
	 * Discards the iterator associated to the given key, possibly releasing resources. 
	 * @param iteratorKey the key for the target event iterator
	 */
	public void closeEventIterator(Serializable iteratorKey)
	{ 
		srItf.closeEventIterator(iteratorKey);
	}
	
	/**
	 * Counts the number of available events matching a given filter.
 	 * @param testName the test name to retrieve events from
	 * @param bladeId the blade identifier in this test to retrieve events from
	 * @param eventTypeLabel the type label of the retrieved events
	 * @param filter the filter object to be used for event selection. If null, all events
	 * are counted. Counting stops before completion if the filter object throws a NoMoreEvent
	 * exception, in which case count value before exception is returned.
	 * @return the number of available blade events matching the provided filter.
	 */
	public long countEvents(String testName, String bladeId, String eventTypeLabel, EventFilter filter) throws ClifException
	{ 
		return srItf.countEvents(testName, bladeId, eventTypeLabel, filter);
	}


	/* (non-Javadoc)
     * @see org.ow2.clif.analyze.api.AnalyzerExternalAccess#getTestPlan(java.lang.String, org.ow2.clif.storage.api.BladeFilter)
     */
    public BladeDescriptor[] getTestPlan(String testName, BladeFilter filter) throws ClifException {
    	return srItf.getTestPlan(testName, filter);
    }
	
}
