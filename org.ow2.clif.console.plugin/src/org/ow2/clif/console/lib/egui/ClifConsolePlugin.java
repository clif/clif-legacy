/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright 2006, 2009-2012 France Telecom
 * Copyright 2017 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.deploy.ClifRegistry;
import org.ow2.clif.server.lib.ClifServerImpl;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.Network;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.Writer;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * The CLIF Console plug-in class (id "org.ow2.clif.console.plugin")
 *
 * @author Manuel AZEMA
 * @author Bruno Dillenseger
 * @author Florian Francheteau
 */
public class ClifConsolePlugin extends AbstractUIPlugin
{
	/**
	 * The CLIF initialization file (in workspace's .metadata directory)
	 */
	static private final String CLIF_INI = "clif.ini";

	static private final String INI_NETWORK_PROP = "network.address";

	/**
	 * The CLIF properties file name in projects
	 */
	static public final String CLIF_PROPS = "clif.props";
	
	/**
	 * CLIF options file replacing clif.props (format/contents slightly differs)
	 */
	static public final String CLIF_OPTS = "clif.opts";

	/**
	 * Fractal ADL definition file of the CLIF application to deploy
	 */

	static public final String CLIF_APPLICATION = "org.ow2.clif.console.lib.egui.ClifApp";

	/** name of the CLIF application component in the registry */
	static public final String CLIFAPP_NAME = "console";

	/**
	 * The singleton instance.
	 */
	private static ClifConsolePlugin plugin;

	private String networkAddr = "localhost";

	/**
	 * Currently connected registry
	 */
	private ClifRegistry registry = null;

	private String registryAddress = null;

	/**
	 * The plugin id.
	 */
	public static final String PLUGIN_ID = "org.ow2.clif.console.plugin";

	/**
	 * Resource bundle.
	 */
	private ResourceBundle resourceBundle;

	private ClifConsole consoleView;	
	private ClifAppFacade clifApp = null;
	private boolean mustRestart = false;

	/**
	 * On first call, creates the registry or connects to an existing registry,
	 * and creates the default local CLIF server. On subsequent calls, does nothing.
	 *
	 * @return the CLIF registry
	 * @throws ClifException if no registry could be created nor connected to, or
	 *                       if the default CLIF server could not be created.
	 */
	public synchronized ClifRegistry getRegistry(boolean update)
		throws ClifException
	{
		String newAddress = System.getProperty("fractal.registry.host")
			+ ":"
			+ System.getProperty("fractal.registry.port");
		if (registry == null || (update && ! newAddress.equals(registryAddress)))
		{
			try
			{
				registry = new ClifRegistry(true);
				registryAddress = newAddress;
				System.out.println("Created " + registry);
			}
			catch (Throwable th)
			{
				try
				{
					registry = new ClifRegistry(false);
					registryAddress = newAddress;
					System.out.println( "Connected to " + registry);
				}
				catch (Exception ex)
				{
					throw new ClifException("Can't connect to CLIF registry " + newAddress, ex);
				}
			}
			ClifServerImpl.create(ExecutionContext.DEFAULT_SERVER, registry);
			registry.bindClifApp(CLIFAPP_NAME, clifApp.getClifApp());
		}
		return registry;
	}


	public ClifConsolePlugin() {
		super();
		plugin = this;
		try {
			resourceBundle = ResourceBundle.getBundle(
				"org.ow2.clif.console.lib.egui.ClifConsolePluginResources");
		} catch (MissingResourceException x) {
			resourceBundle = null;
		}
	}

	/**
	 * This method is called upon plug-in activation
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		ExecutionContext.init(FileLocator.resolve(FileLocator.find(getBundle(), Path.EMPTY, null)).getPath());
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		ExecutionContext.WORKSPACE_PATH = root.getLocation().toOSString();
		File clifResourcesFile = new File(
			getStateLocation().toFile(),
			CLIF_INI);
		if (clifResourcesFile.exists())
		{
			try
			{
				Properties iniProps = new Properties();
				iniProps.load(new FileReader(clifResourcesFile));
				networkAddr = iniProps.getProperty(INI_NETWORK_PROP, "localhost");
				if (! Network.isLocalAddress(networkAddr))
				{
					networkAddr = "localhost";
				}
			}
			catch (Exception ex)
			{
				throw new Exception(
					"Problem while trying to read CLIF plug-in resource file " + clifResourcesFile,
					ex);
			}
		}
		setProperties(new File(ExecutionContext.getBaseDir() + File.separator + ExecutionContext.PROPS_PATH));
		consoleView = new ClifConsole();
		ConsolePlugin.getDefault().getConsoleManager().addConsoles(
			new IConsole[] { consoleView } );
		clifApp = new ClifAppFacade(CLIF_APPLICATION, CLIF_APPLICATION);
	}

	/**
	 * This method is called when the plug-in is stopped
	 */
	public void stop(BundleContext context) throws Exception {
		consoleView.close();
		super.stop(context);
	}

	/**
	 * Returns the shared instance.
	 *
	 * @return ClifConsolePlugin
	 */
	public static ClifConsolePlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the string from the plugin's resource bundle,
	 * or 'key' if not found.
	 *
	 * @param key
	 * @return String
	 */
	public String getResourceString(String key) {
		ResourceBundle bundle = ClifConsolePlugin.getDefault().getResourceBundle();
		try {
			return (bundle != null) ? bundle.getString(key) : key;
		} catch (MissingResourceException e) {
			return key;
		}
	}

	/**
	 * Returns the plugin's resource bundle,
	 *
	 * @return ResourceBundle
	 */
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

	public ClifAppFacade getClifApp()
	{
		return clifApp;
	}

	public String getNetworkAddress()
	{
		return networkAddr;
	}

	public synchronized void setNetworkAddress(String addr)
	{
		if (!addr.equals(networkAddr))
		{
			mustRestart = true;
			networkAddr = addr;
			File clifResourcesFile = new File(
				getStateLocation().toFile(),
				CLIF_INI);
			try
			{
				Writer output = new BufferedWriter(new FileWriter(clifResourcesFile));
				output.write(INI_NETWORK_PROP + "=" + addr);
				output.close();
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
			}
		}
	}

	public boolean mustRestart()
	{
		return mustRestart;
	}

	public synchronized void setProperties(File clifProps)
		throws Exception
	{
		try
		{
			System.clearProperty("fractal.registry.host");
			ExecutionContext.setProperties(new FileInputStream(clifProps));
    		// network-related properties
    		System.setProperty("jonathan.connectionfactory.host", networkAddr);
    		System.setProperty("clif.filestorage.host", networkAddr);
    		System.setProperty("clif.codeserver.host", networkAddr);
			if (System.getProperty("fractal.registry.host") == null)
			{
				System.setProperty("fractal.registry.host", networkAddr);
			}
			// paths-related properties 
    		setSpecialProperties(new FileInputStream(clifProps));
		}
		catch (Exception ex) {
			throw new Exception(
				"Can't set system properties from file " + clifProps,
				ex);
		}
	}

	/**
     * set paths properties whose processing is specific to Eclipse console
     * (workspace/project directory resolution)
     * @param ins InputStream which store CLIF properties
     */
    private synchronized void setSpecialProperties(InputStream ins)
    {
    	try
    	{ 
    		Properties props = new Properties();
    		props.load(ins);
    		String allParam = (String)props.get("clif.parameters");
    		String[] parameters = allParam.split(" ");
    		for (String element : parameters)
    		{
    			if (
    				element.startsWith("-Dclif.codeserver.path")
    				|| element.startsWith("-Dclif.filestorage.dir")
    				|| element.startsWith("-Dclif.eclipse.statdir"))
    			{
    				String[] property = element.split("=");
    				String prop = completeProperty(property[1]);
    				System.setProperty(property[0].substring("-D".length()), prop);
    			}
    		}
    	}
    	catch (Exception e)
    	{
    		e.printStackTrace(System.err);
    	}
    }

    /**
     * Scans all paths separated with ";" and modifies relative paths to
     * absolute paths 
     * @param prop the property representing a list of paths
     * @throws CoreException  A project path could not be resolved
     */
    static private String completeProperty(String prop) throws CoreException {
    	String completeProp="";
    	prop=prop.replaceAll("\"", "");
    	String[] property = prop.split(";");
    	for (int i=0;i<property.length;i++){
    		Path pathFile = new Path(property[i]);
    		if (pathFile.isAbsolute())
    		{
    			completeProp += property[i];
    		}
    		else
    		{
    			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(pathFile.segment(0));
    			if (project != null)
    			{
	    			if (pathFile.segmentCount() > 1)
	    			{
	    				completeProp += project.getFolder(pathFile.removeFirstSegments(1)).getLocation().toOSString();
	    			}
	    			else if (project.getLocation() != null)
	    			{
	    				completeProp += project.getLocation().toOSString();
	    			}
    			}
    		}
    		if (i<property.length-1){
    			completeProp += ";";
    		}
    	}
    	return completeProp;
    }
}
