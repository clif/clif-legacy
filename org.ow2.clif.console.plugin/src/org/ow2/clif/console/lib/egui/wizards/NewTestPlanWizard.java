/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;

/**
 * Wizard to create a new test plan file with de ".ctp" extension.
 * 
 * @author Tsirimiaina Andrianavonimiarina Jaona
 */

public class NewTestPlanWizard extends Wizard implements INewWizard {
    
    /* The only one wizard page. */
    private NewTestPlanWizardPage page;
    
    /* Selection in the workbench. */
    private ISelection selection;
    
    /**
     * Construct a new "create new test plan" wizard.
     */
    public NewTestPlanWizard() {
        super();
        setNeedsProgressMonitor(true);
        setWindowTitle("New CLIF Test Plan");
        setDefaultPageImageDescriptor(ClifConsolePlugin.imageDescriptorFromPlugin(
                ClifConsolePlugin.PLUGIN_ID,"icons/logo_clif_100px.gif"));
    }
    
    /**
     * Add one page to the wizard:
     * The choose container and file name page.
     */
    public void addPages() {
        page = new NewTestPlanWizardPage(selection);
        addPage(page);
    }
    
    /**
     * This method is called when 'Finish' button is pressed in
     * the wizard.
     * Get container and file name to create a new empty test plan file.
     * 
     * @return true if finish successful
     */
    public boolean performFinish() {
        final String containerName = page.getContainerName();
        String fileNameExt = page.getFileName();
        
        /* Test file extension (empty extension become .ctp) */
        int dotLoc = fileNameExt.lastIndexOf('.');
        if (dotLoc == -1) {
            fileNameExt = fileNameExt + ".ctp";
        }
        final String fileName = fileNameExt;
        
        IRunnableWithProgress op = new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor) throws InvocationTargetException {
                try {
                    doFinish(containerName, fileName, monitor);
                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                } finally {
                    monitor.done();
                }
            }
        };
        try {
            getContainer().run(true, false, op);
        } catch (InterruptedException e) {
            return false;
        } catch (InvocationTargetException e) {
            Throwable realException = e.getTargetException();
            MessageDialog.openError(getShell(), "CLIF Console Plug-in", realException.getMessage());
            return false;
        }
        return true;
    }
    
    /**
     * The worker method. It will find the container, create the
     * file, and open the editor on the newly created file.
     * 
     * @param containerName the container name
     * @param fileName the file name
     * @param monitor the progress monitor that shows evolution
     * @throws CoreException 
     */
    
    private void doFinish(String containerName, String fileName, IProgressMonitor monitor)
    throws CoreException {
        
        /* Monitor needed by IFile while creation */
        monitor.beginTask("Creating " + fileName, 2);
        
        /* Test if container exist */
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        IResource resource = root.findMember(new Path(containerName));
        if (resource == null || !resource.exists() || !(resource instanceof IContainer)) {
            throwCoreException("Container \"" + containerName + "\" does not exist.");
        }
        
        /* Create file if it does not exist */
        IContainer container = (IContainer) resource;
        final IFile file = container.getFile(new Path(fileName));
        try {
            InputStream stream = openContentStream();
            if (!file.exists()) {
                file.create(stream, true, monitor);
            }
            stream.close();
        } catch (IOException e) {
            MessageDialog.openError(getShell(),
                    "CLIF Console Plug-in",
            "Cannot create a new file");
        }
        
        monitor.worked(1);
        monitor.setTaskName("Opening file for editing...");
        getShell().getDisplay().asyncExec(new Runnable() {
            public void run() {
                IWorkbenchPage page =
                    PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
                try {
                    IDE.openEditor(page, file, true);
                } catch (PartInitException e) {
                }
            }
        });
        monitor.worked(1);
    }
    
    /**
     * We will initialize file contents with a sample text.
     * 
     * @return an empty InputStream to create file
     */
    private InputStream openContentStream() {
        String contents = "";
        return new ByteArrayInputStream(contents.getBytes());
    }
    
    /**
     * Throw Core Exception with the given message.
     * 
     * @param message the error message
     * @throws CoreException the exception thrown
     */
    private void throwCoreException(String message) throws CoreException {
        IStatus status = new Status(IStatus.ERROR, "CLIFConsole", IStatus.OK, message, null);
        throw new CoreException(status);
    }
    
    /**
     * We will accept the selection in the workbench to see if
     * we can initialize from it.
     */
    public void init(IWorkbench workbench, IStructuredSelection selection) {
        this.selection = selection;
    }
}