/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.tree;

import org.eclipse.core.runtime.IAdaptable;

/**
 * Object in a clif tree view.
 * @author Joan Chaumont
 *
 */
public class ClifTreeObject implements IAdaptable{
    
    private String name;
    private ClifTreeParent parent;
    
    /**
     * Simple constructor with name
     * @param name the name of the object
     */
    public ClifTreeObject(String name) {
        this.name = name;
    }
    
    /**
     * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
     */
    public Object getAdapter(Class adapter) {
        return null;
    }
    
    /**
     * Set name
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Get name
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    
    /**
     * Set parent.
     * @param parent
     */
    public void setParent(ClifTreeParent parent) {
        this.parent = parent;
    }
    
    /**
     * Get parent
     * @return Returns the parent.
     */
    public ClifTreeParent getParent() {
        return parent;
    }
    
    public String toString (){
        return name;
    }
}
