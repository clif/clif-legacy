/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009, 2011 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards.clifProject;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;

/**
 * Third page of the New CLIF project Wizard. 
 * Used to set network-related properties: registry, code server, file storage host.
 * 
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class NetworkPage extends WizardPage{   

	private InteractionManager interaction;

	public NetworkPage() {
		super("wizardPage");
		setTitle("Network configuration");
		setDescription("Network-related properties for CLIF (registry, code server, etc.).");
	}

	/**
	 * Creates and initializes controls through InterationManager
	 */
	public void createControl(Composite parent) {
		interaction = new InteractionManager(this,null);
		Composite comp = interaction.createNetworkContents(parent);
		interaction.initializeNetwork();
		interaction.networkDialogChanged();
		setControl(comp);
	}

	public void setProject(IProject project){
		interaction.initializeNetwork();
	}
}
