/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.egui.tree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.egui.editor.TestPlanEditor;
import org.ow2.clif.console.lib.egui.editor.TestPlanObservable;

/**
 * An eclipse view : display blades sort by servers
 * @author Joan Chaumont
 * 
 */
public class ClifTreeView extends ViewPart implements Observer {
    
    private TreeViewer clifTree;
    private TestPlanObservable testPlanObs;
    
    private Action actionCopy;
    private Action actionPaste;
    private Action actionDelete;
    private Action doubleClickAction;
    
    /* Array of blade to copy. */
    private ArrayList<ClifDeployDefinition> idCopy = null;
    
    /* This listener is used for updating the viewer on open or close event 
     * of a editor part and when this view is open. */
    private IPartListener partListener = new IPartListener() {
        public void partActivated(IWorkbenchPart part) {
        }
        
        public void partBroughtToTop(IWorkbenchPart part) {
            if (part instanceof TestPlanEditor) {
                testPlanObs = ((TestPlanEditor)part).getEditPage().getTestPlan();
                testPlanObs.addObserver(ClifTreeView.this);
                clifTree.setInput(testPlanObs.getTestPlan());
                clifTree.expandAll();
            }
        }
        
        public void partClosed(IWorkbenchPart part) {
            if(!(part instanceof ClifTreeView)) {
                IEditorPart editor = getSite().getPage().getActiveEditor();
                if(editor == null || !(editor instanceof TestPlanEditor)) {
                    clifTree.setInput(null);
                    clifTree.expandAll();
                }
            }
        }
        
        public void partDeactivated(IWorkbenchPart part) {}
        public void partOpened(IWorkbenchPart part) {
            IEditorPart editor = getSite().getPage().getActiveEditor();
            if (editor instanceof TestPlanEditor) {
                TestPlanEditor e = (TestPlanEditor) editor;
                testPlanObs = e.getEditPage().getTestPlan();
                testPlanObs.addObserver(ClifTreeView.this);
                clifTree.setInput(testPlanObs.getTestPlan());
                clifTree.expandAll();
            }
        }
    };
    
    
    /**
     * Simple constructor.
     */
    public ClifTreeView() {
        testPlanObs = new TestPlanObservable();
    }
    
    /**
     * This is a callback that will allow us
     * to create the viewer and initialize it.
     */
    public void createPartControl(Composite parent) {
        clifTree = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
        clifTree.setContentProvider(new ClifTreeContentProvider());
        clifTree.setLabelProvider(new ClifTreeLabelProvider());
        makeActions();
        hookContextMenu();
        hookDoubleClickAction();
        
        IEditorPart edit = getSite().getPage().getActiveEditor();
        if(edit instanceof TestPlanEditor){
            testPlanObs = ((TestPlanEditor)edit).getEditPage().getTestPlan();
        }
        
        getSite().getPage().addPartListener(partListener);
    }
    
    private void hookContextMenu() {
        MenuManager menuMgr = new MenuManager("#PopupMenu");
        menuMgr.setRemoveAllWhenShown(true);
        menuMgr.addMenuListener(new IMenuListener() {
            public void menuAboutToShow(IMenuManager manager) {
                ClifTreeView.this.fillContextMenu(manager);
            }
        });
        Menu menu = menuMgr.createContextMenu(clifTree.getControl());
        clifTree.getControl().setMenu(menu);
        getSite().registerContextMenu(menuMgr, clifTree);
    }
    
    /* Fill context menu depending on which objects are selected in clif tree */
    private void fillContextMenu(IMenuManager manager) {
        manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
        ISelection selection = clifTree.getSelection();
        
        manager.add(actionCopy);
        manager.add(actionPaste);
        manager.add(new Separator());
        manager.add(actionDelete);
        
        actionPaste.setEnabled(true); 
        actionCopy.setEnabled(true);
        actionDelete.setEnabled(true);
        
        IEditorPart edit = getSite().getPage().getActiveEditor();
        if(edit instanceof TestPlanEditor){
            if(!((TestPlanEditor)edit).isEditable()) {
                actionPaste.setEnabled(false); 
                actionCopy.setEnabled(false);
                actionDelete.setEnabled(false);
                return;
            }
        }
        
        if (((IStructuredSelection)selection).size() <= 0) {
            actionCopy.setEnabled(false);
            actionDelete.setEnabled(false);
            return;
        }
        
        ClifTreeParent ref = null;
        for (Object o : ((IStructuredSelection)selection).toList())
        {
            if (ref == null){
                if(o instanceof ClifTreeParent)
                    ref = (ClifTreeParent)o;
                else ref = ((ClifTreeObject)o).getParent();
            }
            
            ClifTreeParent tmp;
            if(o instanceof ClifTreeParent)
                tmp = (ClifTreeParent)o;
            else tmp = ((ClifTreeObject)o).getParent();
            
            if(!tmp.equals(ref))
                actionPaste.setEnabled(false);
            
            if ((o instanceof ClifTreeParent)){
                actionCopy.setEnabled(false);
                actionDelete.setEnabled(false);
            }
        }
        if(idCopy == null){
            actionPaste.setEnabled(false);
        }
    }
    
    private void makeActions() {
        actionCopy = new Action() {
            /**
             * Fill the idCopy arrayList with blades we want to copy
             */
            public void run() {
                idCopy = new ArrayList<ClifDeployDefinition>();
                ISelection selection = clifTree.getSelection();
                for (Object o : ((IStructuredSelection)selection).toList())
                {
                    String name = ((ClifTreeObject)o).getName();
                    name = name.substring(name.indexOf(" ")+1);
                    idCopy.add(testPlanObs.getTestPlan().get(name));
                }
            }
        };
        actionCopy.setText("Copy");
        actionCopy.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
                getImageDescriptor(ISharedImages.IMG_TOOL_COPY));
        actionCopy.setAccelerator(SWT.CTRL + 'C');
        
        actionPaste = new Action() {
            /**
             * Paste blade in idcopy arraylist. Creating new ClifDefinition and
             * adding them in the test plan.
             */
            public void run() {
                String serverName;
                ISelection selection = clifTree.getSelection();
                if (((IStructuredSelection)selection).size() <= 0) {
                    serverName = null;
                }
                else {
                    Object o = ((IStructuredSelection)selection).getFirstElement();
                    ClifTreeParent dest = (o instanceof ClifTreeParent)?(ClifTreeParent)o:((ClifTreeObject)o).getParent();
                    serverName = dest.getName();
                }
                
                
                Iterator<ClifDeployDefinition> iter = idCopy.iterator();
                while (iter.hasNext()) {
                    ClifDeployDefinition def = iter.next();
                    
                    testPlanObs.put(getNewId(), 
                            new ClifDeployDefinition(
                                    (serverName != null)?serverName:def.getServerName(), 
                                            def.getAdlDefinition(),
                                            def.getContext(),
                                            def.getArgument(),
                                            def.getComment(),
                                            def.isProbe()));
                }
            }
        };
        actionPaste.setText("Paste");
        actionPaste.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
                getImageDescriptor(ISharedImages.IMG_TOOL_PASTE));
        actionPaste.setAccelerator(SWT.CTRL + 'V');
        
        actionDelete = new Action() {
            /**
             * Delete selected blades.
             */
            public void run() {
                ISelection selection = clifTree.getSelection();
                for (Object o : ((IStructuredSelection)selection).toList())
                {
                    testPlanObs.remove(((ClifTreeObject)o).getName().split(" ")[1]);
                }
            }
        };
        actionDelete.setText("Delete");
        actionDelete.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
                getImageDescriptor(ISharedImages.IMG_TOOL_DELETE));
        actionDelete.setAccelerator(SWT.DEL);
        doubleClickAction = new Action() {
            /**
             * Select the clicked blade in other views.
             */
            public void run() {
                ISelection selection = clifTree.getSelection();
                Object obj = ((IStructuredSelection)selection).getFirstElement();
                if(!(obj instanceof ClifTreeParent)){
                    testPlanObs.setSelectId(((ClifTreeObject)obj).getName().split(" ")[1]);
                }
            }
        };
    }
    
    private void hookDoubleClickAction() {
        clifTree.addDoubleClickListener(new IDoubleClickListener() {
            public void doubleClick(DoubleClickEvent event) {
                doubleClickAction.run();
            }
        });
    }
    
    /**
     * Get the test plan observable
     * @return Returns the testPlanObs.
     */
    public TestPlanObservable getTestPlanObs() {
        return testPlanObs;
    }
    
    /**
     * Get an unused id for a new blade
     * @return newId a new String id
     */
    private String getNewId() {
        int bladeId = 0;
        String newId = String.valueOf(bladeId++);
        while(testPlanObs.getTestPlan().get(newId) != null) {
            newId = String.valueOf(bladeId++);
        }
        return newId;
    }
    
    /* Observer interface */
    /**
     * Update the viewer when a change has been done in the test plan.
     */
    public void update(Observable o, Object arg) {
        if(arg == null) {
            clifTree.setInput(testPlanObs.getTestPlan());
            clifTree.expandAll();
        }
    }
    
    /* Other View Part methods */
    public void setFocus() {}
    
    public void dispose() {
        getSite().getPage().removePartListener(partListener);
        super.dispose();
    }
}
