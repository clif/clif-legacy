/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name: not supported by cvs2svn $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards.clifProject;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;


/**
 * Second page of the New CLIF project Wizard. 
 * Used to choose folders where statistics and reports 
 * data will be stored
 * 
 * @author Florian Francheteau
 */
public class MainParametersPage extends WizardPage{

	private InteractionManager interaction;

	/**
	 * Constructor
	 */
	public MainParametersPage() {
		super("wizardPage");
		setTitle("Main parameters");
		setDescription("Choose directories where reporting and statistics files will be stored");
	}

	/**
	 * Creates and initializes controls through InterationManager
	 */
	public void createControl(Composite parent) {

		interaction = new InteractionManager(this,null);
		Composite comp = interaction.createMainContents(parent);
		interaction.initialize();
		interaction.initializeMainContents();
		setControl(comp);
	}

	/**
	 * Update statistics and reporting directories fields through
	 * InteractionManager
	 * @param projectPath Location path of the created project 
	 */
	public void initializeStatsReportText(IPath projectPath){
		interaction.initializeStatsReportText(projectPath);
	}

	public void setProject(IProject project){
		interaction.setProject(project);
	}

	public String getReportDir(){
		return interaction.getReportDir();
	}
	public String getStatsDir(){
		return interaction.getStatsDir();
	}

	public InteractionManager getInteractionManager(){
		return interaction;
	}
}
