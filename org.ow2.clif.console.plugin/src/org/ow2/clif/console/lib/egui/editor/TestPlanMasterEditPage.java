/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright 2010 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.editor;

import java.io.File;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.console.lib.egui.editor.exceptions.BadBladePropertiesException;
import org.ow2.clif.console.lib.egui.editor.exceptions.ExistingBladeIdException;
import org.ow2.clif.deploy.ClifAnalyzerAppFacade;

/**
 * Injector and probe Master view in Master/Details pattern.
 * 
 * @author Manuel AZEMA
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 */
public class TestPlanMasterEditPage extends MasterDetailsBlock implements Observer {
    
    /* Page manage by this pattern. */
    private FormPage page;
    
    /* TabOrder that display test plan with blades sort by class. */
    private TestPlanVisualDisplay tableBlade;
    
    private TestPlanObservable testPlan;
    
    private File clifPropsFile;
    
    private boolean changed;
    /**
     * Create a Master/Detail pattern manager for test plan edition.
     * 
     * @param page the page manage by this pattern
     * @param testPlan the test plan to edit
     */
    public TestPlanMasterEditPage(
    	FormPage page,
    	Map<String, ClifDeployDefinition> testPlan,
    	File clifPropsFile)
    {
        super();
        this.page = page;
        this.testPlan = new TestPlanObservable(testPlan);
        this.testPlan.addObserver(this);
        this.clifPropsFile = clifPropsFile;

        changed = false;
    }
    
    /**
     * Create Master Part in Master/Detail pattern.
     * Create a table with an injectors and blades list.
     * 
     * @param managedForm the parent form
     * @param parent the parent composite
     */
    protected void createMasterPart(IManagedForm managedForm, Composite parent) {
        final IManagedForm mForm = managedForm;
        FormToolkit toolkit = managedForm.getToolkit();
        ScrolledForm form = managedForm.getForm();
        form.setText("Test Plan Editor");
        
        /* Create "Injectors and probes" section */
        Section section = toolkit.createSection(parent,
                Section.DESCRIPTION|Section.EXPANDED);
        section.setText("Injectors and probes");
        section.setDescription("All injectors and probes in the test plan");
        toolkit.createCompositeSeparator(section);
        section.marginWidth = 10;
        section.marginHeight = 5;
        
        /* Composite client for injectors and probes table */
        Composite sectionClient = toolkit.createComposite(section, SWT.WRAP);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 1;
        layout.marginHeight = 1;
        sectionClient.setLayout(layout);
        
        /*Create a selection changed listener */
        final SectionPart spart = new SectionPart(section);
        managedForm.addPart(spart);
        ISelectionChangedListener selectionChangedListener = new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                mForm.fireSelectionChanged(spart, event.getSelection());
                mForm.getForm().reflow(true);
            }
        };
        /* Create injectors and probes tab folder */
        tableBlade = new TestPlanVisualDisplay(sectionClient,testPlan.getTestPlan(),managedForm,
                page.getEditor().getEditorInput(),SWT.BORDER|SWT.FLAT,
                selectionChangedListener,null,false,false);
        
        /* Edit buttons composite */
        Composite sectionButtons = toolkit.createComposite(sectionClient, SWT.WRAP);
        sectionButtons.setLayout(new GridLayout());
        sectionButtons.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
        
        /* Add an injector or a probe */
        Button bAdd = toolkit.createButton(sectionButtons, "Add", SWT.PUSH);
        bAdd.setToolTipText("Add a new probe or injector in this tab");
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        bAdd.setLayoutData(gd);
        bAdd.addSelectionListener(actionAddBlade);
        
        /* Remove an injector or a probe */
        Button bRemove = toolkit.createButton(sectionButtons, "Remove", SWT.PUSH);
        bRemove.setToolTipText("Remove the selected probe or injector");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        bRemove.setLayoutData(gd);
        bRemove.addSelectionListener(actionRemoveBlade);
        
        /* Remove all injectors and probes */
        Button bRemoveAll = toolkit.createButton(sectionButtons, "Remove All", SWT.PUSH);
        bRemoveAll.setToolTipText("Remove all probes and injectors");
        gd = new GridData(GridData.FILL_HORIZONTAL);
        bRemoveAll.setLayoutData(gd);
        bRemoveAll.addSelectionListener(actionRemoveAllBlades);
        
        section.setClient(sectionClient);
        toolkit.paintBordersFor(sectionClient);
    }
    
    /**
     * Add 2 actions in the tool bar
     * <ul>
     * <li>Horizontal orientation</li>
     * <li>Vertical orientation</li>
     * </ul>
     */
    protected void createToolBarActions(IManagedForm managedForm) {
        final ScrolledForm form = managedForm.getForm();
        
        /* Horizontal orientation action */
        Action haction = new Action("hor", Action.AS_RADIO_BUTTON) {
            public void run() {
                sashForm.setOrientation(SWT.HORIZONTAL);
                form.reflow(true);
            }
        };
        haction.setChecked(true);
        haction.setToolTipText("Horizontal orientation");
        haction.setImageDescriptor(ClifConsolePlugin.imageDescriptorFromPlugin(
                ClifConsolePlugin.PLUGIN_ID, "icons/hor.ico"));
        
        /* Vertical orientation action */
        Action vaction = new Action("ver", Action.AS_RADIO_BUTTON) {
            public void run() {
                sashForm.setOrientation(SWT.VERTICAL);
                form.reflow(true);
            }
        };
        vaction.setChecked(false);
        vaction.setToolTipText("Vertical orientation");
        vaction.setImageDescriptor(ClifConsolePlugin.imageDescriptorFromPlugin(
                ClifConsolePlugin.PLUGIN_ID, "icons/vor.ico"));
        
        /* Add actions to toolbar */
        form.getToolBarManager().add(haction);
        form.getToolBarManager().add(vaction);
    }
    
    /**
     * Register injectors and probes details page.
     * @param detailsPart the details part
     */
    protected void registerPages(DetailsPart detailsPart) {
        detailsPart.registerPage(String.class, new TestPlanDetailsEditPage(this,testPlan.getTestPlan()));
    }	
    
    /**
     * True if the editor is editable.
     * @param isEditable true if the editor is editable
     */
    public void setEditable(boolean isEditable) {
        sashForm.setEnabled(isEditable);
    }
    
    /**
     * Test if edit page is editable
     * @return boolean true if the test plan is editable
     */
    public boolean isEditable() {
        return sashForm.getEnabled();
    }
    
    /**
     * Get observable test plan.
     * @return TestPlanObservable test plan
     */
    public TestPlanObservable getTestPlan() {
        return testPlan;
    }
    
    /**
     * Get table blade
     * @return Returns the tableBlade.
     */
    public TestPlanVisualDisplay getTableBlade() {
        return tableBlade;
    }
    
    /**
     * Set the changed boolean to true
     * if edit page has changed
     * @param b
     */
    public void setChanged(boolean b) {
        changed = b;
    }

    /**
     * Return true if a modification has been made
     * in edit page
     * @return boolean
     */
    public boolean hasChanged() {
        return changed;
    }

    /**
     * Change editor dirty state.
     * If true, editor needs to be saved.
     * @param isDirty the dirty state
     */
    public void setDirty(boolean isDirty) {
        ((TestPlanEditor)page.getEditor()).setDirty(isDirty);
    }

    /**
     * Refresh test plan display.createPageEdition
     */
    public void refresh() {
        tableBlade.refresh();
    }
    
    /**
     * Modify a blade. Args are blade's id to modify and the new properties.
     * @param bladeId actual blade id
     * @param newBladeId new blade id (can be the same)
     * @param serverName 
     * @param bClass 
     * @param argument 
     * @param comment 
     * @param isProbe 
     * @return the new blade id
     * @throws ExistingBladeIdException
     * @throws BadBladePropertiesException
     */
    public String modifyBlade(String bladeId, String newBladeId, String serverName, String bClass, String argument, String comment, boolean isProbe)
    throws ExistingBladeIdException, BadBladePropertiesException {
        testPlan.setTestPlan(tableBlade.modifyBlade(bladeId, newBladeId, serverName, bClass, argument, comment, isProbe));
        return newBladeId;
    }

    public void updateClifProperties()
    {
    	ClifAnalyzerAppFacade.updateProperties(clifPropsFile);
    }

    /* Commands for add and remove blades. */
    
    /* Add a blade in the open tab. */
    private SelectionListener actionAddBlade = new SelectionAdapter() {
        public void widgetSelected(SelectionEvent event) {
            testPlan.setTestPlan(tableBlade.addBlade());
            setChanged(true);
            setDirty(true);
        }
    };
    
    /* Remove the selected blade. */
    private SelectionListener actionRemoveBlade = new SelectionAdapter() {
        public void widgetSelected(SelectionEvent event) {
            testPlan.setTestPlan(tableBlade.removeSelectedBlade());
            setChanged(true);
            setDirty(true);
        }
    };
    
    /* Remove all blades in every tabs if user confirms. */
    private SelectionListener actionRemoveAllBlades = new SelectionAdapter() {
        public void widgetSelected(SelectionEvent event) {
            if(MessageDialog.openConfirm(
                    page.getEditorSite().getShell(),
                    "Remove all injectors and probes",
            "Do you want to remove all injectors and probes? Unsaved data will be lost.")) {
                testPlan.setTestPlan(tableBlade.removeAllBlades());	
                setChanged(true);
                setDirty(true);			
            }
        }
    };
    
    /* Observer interface */
    /**
     * Update the edit page when a change has been done in the test plan.
     */
    public void update(Observable o, Object arg) {
        if(arg == null) {
            tableBlade.setTestPlan(testPlan.getTestPlan());
            refresh();
            setDirty(true);
        }
        else {
            tableBlade.setSelection((String)arg, false);
        }
    }  
}
