/*******************************************************************************
 * Copyright (c) 2000, 2003 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.ow2.clif.console.lib.egui.rcp;

import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

/**
 * Adds actions to a workbench window.
 */
public final class ClifActionBuilder extends ActionBarAdvisor {

    private IWorkbenchWindow window;

    /**
     * Constructs a new action builder.
     * 
     * @param actionBarConfigurer the configurer
     */
    public ClifActionBuilder(IActionBarConfigurer actionBarConfigurer) {
        super(actionBarConfigurer);
    }

    protected void fillCoolBar(ICoolBarManager cbManager) {
        cbManager.add(new GroupMarker("group.file")); //$NON-NLS-1$
        { // File Group
            IToolBarManager fileToolBar = new ToolBarManager(cbManager.getStyle());
            fileToolBar.add(new Separator(IWorkbenchActionConstants.NEW_GROUP));
            fileToolBar.add(new GroupMarker(IWorkbenchActionConstants.OPEN_EXT));
            fileToolBar.add(new GroupMarker(IWorkbenchActionConstants.SAVE_GROUP));
            fileToolBar.add(getAction(ActionFactory.SAVE.getId()));
            
            fileToolBar.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
            
            // Add to the cool bar manager
            cbManager.add(new ToolBarContributionItem(fileToolBar,IWorkbenchActionConstants.TOOLBAR_FILE));
        }
        
        cbManager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
        
        //cbManager.add(new GroupMarker(IWorkbenchActionConstants.GROUP_EDITOR));
    }
    
    protected void fillMenuBar(IMenuManager menubar) {
        menubar.add(createFileMenu());
        menubar.add(createEditMenu());
        menubar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
        menubar.add(createWindowMenu());
        menubar.add(createFalseHelpMenu());
        menubar.add(createTrueHelpMenu());
    }
    
    /**
     * Creates and returns the File menu.
     * @return MenuManager the file menu
     */
    private MenuManager createFileMenu() {
        MenuManager menu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE); //$NON-NLS-1$
        menu.add(new GroupMarker(IWorkbenchActionConstants.FILE_START));
        
        String newId = ActionFactory.NEW.getId();
        MenuManager newMenu = new MenuManager("New", newId);
        newMenu.add(new Separator(newId));
        newMenu.add(ContributionItemFactory.NEW_WIZARD_SHORTLIST.create(window));
        
        menu.add(newMenu);

        menu.add(new GroupMarker(IWorkbenchActionConstants.NEW_EXT));
        
        menu.add(new Separator());
        menu.add(getAction(ActionFactory.CLOSE.getId()));
        menu.add(getAction(ActionFactory.CLOSE_ALL.getId()));
        menu.add(new GroupMarker(IWorkbenchActionConstants.CLOSE_EXT));
        
        menu.add(new Separator());
        menu.add(getAction(ActionFactory.SAVE.getId()));
        menu.add(getAction(ActionFactory.SAVE_AS.getId()));
        menu.add(getAction(ActionFactory.SAVE_ALL.getId()));
        menu.add(new GroupMarker(IWorkbenchActionConstants.SAVE_EXT));

        menu.add(getAction(ActionFactory.REVERT.getId()));
        menu.add(ContributionItemFactory.REOPEN_EDITORS.create(getActionBarConfigurer().getWindowConfigurer().getWindow()));
        menu.add(new GroupMarker(IWorkbenchActionConstants.MRU));
        
        menu.add(new Separator());
        menu.add(getAction(ActionFactory.QUIT.getId()));
        menu.add(new GroupMarker(IWorkbenchActionConstants.FILE_END));
        return menu;
    }

    /**
     * Creates and returns the Edit menu.
     * @return MenuManager the edit menu
     */
    private MenuManager createEditMenu() {
        MenuManager menu = new MenuManager("&Edit", IWorkbenchActionConstants.M_EDIT); //$NON-NLS-1$
        menu.add(new GroupMarker(IWorkbenchActionConstants.EDIT_START));

        menu.add(getAction(ActionFactory.UNDO.getId()));
        menu.add(getAction(ActionFactory.REDO.getId()));
        menu.add(new GroupMarker(IWorkbenchActionConstants.UNDO_EXT));

        menu.add(getAction(ActionFactory.CUT.getId()));
        menu.add(getAction(ActionFactory.COPY.getId()));
        menu.add(getAction(ActionFactory.PASTE.getId()));
        menu.add(new GroupMarker(IWorkbenchActionConstants.CUT_EXT));

        menu.add(getAction(ActionFactory.SELECT_ALL.getId()));
        menu.add(new Separator());

        menu.add(getAction(ActionFactory.FIND.getId()));
        menu.add(new GroupMarker(IWorkbenchActionConstants.FIND_EXT));

        menu.add(new GroupMarker(IWorkbenchActionConstants.ADD_EXT));

        menu.add(new GroupMarker(IWorkbenchActionConstants.EDIT_END));
        menu.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
        return menu;
    }
    
    private MenuManager createTrueHelpMenu() {
        MenuManager menu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP);
        menu.setVisible(false);
        
        return menu;
    }
    
    private MenuManager createFalseHelpMenu() {
        MenuManager menuFalse = new MenuManager("&Help", "h");
        menuFalse.add(getAction(ActionFactory.HELP_CONTENTS.getId()));
        
        return menuFalse;
    }
    
    private MenuManager createWindowMenu() {
        MenuManager menu = new MenuManager("&Window", IWorkbenchActionConstants.M_WINDOW);//$NON-NLS-1$
        MenuManager view = new MenuManager("&Views");
        view.add(ContributionItemFactory.VIEWS_SHORTLIST.create(window));
        
        MenuManager perspective = new MenuManager("&Perspectives");
        perspective.add(ContributionItemFactory.PERSPECTIVES_SHORTLIST.create(window));
        
        menu.add(view);
        menu.add(perspective);
        menu.add(new Separator());
        menu.add(getAction(ActionFactory.PREFERENCES.getId()));
        return menu;
    }
    
    protected void makeActions(IWorkbenchWindow window) {
        this.window = window;
        
        registerAsGlobal(ActionFactory.NEW.create(window));
        registerAsGlobal(ActionFactory.SAVE.create(window));
        registerAsGlobal(ActionFactory.SAVE_AS.create(window));
        registerAsGlobal(ActionFactory.ABOUT.create(window));
        registerAsGlobal(ActionFactory.SAVE_ALL.create(window));
        registerAsGlobal(ActionFactory.UNDO.create(window));
        registerAsGlobal(ActionFactory.REDO.create(window));
        registerAsGlobal(ActionFactory.CUT.create(window));
        registerAsGlobal(ActionFactory.COPY.create(window));
        registerAsGlobal(ActionFactory.PASTE.create(window));
        registerAsGlobal(ActionFactory.SELECT_ALL.create(window));
        registerAsGlobal(ActionFactory.FIND.create(window));
        registerAsGlobal(ActionFactory.CLOSE.create(window));
        registerAsGlobal(ActionFactory.CLOSE_ALL.create(window));
        registerAsGlobal(ActionFactory.CLOSE_ALL_SAVED.create(window));
        registerAsGlobal(ActionFactory.REVERT.create(window));
        registerAsGlobal(ActionFactory.HELP_CONTENTS.create(window));
        registerAsGlobal(ActionFactory.QUIT.create(window));
        registerAsGlobal(ActionFactory.PREFERENCES.create(window));
    }
    
    /**
     * Registers the action as global action and registers it for disposal.
     * 
     * @param action the action to register
     */
    private void registerAsGlobal(IAction action) {
        getActionBarConfigurer().registerGlobalAction(action);
        register(action);
    }
}