/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright (C) 2009, 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.egui.monitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.supervisor.lib.AlarmObservation;
import org.ow2.clif.supervisor.lib.BladeObservation;

/**
 * @author Tsirimiaina ANDRIANAVONIMIARINA JAONA
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class TabItemFolderPrincipal extends Composite implements Observer, SelectionListener {
    
    /* CTabFolder where tabs blade are located */
    private CTabFolder ctabfolder;
    
    private ClifAppFacade clifApp;
    private Map testPlan;
    
    private Map<String,TableGraphComposite> tableGraph;
    private TableAlarmComposite tableAlarm;
    
    private Map<String,Integer> serverColors;
    private int nbColors;
    
    private Text intervalField;
    private Text timeframeField;
    private Button resetButton;
    private Button refreshButton;
    
    private Button clearButton;
    private Button clearAllButton;
    
    private int interval;
    private int timeFrame;
    
    private static String TIME_FRAME = "300";
    private static String POLLING_PERIOD = "1";
    private static String TAB_ALARMS = "Alarms";
    
    private Group alarmControllerC;
    private Group controllersC;
    /**
     * Constructor
     * @param parent 
     * @param style 
     * @param clifApp 
     * @param testPlan 
     */
    public TabItemFolderPrincipal(CTabFolder parent, int style, ClifAppFacade clifApp, Map testPlan) {
        super(parent, style);
        tableGraph = new HashMap<String,TableGraphComposite>();
        
        serverColors = new HashMap<String,Integer>();
        nbColors = 0;
        
        this.clifApp = clifApp;
        clifApp.addObserver(this);
        
        this.testPlan = testPlan;

        GridLayout layout = new GridLayout(1, false);
        layout.verticalSpacing = 0;
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        this.setLayout(layout);
        this.setLayoutData(null);
        
        ctabfolder = new CTabFolder(this, SWT.BORDER);
        ctabfolder.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        Group allControllersC = new Group(this, SWT.RESIZE);
        GridLayout allLayout = new GridLayout(2, false);
        allControllersC.setLayout(allLayout);
        allControllersC.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        
        /* Control bar 3 columns : 
         * composite timeframe, composite interval, composite buttons */
        controllersC = new Group(allControllersC, SWT.RESIZE);
        GridLayout layout4 = new GridLayout(3, false);
        layout4.horizontalSpacing = 0;
        layout4.verticalSpacing = 0;
        layout4.marginHeight = 0;
        layout4.marginWidth = 0;
        controllersC.setLayout(layout4);
        controllersC.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        
        /* Composite timeframe : label + text + label */
        Composite internalC = new Composite(controllersC, SWT.NONE);
        GridLayout layout5 = new GridLayout(3, false);
        layout5.verticalSpacing = 0;
        layout5.marginHeight = 0;
        layout5.marginWidth = 0;
        internalC.setLayout(layout5);
        Label labeldtfs = new Label(internalC, SWT.LEFT);
        labeldtfs.setText(" Drawing time frame (s)");
        timeframeField = new Text(internalC, SWT.SINGLE | SWT.BORDER | SWT.LEFT);
        GridData gd = new GridData();
        gd.widthHint = 50;
        timeframeField.setLayoutData(gd);
        timeframeField.setText(TIME_FRAME);
        
        /* Composite interval time : label + text + label */
        Composite internal2C = new Composite(controllersC, SWT.NONE);
        GridLayout layoutc2 = new GridLayout(3, false);
        layoutc2.verticalSpacing = 0;
        layoutc2.marginHeight = 0;
        layoutc2.marginWidth = 0;
        internal2C.setLayout(layoutc2);

        Label labelpp = new Label(internal2C, SWT.LEFT);
        labelpp.setText(" Polling period (s)");
        
        intervalField = new Text(internal2C , SWT.BORDER | SWT.SINGLE | SWT.WRAP);
        gd = new GridData();
        gd.widthHint = 40;
        intervalField.setLayoutData(gd);
        intervalField.setText(POLLING_PERIOD);
        
        /* Buttons for blades */
        Composite internal3C = new Composite(controllersC, SWT.NONE);
        internal3C.setLayout(new GridLayout(2,true));
        
        refreshButton = new Button(internal3C, SWT.NONE);
        refreshButton.setText("Refresh");
        refreshButton.addSelectionListener(this);
        
        resetButton = new Button(internal3C, SWT.NONE);
        resetButton.setText("Reset");
        resetButton.addSelectionListener(this);
        
        /* Buttons for alarms */
        alarmControllerC = new Group(allControllersC, SWT.NONE);
        alarmControllerC.setLayout(new GridLayout(2,true));
        
        clearButton = new Button(alarmControllerC, SWT.NONE);
        clearButton.setText("Clear");
        clearButton.addSelectionListener(this);
        
        clearAllButton = new Button(alarmControllerC, SWT.NONE);
        clearAllButton.setText("Clear All");
        clearAllButton.addSelectionListener(this);
        
        alarmControllerC.setVisible(false);
        
        ctabfolder.addSelectionListener(this);
    }
    
    /**
     * Creates Tabs which will allow to sort blades by class.<p>
     * @param listClass list of blade's class name
     */
    public void createTabsByClass(ArrayList<String> listClass){
		for (String listClas : listClass) {
			Map<String, ClifDeployDefinition> blades = new HashMap<String, ClifDeployDefinition>();

			CTabItem ctabitem = new CTabItem(ctabfolder, SWT.NONE);
			ctabitem.setText((String) listClas);

			String lastId = "";

			Iterator it = testPlan.entrySet().iterator();
			while (it.hasNext()) {
				//Get id and class for each blade
				Map.Entry entry = (Map.Entry) it.next();
				String id = (String) entry.getKey();
				ClifDeployDefinition def = (ClifDeployDefinition) entry.getValue();

				if (!serverColors.containsKey(def.getServerName())) {
					serverColors.put(def.getServerName(), GraphColorChooser.COLORS[nbColors]);
					if (++nbColors == GraphColorChooser.COLORS.length) {
						nbColors = 0;
					}
				}

				if (def.getClassName().equals(listClas)) {
					blades.put(id, def);
					lastId = id;
				}
			}

			TableGraphComposite tgcl = new TableGraphComposite(ctabfolder,
					SWT.NONE, clifApp, blades, serverColors);
			tableGraph.put(listClas, tgcl);
			tgcl.setTimeFrame(Integer.parseInt(TIME_FRAME) * 1000);
			ctabitem.setControl(tgcl);
			fillCombo(listClas, lastId);
		}
        
        CTabItem[] items = ctabfolder.getItems();
        for (int i = 0; i < items.length; i++) {
            ((TableGraphComposite)items[i].getControl()).start();
        }
        
        /* Create alarm tab*/
        tableAlarm = new TableAlarmComposite(ctabfolder, SWT.NONE, testPlan, serverColors);
        CTabItem alarms = new CTabItem(ctabfolder, SWT.NONE);
        alarms.setText(TAB_ALARMS);
        alarms.setControl(tableAlarm);
    }
    
    /**
     * gets CTabItem which title is title
     * @param title title researched
     * @return CtabItem the requested tab item
     */
    public CTabItem getCTabItem(String title){
        CTabItem [] ctabitems = ctabfolder.getItems();
        String ctabS;
        for (int i = 0; i < ctabitems.length; i++) {
            ctabS = ctabitems[i].getText(); 
            if (ctabS.equals(title)){
                return ctabitems[i];
            }
        }
        return null;
    }
    
    /**
     * Fills the table with the blades for each Items.<p>
     * @param id 
     * @param bclass 
     */
    public void fillCTabItem(String id, String bclass){
        CTabItem ctabitem = getCTabItem(bclass);
        if (ctabitem != null) {
            TableGraphComposite tgc = (TableGraphComposite) ctabitem.getControl();
            
            BladeOption bop = new BladeOption(true, true, id);
            tgc.addBladeOption(bop);
        }
    }
    
    /**
     * Fills CCombo of CTabItem named bclass
     * @param bclass name of the CTabItem
     * @param bladeId reference id for state labels
     */
    public void fillCombo(String bclass, String bladeId){
        CTabItem ctabitem = getCTabItem(bclass);
        TableGraphComposite tgc = (TableGraphComposite) ctabitem.getControl();
        CCombo viewCombp = tgc.getViewComboInjectors();
        String[] comboElements = clifApp.getStatLabels(bladeId);
        
        viewCombp.removeAll();
        for (int i = 0; i < comboElements.length; i++) {
            viewCombp.add((String)comboElements[i]);
        }
        viewCombp.select(0);
        return;
    }
    
    /**
     * Select a tab
     * @param index index of tab
     */
    public void setSelection(int index){
        ctabfolder.setSelection(index);
    }
    
    // Implements Observer interface
    public void update(Observable o, Object observation) {
        try{
            if (observation instanceof BladeObservation)
            {
                final String id = ((BladeObservation)observation).getBladeId();
                final BladeState state = ((BladeObservation)observation).getState();
                
                final ClifDeployDefinition def = (ClifDeployDefinition)testPlan.get(id);
                
                if(state.equals(BladeState.STARTING) || state.equals(BladeState.RESUMING)) {
                    this.getDisplay().syncExec(new Runnable() {
                        public void run() {
                            beginTimer(def.getClassName(), id);
                        }
                    }); 
                }
                
                if (state.equals(BladeState.STOPPED)
                	|| state.equals(BladeState.ABORTED)
                    || state.equals(BladeState.SUSPENDED)
                    || state.equals(BladeState.COMPLETED)) {
                    this.getDisplay().syncExec(new Runnable() {
                        public void run() {
                            suspendTimer(def.getClassName(), id);
                        }
                    });
                }
                BladeState globalState = clifApp.getGlobalState(null);
                if (globalState.equals(BladeState.STOPPED)
                	|| globalState.equals(BladeState.ABORTED)
                    || globalState.equals(BladeState.COMPLETED))
                {
                    this.getDisplay().syncExec(new Runnable() {
                        public void run() {
                            CTabItem[] items = ctabfolder.getItems();
                            for (int i = 0; i < items.length-1; i++) {
                                ((TableGraphComposite)items[i].getControl()).stop();
                            }
                            clifApp.deleteObserver(TabItemFolderPrincipal.this);
                        }
                    });
                }
            }
            else if (observation instanceof AlarmObservation)
            {
                final AlarmObservation alarm = (AlarmObservation)observation;
                
                this.getDisplay().syncExec(new Runnable() {
                    public void run() {
                        addAlarm(alarm);
                    }
                });
            }
        }catch(SWTException e) {
            return;
        }
    }
    
    private void beginTimer(String bClass, String id) {
        TableGraphComposite tgc = tableGraph.get(bClass);
        tgc.beginTimer(id);
        
        tableAlarm.setBeginTime(id);
    }
    
    private void suspendTimer(String bClass, String id) {
        TableGraphComposite tgc = tableGraph.get(bClass);
        tgc.suspendTimer(id);
        
        tableAlarm.setSuspendedTime(id);
    }
    
    // Implements SelectionListener interface
    public void widgetSelected(SelectionEvent e) {
        Object source = e.getSource();
        
        if(source == refreshButton ) {
            try {
                interval = Integer.parseInt(intervalField.getText().trim()) * 1000;
            } catch(Exception ex){
                System.err.println("unexpected non-integer values in period fields, delay is set to 1 second");
                // default value
                interval = Integer.parseInt(TIME_FRAME) * 1000;
                intervalField.setText(POLLING_PERIOD);
            }
            try {
                timeFrame = Integer.parseInt(timeframeField.getText().trim()) * 1000;
            } catch(Exception ex){
                System.err.println("unexpected non-integer values in period fields, " +
                "total time is set to 100 second");
                // default value
                timeFrame = Integer.parseInt(TIME_FRAME) * 1000;
                timeframeField.setText(TIME_FRAME);
            }
            CTabItem[] items = ctabfolder.getItems();
            for (int i = 0; i < items.length-1; i++) {
            	TableGraphComposite tgc = (TableGraphComposite)items[i].getControl();
                tgc.setDelay(interval);
                tgc.setTimeFrame(timeFrame);
            }
        }
        else if(source == resetButton) {
            CTabItem[] items = ctabfolder.getItems();
            for (int i = 0; i < items.length-1; i++) {
                ((TableGraphComposite)items[i].getControl()).reset();
            }
        }
        else if(source == ctabfolder) {
            boolean alarm = ctabfolder.getSelection().getText().equals(TAB_ALARMS);
            controllersC.setVisible(!alarm);
            alarmControllerC.setVisible(alarm);
        }
        
        else if(source == clearAllButton) {
            tableAlarm.clearAll();
        }
        else if(source == clearButton) {
            tableAlarm.clear();
        }
    }
    
    public void widgetDefaultSelected(SelectionEvent e) {}

    /**
     * Add alarm to "Alarms" tab
     * @param alarmObs
     */
    public void addAlarm(AlarmObservation alarmObs) {
        tableAlarm.addAlarm(alarmObs);
    }
}
