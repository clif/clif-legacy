/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom R&D
 * Copyright (C) 2016 Orange SA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;

/**
 * Unique page for deployment wizard. Used to choose test plan to deploy
 *
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class MainTestDeploymentPage extends WizardPage {

	private ISelection selection;
	private Text containerText;
	private Combo testPlanCombo;

	/**
	 * Constructor for MainTestDeploymentPage
	 * @param selection
	 */
	public MainTestDeploymentPage(ISelection selection) {
		super("wizardPage");
		setTitle("New Test Deployment");
		setDescription("Choose test plan to deploy.");
		this.selection = selection;
	}

	/**
	 * Creates controls
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
		layout.horizontalSpacing = 10;


		// Container
		Label label = new Label(container, SWT.NULL);
		label.setText("&Container:");
		containerText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		containerText.setLayoutData(gd);
		containerText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowse();
			}
		});

		//Test Plan
		label = new Label(container, SWT.NULL);
		label.setText("&Test plan to deploy:");
		testPlanCombo = new Combo(container, SWT.READ_ONLY | SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan=2;
		testPlanCombo.setLayoutData(gd);

		initialize();
		setControl(container);
	}

	/**
	 * Initializes fields.
	 */
	private void initialize() {
		if (selection!=null && selection.isEmpty()==false && selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection)selection;
			if (ssel.size()>1) return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer)obj;
				else
					container = ((IResource)obj).getParent();
				containerText.setText(container.getFullPath().toString());
			}
		}
		dialogChanged();
	}

	/**
	 * Uses the standard container selection dialog to
	 * choose the new value for the container field.
	 */
	private void handleBrowse() {
		ContainerSelectionDialog dialog =
			new ContainerSelectionDialog(
					getShell(),
					ResourcesPlugin.getWorkspace().getRoot(),
					false,
			"Select new file container");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				containerText.setText(((Path)result[0]).toOSString());
			}
		}
	}

	/**
	 * Ensures that both text fields are set and valid.
	 * Container name is valid if it exists.
	 */
	private void dialogChanged() {
		testPlanCombo.removeAll();
		String containerName = getContainerName();
		if (containerName.length() == 0) {
			updateStatus("File container must be specified");
			return;
		}
		//Test if container exist
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(containerName));
		if (resource == null || !resource.exists() || !(resource instanceof IContainer)) {
			updateStatus("Invalid container");
			return;
		}
		try {
			for (int i=0 ; i < ((IContainer)resource).members().length;i++){
				IResource res = ((IContainer)resource).members()[i];
				findTestPlanFiles(res);
			}
		} catch (CoreException e) {
			updateStatus("Invalid container");
			return;
		}
        if (testPlanCombo.getItemCount() > 0) {
            String selectedFile = getSelectedFileFromSelection(selection);
            int indexOfSelectedFile = testPlanCombo.indexOf(selectedFile);
            // If not found, return 0
            if(indexOfSelectedFile<0){
                indexOfSelectedFile=0;
            }
            testPlanCombo.select(indexOfSelectedFile);
        } else {
			updateStatus("A test plan must be selected");
			return;
		}
		updateStatus(null);
	}

    /**
     * Return the content after the last '/' character of the selected element in navigator
     * @param selection selectioned element from navigator tree
     * @return The part after the last '/' character. The ']' which is present as the last character is removed.
     */
    private String getSelectedFileFromSelection(ISelection selection) {
        String[] decomposedPath = selection.toString().replaceAll("]","").split("/");
        if(decomposedPath.length>0){
            return decomposedPath[decomposedPath.length-1];
        }
        return "";
    }

    /**
	 * Adds resource file into test plan combo box if it's a test plan file
	 * @param res resource file
	 */
	private void findTestPlanFiles(IResource res)
	{
		if (res.getType()==IResource.FILE)
		{
			String ext = res.getFileExtension();
			if (ext != null && ext.equalsIgnoreCase("ctp")){
				String name = res.getName();
				testPlanCombo.add(name);
			}
		}
	}

	/**
	 * Show error message and allow "Finish" action or not.
	 * @param message the message to display if error
	 */
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	/**
	 * Return the container name.
	 * @return the container name
	 */
	public String getContainerName() {
		return containerText.getText();
	}

	/**
	 * Return test plan selected
	 * @return test plan selected
	 */
	public String getTestPlan() {
		return testPlanCombo.getText();
	}

}
