/*
* CLIF is a Load Injection Framework
* Copyright (C) 2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.egui;

import java.io.IOException;
import java.io.PrintStream;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * CLIF console for getting System.out and System.err in Eclipse's console view.
 * 
 * @author Bruno Dillenseger
 */
public class ClifConsole extends MessageConsole
{
	private PrintStream stdout, stderr;
	private MessageConsoleStream inMessageStream;

	/**
	 * Redirects System.out and System.err to the console view
	 */
	public ClifConsole()
	{
		super("CLIF Console", null);
		inMessageStream = newMessageStream();
		stdout = System.out;
		stderr = System.err;
		System.setOut(new PrintStream(inMessageStream));
		System.setErr(System.out);
	}


	/**
	 * Restores original System.out and System.err
	 * @throws IOException an exception occurred while closing
	 * the console view's input stream used to display messages
	 */
	public void close()
		throws IOException
	{
		System.setOut(stdout);
		System.setErr(stderr);
		inMessageStream.close();
	}
}
