/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.rcp;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.ow2.clif.analyze.api.AnalyzerExternalAccess;
import org.ow2.clif.deploy.ClifAnalyzerAppFacade;

/**
 * @author Joan Chaumont
 * @author Grégory Calonnier
 * @author Jordan Brunier
 */
public class ClifRunnable implements IApplication
{
	private AnalyzerExternalAccess analyzer = null;
	private ClifAnalyzerAppFacade clifAnalyzerApp = ClifAnalyzerAppFacade.getInstance();


	private URL promptForInstanceLoc(Display display)
	{
		Shell shell = new Shell(display);
		DirectoryDialog dialog = new DirectoryDialog(shell);
		dialog.setText("Select CLIF workspace directory");
		dialog.setMessage("Select the workspace directory for CLIF testing projects.");
		String dir = dialog.open();
		shell.dispose();
		try
		{
			return dir == null ? null : new File(dir).toURL();
		}
		catch (MalformedURLException ex)
		{
			return null;
		}
	}


	/**
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception
	{
		Display display = PlatformUI.createDisplay();
//		Location instanceLoc = Platform.getInstanceLocation();
//		instanceLoc.release();
//		URL url = promptForInstanceLoc(display);
//		if (url == null)
//		{
//			return EXIT_OK;
//		}
//		instanceLoc.setURL(url, true);
		try
		{
			int code = PlatformUI.createAndRunWorkbench(
				display,
				new ClifAdvisor());
			try
			{
				Component analyzerComp = clifAnalyzerApp.getComponentByName("analyzer");
				analyzer = (AnalyzerExternalAccess) analyzerComp.getFcInterface(
					AnalyzerExternalAccess.ANALYZER_EXTERNAL_ACCESS);
			}
			catch (NoSuchInterfaceException ex)
			{
				throw new Error("The analyzer component is not compatible.", ex);
			}
			return code == PlatformUI.RETURN_RESTART ? EXIT_RESTART : EXIT_OK;
		}
		finally
		{
			if (display != null)
			{
				display.dispose();
			}
		}
	}

	/**
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop()
	{
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null)
		{
			return;
		}
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run()
			{
				if (! display.isDisposed())
				{
					workbench.close();
				}
			}
		});
	}
}
