/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright (C) 2009, 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.console.lib.egui.monitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.deploy.ClifAppFacade;

/**
 * Content of CTabItems.
 * @author Tsirimiaina ANDRIANAVONIMIARINA JAONA
 * @author Florian Francheteau
 * @author Bruno Dillenseger
 */
public class TableGraphComposite
	extends Composite 
    implements
    	ISelectionChangedListener,
    	SelectionListener,
    	PaintListener
{    
    private TableViewer bladesTableviewer;
    private Table bladesTable;
    private List<BladeOption> listBlades;
    
    private CCombo viewComboInjectors;
    private Button collectDataCheckBox;
    private String pathToStoreMonitoring;
    
    private Graph canvas;
    private ActionTestReport updateTestReport;
    
    private static final String DISPLAY = "Show";
    private static final String COLLECT = "Listen";
    private static final String BLADE = "Id";
    private static final String TIME = "Time";
    
    private Timer timer;
    private Object mutex = new Object();
    
    private ClifAppFacade clifApp;

	private static int TIME_FRAME = 100000;

	/**
     * This class represents the cell modifier for the 
     * TableViewer of the Monitoring view
     */
    public class BladeOptionCellModifier implements ICellModifier {
        
        private TableViewer tableviewer;
        
        /**
         * Constructor.
         * @param tableviewer 
         */
        public BladeOptionCellModifier(TableViewer tableviewer) {
            super();
            this.tableviewer = tableviewer;
        }
        
        /**
         * Returns whether the property can be modified
         * 
         * @param element the element
         * @param property the property
         * @return boolean
         * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
         */
        public boolean canModify(Object element, String property) {
            if( TableGraphComposite.BLADE.equals(property)){
                return false;
            }
            return true;
        }
        
        /**
         * Returns the value for the property
         *
         * @param element the element
         * @param property the property
         * @return Object
         * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
         */
        public Object getValue(Object element, String property) {
            
            BladeOption bo = (BladeOption) element;
            
            if (TableGraphComposite.DISPLAY.equals(property)){
                return Boolean.valueOf(bo.isBooldisplay());
            }
            else if (TableGraphComposite.COLLECT.equals(property)){
                return Boolean.valueOf(bo.isBoolcollect());
            }
            
            return null;
        }
        
        /**
         * Modifies the element
         *
         * @param element the element
         * @param property the property
         * @param value the value
         * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
         */
        public void modify(Object element, String property, Object value) {
            
            if (element instanceof Item) element = ((Item) element).getData();
            
            BladeOption bo = (BladeOption) element;
            
            if (TableGraphComposite.DISPLAY.equals(property)){
                bo.setBooldisplay(((Boolean) value).booleanValue());
            }
            else if (TableGraphComposite.COLLECT.equals(property)){
                bo.setBoolcollect(((Boolean) value).booleanValue());
            }
            
            // Force the viewer to refresh
            tableviewer.refresh();
        }
    }
    
    public class BladeTableContentProvider implements  IStructuredContentProvider {
        
        /**
         * Constructor.
         */
        public BladeTableContentProvider() {
            super();
        }
        
        /**
         * Returns an array of BladeOption objects
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return ((List) inputElement).toArray();
        }
        
        /**
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {	}
        
        /**
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {	}
        
    }
    
    public class BladeLabelProvider implements ITableLabelProvider, IColorProvider {
        
        private Map<String,Integer> rgbId;
        private Display d;
        
        private ImageRegistry imageRegistry;
        
        public BladeLabelProvider(Display d, Map<String, ClifDeployDefinition> testPlan, Map<String,Integer> serverColors){
            imageRegistry = new ImageRegistry();
            
            rgbId = new HashMap<String,Integer>();
			for (String serverName : serverColors.keySet()) {				
				for (String id : testPlan.keySet()) {
					if ((testPlan.get(id)).getServerName().equals(serverName)) {
						rgbId.put(id, serverColors.get(serverName));
					}
				}
			}
            
            this.d = d;
            
            imageRegistry.put("checked", ClifConsolePlugin.imageDescriptorFromPlugin(
                    ClifConsolePlugin.PLUGIN_ID,"icons/checked.gif"));
            imageRegistry.put("unchecked", ClifConsolePlugin.imageDescriptorFromPlugin(
                    ClifConsolePlugin.PLUGIN_ID,"icons/unchecked.gif"));  
        }
        
        private Image getImage (boolean checked) {
            return (checked)?imageRegistry.get("checked"):imageRegistry.get("unchecked");
        }
        /**
         * Returns the image
         *
         * @param element the element
         * @param columnIndex the column index
         * @return Image
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            BladeOption bo = (BladeOption)element;
            
            switch(columnIndex) {
            case 0 :
                return getImage(bo.isBooldisplay());
            case 1 :
                return getImage(bo.isBoolcollect());
            case 2 :
                break;
            case 3 :
                break;
            }
            
            return null;
        }
        
        /**
         * Returns the column text
         * 
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
         */
        public String getColumnText(Object element, int columnIndex) {
            
            BladeOption bo = (BladeOption) element;
            
            switch(columnIndex) {
            case 0 :
                break;
            case 1 :
                break;
            case 2 :
                return bo.getBladeId();
            case 3 :
                return bo.getEllapsedTime();
            }
            return null;
        }
        
        /**
         * Disposes any created resources
         * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
         */
        public void dispose() {	}
        
        /**
         * Returns whether altering this property on this element will affect the label
         *
         * @param element the element
         * @param property the property
         * @return boolean
         * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
         */
        public boolean isLabelProperty(Object element, String property) {
            return false;
        }
        
        /**
         * Adds a listener
         *
         * @param listener the listener
         * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
         */
        public void addListener(ILabelProviderListener listener) { }
        
        /**
         * Removes a listener
         *
         * @param listener the listener
         * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
         */
        public void removeListener(ILabelProviderListener listener) { }

        public Color getForeground(Object element) {
            BladeOption bo = (BladeOption) element;
            
            return d.getSystemColor(((Integer)rgbId.get(bo.getBladeId())).intValue());
        }

        public Color getBackground(Object element) {
            return null;
        }
    }
    
    /**
     * @param parent TabFolder where this widget is added
     * @param style style 
     * @param clifApp 
     * @param selectedTestPlan 
     * @param serverColors 
     */
    public TableGraphComposite(Composite parent, int style, 
            ClifAppFacade clifApp, Map<String,ClifDeployDefinition> selectedTestPlan, Map<String, Integer> serverColors) {
        super(parent, style);
        this.clifApp = clifApp;

		// init part
        listBlades = new ArrayList<BladeOption>();
        
        GridLayout layoutm = new GridLayout(1, true);
        layoutm.verticalSpacing = 0;
        layoutm.marginHeight = 0;
        layoutm.marginWidth = 0;
        this.setLayout(layoutm);
        this.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        // Main Composite
        Composite pageItemContent = new Composite(this, SWT.NONE);
        GridLayout layout = new GridLayout(1, true);
        layout.verticalSpacing = 0;
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        pageItemContent.setLayout(layout);
        pageItemContent.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        // Composite which contains comboCheckTableC and the graph 
        final SashForm graphLineC = new SashForm(pageItemContent, SWT.NONE);
        GridLayout layout2 = new GridLayout(2, false);
        layout2.verticalSpacing = 0;
        layout2.marginHeight = 0;
        layout2.marginWidth = 0;
        graphLineC.setLayout(layout2);
        graphLineC.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        // Composite which contains tableComposite and the ComboCheck
        Composite comboTableC = new Composite(graphLineC, SWT.BORDER); //SWT.BORDER
        GridLayout layout3 = new GridLayout(1, true);
        layout3.verticalSpacing = 0;
        layout3.marginHeight = 0;
        layout3.marginWidth = 0;
        comboTableC.setLayout(layout3);
        comboTableC.setLayoutData(new GridData(GridData.FILL_VERTICAL));
       
        // Composite which contains table
        Composite tableComposite = new Composite(comboTableC, SWT.NONE);
        GridLayout flayout = new GridLayout(1, true);
        flayout.marginHeight = 0;
        flayout.marginWidth = 0;
        tableComposite.setLayout(flayout);
        tableComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
        
        // Add the TableViewer
        bladesTableviewer = new TableViewer(tableComposite,SWT.FULL_SELECTION | SWT.BORDER);
        bladesTableviewer.setContentProvider(new BladeTableContentProvider());
        bladesTableviewer.setLabelProvider(new BladeLabelProvider(parent.getDisplay(), selectedTestPlan, serverColors));
        bladesTableviewer.setInput(listBlades);
        
        // Set up the table
        bladesTable = bladesTableviewer.getTable();
        bladesTable.setLayoutData(new GridData(GridData.FILL_BOTH));
        new TableColumn(bladesTable, SWT.CENTER, 0).setText(DISPLAY);
        new TableColumn(bladesTable, SWT.CENTER, 1).setText(COLLECT);
        new TableColumn(bladesTable, SWT.CENTER, 2).setText(BLADE);
        new TableColumn(bladesTable, SWT.CENTER, 3).setText(TIME);
        
        for (int i = 0, n = bladesTable.getColumnCount(); i < n; i++) {
            bladesTable.getColumn(i).pack();
        }
        
        bladesTable.getColumn(0).addSelectionListener(this);
        bladesTable.getColumn(1).addSelectionListener(this);
        
        bladesTable.setHeaderVisible(true);
        bladesTable.setLinesVisible(true);
        
        String [] columnNames = new String[]{DISPLAY, COLLECT, BLADE, TIME};
        // Create the cell editors
        CellEditor[] editors = new CellEditor[columnNames.length];
        editors[0] = new CheckboxCellEditor(bladesTable); // Column 1 : Display
        editors[1] = new CheckboxCellEditor(bladesTable); // Column 2 : Collect
        
        // Set the editors, cell modifier, and column properties
        bladesTableviewer.setColumnProperties(columnNames);
        bladesTableviewer.setCellModifier(new BladeOptionCellModifier(bladesTableviewer));
        bladesTableviewer.setCellEditors(editors);
        bladesTableviewer.addSelectionChangedListener(this);
        
        viewComboInjectors = new CCombo(comboTableC, SWT.DROP_DOWN | SWT.READ_ONLY | SWT.BORDER);
        viewComboInjectors.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
        GridData gct = new GridData(GridData.FILL_HORIZONTAL);
        viewComboInjectors.setLayoutData(gct);
        viewComboInjectors.addSelectionListener(this);
      
        collectDataCheckBox = new Button(comboTableC, SWT.CHECK);
        collectDataCheckBox.setText("Store monitoring data");
        collectDataCheckBox.setLayoutData(gct);
        collectDataCheckBox.addSelectionListener(this);
        
        canvas = new Graph(graphLineC, SWT.V_SCROLL | SWT.H_SCROLL, selectedTestPlan, serverColors);
        canvas.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
        canvas.setTimeFrame(TIME_FRAME);
        
        // adjusts weight of "comboTableC" and "pointsTable"
        int [] weights = {2,5};
        graphLineC.setWeights(weights);
        
        updateTestReport = new ActionTestReport(clifApp, this);
        updateTestReport.setDelay(1000);
        canvas.addPaintListener(this);
        
        timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                try {
                    TableGraphComposite.this.getDisplay().syncExec(new Runnable() {
                        public void run () {
                            try {
                                bladesTableviewer.refresh();
                            } catch (SWTException e) {
                                timer.cancel();
                            }
                        }
                    });  
                }catch (SWTException e){
                    timer.cancel();
                }
            }
        }, new Date(), 1000);
    }

    /**
     * Get blades whose monitoring data must be picked up
     * @return blades which are true in listen column
     */
    public Object [] getInjectorsToCollect()
    {
        List<String> collectBlades = new ArrayList<String>();
        if(bladesTable.isDisposed())
        {
            return null;
        }
        for (int i=0; i < bladesTable.getItemCount(); i++)
        {
            if(((BladeOption)bladesTable.getItem(i).getData()).isBoolcollect())
            {
                String id = bladesTable.getItem(i).getText(2);
                collectBlades.add(id);
            }
        }
        return collectBlades.toArray();
    }

    /**
     * Get blades whose monitoring data must be drawn
     * @return blades which are true in draw column
     */
    public String[] getInjectorsToDisplay()
    {
        List<String> displayBlades = new ArrayList<String>();
        if(bladesTable.isDisposed())
        {
            return null;
        }
        for(int i=0; i < bladesTable.getItemCount(); i++)
        {	  
            if(((BladeOption)bladesTable.getItem(i).getData()).isBooldisplay())
            {
                String id = bladesTable.getItem(i).getText(2);
                displayBlades.add(id);
            }
        }
        return displayBlades.toArray(new String[displayBlades.size()]);
    }
    
    
    /**
     * Store monitoring data in a CSV File
     * @param bladeId 	identifier of the blade
     * @param label 	blade labels
     * @param time		time of measure to collect
     * @param stat		blade statistics (corresponding to the label)
     */
    public void storeMonitoring(String bladeId, String[] label, int time, long[] stat){
    	File f = new File(pathToStoreMonitoring + "/" + (String)bladeId+".csv");
    	FileOutputStream fileOutput;
    	FileChannel channel;
    	String writeOutput;
    	time = time/1000;
    	try {
    		if (!f.exists()){
				f.createNewFile();
				fileOutput = new FileOutputStream(f,false);
	    		channel = fileOutput.getChannel();
	    		channel.tryLock();
	    		writeOutput = "#Time(sec),";
	    		fileOutput.write(writeOutput.getBytes());
				for (int i=0; i<label.length-1;i++){
					writeOutput = label[i]+",";
					fileOutput.write(writeOutput.getBytes());
				}
				writeOutput = label[label.length-1]+"\n";
				fileOutput.write(writeOutput.getBytes());
				IWorkspaceRoot workspace = ResourcesPlugin.getWorkspace().getRoot();
	    		workspace.refreshLocal(IProject.DEPTH_INFINITE, null);
			}else{
				fileOutput = new FileOutputStream(f,true);
				channel = fileOutput.getChannel();
	    		channel.tryLock();
			}
    		writeOutput = time+",";
    		fileOutput.write(writeOutput.getBytes());
			for (int i=0; i<stat.length-1;i++){
				writeOutput = stat[i]+",";
				fileOutput.write(writeOutput.getBytes());
			}
			writeOutput = stat[stat.length-1]+"\n";
			fileOutput.write(writeOutput.getBytes());
			fileOutput.close();
		} catch (IOException ex) {
			ex.printStackTrace(System.err);
    	} catch (CoreException e) {
    		e.printStackTrace();
		}
    }
    
    /**
     * Returns (or creates if directories don't exist) the path where the CSV file will be filled
     * @param 	tabTitle	name of the tab where data will be stored
     * @return 	String	that corresponds to the path where the CSV file will be filled
     */
    public String constructStatsTree(String tabTitle)
    {
    	File csvFile = new File(
    		new File(System.getProperty("clif.eclipse.statdir"), clifApp.getCurrentTestId()),
    		tabTitle);
    	csvFile.mkdirs();
    	return csvFile.getPath();
    }
    
    /**
     * Gets value of Collect check box
     * @return boolean 	corresponding to the value of collectDataCheckBox
     */
    public boolean getCollectDataValue() {
        return collectDataCheckBox.getSelection();
    }    

    /**
     * Adds a BladeOption to listBlades.
     * @param bladeOption BladeOption to add
     */ 
    public void addBladeOption(BladeOption bladeOption){
        listBlades.add(bladeOption);
        bladesTableviewer.refresh();
        
        canvas.setLabels(clifApp, bladeOption.getBladeId());
    }
    
    /**
     * Removes last BladeOption added in listBlades.
     */
    public void removeLastBladeEntered() {
        if (listBlades.size() > 0) {
            listBlades.remove(listBlades.size() - 1);
            bladesTableviewer.refresh();
        }
    }
    
    /**
     * Removes all BladeOption
     */
    public void removeAllBlades(){
        listBlades.clear();
    }

    /**
     * Get the canvas where graph are drawn
     * @return Graph the canvas.
     */
    public Graph getCanvas() {
        return canvas;
    }
    
    /**
     * Get the combo
     * @return CCombo the combo
     */
    public CCombo getViewComboInjectors() {
        return viewComboInjectors;
    }
        
    /**
     * Function called when the test is starting.
     * Begin the timer for the requested blade.
     * @param id
     */
    public void beginTimer(String id)
    {
        Iterator<BladeOption> iter = listBlades.iterator();
        while (iter.hasNext())
        {
            BladeOption bo = iter.next();
            if(bo.getBladeId().equals(id))
            {
                bo.beginTimer();
                return;
            }
        }
    }

    /**
     * Function called when the test is suspending.
     * Suspend the timer for the requested blade.
     * @param id
     */
    public void suspendTimer(String id)
    {
        Iterator<BladeOption> iter = listBlades.iterator();
        while (iter.hasNext())
        {
            BladeOption bo = iter.next();
            if(bo.getBladeId().equals(id))
            {
                bo.suspendTimer();
                return;
            }
        }
    }

    /**
     * Redraw graph with the new collected stat
     */
    public void updateGraph() {
        canvas.redraw();
    }

    /**
     * Add point in the graph
     * @param injectorID
     * @param label 
     * @param totalTime
     * @param stat
     */
    public void addPoint(String injectorID, String label, int totalTime, long stat) {
        canvas.addPoint(injectorID,label,totalTime,stat);
    }

    /**
     * Remove all points in the graph
     */
    public void removeAllPoints() {
        canvas.removeAllPoints();
    }

    public void dispose() {
        synchronized (mutex) {
            super.dispose();
            mutex.notify();
        }
    }

    //////////////////////////////////////////
    // interface ISelectionChangedListener  //
    //////////////////////////////////////////
    /**
     * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
     */
    public void widgetDefaultSelected(SelectionEvent event) {
        updateGraph();
    }

    /**
     * Calls when user clicks the cells of blades Table
     * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
     */
    public void selectionChanged(SelectionChangedEvent event) {
        Object source = event.getSource();
        
        if (source == this.bladesTableviewer){
            IStructuredSelection s =  (IStructuredSelection)bladesTableviewer.getSelection();
            canvas.setPriority((((BladeOption)s.getFirstElement()).getBladeId()));
        }
        updateGraph();
    }
    
    public void paintControl(PaintEvent e) {
        String[] ids = getInjectorsToDisplay();
        String label = viewComboInjectors.getText();
        canvas.drawGraph(ids, label);
    }

    //////////////////////////////////
    // interface SelectionListener  //
    //////////////////////////////////
    /**
     * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
     */
    public void widgetSelected(SelectionEvent e) {
        Object source = e.getSource();

        if (source == this.viewComboInjectors) {
            updateGraph();
        }
        else if(source instanceof TableColumn) {
            String colText = ((TableColumn)source).getText();
            
            if(colText == DISPLAY) {
                checkUncheckDisplay();
            }
            else {
                checkUncheckCollect();
            }
        }
        else if(source == this.collectDataCheckBox){
        	if (collectDataCheckBox.getSelection()){
        		pathToStoreMonitoring = constructStatsTree(((CTabFolder)this.getParent()).getSelection().getText());
        	}
        }
    }

    private void checkUncheckDisplay() {
        boolean unchecked = true;
    
        for(int i=0; i < bladesTable.getItemCount(); i++) {
            BladeOption bo = (BladeOption)bladesTable.getItem(i).getData();
            if(!bo.isBooldisplay()) {
                bo.setBooldisplay(true);
                unchecked = false;
            }
        }
        if(unchecked) {
            for(int i=0; i < bladesTable.getItemCount(); i++) {
                ((BladeOption)bladesTable.getItem(i).getData()).setBooldisplay(false);
            }
        }
    }

    private void checkUncheckCollect() {
        boolean unchecked = true;
        
        for(int i=0; i < bladesTable.getItemCount(); i++) {
            BladeOption bo = (BladeOption)bladesTable.getItem(i).getData();
            if(!bo.isBoolcollect()) {
                bo.setBoolcollect(true);
                unchecked = false;
            }
        }
        if(unchecked) {
            for(int i=0; i < bladesTable.getItemCount(); i++) {
                ((BladeOption)bladesTable.getItem(i).getData()).setBoolcollect(false);
            }
        }
    }

    /**
     * Set delay interval beetween two graph redraw
     * @param delay
     */
    public void setDelay(int delay) {
        updateTestReport.setDelay(delay);
    }

    /**
     * Reset graph draw
     */
    public void reset() {
        updateTestReport.reset();
    }

    /**
     * Set nbpoints to display
     * @param nbPoints
     */
    public void setTimeFrame(int nbPoints) {
        canvas.setTimeFrame(nbPoints);
    }
    
    /**
     * Start drawing thread
     */
    public void start () {
        updateTestReport.start();
    }

    /**
     * Stop drawing thread
     */
    public void stop() {
        updateTestReport.stopThread();
    }    
}
