/*
 * CLIF is a Load Injection Framework
 * Copyright 2005 Manuel Azema, Tsirimiaina Andrianavonimiarina Jaona
 * Copyright 2010 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui;


import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * An Eclipse perspective for the CLIF Console : CLIF Console perspective.
 * This perspective have a navigator and a properties view on the left,
 * an editor view and a supervision monitor view on the bottom.
 * 
 * @author Manuel AZEMA
 * @author Bruno Dillenseger
 */
public class ClifConsolePerspective implements IPerspectiveFactory {
    /**
     * CLIF Console perspective Id
     */
    public static final String CLIF_CONSOLE_PERSPECTIVE_ID = 
        "org.ow2.clif.console.plugin.clifConsolePerspective";
    
    /**
     * Monitor view id
     */
    public static final String ID_MONITOR_VIEW = 
        "org.ow2.clif.console.plugin.monitorView";
    /**
     * Tree view id
     */
    public static final String ID_TREE_VIEW = 
        "org.ow2.clif.console.plugin.clifTreeView";
        /**
     * Tree view id
     */
    public static final String ID_SWING_VIEW=
        "org.ow2.clif.console.plugin.SwingAnalyzePage";

    /**
     * Create the Clif Console Perspective with:
     * <ul>
     * <li>Navigator view on the top left</li>
     * <li>ClifTree view on the bottom left</li>
     * <li>Properties view on the bottom left</li>
     * <li>Editor on the center</li>
     * </ul>
     */
    public void createInitialLayout(IPageLayout layout) {
		/* Get the editor area. */
        String editorArea = layout.getEditorArea();
        
        /* Top left: Resource Navigator view and Bookmarks view placeholder */
        IFolderLayout topLeft = layout.createFolder("topLeft", 
                IPageLayout.LEFT, 0.20f, editorArea);
        topLeft.addView(IPageLayout.ID_RES_NAV);
        
        /* Bottom left: Tree view */
        IFolderLayout bottomLeft = layout.createFolder("bottomLeft", 
                IPageLayout.BOTTOM, 0.66f, "topLeft");
        bottomLeft.addView(ID_TREE_VIEW);
        
        /* Bottom right : Monitor view and Console view */
        IFolderLayout bottomRight = layout.createFolder("bottomRight",
                IPageLayout.BOTTOM, 0.66f, editorArea);
        bottomRight.addView(ID_MONITOR_VIEW);
        bottomRight.addView(IConsoleConstants.ID_CONSOLE_VIEW);

        /* Add CLIF view in Show View menu */
        layout.addShowViewShortcut(IPageLayout.ID_RES_NAV);
        layout.addShowViewShortcut(ID_TREE_VIEW);
        layout.addShowViewShortcut(ID_MONITOR_VIEW);
        layout.addShowViewShortcut(ID_SWING_VIEW);
        layout.addShowViewShortcut(IConsoleConstants.ID_CONSOLE_VIEW);
    }
}
