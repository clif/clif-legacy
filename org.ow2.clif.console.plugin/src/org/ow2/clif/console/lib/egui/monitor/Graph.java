/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2008, 2009 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.egui.monitor;

import java.util.*;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.egui.ClifConsolePlugin;
import org.ow2.clif.deploy.ClifAppFacade;

/**
 * Extends canvas for drawing the curve of blade's stats.
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 * @author Florian Francheteau
 */
public class Graph extends Canvas implements SelectionListener, KeyListener {
	private Map<String,Map<String,List<Point>>> listPoints;
	private Map<String,Integer> maxValues;
	private String currentLabel;

	//components for the zoom feature on the X axis
	private Button zoomInXButton;
	private Button zoomOutXButton;
	private Text textX;

	private ScrollBar barX;
	private int hSelection;
	private int initMinTime;
	private int zoomX = 1;
	private int timeFrameWithoutZoom;

	//components for the zoom feature on the Y axis
	private Button zoomInYButton;
	private Button zoomOutYButton;
	private Text textY;

    private Composite compZoom;
	private ScrollBar barY;
	private int zoomY = 1;
	private int initMax=0;
	private int minY=0;

	private int time;
	private int timeFrame;
	private int minTime;

	private final int MAX_ZOOM_X=16;
	private final int MAX_ZOOM_Y=999;

	private int marginH;
	private int marginW;

	/* rgbId = colors map by id
	 * priority represents the blade to be drawn at first plan*/
	private Map<String,Integer> rgbId;
	private String priority;

	/* Coords of mouse pointer click*/
	private int coordsX;
	private int coordsY;


    /* These values are used for scale draw.
      * Used with the calculateScale function, this returns
      * correct scale values depending on the max value
      * of the graph. */
    static private final int[][] scaleValues =
	{
		{4,8},
		{4,8,12},
		{4,8,12,16},
		{10,20},
		{10,20,30},
		{10,20,30,40},
		{20,40},
		{20,40,60},
		{20,40,60},
		{20,40,60,80},
		{20,40,60,80}
	};

    /**
	 * Constructor
	 * @param parent
	 * @param style
	 * @param testPlan
	 * @param serverColors
	 */
	public Graph(Composite parent, int style, Map<String, ClifDeployDefinition> testPlan,
				 Map<String, Integer> serverColors) {
		super(parent, style);

		listPoints = new HashMap<String,Map<String,List<Point>>>();
		maxValues = new HashMap<String,Integer>();

		rgbId = new HashMap<String,Integer>();
		priority = "";

		/* A color for each server.
		 * Initialize a hashmap in listpoints for each blade*/
		for (String serverName : serverColors.keySet()) {
			for (String id : testPlan.keySet()) {
				if ((testPlan.get(id)).getServerName().equals(serverName)) {
					rgbId.put(id, serverColors.get(serverName));
				}

				listPoints.put(id, new HashMap<String, List<Point>>());
			}
		}
		time = 0;

		//icons for zoom in and zoom out buttons
		ImageRegistry imageRegistry = new ImageRegistry();
		imageRegistry.put("magnify",  ClifConsolePlugin.imageDescriptorFromPlugin(
				ClifConsolePlugin.PLUGIN_ID,"icons/magnify.png"));
        imageRegistry.put("zoomInX",  ClifConsolePlugin.imageDescriptorFromPlugin(
				ClifConsolePlugin.PLUGIN_ID,"icons/zoomInx.png"));
		imageRegistry.put("zoomOutX",  ClifConsolePlugin.imageDescriptorFromPlugin(
				ClifConsolePlugin.PLUGIN_ID,"icons/zoomOutx.png"));
		imageRegistry.put("zoomInY",  ClifConsolePlugin.imageDescriptorFromPlugin(
				ClifConsolePlugin.PLUGIN_ID,"icons/zoomIny.png"));
		imageRegistry.put("zoomOutY",  ClifConsolePlugin.imageDescriptorFromPlugin(
				ClifConsolePlugin.PLUGIN_ID,"icons/zoomOuty.png"));

        compZoom = new Composite(this, SWT.BORDER);
		GridLayout gridLayoutZoom = new GridLayout(1, true);
		gridLayoutZoom.marginTop=0;
		gridLayoutZoom.marginBottom=0;
		gridLayoutZoom.marginHeight=5;
		gridLayoutZoom.marginWidth=0;
		gridLayoutZoom.marginLeft=0;
		gridLayoutZoom.marginRight=0;
        compZoom.setLayout(gridLayoutZoom);

        Label label = new Label(compZoom,SWT.NONE);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.CENTER;
		gridData.grabExcessHorizontalSpace = true;
        label.setLayoutData(gridData);
        label.setImage(imageRegistry.get("magnify"));

        zoomInXButton = new Button(compZoom,SWT.PUSH);
		zoomInXButton.setImage(imageRegistry.get("zoomInX"));
		zoomInXButton.addSelectionListener(this);
		textX = new Text(compZoom,SWT.BORDER);
		textX.setTextLimit(2);
		textX.setText(String.valueOf(zoomX));
		textX.addKeyListener(this);
		zoomOutXButton = new Button(compZoom,SWT.PUSH);
		zoomOutXButton.setImage(imageRegistry.get("zoomOutX"));
		zoomOutXButton.addSelectionListener(this);
		zoomOutXButton.setEnabled(false);

		hSelection=0;
		barX = this.getHorizontalBar();
		barX.setMaximum(barX.getThumb());
		barX.setIncrement(barX.getMaximum());
		//listener for horizontal scrollbar
		barX.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				int diff = barX.getSelection()-hSelection;
				minTime = minTime + (diff*(timeFrame/barX.getThumb()));
				hSelection=barX.getSelection();
			}
		});

		zoomInYButton = new Button(compZoom,SWT.PUSH);
		zoomInYButton.setImage(imageRegistry.get("zoomInY"));
		zoomInYButton.addSelectionListener(this);

		textY = new Text(compZoom,SWT.BORDER);
		textY.setTextLimit(3);
		textY.setText(String.valueOf(zoomY));
		textY.addKeyListener(this);
		zoomOutYButton = new Button(compZoom,SWT.PUSH);
		zoomOutYButton.setImage(imageRegistry.get("zoomOutY"));
		zoomOutYButton.addSelectionListener(this);
		zoomOutYButton.setEnabled(false);
		hSelection=0;
		barY = this.getVerticalBar();
		barY.setMaximum(barY.getThumb());
		barY.setIncrement(barY.getMaximum());

		/* Calculate margin (sufficient space for scale values)*/
		GC gc = new GC(this);
		marginH = gc.getFontMetrics().getHeight() + 4;
		marginW = gc.getFontMetrics().getAverageCharWidth()*10 + 4;
		gc.dispose();

		/* Mouse listener for getting coords under the mouse */
		coordsX = coordsY = -1;
		this.addMouseTrackListener(new MouseTrackListener() {
			public void mouseEnter(MouseEvent e) {}
			public void mouseExit(MouseEvent e) {}

			public void mouseHover(MouseEvent e) {
				Rectangle rec = getClientArea();
				if(e.x >= marginW && e.x <= rec.width - marginW
						&& e.y >= marginH && e.y <= rec.height - marginH) {
					coordsX = e.x;
					coordsY = e.y;
				}
				else {
					coordsX = coordsY = -1;
				}
				drawCoords(maxValues.get(currentLabel));
			}
		});
	}

	/**
	 * Creates a LinkedList for each labels for this blade
	 * @param clifApp the clif app where blade is deploy
	 * @param id the blade id
	 */
	public void setLabels(ClifAppFacade clifApp, String id){
		String[] labels = clifApp.getStatLabels(id);
		for (int i = 0; i < labels.length; i++) {
			listPoints.get(id).put(labels[i], new LinkedList<Point>());
			listPoints.get(id).get(labels[i]).add(new Point(0,0));
			if(!maxValues.containsKey(labels[i])) {
				maxValues.put(labels[i],0);
			}
		}
	}

	/**
	 * Add point in graph
	 * @param injector
	 * @param label
	 * @param time
	 * @param stat
	 */
	public void addPoint(String injector, String label, int time, long stat)
	{
		if (this.time < time)
		{
			this.time = time;
		}
		int st = (int)stat;
		Map<String,List<Point>> injectorPoints = listPoints.get(injector);
		if(injectorPoints.get(label) != null)
		{
			injectorPoints.get(label).add(new Point(time, st));
			if(st > maxValues.get(label))
			{
				maxValues.put(label,st);
			}
		}
	}

	private void removePoints()
	{
		//points are removed regardless of zoom level
		if(time > timeFrameWithoutZoom + initMinTime)
		{
			int tmpTime = initMinTime;
			initMinTime=time-timeFrameWithoutZoom;
			minTime=minTime + (initMinTime-tmpTime);
			Iterator<Map<String,List<Point>>> iter = listPoints.values().iterator();
			while (iter.hasNext())
			{
				Map<String,List<Point>> labels = iter.next();
				Iterator<List<Point>> labelsIter = labels.values().iterator();
				while (labelsIter.hasNext())
				{
					List<Point> points = labelsIter.next();
					while (points.size() > 0 && points.get(0).x < initMinTime)
					{
						points.remove(0);
					}
				}
			}
		}
	}

	/**
	 * Remove all points in the listPoints.
	 */
	public void removeAllPoints()
	{
		Iterator<Map<String,List<Point>>> iter = listPoints.values().iterator();
		while (iter.hasNext())
		{
			Map<String,List<Point>> labels = iter.next();
			Iterator<List<Point>> labelsIter = labels.values().iterator();
			while (labelsIter.hasNext())
			{
				labelsIter.next().clear();
			}
		}
		Iterator<String> iterMax = maxValues.keySet().iterator();
		while (iterMax.hasNext())
		{
			maxValues.put(iterMax.next(), 0);
		}
		minTime = 0;
		initMinTime=0;
		time = 0;
	}

	/**
	 * Draw the graph for each id in ids
	 * @param ids the blades id to draw
	 * @param label the label to draw
	 */
	public void drawGraph(String[] ids, String label) {
		removePoints();

		currentLabel = label;
		Rectangle rec = getClientArea();
		Rectangle graph = new Rectangle(marginW, marginH, rec.width-2*marginW, rec.height-2*marginH);

		drawXAxis();
		drawYAxis();

		if(!label.equals("") && ids != null) {
			drawScale(label);

			int max = maxValues.get(label);
			int localMax = rec.height;

			boolean drawPriority = false;

			for (int i = 0; i < ids.length; i++) {
				if(!ids[i].equals(priority)) {
					localMax = drawCurve(ids[i], label, graph, max, localMax);
				}
				else {
					drawPriority = true;
				}
			}
			if(drawPriority) {
				localMax = drawCurve(priority, label, graph, max, localMax);
			}
			drawTime(localMax);
			drawCoords(max);
		}

		compZoom.setLocation(graph.width+marginW,graph.height/2+marginH-compZoom.getSize().y/2);
        compZoom.pack();
		textX.setSize(25,20);
		textY.setSize(25,20);
	}

	/**
	 * Set the blade id which will be draw in first plan.
	 * @param bladeId
	 */
	public void setPriority(String bladeId) {
		priority = bladeId;
	}

	/**
	 * Set max time reference used for X coords
	 * @param timeFrame
	 */
	public void setTimeFrame(int timeFrame) {
		this.timeFrame = timeFrame;
		this.timeFrameWithoutZoom = timeFrame;
	}

	/* All draw function : axis, coords, time, curve and scale */

	private void drawXAxis () {
		Rectangle rec = getClientArea();
		GC gc = new GC(this);
		gc.setLineWidth(2);
		gc.drawLine(marginW,rec.height-marginH,rec.width-marginW,rec.height-marginH);

		gc.dispose();
	}

	private void drawYAxis () {
		Rectangle rec = getClientArea();
		GC gc = new GC(this);
		gc.setLineWidth(2);
		gc.drawLine(marginW,rec.height-marginH,marginW,marginH);

		gc.dispose();
	}

	private void drawTime(int max) {
		Rectangle rec = getClientArea();
		GC gc = new GC(this);

		int timePos = (int)((long)(time - minTime) * (rec.width-2*marginW) / timeFrame + marginW);

		Rectangle graph = new Rectangle(0, marginH, rec.width-2*marginW, rec.height-2*marginH);

		int dec = getDecimals(timeFrame);

		int step = scaleValues[calculateScale(timeFrame)][0]*dec/1000;

		/* Set values nbVal*2 if size is sufficient */
		if(graph.width/50 > (timeFrame/1000)/step*2) {
			step = step/2;
		}

		/* Steps before minTime */
		int i = 1;
		while(step*i < minTime/1000) {
			i++;
		}

		/* Draw values */
		int maxHeight = rec.height - marginH + 2;
		gc.setLineWidth(1);
		gc.setLineStyle(SWT.LINE_SOLID);
		while (step*i <= (minTime+timeFrame)/1000) {
			int val = step*i++;
			int pos = (int)(((long)val*1000-minTime) * graph.width/timeFrame + marginW);
			if(pos >= marginW && pos < graph.width + marginW) {
				gc.drawText(FormatTime.getTime(val*1000), pos, maxHeight);
				gc.drawLine(pos,rec.height-marginH,pos,rec.height-marginH+3);
			}
		}

		if(timePos >= marginW && timePos <= graph.width + marginW) {
			gc.setLineStyle(SWT.LINE_DOT);
			gc.drawLine(timePos,rec.height-marginH,timePos,max);
			gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_RED));
			gc.drawText(FormatTime.getTime(time), timePos, maxHeight);
		}
		gc.dispose();
	}

	private void drawCoords(int max) {
		GC gc = new GC(this);
		Rectangle graph = new Rectangle(marginW, marginH,
				getClientArea().width-2*marginW, getClientArea().height-2*marginH);

		gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		gc.fillRectangle(0,0,getClientArea().width,marginH-1);
		gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_BLACK));

		if(coordsX != -1) {
			int time = (int)((coordsX-marginW)*(long)timeFrame/graph.width + minTime);
			int val = (int)((graph.height + marginH - coordsY)*(long)max/graph.height);
			//Y value depends on zoom level
			double tmp = barY.getMaximum()-(barY.getSelection()+barY.getThumb());
			double tmp2 = (double)max/(double)barY.getMaximum();
			tmp = tmp * tmp2;
			tmp = tmp + ((double)val/(double)zoomY);
			val = (int)tmp;
			gc.drawString("Time: " + time/1000 + " Value: " + val, 0, 0);
		}
		else {
			gc.drawText("Time: - Value: -", 0, 0);
		}
		gc.dispose();
	}

	/**
	 * Draw the curve for this blade.
	 * @param id
	 * @param label
	 * @param graph
	 * @param max
	 * @param localMax
	 * @return int
	 */
	private int drawCurve(String id, String label, Rectangle graph, int max, int localMax) {
		GC gc = new GC(this);

		Map<String,List<Point>> injectorPoints = listPoints.get(id);
		List<Point> pointList = injectorPoints.get(label);
		Point[] points = pointList.toArray(new Point[pointList.size()]);
		//number of points to display. Points outside the X zoomed area are not displayed
		int pointsToDisplay=0;

		max = calculateMaxYValue(max);

		//Calculate the number of points to Display
		for (int j = 0; j < points.length; j++) {
			Point p = points[j];
			if ((p.x>=minTime)&&(p.x<=minTime+timeFrame)){
				pointsToDisplay++;
			}
		}
		int[] intPoints = new int[pointsToDisplay*2];
		int i=0;
		int stop=0;
		int start=0;
		int sup=0;
		int between=0;
		gc.setForeground(getDisplay().getSystemColor(((Integer)rgbId.get(id)).intValue()));
		for (int j = 0; j < points.length; j++) {

			Point p = points[j];
			//Only points inside the X zoomed area are displayed
			if ((p.x>=minTime)&&(p.x<=minTime+timeFrame)){
				intPoints[i*2] = (int) ((p.x-minTime) * (long)graph.width / timeFrame+marginW);
				//if point is below Y zoomed area
				if ((max == 0)||(p.y<minY)) {
					intPoints[i*2+1] = (graph.height + marginH);
					if (max!=0){
						//if we have to draw a line connecting a point over and a point below a zoomed area
						if (between==2){
							int[] partCurve = new int[4];
							partCurve[0]=intPoints[(i*2)-2];
							partCurve[1]=marginH;
							partCurve[2]=intPoints[i*2];
							partCurve[3]=(graph.height + marginH);
							gc.drawPolyline(partCurve);
						}
						between=1;
					}
					if (start!=stop+1){
						if (sup==0){
							drawCurvePortion(stop,start,intPoints,gc, 2);
							start=i+1;
						}else if (start!=0) {
							drawCurvePortionWithStartLine(sup, stop, start, graph.height, intPoints, gc, 2);
							start=i+1;
						}
					}else{
						start=i+1;
						stop=i;
					}
					if (max!=0){
						sup=1;
					}
				}
				//if point is over Y zoomed area
				else if (p.y>max){
					intPoints[i*2+1] = marginH;
					//if we have to draw a line connecting a point below and a point over a zoomed area
					if (between==1){
						int[] partCurve = new int[4];
						partCurve[0]=intPoints[(i*2)-2];
						partCurve[1]=(graph.height + marginH);
						partCurve[2]=intPoints[i*2];
						partCurve[3]=marginH;
						gc.drawPolyline(partCurve);
					}
					between=2;
					if (start!=stop+1){
						if (sup==0){
							drawCurvePortion(stop,start,intPoints,gc, 2);
							start=i+1;
						}else if (start!=0) {
							drawCurvePortionWithStartLine(sup, stop, start, graph.height, intPoints, gc, 2);
							start=i+1;
						}

					}else{
						start=i+1;
						stop=i;
					}
					sup=2;
				}else{
					//if point is in Y zoomed area
					intPoints[i*2+1] = graph.height - ((int)((long)(p.y-minY) * (long)graph.height / (long)(max-minY)))+marginH;
					stop=i+1;
					between=0;
				}
				i++;

			}
		}
		if (intPoints.length > 0 && intPoints[intPoints.length-1] < localMax)
		{
			localMax = intPoints[intPoints.length-1];
		}
		// draw the final curve portion
		if (start!=stop+1){
			if (sup==0){
				drawCurvePortion(stop, start, intPoints, gc, 0);
			}else if (start!=0){
				drawCurvePortionWithStartLine(sup, stop, start, graph.height, intPoints, gc, 0);
			}
		}
		gc.dispose();
		return localMax;
	}

	/**
	 * Draw a portion of a curved line using the initial list of points
	 * @param stop			integer representing the point located at the end of the curve
	 * @param start 		integer representing the point located at the beginning of the curve
	 * @param intPoints		initial list of points
	 * @param gc			graph composite
	 * @param type			if it's an intermediate drawn curve, type =2. If it's the final
	 * drawn curve, type=0
	 */
	private void drawCurvePortion(int stop, int start, int[] intPoints, GC gc, int type){
		int[] partCurve = new int[(stop-start)*2+type];
		int m=0;
		for (int k=start*2;k<(stop*2+type);k++){
			partCurve[m]=intPoints[k];
			m++;
		}
		gc.drawPolyline(partCurve);
	}

	/**
	 * Draw a portion of a curved line using the initial list of points. Draw a start line if
	 * first point is below or over zoomed area
	 * @param sup			specifies if first point is below or over zoomed area
	 * @param stop			integer representing the point located at the end of the curve
	 * @param start 		integer representing the point located at the beginning of the curve
	 * @param height		height of graph
	 * @param intPoints		initial list of points
	 * @param gc			graph composite
	 * @param type			if it's an intermediate drawn curve, type =2. If it's the final
	 * drawn curve, type=0
	 */
	private void drawCurvePortionWithStartLine(int sup, int stop, int start, int height, int[] intPoints, GC gc, int type){
		int[] partCurve = new int[(stop-start)*2+2+type];
		int m=2;
		for (int k=start*2;k<(stop*2+type);k++){
			partCurve[m]=intPoints[k];
			m++;
		}
		partCurve[0]=intPoints[(start*2)-2];
		if (sup==1){
			partCurve[1]=(height + marginH);
		}else {
			partCurve[1]= marginH;
		}
		gc.drawPolyline(partCurve);
	}


	/* Functions used for calculating the scale values */

	private void drawScale(String label){
		Rectangle rec = getClientArea();
		GC gc = new GC(this);
		Rectangle graph = new Rectangle(0, marginH, rec.width-2*marginW, rec.height-2*marginH);
		gc.setLineStyle(SWT.LINE_DASH);

		//calculate maximum and minimum for Y axis depending on zoom level
		int max = maxValues.get(label);
		initMax=max;
		max = calculateMaxYValue(initMax);
		minY = (int)((double)max -((double)initMax/(double)zoomY));

		int dec = getDecimals(max-minY);
		int [] values = null;
		int []tmp = null;
		tmp = scaleValues[calculateScale(max-minY)];


		//Special case if max<10
		if(max-minY < 10 && max-minY > 1) {
			values = new int[tmp.length];
			for (int i = 0; i < values.length; i++) {
				values[i]=tmp[i]/10;
			}
		}
		else {
			//Set values nbVal*2 if size is sufficient
			if(graph.height/50 > tmp.length*2) {
				int middle = tmp[0]/2;
				if(tmp[tmp.length-1] < max-middle*dec) {
					values = new int[tmp.length*2+1];
				}
				else {
					values = new int[tmp.length*2];
				}

				for (int i = 1; i <= values.length; i++) {
					values[i-1] = middle*i;
				}
			}
			else {
				values = tmp;
			}
		}

		if(max != 0) {
			int maxWidth = rec.width-marginW;
			int maxPos = marginH;
			//Draw scaleValues
			for (int i = 0; i < values.length; i++) {
				int val = values[values.length-i-1]*dec;
				int pos = (int)((long)(max-minY-val) * (long)graph.height/((long)max-minY)) + marginH;
				gc.drawLine(marginW,pos,maxWidth,pos);
				if(Math.abs(pos - maxPos) > gc.getFontMetrics().getHeight()) {
					gc.drawText(String.valueOf(val+minY), 0, pos);
				}
			}

			//Draw max
			gc.setForeground(this.getDisplay().getSystemColor(SWT.COLOR_RED));
			gc.drawText(String.valueOf(max), 0, maxPos);
			gc.drawLine(marginW, maxPos, maxWidth, maxPos);
		}
		gc.dispose();
	}

	private int calculateScale(int val) {
		if(val < 10 && val > 1) {
			return val+1;
		}
		while(val/100 >= 1) {
			val = val/10;
		}
		if(val < 20) {
			if(val<12) {
				return 0;
			}
			return (val < 16)?1:2;
		}
		return val/10+1;
	}

	/**
	 * Calculate max Y value depending on zoom level
	 * @param initMax	old maximum Y value
	 * @return the maximum Y value
	 */
	private int calculateMaxYValue(int initMax){
		double tmpMax = barY.getMaximum()-(barY.getSelection()+barY.getThumb());
		double tmpMax2 = (double)initMax/(double)barY.getMaximum();
		tmpMax = tmpMax * tmpMax2;
		tmpMax = tmpMax + ((double)initMax/(double)zoomY);
		return (int)tmpMax;
	}

	private int getDecimals(int val) {
		int dec = 0;
		while((val = val/10) >= 1) {dec++;}
		return (dec==0)?1:(int)Math.pow(10,dec-1);
	}

	public void widgetDefaultSelected(SelectionEvent e) {
	}

	public void widgetSelected(SelectionEvent e) {
		Object source = e.getSource();
		//when user clicks on zoom in X button
		if(source == zoomInXButton ) {
			if (zoomX<MAX_ZOOM_X){
				int oldZoom=zoomX;
				zoomX=zoomX*2;
				if (zoomX>MAX_ZOOM_X){
					zoomX=MAX_ZOOM_X;
				}
				textX.setText(String.valueOf(zoomX));
				barX.setMaximum(((zoomX)*barX.getMaximum())/oldZoom);
				timeFrame=((oldZoom)*timeFrame)/(zoomX);
				int zoom = zoomX;
				zoom = timeFrameWithoutZoom/zoom;
				zoom = zoom/barX.getThumb();
				barX.setSelection((minTime-initMinTime)/zoom);
				hSelection=barX.getSelection();
				zoomOutXButton.setEnabled(true);
				if (Integer.parseInt(textX.getText())>=MAX_ZOOM_X){
					zoomInXButton.setEnabled(false);
				}
			}
		}
		//when user clicks on zoom out X button
		if(source == zoomOutXButton ) {
			if (zoomX>1){
				int oddZoom=zoomX%2;
				int oldZoom=zoomX;
				zoomX=zoomX/2;
				if (oddZoom==1){
					zoomX=zoomX+1;
				}
				if (zoomX<1){
					zoomX=1;
				}
				barX.setMaximum(((zoomX)*barX.getMaximum())/oldZoom);
				timeFrame=((oldZoom)*timeFrame)/(zoomX);
				while (minTime+timeFrame>initMinTime+timeFrameWithoutZoom){
					minTime=minTime - (barX.getThumb()*1000);
				}
				textX.setText(String.valueOf(zoomX));
				int zoom = zoomX;
				zoom = timeFrameWithoutZoom/zoom;
				zoom = zoom/barX.getThumb();
				barX.setSelection((minTime-initMinTime)/zoom);
				hSelection=barX.getSelection();
				zoomInXButton.setEnabled(true);
				if (Integer.parseInt(textX.getText())<=1){
					zoomOutXButton.setEnabled(false);
					minTime=initMinTime;
				}
			}
		}
		//when user clicks on zoom in Y button
		if (source == zoomInYButton){
			if (zoomY<MAX_ZOOM_Y){
				int oldZoom=zoomY;
				zoomY=zoomY*2;
				if (zoomY>MAX_ZOOM_Y){
					zoomY=MAX_ZOOM_Y;
				}
				textY.setText(String.valueOf(zoomY));
				barY.setMaximum(((zoomY)*barY.getMaximum())/oldZoom);
				barY.setSelection(barY.getMaximum()-minY);
				zoomOutYButton.setEnabled(true);
				if (Integer.parseInt(textY.getText())>=MAX_ZOOM_Y){
					zoomInYButton.setEnabled(false);
				}
			}
		}
		//when user clicks on zoom out Y button
		if(source == zoomOutYButton ) {
			if (zoomY>1){
				int oddZoom=zoomY%2;
				int oldZoom=zoomY;
				zoomY=zoomY/2;
				if (oddZoom==1){
					zoomY=zoomY+1;
				}
				if (zoomY<1){
					zoomY=1;
				}
				barY.setMaximum(((zoomY)*barY.getMaximum())/oldZoom);
				barY.setSelection(barY.getMaximum()-minY);
				textY.setText(String.valueOf(zoomY));
				zoomInYButton.setEnabled(true);
				if (Integer.parseInt(textY.getText())<=1){
					zoomOutYButton.setEnabled(false);
				}
			}
		}
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
		if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
			try{
				int zoomXAxis = Integer.parseInt(textX.getText());
				//if zoom X level is changed
				if (zoomXAxis!=zoomX) {
					if (zoomXAxis>=1){
						if (zoomXAxis<=MAX_ZOOM_X){
							timeFrame=timeFrameWithoutZoom;
							zoomX=zoomXAxis;
							barX.setMaximum((barX.getThumb()*zoomX));
							timeFrame=timeFrame/zoomX;
							zoomXAxis = timeFrameWithoutZoom/zoomXAxis;
							zoomXAxis = zoomXAxis/barX.getThumb();
							barX.setSelection((minTime-initMinTime)/zoomXAxis);
							hSelection=barX.getSelection();
							if (Integer.parseInt(textX.getText())>=MAX_ZOOM_X){
								zoomInXButton.setEnabled(false);
								zoomOutXButton.setEnabled(true);
							}else if (Integer.parseInt(textX.getText())<=1){
								zoomInXButton.setEnabled(true);
								zoomOutXButton.setEnabled(false);
							}else{
								zoomInXButton.setEnabled(true);
								zoomOutXButton.setEnabled(true);
							}
						}else{
							MessageDialog.openError(
									getShell(),
									"CLIF",
									"Zoom level above "+MAX_ZOOM_X+" is unauthorized");
						}
					}else{
						MessageDialog.openError(
								getShell(),
								"CLIF",
						"Zoom level below 1 is unauthorized");
					}
				}
				int zoomYAxis = Integer.parseInt(textY.getText());
				//if zoom Y level is changed
				if (zoomYAxis!=zoomY) {
					if (zoomYAxis>=1){
						if (zoomYAxis<=MAX_ZOOM_Y){
							barY.setMaximum(((zoomYAxis)*barY.getMaximum())/zoomY);
							barY.setSelection(barY.getMaximum()-minY);
							zoomY=zoomYAxis;
							if (Integer.parseInt(textY.getText())>=MAX_ZOOM_Y){
								zoomInYButton.setEnabled(false);
								zoomOutYButton.setEnabled(true);
							}else if (Integer.parseInt(textY.getText())<=1){
								zoomInYButton.setEnabled(true);
								zoomOutYButton.setEnabled(false);
							}else{
								zoomInYButton.setEnabled(true);
								zoomOutYButton.setEnabled(true);
							}
						}else{
							MessageDialog.openError(
									getShell(),
									"CLIF",
									"Zoom level above "+MAX_ZOOM_Y+" is unauthorized");
						}
					}else{
						MessageDialog.openError(
								getShell(),
								"CLIF",
						"Zoom level below 1 is unauthorized");
					}
				}
			}catch(NumberFormatException exception){
				MessageDialog.openError(
						getShell(),
						"CLIF",
				"Incorrect zoom level");
			}
		}
	}
}
