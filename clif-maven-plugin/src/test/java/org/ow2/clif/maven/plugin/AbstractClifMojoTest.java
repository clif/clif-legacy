package org.ow2.clif.maven.plugin;

import org.apache.commons.io.FileUtils;

import java.io.File;

import static org.junit.Assert.assertNotNull;

/**
 * Abstract class defining commons utility methods.
 *
 * @author Julien Coste
 */
public abstract class AbstractClifMojoTest
{

    protected static final int DEFAULT_TIMEOUT = 10;

    protected File getFileResource( String filename )
    {
        File res = FileUtils.toFile( Thread.currentThread().getContextClassLoader().getResource( filename ) );
        assertNotNull( "Resource [" + filename + "] unvailable", res );
        return res;
    }


}
