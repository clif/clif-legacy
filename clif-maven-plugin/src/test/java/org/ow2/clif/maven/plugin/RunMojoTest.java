package org.ow2.clif.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Julien Coste
 */
public class RunMojoTest
    extends AbstractClifMojoTest
{
    BufferStreamConsumer bscOut = new BufferStreamConsumer();
    BufferStreamConsumer bscErr = new BufferStreamConsumer();


    @Test
    public void testDoExecute()
        throws Exception
    {
        RunMojo mojo = new RunMojo();
        mojo.err = bscErr;
        mojo.out = bscOut;

        mojo.clifhome = getFileResource( "goodInstallation" );
        mojo.testplanName = "dummy";
        mojo.testrunId = "dummyRun";

        mojo.timeOut = DEFAULT_TIMEOUT;

        try
        {
        mojo.execute();
            fail();
        }
        catch (MojoExecutionException mee)
        {
            assertEquals( "Forked JVM has been killed on time-out after 10 seconds", mee.getMessage());
            System.out.println( "out = " + bscOut.getOutput() );
            System.err.println("err = "+ bscErr.getOutput());
            assertEquals( "", bscOut.getOutput() );
            assertEquals( "", bscErr.getOutput() );
        }
    }
}
