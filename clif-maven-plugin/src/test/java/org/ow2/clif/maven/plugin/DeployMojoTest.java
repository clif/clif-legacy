package org.ow2.clif.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * @author Julien Coste
 */
public class DeployMojoTest
    extends AbstractClifMojoTest
{
    BufferStreamConsumer bscOut = new BufferStreamConsumer();
    BufferStreamConsumer bscErr = new BufferStreamConsumer();


    @Test
    public void testDoExecute()
        throws Exception
    {
        DeployMojo mojo = new DeployMojo();
        mojo.err = bscErr;
        mojo.out = bscOut;

        mojo.clifhome = getFileResource( "goodInstallation" );
        mojo.testplanName = "dummy";
        mojo.reportDirectory = new File( "target/clif-reports" );
        mojo.testplan = getFileResource( "dummy/dummy.ctp" );
        mojo.timeOut = DEFAULT_TIMEOUT;

        try
        {
            mojo.execute();
            fail();
        }
        catch (MojoExecutionException mee)
        {
            assertEquals( "Forked JVM has been killed on time-out after 10 seconds", mee.getMessage());
            System.out.println("out = "+ bscOut.getOutput());
            System.err.println("err = "+ bscErr.getOutput());
            assertTrue( bscOut.getOutput().contains("Trying to connect to an existing CLIF Registry") );
        }
    }
}
