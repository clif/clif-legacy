/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003,2011 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.api;

import java.util.Map;
import java.io.Serializable;
import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Interface for controlling the storage system
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public interface StorageAdmin
{
	static public final String STORAGE_ADMIN = "Storage administration";

	/**
	 * Terminates the storage system
	 */
	public void terminate();

	/**
	 * Collects the latest test data
	 */
	public void collect(Serializable selBlades, CollectListener listener);

	/**
	 * Informs the storage system about the beginning of a new test
	 * @param testId test identifier object
	 * @param definitions map of blade deployment definitions, indexed by a unique blade
	 * identifier
	 * @see org.ow2.clif.deploy.DeployDefinition
	 */
	public void newTest(Serializable testId, Map<String,ClifDeployDefinition> definitions)
		throws ClifException;
}
