/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2004, 2005, 2010, 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.api;

import org.ow2.clif.storage.api.BladeEvent;
import java.util.Map;
import java.util.HashMap;

/**
 * Abstract implementation of a blade event.
 * All events managed by CLIF shall derive from this class. Each event type shall have a unique
 * label, which must be registered using the static registerEventFieldLabels method
 * (typically in the a static section of the event class).
 * Events are "Comparable" on their dates.
 * @see #registerEventFieldLabels(String, String[], EventFactory)
 * @author Bruno Dillenseger
 */
abstract public class AbstractEvent implements BladeEvent
{
	private static final long serialVersionUID = 1686188891752911587L;
	static private Map<String,String[]> eventFieldLabels = new HashMap<String,String[]>();
	static private Map<String,EventFactory> factories = new HashMap<String,EventFactory>();
	static public final String DEFAULT_SEPARATOR = ",";


	/**
	 * Registers the given event type label (which must be unique) and associates the fields
	 * labels for this event type.
	 * @param event_label the unique label designating this event type
	 * @param field_labels the array of field labels, describing the content of each field
	 * this type of event holds. These labels must be in the same order as the fields are
	 * printed when the toString() method is called on this type of event.
	 * @see BladeEvent#toString(long, String)
	 */
	static protected synchronized void registerEventFieldLabels(
		String event_label,
		String[] field_labels,
		EventFactory factory)
	{
		eventFieldLabels.put(event_label, field_labels);
		factories.put(event_label, factory);
	}


	/**
	 * Get field labels of a given event type.
	 * @param event_label label of the event type
	 * @return the field labels of the given event type.
	 */
	static public String[] getEventFieldLabels(String event_label)
	{
		return eventFieldLabels.get(event_label);
	}


	static public EventFactory getEventFactory(String event_label)
	{
		return factories.get(event_label);
	}


	protected long date = -1;


	public AbstractEvent()
	{
	}


	public AbstractEvent(long date)
	{
		this.date = date;
	}


	@Override
	public long getDate()
	{
		return date;
	}


	public void setDate(long date)
	{
		this.date = date;
	}


	//////////////////////////
	// interface Comparable //
	//////////////////////////


	/**
	 * The order is based on dates. In case dates are the same, an arbitrary order is applied based
	 * on hashcodes.
	 */
	@Override
	public int compareTo(Object obj)
		throws ClassCastException
	{
		int diff = (int)(date - ((BladeEvent)obj).getDate());
		return diff == 0 ? hashCode() - obj.hashCode() : diff;
	}
}
