/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003, 2004, 2005 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.api;

import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.StringSplitter;
import java.io.Serializable;


/**
 * This class represent the different alarm generated by injector or console
 *
 * @author Julien Buret
 * @author Nicolas Droze
 * @author Bruno Dillenseger
 */
public class AlarmEvent extends AbstractEvent
{
	private static final long serialVersionUID = -4174353216556803647L;
	static public final String DATE_FIELD = "date";
	static public final String SEVERITY_FIELD = "severity";
	static public final String ARGUMENT_FIELD = "argument";
	static public final String EVENT_TYPE_LABEL = "alarm";
	static private final String[] EVENT_FIELD_LABELS =
		new String[] { DATE_FIELD, SEVERITY_FIELD, ARGUMENT_FIELD };
	static public final int INFO = 0;
	static public final int WARNING = 1;
	static public final int ERROR = 2;
	static public final int FATAL = 3;


	static
	{
		AbstractEvent.registerEventFieldLabels(
			EVENT_TYPE_LABEL,
			EVENT_FIELD_LABELS,
			new EventFactory() {
				@Override
				public BladeEvent makeEvent(String separator, String line)
					throws ClifException
				{
					try
					{
						String[] values = StringSplitter.split(line,separator, new String[3]);
						return new AlarmEvent(
							Long.parseLong(values[0]),
							Integer.parseInt(values[1]),
							values[2]);
					}
					catch (Exception ex)
					{
						throw new ClifException(
							"Could not make an AlarmEvent instance from " + line,
							ex);
					}
				}
			});
	}


	public int severity;
	public Serializable argument;


	public AlarmEvent(long date, int severity, Serializable argument)
	{
		super(date);
		this.severity = severity;
		this.argument = argument;
	}


	@Override
	public String toString()
	{
		return toString(0, DEFAULT_SEPARATOR);
	}


	//////////////////////////
	// BladeEvent interface //
	//////////////////////////


	@Override
	public String getTypeLabel()
	{
		return EVENT_TYPE_LABEL;
	}


	@Override
	public String toString(long dateOrigin, String separator)
	{
		return
			(date - dateOrigin) + separator
			+ severity + separator
			+ (argument == null ? null : argument.toString().replace("\n", "\t").replace("\r", "\t"));
	}


	@Override
	public String[] getFieldLabels()
	{
		return EVENT_FIELD_LABELS;
	}


	@Override
	public Object getFieldValue(String fieldLabel)
	{
		if (fieldLabel != null)
		{
			if (fieldLabel.equals(EVENT_FIELD_LABELS[0]))
			{
				return new Long(date);
			}
			else if (fieldLabel.equals(EVENT_FIELD_LABELS[1]))
			{
				return new Integer(severity);
			}
			if (fieldLabel.equals(EVENT_FIELD_LABELS[2]))
			{
				return argument;
			}
		}
		return null;
	}
}
