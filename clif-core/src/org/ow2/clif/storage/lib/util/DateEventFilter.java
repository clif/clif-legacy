/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.util;

import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.EventFilter;


/**
 * Helper class for filtering blade events according to their date.
 *
 * @author Bruno Dillenseger
 */
public class DateEventFilter implements EventFilter
{
	private static final long serialVersionUID = 2945970481498534083L;

	private long from, to;


	/**
	 * Creates a new blade event filter selecting events whose date is between the given bounds
	 * (inclusive).
	 * If the lower bound is negative, then all events prior to the upper bound are accepted.
	 * If the upper bound is negative, then all events after the lower bound are accepted.
	 * If both the upper bound and the lower bound are negative, then all events are accepted.
	 * If the lower bound is greater than the upper bound, then no event is accepted.
	 * @param from the lower date bound (inclusive), unless negative
	 * @param to the upper date bound (inclusive), unless negative
	 */
	public DateEventFilter(long from, long to)
	{
		this.from = from;
		this.to = to;
	}


	///////////////////////////
	// EventFilter interface //
	///////////////////////////


	@Override
	public boolean accept(BladeEvent event)
	{
		return
			(from < 0 || event.getDate() >= from)
			&& (to < 0 || event.getDate() <= to);
	}
}
