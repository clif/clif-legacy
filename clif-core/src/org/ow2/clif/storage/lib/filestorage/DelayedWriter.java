/*
* CLIF is a Load Injection Framework
* Copyright (C) 2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.storage.lib.filestorage;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import org.ow2.clif.storage.api.AbstractEvent;
import org.ow2.clif.storage.api.BladeEvent;

/**
 * This class supports delayed and timely ascending ordered writing of blade events
 * to a BufferedWriter object. The goal of this feature is to write event blades
 * in the chronological order of their starting date, regardless of the time they are
 * actually available for writing, provided the delay is sufficient.
 * This is mostly useful for action events: depending on response times variations,
 * one action event E2 may be available for writing before another action event
 * E1, while the corresponding request has been sent after, because the response time was
 * shorter. Thanks to delayed writing, and as long as the maximum variation of response
 * times is shorter than the delay, action reports are written in chronological order
 * with regard to the request start date. But, when the maximum variation of response
 * times from one request to another becomes greater than the delay, there is no guaranty
 * that this order will be respected.
 * The delay value in seconds may be set through system property defined by {@link #DELAY_S_PROP}.
 * Otherwise, default delay is defined by {@link #DELAY_S_DEFAULT}.
 * 
 * @author Bruno Dillenseger
 */
public class DelayedWriter extends Thread
{
	/** minimum age of events before actually writing them */
	static protected long delay_ms;
	static public final String DELAY_S_PROP = "clif.filestorage.delay_s";
	static public final String DELAY_S_DEFAULT = "60";
	/** maximum number of young events waiting to be written (subsumes delay_ms) */
	static protected long maxPending;
	static public final String MAXPENDING_PROP = "clif.filestorage.maxpending";
	static public final String MAXPENDING_DEFAULT = "1000";

	/**
	 * Delay initialization (system property or default value)
	 */
	static
	{
		try
		{
			delay_ms = 1000 * Integer.parseInt(System.getProperty(DELAY_S_PROP, DELAY_S_DEFAULT));
			maxPending = 1000 * Integer.parseInt(System.getProperty(MAXPENDING_PROP, MAXPENDING_DEFAULT));
		}
		catch (NumberFormatException ex)
		{
			ex.printStackTrace(System.err);
		}
	}

	protected SortedSet<BladeEvent> eventQ = Collections.synchronizedSortedSet(new TreeSet<BladeEvent>());
	/** Initial time in ms */
	protected long offset;
	protected BufferedWriter writer;
	/** set to true when the writer must be closed */
	protected volatile boolean closing = false;


	/**
	 * Creates a delayed writer that will order blade events according to their
	 * date, before actually writing them using the provided BufferedWriter object,
	 * after the delay has expired.
	 * @param out the buffered writer to be used for finally writing blades event
	 * @param dateOffset_ms the initial date in ms (this value will be subtracted
	 * from the events dates when they are finally written to the buffered writer). 
	 */
	public DelayedWriter(BufferedWriter out, long dateOffset_ms)
	{
		super("Delayed blade event writer");
		writer = out;
		offset = dateOffset_ms;
		start();
	}


	/**
	 * Delayed writing of a blade event. The delay applies from the 
	 * date of the event (not current date when calling this method).
	 * @param event the blade event to write
	 * @throws IOException the delayed writer has been closed and
	 * cannot be used anymore.
	 */
	public synchronized void write(BladeEvent event)
		throws IOException
	{
		if (! closing)
		{
			eventQ.add(event);
		}
		else
		{
			throw new IOException("This writer is closed.");
		}
	}


	/**
	 * Closes the underlying buffered writer, after flushing all pending
	 * blade events to it. Any further attempt to write a blade event to
	 * this delayed writer will be rejected. 
	 */
	public synchronized void close()
	{
		closing = true;
		interrupt();
		try
		{
			if (writer != null)
			{
				wait();
			}
		}
		catch (InterruptedException ex)
		{
			System.err.println("Ignored: " + ex);
			ex.printStackTrace(System.err);
		}
	}


	/**
	 * The delay writer's activity consists in getting events from the
	 * blade events queue when they are sufficiently old (with respect
	 * to delay and blade event dates), and writing them to the
	 * underlying buffered writer. Sleeps if no event is old enough.
	 * The sleep duration depends on the delay value and the oldest
	 * pending event's date if any. Sleep is interrupted and all pending
	 * events are flushed to the buffered writer when the delayed writer
	 * is closed.
	 */
	@Override
	public void run()
	{
		BladeEvent firstEvent = null;
		long sleepTime;

		while (! closing)
		{
			sleepTime = delay_ms;
			if (! eventQ.isEmpty())
			{
				firstEvent = eventQ.first();
				sleepTime += firstEvent.getDate() - System.currentTimeMillis();
			}
			if (sleepTime <= 0 || eventQ.size() > maxPending)
			{
				eventQ.remove(firstEvent);
				try
				{
					writer.write(firstEvent.toString(offset, AbstractEvent.DEFAULT_SEPARATOR));
					writer.newLine();
				}
				catch (Exception ex)
				{
					System.err.println("Ignored: " + ex);
					ex.printStackTrace(System.err);
				}
			}
			else
			{
				try
				{
					writer.flush();
					sleep(delay_ms);
				}
				catch (IOException ex)
				{
					System.err.println("Ignored: " + ex);
					ex.printStackTrace(System.err);
				}
				catch (InterruptedException ex)
				{
					// interrupted by stop() => closing becomes true, so loop ends
				}
			}
		}
		synchronized (this)
		{
			try
			{
				for (BladeEvent event : eventQ)
				{
					writer.write(event.toString(offset, AbstractEvent.DEFAULT_SEPARATOR));
					writer.newLine();
				}
			}
			catch (Exception ex)
			{
				System.err.println("Ignored: " + ex);
				ex.printStackTrace(System.err);
			}
			try
			{
				writer.close();
			}
			catch (IOException ex)
			{
				System.err.println("Ignored: " + ex);
				ex.printStackTrace(System.err);
			}
			writer = null;
			notify();
		}
	}
}
