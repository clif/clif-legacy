/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
* Copyright (C) 2014 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for handling throwable objects.
 * @author Bruno Dillenseger
 */
abstract public class ThrowableHelper
{
	/**
	 * This method returns a recursive dump of an Throwable object as a simple String 
	 * @param ex the throwable object (Exception, Error...)
	 * @return the full stack trace as a single String
	 */
	static public String getStackTrace(Throwable ex)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		PrintStream outStrm = new PrintStream(out);
		ex.printStackTrace(outStrm);
		return out.toString();
	}


	/**
	 * This method returns a recursive dump of an Throwable object as an array of strings 
	 * @param ex the throwable object (Exception, Error...)
	 * @return the full stack trace as an array of strings containing the stack trace lines
	 */
	static public String[] getStackTraceLines(Throwable ex)
	{
		List<String> lines = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new StringReader(getStackTrace(ex)));
		try
		{
			for (String line = reader.readLine() ; line != null ; line = reader.readLine())
			{
				lines.add(line);
			}
			reader.close();
		}
		catch (IOException exc)
		{
			// should never occur
		}
		return lines.toArray(new String[lines.size()]);
	}


	/**
	 * Recursively gets exception messages and concatenate them.
	 * @param ex the exception to get messages from
	 * @return A string consisting of exception messages separated with newlines.
	 */
	static public String getMessages(Throwable ex)
	{
		String messages = ex.getMessage();
		if (ex.getCause() == null || ex.getCause() == ex)
		{
			return messages;
		}
		else
		{
			return messages + "\n Cause: " + getMessages(ex.getCause());
		}
	}
}
