/*
* CLIF is a Load Injection Framework
* Copyright (C) 2006, 2009-2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This class helps gathering configuration parameters to ease portability between
 * several assemblies of CLIF. It also configures log libraries and system properties.
 * 
 * @author Bruno Dillenseger
 * @author Florian Francheteau
 */
public abstract class ExecutionContext
{
	// Properties related to possible multiple retries, "best effort/fault tolerant"
	// policy in case of deployment failure, and deployment time out.
	static public final String DEPLOY_RETRIES_PROP_NAME = "clif.deploy.retries";
	static public final String DEPLOY_RETRIES_PROP_DEFAULT = "0";
	static public final String DEPLOY_RETRYDELAY_PROP_NAME = "clif.deploy.retrydelay";
	static public final String DEPLOY_RETRYDELAY_PROP_DEFAULT = "2000";
	static private final String DEPLOY_TIMEOUT_PROP_NAME = "clif.deploy.timeout";
	static private final String DEPLOY_TIMEOUT_PROP_DEFAULT = "0";

	/**
	 * When this property's value is true/on/yes/1, deployment will be successful even
	 * if some blades could not be deployed (they are discarded from the test)
	 */
	static private final String DEPLOY_BESTEFFORT_PROP_NAME = "clif.deploy.besteffort";
	static private final String DEPLOY_BESTEFFORT_PROP_DEFAULT = "false";

	/**
	 * When this property's value is true/on/yes/1, CLIF assumes
	 * that a global time is maintained, with a protocol such as
	 * Network Time Protocol. In this case, all events/measures
	 * timestamps are relative to the global initialization date,
	 * i.e. the date when initialization is triggered by calling
	 * the Supervisor's init() method. The accuracy of time-correlation
	 * between events/measures coming from distributed blades is NTP's
	 * accuracy.  
	 * Otherwise, timestamps are relative to local initialization
	 * dates. The accuracy of time-correlation between events/measures
	 * coming from distributed blades is the time elapsed between the
	 * call to the Supervisor's init() method and the call on the blade's
	 * init() method.
	 */
	static private final String NTP_PROP_NAME = "clif.globaltime";
	static private final String NTP_PROP_DEFAULT = "false";

	/**
	 * When this property's value is true/on/yes/1, all deployments share
	 * a common code server (hopefully with a relevant common code server path).
	 * Otherwise, each deployment creates its own code server. 
	 */
	static private final String CODESERVER_SHARED_PROP_NAME = "clif.codeserver.shared";
	static private final String CODESERVER_SHARED_PROP_DEFAULT = "false";

	/**
	 * Name of the default CLIF server possibly launched by a user
	 * interface for convenience
	 */
	static public final String DEFAULT_SERVER = "local host";

	/**
	 * Base directory of CLIF runtime environment (may be a relative or absolute filesystem path).
	 * Must end with a filesystem path separator.
	 */
	static private String baseDir = null;
	
	/**
	 * absolute path to the file holding MaxQ properties in current project.
	 */
	static private String maxQPropFile = null;
	
	/**
	 * relative path to the file holding MaxQ properties in console plugin
	 */
	static public final String DEFAULT_MAXQ_PROPERTY_FILE = "etc/maxq.properties";

	/**
	 * relative path to the file holding CLIF system properties
	 */
	static public final String PROPS_PATH = "etc/clif.props";

	/**
	 * relative path to the file holding CLIF options
	 */
	static public final String OPTS_PATH = "etc/clif.opts";

	/**
	 * relative path to the file holding the list of classes to prefetch (including all dependencies)
	 */
	static public final String PREFETCH_FILE = "etc/prefetch.list";

	/**
	 * absolute path to the workspace
	 */
	static public String WORKSPACE_PATH;

	/**
	 * Names of CLIF system properties whose value is a path
	 */
	static public String[] PROPERTIES_WITH_PATH = {
		"java.security.policy","julia.config","clif.codeserver.path","clif.filestorage.dir"};

	/**
	 * Sets the base directory of CLIF runtime environment, and configures log libraries.
	 * @param path the filesystem path to the base directory of CLIF runtime environment.
	 * Must have a trailing filesystem separator.
	 */
	static public void init(String path)
	{
		File pathFile = new File(path);
		if (pathFile.isAbsolute())
		{
			baseDir = path;
		}
		else
		{
			baseDir = pathFile.getAbsolutePath() + File.separator;
		}
	}


	/**
	 * Gets the base directory of CLIF runtime environment.
	 * @return the base directory of CLIF runtime environment, or null if this directory
	 * has not been set.
	 */
	static public String getBaseDir()
	{
		return baseDir;
	}


	/**
	 * Sets system properties according to the contents of an input stream.
	 * The stream is read as a properties-formatted content, from which
	 * property clif.parameters is analyzed and transformed into system
	 * properties.
	 * @param ins input stream properties are read from
	 */
	static public void setProperties(InputStream ins)
	{
		try
		{
			Properties props = new Properties();
			props.load(ins);
			String allParam = (String)props.get("clif.parameters");
			String[] parameters = allParam.split(" ");
			for (int i = 0; i < parameters.length; i++)
			{
				if(parameters[i].startsWith("-D"))
				{
					String[] property = parameters[i].replaceFirst("-D","").split("=");
					System.setProperty(property[0], property[1]);
					for (String propName : PROPERTIES_WITH_PATH)
					{
						if (propName.equals(property[0]))
						{
							if ((!"clif.filestorage.dir".equals(property[0])) || WORKSPACE_PATH == null)
								System.setProperty(property[0], completePath(property[0]));
							break;
						}
					}
				}
			}
			props.remove("clif.parameters");
			for (Object key : props.keySet())
			{
				System.setProperty(
					(String)key,
					props.getProperty((String)key));
			}
		}
		catch (IOException e)
		{
			e.printStackTrace(System.err);
		}
	}


	static private String completePath(String property)
	{
		String path = "";
		try
		{
			String[] paths = System.getProperty(property).replaceAll("\"","").split(";");
			for (int i = 0; i < paths.length-1; i++)
			{
				path += foundPath(paths[i]) + ";";
			}
			path += foundPath(paths[paths.length-1]);
			return path;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return path;
	}


	/**
	 * @param path
	 * @return String the correct path
	 * @throws IOException
	 */
	static private String foundPath(String path) throws IOException
	{
		if (new File(path).isAbsolute())
		{
			return path;
		}
		else
		{
			return getBaseDir() + path;
		}
	}
	
	
	/**
	 * Sets absolute location of maxq.properties file in current project
	 * @param dir absolute location of maxq.properties
	 */
	static public void setMaxQPropFile(String dir){
		maxQPropFile=dir;
	}

	/**
	 * Gets location of maxq.properties file in current project
	 * @return String 	the absolute path of maxq.properties
	 */
	static public String getMaxQPropFile(){
		return maxQPropFile;
	}

	/**
	 * @return true if CLIF can assume that a global time is
	 * maintained by some protocol such as Network Time Protocol,
	 * false otherwise.
	 */
	static public boolean useGlobalTime()
	{
		return StringHelper.isEnabled(
			System.getProperty(NTP_PROP_NAME, NTP_PROP_DEFAULT));
	}

	/**
	 * @return true if a test plan can be considered as successfully deployed
	 * event if some blades could not be deployed, false otherwise (i.e. all
	 * blades must be successfully deployed).
	 */
	static public boolean deploymentIsBestEffort()
	{
		return StringHelper.isEnabled(System.getProperty(
			DEPLOY_BESTEFFORT_PROP_NAME,
			DEPLOY_BESTEFFORT_PROP_DEFAULT));
	}

	/**
	 * @return the deployment time out setting, in ms
	 */
	static public long getDeploymentTimeOut()
	{
		return Long.parseLong(
			System.getProperty(
				DEPLOY_TIMEOUT_PROP_NAME,
				DEPLOY_TIMEOUT_PROP_DEFAULT));
	}


	/**
	 * @return true if a unique, shared code server must be used by all
	 * deployments, false otherwise (one code server per deployment).
	 */
	public static boolean codeServerIsShared()
	{
		return StringHelper.isEnabled(
			System.getProperty(CODESERVER_SHARED_PROP_NAME, CODESERVER_SHARED_PROP_DEFAULT));
	}
}
