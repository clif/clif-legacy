/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import org.ow2.clif.storage.api.CollectListener;

/**
 * 
 * @author Bruno Dillenseger
 */
public class TextCollectMonitor implements CollectListener
{
	protected long fullsize = -1;
	protected long bladesize = -1;
	protected long progress = -1;

	@Override
	public void collectStart(String testId, long size)
	{
		fullsize = size;
		progress = 0;
		bladesize = 0;
		System.out.println("starting collecting " + size + " bytes for test " + testId);
	}

	@Override
	public void bladeCollectStart(String bladeId, long size)
	{
		progress += bladesize;
		bladesize = size;
		System.out.println("starting collecting " + size + " bytes from blade " + bladeId);
	}

	@Override
	public void progress(String bladeId, long done)
	{
		System.out.println(
			"progress: " + done + "/" + bladesize + " for blade " + bladeId
			+ ", " + (progress + done) + "/" + fullsize + " for full collect");
	}

	@Override
	public void done()
	{
		progress += bladesize;
		System.out.println("done " + progress + "/" + fullsize);
	}

	@Override
	public boolean isCanceled()
	{
		return false;
	}

	@Override
	public boolean isCanceled(String bladeId)
	{
		return false;
	}
}
