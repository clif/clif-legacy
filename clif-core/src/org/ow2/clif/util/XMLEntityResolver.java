/*
* CLIF is a Load Injection Framework
* Copyright (C) 2007 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name$
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.util;

import java.io.File;
import java.io.InputStream;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

/**
 * Entity resolver for XML parsing. Relies on the application class loader to get resources
 * (typically DTD specification files embedded in CLIF's runtime jar).
 * Understands "classpath:", "file://" and empty prefixes.
 * Handles cases where the XML parser adds current working directory to relative paths.
 * 
 * @author Bruno Dillenseger
 */
public class XMLEntityResolver implements EntityResolver
{
	static public final String CLASSPATH_SCHEME = "classpath:";
	static public final String FILE_SCHEME = "file://";


	public XMLEntityResolver()
	{
	}


	@Override
	public InputSource resolveEntity(String publicId, String systemId)
	{
		InputSource result = null;
		String rsc = systemId;
		if (rsc.startsWith(CLASSPATH_SCHEME))
		{
			rsc = rsc.substring(CLASSPATH_SCHEME.length());
		}
		else if (rsc.startsWith(FILE_SCHEME))
		{
			rsc = rsc.substring(FILE_SCHEME.length());
		}
		try
		{
			InputStream in = this.getClass().getClassLoader().getResourceAsStream(rsc);
			if (in == null)
			{
				// some XML parsers add current working directory to relative paths - remove it
				String cwd = new File("").getAbsolutePath();
				if (rsc.startsWith(cwd))
				{
					rsc = rsc.substring(cwd.length());
					if (rsc.startsWith(File.separator))
					{
						// some class loaders don't like the heading separator - remove it
						rsc = rsc.substring(File.separator.length());
					}
					in = this.getClass().getClassLoader().getResourceAsStream(rsc);
				}
			}
			if (in != null)
			{
				result = new InputSource(in);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
		}
		return result;
	}
}
