/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004, 2005, 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;


/**
 * This class implements a Java resource or class server. The typical client of this code server is
 * the ClifClassLoader.
 * The requested resource or class is first looked for in jar files in CLIF's extension libraries
 * directory.
 * Then, current directory and directories set by property clif.codeserver.path are visited, in
 * this order. The property, when set, should contain a ';' separated list of directory paths.
 * <p>
 * In detail, the codeserver listens on a given port, and upon any connection request:
 * <ul>
 * <li>reads from the socket an UTF String representing the class or resource name
 * <li>writes a positive int stating the resource or class file length or a negative int providing
 * an error code (resource/class not found or resource/class too big).
 * <li>if the class/resource file has been found and can be transfered, all of its bytes are written
 * in the socket.
 * <li>the socket is not closed by the code server, except when an exception occurs while handling
 * the socket.
 * </ul>
 * As of current implementation, class/resource files bigger than 2GB can't be transfered.
 * @see org.ow2.clif.util.ClifClassLoader
 * @author Bruno Dillenseger
 */
public class CodeServer extends ServerSocket implements Runnable
{
	static public final int DEFAULT_PORT = 1357;
    static public final int NO_SUCH_RESOURCE = -1;
    static public final int RESOURCE_TOO_BIG = -2;
    static public final String DEFAULT_PATH = "";
    static public final String PATH_SEPARATOR = ";";
    static private Map<String,JarFile> libExtMap = new HashMap<String,JarFile>();
    static private File[] paths;
    static CodeServer singleton = null;
    static volatile boolean renew = false;
    
    
    public static void launch(InetAddress localAddr, int port, String extdir, String path)
    throws IOException
    {
        if (singleton != null)
        {
            try
            {
                synchronized (singleton) {
                    renew = true;
                    singleton.close();
                    singleton.wait();
                    renew = false;
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace(System.err);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // index content of jar files in Clif's lib/ext directory
        try
        {
            File dir = new File(extdir);
            if (dir.canRead())
            {
                String[] list = dir.list();
                for (int i=0 ; i<list.length ; ++i)
                {
                    if (list[i].endsWith(".jar"))
                    {
                        JarFile jar = new JarFile(extdir + list[i]);
                        Enumeration<JarEntry> entries = jar.entries();
                        while (entries.hasMoreElements())
                        {
                            libExtMap.put(entries.nextElement().getName(), jar);
                        }
                    }
                }
            }
        }
        catch (IOException ex)
        {
            System.err.println(
                    "Warning - problem while indexing " + extdir + " Jar files.\n" + ex);
            ex.printStackTrace(System.err);
        }
        // parses the path property
        StringTokenizer parser = new StringTokenizer(
                path,
                PATH_SEPARATOR);
        paths = new File[parser.countTokens()];
        for (int i=0 ; i<paths.length ; ++i)
        {
            paths[i] = new File(parser.nextToken());
        }
        singleton = new CodeServer(null, port);
    }
    
    
    /**
     * Creates a new code server.
     * @param localAddr the IP address available on local host the code server must bind to.
     * If null, the code server will listen on all available local addresses.
     * @param port the port number the code server must listen to, between 0 and 65535 inclusive.
     * If zero, an arbitrary free port will be used.
     * @throws IOException if the server socket this code server relies on
     * could not be launched.
     */
    private CodeServer(InetAddress localAddr, int port)
    throws IOException
    {
        super(port, 0, localAddr);
        setReuseAddress(true);
        new Thread(this, "code server on " + this.getLocalSocketAddress()).start();
    }
    
    @Override
	public void run()
    {
        boolean go_on = true;
        
        while (go_on)
        {
            try
            {
                new ClassServerRequest(accept());
            }
            catch (IOException ex)
            {
                synchronized(singleton)
                {
                    if (renew)
                    {
                        go_on = false;
                        singleton.notify();
                    }
                    else
                    {
                        ex.printStackTrace(System.err);
                    }
                }
            }
        }
    }
    
    
    class ClassServerRequest extends Thread
    {
        Socket sock;
        
        
        public ClassServerRequest(Socket sock)
        {
            super("code server request handler " + sock);
            this.sock = sock;
            try
            {
                sock.setReuseAddress(true);
                sock.setSoLinger(false, -1);
            }
            catch (SocketException ex)
            {
                ex.printStackTrace(System.err);
            }
            start();
        }
        
        
        @Override
		public void run()
        {
            DataOutputStream douts;
            DataInputStream dins;
            try
            {
                douts = new DataOutputStream(sock.getOutputStream());
                dins = new DataInputStream(sock.getInputStream());
            }
            catch (IOException ex)
            {
                throw new Error("can't properly handle incoming connection to codeserver", ex);
            }
            while (sock != null)
            {
                try
                {
                    String filename = dins.readUTF();
                    // try to get content from local file system
                    File inputFile = new File(filename);
                    // try to get content from lib/ext/*.jar
                    JarFile jar = libExtMap.get(filename);
                    // for possible jar content, ignore heading / if any
                    while (jar == null && filename.startsWith("/"))
                    {
                    	filename = filename.substring(1);
                    	jar = libExtMap.get(filename);
                    }
                    if (jar == null)
                    {
                        for (int i=0 ; i<paths.length && !inputFile.canRead() ; ++i)
                        {
                            inputFile = new File(paths[i], filename);
                        }
                    }
                    if (jar == null && ! inputFile.canRead())
                    {
                        douts.writeInt(NO_SUCH_RESOURCE);
                        douts.flush();
                    }
                    else
                    {
                        long file_length = 0;
                        ZipEntry entry = null;
                        if (jar == null)
                        {
                            file_length = inputFile.length();
                        }
                        else
                        {
                            entry = jar.getEntry(filename);
                            file_length = entry.getSize();
                        }
                        InputStream is;
                        if (jar != null)
                        {
                            is = jar.getInputStream(entry);
                        }
                        else
                        {
                            is = new FileInputStream(inputFile);
                        }
                        if (file_length > Integer.MAX_VALUE)
                        {
                            douts.writeInt(RESOURCE_TOO_BIG);
                            douts.flush();
                        }
                        else
                        {
                            douts.writeInt((int)file_length);
                            byte[] buffer = new byte[(int)file_length];
                            while (file_length > 0)
                            {
                                int n = is.read(buffer, 0, (int)file_length);
                                douts.write(buffer, 0, n);
                                file_length -= n;
                            }
                            douts.flush();
                        }
                    }
                }
                catch (EOFException ex)
                {
                	// normal situation when renewing the code server singleton
                	try
                    {
                        sock.close();
                    }
                    catch (IOException exc)
                    {
                        System.err.println("Exception while closing code server on " + sock);
                        ex.printStackTrace(System.err);
                    }
                   	sock = null;
                }
                catch (IOException ex)
                {
                    try
                    {
                        sock.close();
                    }
                    catch (IOException exc)
                    {
                        System.err.println("Exception while closing code server on " + sock);
                        ex.printStackTrace(System.err);
                    }
                    System.err.println("Code server on " + sock + " closed for unexpected reason");
                    ex.printStackTrace(System.err);
                    sock = null;
                }
            }
        }
    }
}
