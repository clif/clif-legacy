/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import java.util.*;

/**
 * @author Rémi Druilhe
 */
public class RTPSession 
{
	private Integer payloadType;
	private Integer sequenceNumber;
	private Long timestamp;
	private Long ssrc;
	private Double duration;
	private Float sampling;
	private Integer port;
	
	public RTPSession(Integer sequenceNumber, Long timestamp, Long ssrc, Double duration, Integer port)
	{
		Random random = new Random();
		
		if(sequenceNumber.compareTo(0) != 0)
			this.sequenceNumber = sequenceNumber;
		else
			this.sequenceNumber = random.nextInt(65535);
		
		if(timestamp.compareTo(0L) != 0)
			this.timestamp = timestamp;
		else
		{
			Long temp = random.nextLong();
			
			if(new Integer(Long.signum(temp)).compareTo(0) < 0)
				this.timestamp = -temp % 4294967295L;
			else
				this.timestamp = temp % 4294967295L;
		}
		
		if(ssrc.compareTo(0L) != 0)
			this.ssrc = ssrc;
		else
		{
			Long temp = random.nextLong();
			
			if(new Integer(Long.signum(temp)).compareTo(0) < 0)
				this.ssrc = -temp % 4294967295L;
			else
				this.ssrc = temp % 4294967295L;
		}

		this.duration = duration;
		this.port = port;
	}
	
	public RTPSession(Integer sequenceNumber, Long timestamp, Long ssrc, Double duration, Float sampling, Integer port)
	{
		Random random = new Random();
		
		this.duration = duration;
		this.sampling = sampling;
		
		if(sequenceNumber.compareTo(0) != 0)
			this.sequenceNumber = sequenceNumber;
		else
			this.sequenceNumber = random.nextInt(65535);
		
		if(timestamp.compareTo(0L) != 0)
			this.timestamp = timestamp;
		else
			this.timestamp = random.nextLong() % 4294967295L;
		
		if(ssrc.compareTo(0L) != 0)
			this.ssrc = ssrc;
		else
			this.ssrc = random.nextLong() % 4294967295L;
		
		this.port = port;
	}
	
	/**
	 * Increment by one for each new RTP packet.
	 */
	public Integer incrementSequenceNumber()
	{
		sequenceNumber = (sequenceNumber + 1) % 65535; 
		
		return sequenceNumber;
	}
	
	/**
	 * The increment depends on the duration of each packets.
	 * Ex: if the duration is 20 ms, the next timestamp will be :
	 * timestamp + 20 * sampling * 0.001. 
	 */
	public Long incrementTimestamp()
	{
		Double localTimestamp = timestamp + duration * sampling * 0.001;
		
		timestamp = localTimestamp.longValue() % 4294967295L;
		
		return timestamp;
	}
	
	public void close()
	{
		sequenceNumber = null;
		timestamp = null;
		ssrc = null;
		duration = null;
		sampling = null;
		port = null;
	}
	
	// Get methods
	public Integer getPayloadType()
	{
		return payloadType;
	}
	
	public Integer getSequenceNumber()
	{
		return sequenceNumber;
	}
	
	public Long getTimestamp()
	{
		return timestamp;
	}
	
	public Long getSsrc()
	{
		return ssrc;
	}
	
	public Double getDuration()
	{
		return duration;
	}
	
	public Float getSampling()
	{
		return sampling;
	}
	
	public Integer getPort()
	{
		return port;
	}
	
	//Set methods
	public void setPayloadType(Integer payloadType)
	{
		this.payloadType = payloadType;
	}
	
	public void setSequenceNumber(Integer sequenceNumber)
	{
		this.sequenceNumber = sequenceNumber;
	}
	
	public void setTimestamp(Long timestamp)
	{
		this.timestamp = timestamp;
	}
	
	public void setSsrc(Long ssrc)
	{
		this.ssrc = ssrc;
	}
	
	public void setDuration(Double duration)
	{
		this.duration = duration;
	}
	
	public void setSampling(Float sampling)
	{
		this.sampling = sampling;
	}
	
	public void setPort(Integer port)
	{
		this.port = port;
	}
}
