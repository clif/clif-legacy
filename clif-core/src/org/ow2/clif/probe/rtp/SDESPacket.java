/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Create a RTCP SDES packet.
 * 
 *         0                   1                   2                   3
 *         0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * header |V=2|P|    SC   | PT=SDES=202 |               length            |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * chunk  |                            SSRC/CSRC_1                        |
 *   1    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                            SDES items                         |
 *        |                               ...                             |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * chunk  |                            SSRC/CSRC_2                        |
 *   2    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *        |                            SDES items                         |
 *        |                               ...                             |
 *        +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
 * 
 * Cf. RFC 3550 for details about construction of SDES packet.
 * 
 * @author Rémi Druilhe
 */
public class SDESPacket extends RTCPPacket
{
	private Integer version = 2;		// Version (-) : 2 bits
	private Integer padding = 0;		// Padding (-) : 1 bit
	private Integer packetType = 202;	// Packet Type (-) : 8 bits
	
	private LinkedList<String> itemType = new LinkedList<String>();
	
	private HashMap<Long, byte[]> items = new HashMap<Long, byte[]>();
	private Integer itemsSize = 0;
	
	/**
	 * Constructor
	 */
	public SDESPacket()
	{
		itemType.add("END");
		itemType.add("CNAME");
		itemType.add("NAME");
		itemType.add("EMAIL");
		itemType.add("PHONE");
		itemType.add("LOC");
		itemType.add("TOOL");
		itemType.add("NOTE");
		itemType.add("PRIV");
	}
	
	/**
	 * Method to create a SDES packet.
	 * 
	 * @return the SDES packet in bytes.
	 */
	@Override
	public byte[] createPacket()
	{
		Integer length = 4 + itemsSize;
		Integer count = items.size();
		
		Integer boundary = new Double(Math.ceil(new Double(length / 4.0))).intValue();
		
		byte[] sdes = new byte[4 * boundary];
		
		Integer header = (version << 6) & 0xC0;
		header |= (padding << 5) & 0x20;
		
		header |= count & 0x1F;
		sdes[0] = header.byteValue();

		header = packetType & 0xFF;
		sdes[1] = header.byteValue();
		
		Integer tempLength = ((4 * boundary) >> 2) - 1;
		
		header = tempLength >> 8;
		sdes[2] = header.byteValue();
		header = tempLength & 0xFF;
		sdes[3] = header.byteValue();

		Iterator<Long> it = items.keySet().iterator();
		Integer offset = 4;
		
		while(it.hasNext())
		{
			Long ssrc = it.next();
			
			byte[] tempReport = items.get(ssrc);
			
			sdes[offset] = new Long(ssrc >> 24).byteValue();
			sdes[offset+1] = new Long((ssrc >> 16) & 0xFF).byteValue();
			sdes[offset+2] = new Long((ssrc >> 8) & 0xFF).byteValue();
			sdes[offset+3] = new Long(ssrc & 0xFF).byteValue();
			
			offset = offset + 4;
			
			for(int i=0; i<tempReport.length; i++)
			{
				sdes[i+offset] = tempReport[i];
			}
			
			offset = offset + tempReport.length;
		}
		
		for(int i=0; i<4*boundary-length; i++)
		{
			sdes[offset+i] = new Integer(0).byteValue();
		}
		
		return sdes;
	}
	
	/**
	 * Add an item the SDES packet.
	 * 
	 * @param ssrc :  the SSRC associated to the item.
	 * @param item : the item to add to the packet.
	 */
	public void addSdesItem(Long ssrc, byte[] item)
	{
		if(items.containsKey(ssrc))
		{
			byte[] tempItem = items.remove(ssrc);
			byte[] finalItem = new byte[item.length + tempItem.length];
			
			for(int i=0; i<tempItem.length; i++)
			{
				finalItem[i] = tempItem[i];
			}
			
			for(int i=0; i<item.length; i++)
			{
				finalItem[i+tempItem.length] = item[i];
			}
			
			items.put(ssrc, finalItem);
			itemsSize = itemsSize + item.length;
		}
		else
		{
			items.put(ssrc, item);
			itemsSize = itemsSize + item.length + 4;
		}
	}
	
	/**
	 * Create an item to add in an SDES packet.
	 * 
	 * @param type : the numerical type of item.
	 * @param value : the value to add to the item.
	 * @return an item of SDES as formated in RFC 3550.
	 */
	public byte[] createItem(Integer type, String value)
	{
		byte[] item = new byte[value.length() + 2];
		
		Integer temp = type & 0xFF;
		item[0] = temp.byteValue();

		temp = value.length() & 0xFF;
		item[1] = temp.byteValue();

		for(int i=0; i<value.length(); i++)
		{
			item[i+2] = value.getBytes()[i];
		}
		
		return item;
	}
	
	/**
	 * Returns the value associated to the item name.
	 * 
	 * END   : 0
	 * CNAME : 1
	 * NAME  : 2
	 * EMAIL : 3
	 * PHONE : 4
	 * LOC   : 5
	 * TOOL  : 6
	 * NOTE  : 7
	 * PRIV  : 8
	 * 
	 * @param name : the item type (not case sensitive).
	 * @return the integer value.
	 */
	public Integer getItemType(String name)
	{
		return itemType.indexOf(name.toUpperCase());
	}
	
	/**
	 * Returns the packet type. Here it is 202.
	 */
	@Override
	public Integer getPacketType()
	{
		return packetType;
	}
}
