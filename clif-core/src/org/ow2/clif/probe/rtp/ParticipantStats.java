/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2009 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.probe.rtp;

/**
 * Class to keep statistics about each participants.
 * 
 * @author Remi Druilhe
 */
public class ParticipantStats 
{
	private Long ssrc;
	private Double sampling = 0D;
	private Integer port;

	// For time jitter
	private Long timeJitter = 0L;
	private Long timeJitterSum = 0L;
	private Long lastTimeJitterSum = 0L;
	private Long timeJitterSumSquare = 0L;
	private Long lastTimeJitterSumSquare = 0L;
	private Long previousTimestamp = 0L;
	private Long previousTime = 0L;

	// For statistics
	private Integer cycle = 0;
	private Integer previousSeqNum = 0;
	private Integer lastSeqNum;
	private boolean newMeasure = false;
	private Integer seqNumMax;
	private Integer packetSum = 0;
	private Integer lastPacketSum = 0;
	private Integer fractionLost = 0;
	private Integer packetLostSum = 0;
	private Long lsr = 0L;
	private Long dlsr = System.currentTimeMillis();
	private boolean firstReport = true;
	
	private boolean rtpSender = true;
	
	/**
	 * Constructor
	 * 
	 * @param ssrc : the SSRC of the participant.
	 * @param seqNum : the sequence number of the first RTP packet of the participant.
	 * @param port : the RTP port of the participant.
	 */
	public ParticipantStats(Long ssrc, Integer seqNum, Integer port)
	{
		this.ssrc = ssrc;
		this.lastSeqNum = seqNum;
		this.seqNumMax = seqNum + 1;
		this.port = port;
	}
	
	/**
	 * Increment number of packet by 1.
	 */
	public void incPacketSum()
	{
		packetSum++;
	}
	
	/**
	 * Calculate the time jitter, the sum of time jitter and the sum of square time jitter according to the RFC 3550.
	 * 
	 * @param time : the receive time of the last RTP packet.
	 * @param timestamp : the receive timestamp of the last RTP packet.
	 */
	public void calculateTimeJitter(Long time, Long timestamp)
	{
		Long diff = new Double((time - previousTime) * 0.001 - (timestamp - previousTimestamp) / sampling).longValue();
		
		timeJitter = timeJitter + (Math.abs(diff) - timeJitter) / 16;
		
		timeJitterSum = timeJitter + timeJitterSum;
		
		timeJitterSumSquare = timeJitter * timeJitter + timeJitterSumSquare;
	}
	
	/**
	 * Compare the sequence number with the previous maximum calculated.
	 * 
	 * @param sequenceNumber : the sequence number to compare with the max.
	 */
	public void calculateSeqNumMax(Integer sequenceNumber)
	{
		if(sequenceNumber.compareTo(0) == 0)
		{
			seqNumMax = sequenceNumber;
			cycle++;
		}
		else
		{
			seqNumMax = Math.max(seqNumMax, sequenceNumber);
		}
	}
	
	/**
	 * Calculate the fraction lost according to the RFC 3550.
	 */
	public void calculateFractionLost()
	{
		Integer numberOfPacket = seqNumMax - lastSeqNum;
		
		if(numberOfPacket.compareTo(0) ==0)
			numberOfPacket = 1;
		
		fractionLost =  Math.round(((seqNumMax - lastSeqNum - packetSum) / numberOfPacket) * 256);
	}
	
	/**
	 * Calculate the number of packets lost according to the RFC 3550.
	 */
	public void calculateCumulativePacketLost()
	{	
		if(rtpSender)
			packetLostSum = packetLostSum + seqNumMax - lastSeqNum - packetSum;
	}
	
	/**
	 * Start a new measure. Useful for the next RTCP packet. 
	 */
	public void newMeasure()
	{
		this.newMeasure = true;
		lastTimeJitterSum = timeJitterSum;
		this.timeJitterSum = 0L;
		lastTimeJitterSumSquare = timeJitterSumSquare;
		this.timeJitterSumSquare = 0L;
		lastPacketSum = packetSum;
		this.packetSum = 0;
		lastSeqNum = previousSeqNum;
	}
	
	/**
	 * Calculate the LSR according to the RFC 3550.
	 * 
	 * @param MswNtpTimestamp : the MSW NTP timestamp of the SR report.
	 * @param LswNtpTimestamp : the LSW NTP timestamp of the SR report.
	 * @return the 32 middle bits of the NTP timestamp.
	 */
	public Long calculateLsr(Long MswNtpTimestamp, Long LswNtpTimestamp)
	{
		return (((MswNtpTimestamp & 0x0000FFFF) << 16) + ((LswNtpTimestamp & 0xFFFF0000) >> 16));
	}
	
	/**
	 * If the participant send RTP packet, he becomes a sender.
	 */
	public void rtpSender()
	{
		this.rtpSender = true;
	}
	
	/**
	 * If the participant doesn't send RTP packet, he becomes a receiver.
	 */
	public void rtpReceiver()
	{
		this.rtpSender = false;
	}
	
	// Get methods
	public Integer getPacketSum()
	{
		return packetSum;
	}
	
	public Integer getLastPacketSum()
	{
		return lastPacketSum;
	}
	
	public Long getTimeJitter()
	{
		return timeJitter;
	}
	
	public Long getTimeJitterSum()
	{
		return timeJitterSum;
	}
	
	public Long getLastTimeJitterSum()
	{
		return lastTimeJitterSum;
	}
	
	public Long getTimeJitterSumSquare()
	{
		return timeJitterSumSquare;
	}
	
	public Long getLastTimeJitterSumSquare()
	{
		return lastTimeJitterSumSquare;
	}
	
	public Integer getCycle()
	{
		return cycle;
	}
	
	public Integer getSeqNumMax()
	{
		return seqNumMax;
	}
	
	public Long getSsrc()
	{
		return ssrc;
	}
	
	public Long getLsr()
	{
		return lsr;
	}
	
	public Long getDlsr()
	{
		return dlsr;
	}
	
	public boolean getNewMeasure()
	{
		return newMeasure;
	}
	
	public boolean getFirstReport()
	{
		return firstReport;
	}
	
	public Integer getPort()
	{
		return port;
	}
	
	public Long getPreviousTime()
	{
		return previousTime;
	}
	
	public Integer getFractionLost()
	{
		return fractionLost;
	}
	
	public Integer getCumulativePacketLost()
	{
		return packetLostSum;
	}
	
	// Set methods
	public void setPacketSum(Integer packetSum)
	{
		this.packetSum = packetSum;
	}
	
	public void setTimeJitter(Long timeJitter)
	{
		this.timeJitter = timeJitter;
	}
	
	public void setTimeJitterSum(Long timeJitterSum)
	{
		this.timeJitterSum = timeJitterSum;
	}
	
	public void setTimeJitterSumSquare(Long timeJitterSumSquare)
	{
		this.timeJitterSumSquare = timeJitterSumSquare;
	}
	
	public void setPreviousTime(Long previousTime)
	{
		this.previousTime = previousTime;
	}
	
	public void setPreviousTimestamp(Long previousTimestamp)
	{
		this.previousTimestamp = previousTimestamp;
	}
	
	public void setPreviousSeqNum(Integer previousSeqNum)
	{
		this.previousSeqNum = previousSeqNum;
	}
	
	public void setSampling(Double sampling)
	{
		this.sampling = sampling;
	}
	
	public void setSsrc(Long ssrc)
	{
		this.ssrc = ssrc;
	}
	
	public void setLsr(Long lsr)
	{
		this.lsr = lsr;
	}
	
	public void setDlsr(Long time)
	{
		this.dlsr = time;
	}
	
	public void setFirstReport()
	{
		this.firstReport = false;
	}
	
	public void setLastSeqNum(Integer sequenceNumber)
	{
		this.lastSeqNum = sequenceNumber;
	}
}
