/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005,2012 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.jmx_jvm;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ClifClassLoader;


/**
 * Insert for JMX JVM probe
 * @author Cyrille Puget
 * @author Bruno Dillenseger
 */
public class Insert extends AbstractDumbInsert
{
	// metrics retrieved
	private long committed = 0;
	private long used = 0;
	private long max = 0;
	// application server properties
	private String user = null;
	private String password = null;
	private String protocol = null;
	private String hostname = null;
	private int port;
	private String jndiroot = null;
	private String protocol_provider_package = null;
	private String mbeanservername = null;
	private String location = null;

	private JMXServiceURL serviceURL;
	private Map<String,String> connectionProps = new HashMap<String,String>();
	private MBeanServerConnection connection = null;
	private ObjectName memory = null;
	
	private CompositeDataSupport heapMemoryUsage = null;
	
	public Insert() throws Exception
	{
		super();
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + JMX_JVMEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);
	}

	@Override
	public ProbeEvent doProbe()
	{
		JMX_JVMEvent result = null;
		try
		{
			// Get the HeapMemoryUsage
			heapMemoryUsage = (CompositeDataSupport) connection.getAttribute(memory, "HeapMemoryUsage");

			// retrieve memory information and convert from bytes to mega bytes
			used = ((Long) heapMemoryUsage.get("used")).longValue() >> 20;
			committed = ((Long) heapMemoryUsage.get("committed")).longValue() >> 20;
			max = ((Long) heapMemoryUsage.get("max")).longValue() >> 20;
			
			long[] values = new long[3];
			// free memory (MB)
			values[0] = committed - used;
			// used memory %
			values[1] = 100 * used/committed;
			// free usable memory %
			values[2] = 100 * (max - used)/max;
			result = new JMX_JVMEvent(System.currentTimeMillis(), values);
		}
		catch (Exception e)
		{
			connect();
		}
		return result;
	}

	@Override
	public void setExtraArguments(List<String> arg_probe_config) throws ClifException {
		
		// retrieve the third argument of the 'class' arguments, should be the property file 
		String propertiesFileName = arg_probe_config.get(0);
		Properties props = new Properties();
		try
		{
			props.load(ClifClassLoader.getClassLoader().getResourceAsStream(propertiesFileName));
		}
		catch (IOException ioe)
		{
			throw new ClifException(
				"Could not get jmx_jvm probe's configuration from file \"" + propertiesFileName + "\".",
				ioe);
		}
		user = props.getProperty("server.user.name");
		password = props.getProperty("server.user.password");
		protocol = props.getProperty("server.connection.protocol");
		hostname = props.getProperty("server.host.name");
		port = Integer.valueOf(props.getProperty("server.host.port")).intValue();
		jndiroot = props.getProperty("server.jmx.connection.jndiroot");
		protocol_provider_package = props.getProperty("server.jmx.protocol_provider_package");
		location = props.getProperty("server.jmx.location");
		mbeanservername = props.getProperty("server.jmx.mbeanservername");
		connect();
	}

	@Override
	public String getHelpMessage()
	{
		return super.getHelpMessage() + " <configuration file>";
	}

	private void connect()
	{
		try
		{
			// JMX Service URL
			serviceURL = new JMXServiceURL(protocol, hostname, port, jndiroot + hostname + ":" +port + "/" + mbeanservername);

		    // Setup connection properties
		    connectionProps.put(Context.SECURITY_PRINCIPAL, user);
		    connectionProps.put(Context.SECURITY_CREDENTIALS, password);
		    connectionProps.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, protocol_provider_package);

	    	// Create a connection to the DomainRuntimeMBeanServer
	    	JMXConnector connector = JMXConnectorFactory.connect(serviceURL, connectionProps);
	    	// Get the DomainRuntimeMBeanServer
	    	connection = connector.getMBeanServerConnection();
	    	// Get the Memory MBean
	    	if (location == null || location.equals(""))
	    	{
	    		memory = new ObjectName("java.lang:type=Memory");
	    	}
	    	else
	    	{
	    		memory = new ObjectName("java.lang:Location=" + location +",type=Memory");
	    	}
		}
		catch (Exception e)
		{
			ClifException clifEx = new ClifException(
				"Exception when setting the jmx jvm probe's connection to " + serviceURL,
				e);
			System.err.println(e.toString());
			bir.alarm(
				new AlarmEvent(
					System.currentTimeMillis(),
					AlarmEvent.WARNING,
					clifEx));
		}
	}
}
