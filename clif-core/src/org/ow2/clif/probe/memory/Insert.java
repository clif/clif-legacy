/*
* CLIF is a Load Injection Framework
* Copyright (C) 2014, 2015 Orange
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.memory;

import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Swap;
import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.probe.util.SigarUtil;
import org.ow2.clif.storage.api.ProbeEvent;
import org.ow2.clif.supervisor.api.ClifException;


/**
 * Insert for memory probe based on SIGAR
 * @author Bruno Dillenseger
 */
public class Insert extends AbstractDumbInsert
{
	private Mem memProbe;
	private Swap swapProbe;


	public Insert()
	throws ClifException
	{
		super();
		memProbe = new Mem();
		swapProbe = new Swap();
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + MemoryEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);	
	}


	@Override
	public ProbeEvent doProbe()
	{
		MemoryEvent event = null;
		try
		{
			memProbe.gather(SigarUtil.getSigar());
			swapProbe.gather(SigarUtil.getSigar());
			long[] values = new long[6];
			values[0] = (100 * memProbe.getActualUsed() >> 20) / memProbe.getRam();
			values[1] = memProbe.getActualUsed() >> 20;
			values[2] = memProbe.getActualFree() >> 20;
			values[3] = swapProbe.getFree() >> 20;
			if (swapProbe.getTotal() > 0)
			{
				values[4] = (100 * swapProbe.getUsed()) / swapProbe.getTotal();
			}
			else
			{
				values[4] = 0;
			}
			values[5] = swapProbe.getUsed() >> 20;
			event = new MemoryEvent(System.currentTimeMillis(), values);
		}
		catch (Exception ex)
		{
			ex.printStackTrace(System.err);
		}
		return event;
	}


	static public void main(String args[])
	throws Exception
	{
		Insert ins = new Insert();
		while (true)
		{
			ProbeEvent measurement = ins.doProbe();
			System.out.println(measurement);
			Thread.sleep(1000);
		}
	}
}
