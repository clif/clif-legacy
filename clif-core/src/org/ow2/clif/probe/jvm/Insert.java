/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2010 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.probe.jvm;

import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.ProbeEvent;


/**
 * Insert for JVM probe
 * @author Bruno Dillenseger
 * @author Emmanuel Varoquaux
 */
public class Insert extends AbstractDumbInsert
{
	public Insert()
	{
		super();
		new GCTracer();
		
		eventStorageStatesMap.put("store-lifecycle-events", storeLifeCycleEvents);
		eventStorageStatesMap.put("store-alarm-events", storeAlarmEvents);
		eventStorageStatesMap.put("store-" + JVMEvent.EVENT_TYPE_LABEL + "-events", storeProbeEvents);
	}


	@Override
	public ProbeEvent doProbe()
	{
		Runtime runtime = Runtime.getRuntime();
		long[] values = new long[3];
		values[0] = runtime.freeMemory() >> 20;
		values[1] = runtime.totalMemory() >> 20;
		values[2] = runtime.maxMemory() >> 20;
		values[2] = 100 + (100 * (values[0] - values[1])) / values[2];
		values[1] = (100 * (values[1] - values[0])) / values[1];
		return new JVMEvent(System.currentTimeMillis(), values);
	}


	public class GCTracer
	{
		@Override
		public void finalize()
		{
			if (! terminated)
			{
				if (started)
				{
					bir.alarm(new AlarmEvent(System.currentTimeMillis(), AlarmEvent.INFO, "garbage collection"));
				}
				new GCTracer();
			}
		}
	}
}
