/*
* CLIF is a Load Injection Framework
* Copyright (C) 2008, 2009 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.scenario.isac.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * General methods for escaping/unescaping characters in Isac parameters' values.
 * (originally moved from class org.ow2.clif.scenario.isac.egui.plugins.gui.ParameterWidget)
 * 
 * @author Bruno Dillenseger
 * @author JC Meillaud
 * @author A Peyrard
 */
public abstract class ParameterParser
{
	static public List<String> getNField(String value)
	{
		List<String> realTokens = getRealTokens(";", value);
		for (int i=0 ; i<realTokens.size() ; ++i)
		{
			realTokens.set(i, unescape(realTokens.get(i)));
		}
		return realTokens;
	}

	static public String getRadioGroup(String value)
	{
		List<String> realTokens = getRealTokens(";", value);
		if (realTokens.size() == 0)
		{
			return "";
		}
		else
		{
			return unescape(realTokens.get(0));
		}
	}

	static public List<String> getCheckBox(String value)
	{
		return getNField(value);
	}

	static public String getCombo(String value)
	{
		return getRadioGroup(value);
	}
	
	/**
	 * This method analyzes the value to be set in the table, and remove all
	 * escape characters
	 * 
	 * @param value
	 *            The value containing the serialization of all values of the
	 *            table
	 * @return The vector containing all the entries, each entries is in a
	 *         vector, which contains the string values
	 */
	static public List<List<String>> getTable(String value)
	{
		List<List<String>> result = new ArrayList<List<String>>();
		List<String> tempEntries = getRealTokens(";",value);
		// now analyze all entries values, and get the '|' separator
		List<List<String>> allValues = new ArrayList<List<String>>();
		for (int i = 0; i < tempEntries.size(); i++)
		{
			String tempEntry = tempEntries.get(i);
			List<String> values = getRealTokens("|",tempEntry);
			allValues.add(values);
		}
		// now analyze all values, and get the '=' separator
		for (int i = 0; i < allValues.size(); i++)
		{
			List<String> resultValues = new ArrayList<String>();
			List<String> tempValues = allValues.get(i);
			for (int j = 0; j < tempValues.size(); j++)
			{
				String v = tempValues.get(j);
				List<String> temp = getRealTokens("=",v);
				String res;
				if (temp.size() > 1)
				{
					res = temp.get(1);
				}
				else
				{
					res = "";
				}
				res = unescape(res);
				resultValues.add(res);
			}
			result.add(resultValues);
		}
		return result;
	}
	
	/**
	 * Get the number of escape character in the end of the string
	 * @param value The string to be analyzed
	 * @return 0 if no escape character was found, else the number
	 */
	static private int getNumberOfEscapeCharacter(String value) {
		int result = 0;
		if (value.length() > 0) {
			int last = value.lastIndexOf("\\");
			if (last == value.length()-1) {
				result = 1 + getNumberOfEscapeCharacter(value.substring(0,last));
			}
		}
		return result;
	}
	
	/**
	 * Add escape separator before each separator
	 * @param separator The separator character
	 * @param value The value of the string to add the escape characters
	 * @return The string modified
	 */
	static public String addEscapeCharacter(String separator, String value) {
		String result = "";
		StringTokenizer st = new StringTokenizer(value,separator,true);
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (token.equals(separator)) {
				result = result.concat("\\"+token);
			}
			else {
				result = result.concat(token) ;
			}
		}
		return result;
	}
	
	/**
	 * Split the string value with the separator, and check if there is no escape character before
	 * the separator
	 * @param separator The separator
	 * @param value The string value to be split
	 * @return The vector containing each token
	 */
	static public List<String> getRealTokens(String separator, String value)
	{
		List<String> result = new ArrayList<String>();
		if (value != null)
		{
			StringTokenizer st = new StringTokenizer(value,separator,true);
			String currentRealToken = "";
			while (st.hasMoreTokens()) {
				String token = st.nextToken();
				// the token is the separator
				if (token.equals(separator)) {
					int nb = getNumberOfEscapeCharacter(currentRealToken);
					// if nb is even, we don't have any escape character  
					if (nb%2 == 0) {
						result.add(currentRealToken);
						currentRealToken = "";
					}
					// nb is odd 
					else {
						// remove the escape character
						currentRealToken = currentRealToken.substring(0,currentRealToken.length()-1);
						// add the separator character
						currentRealToken = currentRealToken.concat(separator);
						// if it's the last token add it to the result
						if (!st.hasMoreTokens()) {
							result.add(currentRealToken);
						}
					}
				}
				// the token is a string 
				else {
					currentRealToken = currentRealToken.concat(token);
					// if it's the last token add it to the result
					if (!st.hasMoreTokens()) {
						result.add(currentRealToken);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Replaces every escape sequences by the corresponding original character 
	 * @param value the string where some characters are escaped by '\' character
	 * @return the unescaped string
	 */
	static public String unescape(String value)
	{
		StringBuilder result = new StringBuilder();
		char c;
		boolean escape = false; // escape sequence
		for (int i=0 ; i<value.length() ; ++i)
		{
			c = value.charAt(i);
			if (escape)
			{
				result.append(c);
				escape = false;
			}
			else
			{
				if (c == '\\')
				{
					escape = true;
				}
				else
				{
					result.append(c);
				}
			}
		}
		return result.toString();
	}
}
