/*
* CLIF is a Load Injection Framework
* Copyright (C) 2004 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF 
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.scenario.isac.plugin;

/**
 * This interface force the implementation of some methods used in a isac plugin session object
 * 
 * @author JC Meillaud
 * @author A Peyrard
 */
public interface SessionObjectAction {
	
	/**
	 * This method will be used to create anew instance of a session object, 
	 * thanks to this method we could share some references between sessionobject implementation
	 * instances, and initialize some new values for the object which don't need to be 
	 * shared
	 * @return The 'cloned' object
	 */
	public Object createNewSessionObject() ;
	
	
	/**
	 * This method will be used to reset datas of a sessionobject implementation
	 */
	public void reset() ;
	
	
	/**
	 * This method will be call when we stop using this SO, so if we have some
	 * active connection, this method will permit to close them 
	 */
	public void close() ;
}
