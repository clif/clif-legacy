/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2010 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;


/**
 * Responsible for stopping the ISAC extended engine with its own thread
 * when all vUser groups have complete.
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
class Supervisor implements Runnable
{
	private final IsacExtendedEngine engine;
	private int groups;
	private Thread engineStopper;

	/**
	 * Creates a new ISAC engine supervisor, with an initial
	 * number of vUser groups.
	 * @param engine the ISAC extended engine
	 * @param groups the number of vUsers groups (i.e. behaviors defined in current scenario) - 
	 * must be strictly positive. 
	 */
	Supervisor(IsacExtendedEngine engine, int groups)
	{
		this.engine = engine;
		this.groups = groups;
		engineStopper = new Thread(this, "ISAC supervisor stopping engine");
	}

	/**
	 * Warns that a vUser group has completed execution
	 * (i.e. no more active vUser in the group).
	 * When the number of active groups reaches zero,
	 * launches a thread to stop the engine.
	 */
	synchronized void signal()
	{
		if (--groups == 0)
		{
			engineStopper.start();
		}
	}

	/**
	 * activity: just stops the ISAC engine
	 */
	public void run()
	{
		engine.doStop(true);
	}
}
