/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2010 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.util.Map;
import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;
import org.ow2.clif.server.api.BladeInsertResponse;


/**
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
class Scheduler {
	/* Parameters */
	private final Clock					clock;

	/* Fields */
	private final JobQueue				jobQueue;
	private final JobPool				jobPool;

	/* Constructors */

	protected Scheduler(
		Clock clock,
		BladeInsertResponse bladeInsertResponse,
		Map<String,SessionObjectAction> specimenMap,
		long jobDelay)
	{
		this.clock = clock;
		this.jobPool = new JobPool(specimenMap);
		jobQueue = new JobQueue(clock, jobDelay, bladeInsertResponse);
	}

	/*
	 * Interface for Group
	 */

	/* Adds n jobs of the given group into the job queue. */
	protected void add(Group group, int n) {
		/* No need to synchronize */
		while (n-- > 0) {
			Job j = jobPool.getJob();
			j.init(group, clock.getDate());
			jobQueue.insert(j);
		}
	}

	/* Removes a maximum of n jobs of the given group from the job queue.
	 * Doesn't check that the group is actually interruptible. */
	protected void remove(Group group, int n) {
		jobQueue.remove(group, n);
	}

	/*
	 * Interface for ExecutionThread
	 */

	protected Job getJob() throws InterruptedException
	{
		synchronized (clock)
		{
			if (clock.isStopped())
			{
				clock.wait();
			}
		}
		return jobQueue.pop();
	}

	protected void sleep(Job job, long duration) {
		job.alarm = clock.getDate() + duration;
		synchronized (job.group) {
			jobQueue.insert(job);
			job.group.balance(); /* Check whether the job is allowed to stay alive */
		}
	}

	protected void free(Job job) {
		job.free();
		job.group.balance(); /* Experimental */ //deadlock with Timer thread managing load profile when the scenario completes, see bug 308452 
	}

	/*
	 * Interface for IsacExtendedEngine
	 */

	protected void destroy() {
		jobQueue.free();
		jobPool.destroy();
	}

	/**
	 * Sets the "job delay" threshold (detection of job deadline violation)
	 * @param jobDelay
	 */
	protected void setJobDelay(long jobDelay) {
		jobQueue.setDelay(jobDelay);
	}
}
