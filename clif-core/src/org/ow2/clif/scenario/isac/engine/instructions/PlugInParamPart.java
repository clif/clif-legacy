/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.instructions;

/**
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
public class PlugInParamPart {
	public static final int	STRING			= 0;
	public static final int	SESSION_ID		= 1;
	public static final int	CONTEXT_CALL	= 2;
	public static final int PROPERTY = 3;

	public int				type;
	public String			str;               /* type == STRING */
	public String			plugInId;          /* type == CONTEXT_CALL */
	public String			variable;          /* type == CONTEXT_CALL or PROPERTY */

	@Override
	public String toString()
	{
		switch (type)
		{
		case STRING:
			return str;
		case SESSION_ID:
			return "$#";
		case CONTEXT_CALL:
			return "${" + plugInId + ":" + variable +"}";
		case PROPERTY:
			return "${" + variable + "}";
		default:
			return "Plug-in parameter part " + str + "/" + plugInId + "/" + variable + " has an unspecified type " + type;
		}
	}
}
