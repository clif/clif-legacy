/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;

/**
 * @author Emmanuel Varoquaux
 */
class Job {
	/* Parameters */
	protected final JobPool			pool;

	/* Fields */
	private static volatile long	nextId;
	protected long					id;
	protected Group					group;

	/* Scheduling data */
	protected Job					previous;
	protected Job					next;
	protected long					alarm;

	/* Context */
	protected final Map<String,SessionObjectAction> sessionObjectMap;
	protected int					ip;

	/* Constructs a uninitialized Job object. */
	protected Job(JobPool pool, Map<String,SessionObjectAction> specimenMap) {
		this.pool = pool;
		sessionObjectMap = new HashMap<String,SessionObjectAction>();
		for (Map.Entry<String,SessionObjectAction> entry : specimenMap.entrySet())
		{
			sessionObjectMap.put(entry.getKey(), (SessionObjectAction)entry.getValue().createNewSessionObject());
		}
	}

	void recycle()
	{
		Iterator<SessionObjectAction> iter = sessionObjectMap.values().iterator();
		while (iter.hasNext())
		{
			iter.next().reset();
		}
	}

	/* Call this method before any use of a Job object. */
	protected void init(Group group, long alarm) {
		id = nextId++;
		this.group = group;
		next = previous = null;
		this.alarm = alarm;
		ip = 0;
		group.incPopulation();
	}

	/* Call this method after any use of a Job object. */
	protected void free() {
		group.decPopulation();
		pool.freeJob(this);
	}

	/* Call this method to definitively discard a Job object. */
	protected void destroy() {
		for (SessionObjectAction soa : sessionObjectMap.values())
		{
			// an exception/error thrown while closing a session object should not
			// interrupt the destroy process
			try
			{
				soa.close();
			}
			catch (Throwable th)
			{
				System.err.println("Warning: ignored exception while closing session object " + soa);
				th.printStackTrace(System.err);
			}
		}
	}
}
