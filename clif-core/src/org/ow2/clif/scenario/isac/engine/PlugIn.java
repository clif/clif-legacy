/*
 * CLIF is a Load Injection Framework Copyright (C) 2005,2007 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.ow2.clif.scenario.isac.exception.IsacRuntimeException;
import org.ow2.clif.util.ClifClassLoader;
import org.ow2.clif.util.XMLEntityResolver;

/**
 * This class collects data about a plug-in, by reading its
 * configuration files.
 *
 * @author Emmanuel Varoquaux
 * @author Bruno Dillenseger
 */
/*
 * This class collects data about a plug-in from its file of properties
 * 'isac-plugin.properties'. It provides a constructor which initializes the class
 * given the plug-in name.
 */
public class PlugIn {

	/* Handles parsing of plug-in XML file */
	class PluginXMLFileHandler extends DefaultHandler {
		private Map<String,String> params;
		private int number;
		protected Class<?> sessionObjectClass;

		@Override
		public void startElement(String namespaceURI, String localName,
				String qName, Attributes atts) {
			if (qName.equals("object"))
				try {
					sessionObjectClass = Class.forName(atts.getValue("class"),
							true, ClifClassLoader.getClassLoader());
				}
				catch (ClassNotFoundException e) {} /* Keep sessionObjectClass null */
			else if (qName.equals("sample")) {
				number = Integer.parseInt(atts.getValue("number"));
				sampleConversionMap.put(atts.getValue("name"), number);
			}
			else if (qName.equals("timer")) {
				number = Integer.parseInt(atts.getValue("number"));
				timerConversionMap.put(atts.getValue("name"), number);
			}
			else if (qName.equals("control")) {
				number = Integer.parseInt(atts.getValue("number"));
				controlConversionMap.put(atts.getValue("name"), number);
			}
			else if (qName.equals("test")) {
				number = Integer.parseInt(atts.getValue("number"));
				testConversionMap.put(atts.getValue("name"),number);
			}
			else if (qName.equals("params"))
				params = new HashMap<String,String>();
			else if (qName.equals("param"))
				params.put(atts.getValue("name"), atts.getValue("type"));
		}

		@Override
		public void endElement(String namespaceURI, String localName,
				String qName) {
			if (qName.equals("sample"))
				sampleParams.put(number, params);
			else if (qName.equals("timer"))
				timerParams.put(number, params);
			else if (qName.equals("control"))
				controlParams.put(number, params);
			else if (qName.equals("test"))
				testParams.put(number, params);
		}
	}

	/* Data from 'isac-plugin.properties' */
	public final String	name;
	public final String	xmlFile;
	public final String	guiFile;

	/* Data from XML configuration file */
	public final Class<?>	sessionObjectClass; // The class of the session objects of this plug-in
	public final Map<String,Integer> sampleConversionMap = new HashMap<String,Integer>(); // maps sample action names to their method number
	public final Map<Integer,Map<String,String>>sampleParams = new HashMap<Integer,Map<String,String>>(); // maps sample method numbers to their parameters
	public final Map<String,Integer> timerConversionMap = new HashMap<String,Integer>(); // maps timer action names to their method number
	public final Map<Integer,Map<String,String>>timerParams = new HashMap<Integer,Map<String,String>>(); // maps timer method numbers to their parameters
	public final Map<String,Integer> controlConversionMap = new HashMap<String,Integer>(); // maps control action names to their method number
	public final Map<Integer,Map<String,String>> controlParams = new HashMap<Integer,Map<String,String>>(); // maps control method numbers to their parameters
	public final Map<String,Integer> testConversionMap = new HashMap<String,Integer>(); // maps test action names to their method number
	public final Map<Integer,Map<String,String>> testParams = new HashMap<Integer,Map<String,String>>(); // maps test method numbers to their parameters

	/**
	 * Constructs a new <code>PlugIn</code> by reading its configuration files.
	 * 
	 * @param plugInName The name of the plug-in.
	 */
	public PlugIn(String plugInName) {
		Properties properties = new Properties();
		InputStream inStrm;
		inStrm = ClifClassLoader.getClassLoader().getResourceAsStream(
			plugInName + "/isac-plugin.properties");
		if (inStrm == null)
		{
			throw new IsacRuntimeException("Could not get isac-plugin.properties file for ISAC plug-in " + plugInName);
		}
		try {
			properties.load(inStrm);
			inStrm.close();
		}
		catch (IOException e) {
			throw new IsacRuntimeException(plugInName
					+ "/isac-plugin.properties: IO error", e);
		}
		catch (Exception e) {
			throw new IsacRuntimeException("Failed to load plugin " + plugInName,
					e);
		}

		name = properties.getProperty("plugin.name");
		xmlFile = properties.getProperty("plugin.xmlFile");
		guiFile = properties.getProperty("plugin.guiFile");

		try {
			InputSource inSrc = new InputSource(ClifClassLoader.getClassLoader()
				.getResourceAsStream(plugInName + "/" + xmlFile));
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(true);
			SAXParser saxParser = factory.newSAXParser();
			XMLReader reader = saxParser.getXMLReader();
			PluginXMLFileHandler XMLFilehandler = new PluginXMLFileHandler();
			reader.setContentHandler(XMLFilehandler);
			reader.setErrorHandler(XMLFilehandler);
			reader.setEntityResolver(new XMLEntityResolver());
			reader.parse(inSrc);
			sessionObjectClass = XMLFilehandler.sessionObjectClass;
		}
		catch (ParserConfigurationException e) {
			throw new IsacRuntimeException(
					"Cannot satisfy the parser configuration", e);
		}
		catch (IOException e) {
			throw new IsacRuntimeException(plugInName + "/" + xmlFile
					+ ": IO error", e);
		}
		catch (SAXException e) {
			throw new IsacRuntimeException(plugInName + "/" + xmlFile
					+ ": Parse error", e);
		}
	}
}
