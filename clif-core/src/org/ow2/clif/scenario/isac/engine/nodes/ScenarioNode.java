/*
 * CLIF is a Load Injection Framework Copyright (C) 2005 France Telecom R&D
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * CLIF
 * 
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.ListIterator;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import org.jdom.Attribute;
import org.jdom.Element;

/**
 * @author Emmanuel Varoquaux
 */
public class ScenarioNode {
	public Set<PlugInNode> plugIns = new HashSet<PlugInNode>();
	public Set<BehaviorNode> behaviors	= new HashSet<BehaviorNode>();
	public Set<GroupNode> groups = new HashSet<GroupNode>();

	private void analysePlugIns(Element element) throws NodeException {
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next()).getName());
		
		i = element.getChildren().listIterator();
		
		while (i.hasNext()) {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("use"))
				throw new BadElementException(name, "use");
			plugIns.add(new PlugInNode(e));
		}
	}
	
	private void analyseBehaviors(Element element) throws NodeException {
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next()).getName());
		
		i = element.getChildren().listIterator();
		
		if (!i.hasNext())
			throw new MissingElementException("plugins");
		e = (Element)i.next();
		name = e.getName();
		if (!name.equals("plugins"))
			throw new BadElementException(name, "plugins");
		analysePlugIns(e);
		
		if (!i.hasNext())
			throw new MissingElementException("behavior");
		do {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("behavior"))
				throw new BadElementException(name, "behavior");
			behaviors.add(new BehaviorNode(e));
		}
		while (i.hasNext());
	}
		
	private void analyseLoadProfile(Element element) throws NodeException {
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next()).getName());
		
		i = element.getChildren().listIterator();
		
		while (i.hasNext()) {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("group"))
				throw new BadElementException(name, "group");
			groups.add(new GroupNode(e));
		}
	}
	
	public ScenarioNode(Element element) throws NodeException {
		ListIterator i;
		Element e;
		String name;

		i = element.getAttributes().listIterator();
		if (i.hasNext())
			throw new UnexpectedAttributeException(((Attribute)i.next()).getName());
		
		i = element.getChildren().listIterator();

		if (!i.hasNext())
			throw new MissingElementException("behaviors");
		e = (Element)i.next();
		name = e.getName();
		if (!name.equals("behaviors"))
			throw new BadElementException(name, "behaviors");
		analyseBehaviors(e);

		if (!i.hasNext())
			throw new MissingElementException("loadprofile");
		e = (Element)i.next();
		name = e.getName();
		if (!name.equals("loadprofile"))
			throw new BadElementException(name, "loadprofile");
		analyseLoadProfile(e);
	}
	
	@Override
	public String toString()
	{
		StringBuilder res = new StringBuilder();
		for (
			Iterator<PlugInNode> i = plugIns.iterator();
			i.hasNext();
			res.append(i.next().toString()));
		res.append("\n");
		for (
			Iterator<BehaviorNode> i = behaviors.iterator();
			i.hasNext();
			res.append(i.next().toString()));
		res.append("\n");
		for (
			Iterator<GroupNode> i = groups.iterator();
			i.hasNext();
			res.append(i.next().toString()));
		res.append("\n");
		return res.toString();
		
	}
}
