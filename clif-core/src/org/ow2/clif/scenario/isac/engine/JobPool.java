/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine;

import java.util.Map;

import org.ow2.clif.scenario.isac.plugin.SessionObjectAction;

/**
 * @author Emmanuel Varoquaux
 */
class JobPool {
	/* Parameters */
	private final Map<String,SessionObjectAction> specimenMap;

	/* Fields */
	private Job	first	= null;
	
	protected JobPool(Map<String,SessionObjectAction> specimenMap) {
		this.specimenMap = specimenMap;
	}
	
	protected synchronized Job getJob() {
		if (first == null)
			return new Job(this, specimenMap);
		Job tmp = first;
		tmp.recycle();
		first = first.next;
		return tmp;
	}

	protected synchronized void freeJob(Job job) {
		job.next = first;
		first = job;
	}
	
	protected void destroy() {
		while (first != null) {
			Job tmp = first;
			first = first.next;
			tmp.destroy();
		}
	}
}
