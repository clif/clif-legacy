/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.scenario.isac.engine.nodes;

import java.util.ArrayList;
import java.util.ListIterator;

import org.jdom.Attribute;
import org.jdom.Element;

/**
 * @author Emmanuel Varoquaux
 */
public class GroupNode {
	public String		behavior;
	public boolean		forceStop	= true;
	public ArrayList<RampNode>	ramps		= new ArrayList<RampNode>();

	private void verify() throws NodeException {
		int i, n;
		RampNode.Point p = null;

		for (i = 0, n = ramps.size(); i < n; i++) {
			RampNode r = ramps.get(i);
			RampNode.Point start = r.points.get("start"), end = r.points
					.get("end");
			if (start == null)
				throw new NodeException(
						"ramp without \"start\" point in \"group\" element");
			if (end == null)
				throw new NodeException(
						"ramp without \"end\" in \"group\" element");
			if (p != null && (start.x != p.x || start.y != p.y))
				throw new NodeException(
						"The \"start\" point of a ramp doesn't equal to the \"end\" point of the previous ramp in \"group\" element");
			if (end.x <= start.x)
				throw new NodeException("Invalid ramp in \"group\" element");
			if (end.y < 0)
				throw new NodeException("Invalid point in \"group\" element");
			p = end;
		}
	}

	public GroupNode(Element element) throws NodeException {
		ListIterator i;
		Attribute a;
		Element e;
		String name, value;
		boolean forceStopDefined;

		forceStopDefined = false;
		for (i = element.getAttributes().listIterator(); i.hasNext();) {
			a = (Attribute)i.next();
			name = a.getName();
			if (name.equals("behavior")) {
				if (behavior != null)
					throw new DuplicatedAttributeException("behavior");
				behavior = a.getValue();
			}
			else if (name.equals("forceStop")) {
				if (forceStopDefined)
					throw new DuplicatedAttributeException("forceStop");
				value = a.getValue();
				if (value.equals("true"))
					forceStop = true;
				else if (value.equals("false"))
					forceStop = false;
				else
					throw new IllegalAttributeValueException(value);
				forceStopDefined = true;
			}
			else
				throw new UnexpectedAttributeException(name);
		}
		if (behavior == null)
			throw new MissingAttributeException("behavior");

		i = element.getChildren().listIterator();

		if (!i.hasNext())
			throw new MissingElementException("ramp");
		do {
			e = (Element)i.next();
			name = e.getName();
			if (!name.equals("ramp"))
				throw new BadElementException(name, "ramp");
			ramps.add(new RampNode(e));
		}
		while (i.hasNext());
		verify();
	}

	public void print() {
		System.out.println("group: behavior=\"" + behavior + "\", forceStop=\""
				+ forceStop + "\"");
		for (int i = 0, n = ramps.size(); i < n; i++)
			ramps.get(i).print();
	}
}
