package org.ow2.clif.analyze.lib.report;

import java.awt.Container;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import org.ow2.clif.analyze.statistics.util.data.ListOfLong;
import org.ow2.clif.analyze.statistics.util.data.StatOnLongs;

public class LogAndDebug {

	//  log and trace methods
	static int tabsMem = 0;
	
	static{
		debug = new Boolean(System.getProperty("clif.analyze.debug", "false"));
	}


	//  tabs processing in  output flow
	public static String tabs(int nb)
	{
		String retString = "";
		String tab = "  ";
		tabsMem = Math.max(0, nb);
		for (int i = 1; i <= tabsMem; i++){
			retString += tab;
		}
		return retString;
	}

	public static String tabs(){
		return tabs(tabsMem);
	}

	public static String tabsp(){
		return tabs(++tabsMem);		
	}

	public static String tabsm(){
		return tabs(--tabsMem);		
	}

	//	debug
	public static boolean debug;

	String debugStack = "";

	@SuppressWarnings("unused")
	private void pushNoDebug() {
		//		tracep("("+debugStack+")");
		String msg =  debug?"debug":"no debug";
		msg += 	" = " + Thread.currentThread().getStackTrace()[2].getMethodName();

		System.out.println(tabsp() + "+ pushNoDebug(" + debugStack + ") current: " + msg);
		System.out.flush();
		debugStack = debug?"1":"0" + debugStack;
		debug = false;
		tracem("("+debugStack+")" + " = " + Thread.currentThread().getStackTrace()[2].getMethodName());
	}

	@SuppressWarnings("unused")
	private void pushDebug() {
		tracep("("+debugStack+")" + " = " + Thread.currentThread().getStackTrace()[2].getMethodName());
		debugStack = debug?"1":"0" + debugStack;
		debug = true;
		tracem("("+debugStack+")" + " = " + Thread.currentThread().getStackTrace()[2].getMethodName());
	}

	@SuppressWarnings("unused")
	private void popDebug() {
		//		tracep("("+debugStack+")");
		String msg =  debug?"debug":"no debug";
		msg += " = " + Thread.currentThread().getStackTrace()[2].getMethodName();
		System.out.println(tabsp() + "+ popDebug(" + debugStack + ") current: " + msg);
		System.out.flush();
		if (debugStack.length()>0){
			debug = debugStack.substring(0, 1).equals("1")?true:false;
			debugStack = debugStack.substring(1);
		}else{
			System.out.println(tabs() + "*** debugStack is already empty ***");
			System.out.flush();
			debug = false;
		}
		tracem("("+debugStack+")"+" = " + Thread.currentThread().getStackTrace()[2].getMethodName());
	}


	public static void err(String msg){
//		if (debug){
			String methodName = "[";
			methodName += Thread.currentThread().getStackTrace()[2].getClassName() + "]";
			methodName += Thread.currentThread().getStackTrace()[2].getMethodName();
			System.err.println(methodName  + "*** ERROR *** " + msg);
//		}
//		System.err.println(tabs() + "*** ERROR *** " + msg);
		System.err.flush();
	}


	public static void warning(String msg){
		if (debug){
			String methodName = "[";
			methodName += Thread.currentThread().getStackTrace()[2].getClassName() + "]";
			methodName += Thread.currentThread().getStackTrace()[2].getMethodName();
			System.err.println("*** WARNING *** " + methodName + ": " +  msg);
		}else{
			System.err.println(tabs() + "*** WARNING *** " + msg);
			System.err.flush();
		}
	}


	/**
	 * @param msg : message to print on Sysytem.output
	 */
	public static void log(String msg){
		if (debug){
			System.out.println(tabs() + msg);
			System.out.flush();
		}
	}

	/**
	 * prints a message on System.output without "\n"
	 * @param msg : message to print on Sysytem.output
	 */
	public static void logn(String msg){
		if (debug){
			System.out.print(tabs() + msg);
		}
	}

	/**
	 * prints a message on System.output and then increments the tabs count
	 * @param msg : message to print on Sysytem.output
	 */
	public static void logp(String msg){	// log message then increment tabs
		log(msg);
		tabsp();
	}

	/**
	 * increments the tabs count and then prints a message on System.output
	 * @param msg : message to print on Sysytem.output
	 */
	void logpp(String msg){	// increment tabs then log message 
		tabsp();
		log(msg);
	}

	/**
	 * decrements the tabs count and then prints a message on System.output
	 * @param msg : message to print on Sysytem.output
	 */
	public static void logm(String msg){	// log message then decrement tabs
		tabsm();
		log(msg);
	}


	public static void tracep() {
		String methodName = "[" + Thread.currentThread().getStackTrace()[2].getClassName() + "]" + Thread.currentThread().getStackTrace()[2].getMethodName();
		logp("+ " + methodName );
	}

	/**
	 * increments the tabs count and then prints the calling method's name
	 * with a message. Usually "(param1, param2)".
	 * @param msg : message to print on Sysytem.output
	 */
	public static void tracep(String msg){	// logs call stack then increment tabs
		String methodName = "[" + Thread.currentThread().getStackTrace()[2].getClassName() + "]" + Thread.currentThread().getStackTrace()[2].getMethodName();
		logp("+ " + methodName +  msg);
	}


	public static void tracem() {
		String methodName = "[" + Thread.currentThread().getStackTrace()[2].getClassName() + "]" + Thread.currentThread().getStackTrace()[2].getMethodName();
		logm("- " + methodName);
	}


	/**
	 * decrements the tabs count and then prints the calling method's name
	 * with a message. Usually "(param1, param2) return: <value>".
	 * @param msg : message to print on Sysytem.output
	 */

	public static void tracem(String msg){	// log stack then decrement tabs
//		String methodName = "[" + Thread.currentThread().getStackTrace()[2].getClassName() + "]" + Thread.currentThread().getStackTrace()[2].getMethodName();
		String methodName = "[";
		methodName += Thread.currentThread().getStackTrace()[2].getClassName() + "]";
		methodName += Thread.currentThread().getStackTrace()[2].getMethodName();
		logm("- " + methodName +  msg);
	}

	/**
	 * prints the calling method's name plus a message
	 */
	public static void trace() { 
		String methodName = "[";
		methodName += Thread.currentThread().getStackTrace()[2].getClassName() + "]";
		methodName += Thread.currentThread().getStackTrace()[2].getMethodName();
		log("* " + methodName + "()");
	}


	/**
	 * prints the calling method's name plus a message
	 * @param msg : message to print on Sysytem.output
	 */
	public static void trace(String msg) { 
		String methodName = "[";
		methodName += Thread.currentThread().getStackTrace()[2].getClassName() + "]";
		methodName += Thread.currentThread().getStackTrace()[2].getMethodName();
		log("* " + methodName + " " +  msg);
	}


	/**
	 * prints the methods calling stack up to a depth "depth"
	 * @param depth
	 */
	public void showStack(int depth){
		logp("calling stack:");
		for(int i= 2; i<=depth;i++){
			//			log(i-1+") " + Thread.currentThread().getStackTrace()[i].getClass().getSimpleName()
			log(i-1+") " + Thread.currentThread().getStackTrace()[i].getClassName() + " > " +  Thread.currentThread().getStackTrace()[i].getMethodName());
		}
		logm("");
	}




	public void setBorder(Container container, String title) {
		TitledBorder titleBorder;
		titleBorder = BorderFactory.createTitledBorder(title);
		((JComponent) container).setBorder(titleBorder);
	}




	public static String toText(Object _object) {
		String className;
		String ret;
		if (null == _object){
			ret = "<null>";
		}else{
			if (_object.getClass().getSimpleName().equals("DefaultMutableTreeNode")){
				Object obj = ((DefaultMutableTreeNode) _object).getUserObject();
				_object = obj;
			}
			className = _object.getClass().getSimpleName();
			ret = "<unknown class: " + className + ">";
			if (className.equals("Report")){		// it's the report itself
				ret = "<Report> \"" + ((Report) _object).getTitle() + "\"";
			} else if (className.equals("Section")){	// it's a Section
				ret = "<Section> \"" + ((Section) _object).getTitle() + "\"";
			}else if (className.equals("Dataset")){	// it's a Dataset
				ret = "<Dataset> \"" + ((Dataset) _object).getTitle() + "\"";
			}else if (className.equals("Datasource")){	// it's a Datasource
				ret = "<Datasource> \"" + ((Datasource) _object).getName() + "\"";
			}else if (className.equals("FieldsValuesFilter")){	// it's a FieldsValuesFilter
				ret = "<FieldsValuesFilter> " + ((FieldsValuesFilter) _object).toString();
			}else if (className.equals("JComboBox")){
				@SuppressWarnings("unchecked")
				JComboBox<String> combo = (JComboBox<String>) _object;
				StringBuffer s = new StringBuffer("[");
				String comma = "";
				for(int i=0; i < combo.getItemCount(); i++){
					s.append(combo.getItemAt(i) + comma);
					comma = ", ";
				}
				ret = s.toString() + "]";
			}
		}
		return ret;
	}


	/**
	 * prints, on Sysytem.output, the calling method's name plus the message
	 * "*** Unimplemented Method \"
	 */
	public static void unimplemented() {
		String methodName = "[" + Thread.currentThread().getStackTrace()[2].getClassName() + "]" + Thread.currentThread().getStackTrace()[2].getMethodName();
		String t =  " *** Unimplemented Method ***" + methodName + "\n";
//		if(debug){
			System.err.println(t);
//		}
	}

	public static void unimplemented(int depth) {
		String methodName = "[" + Thread.currentThread().getStackTrace()[2].getClassName() + "]" + Thread.currentThread().getStackTrace()[2].getMethodName();
		String t =  " *** Unimplemented Method ***" + methodName + "\n";
		int n = Thread.currentThread().getStackTrace().length - 1;
		if (n > depth){n = 5;}
		t += "   calling stack:\n";
		for (int i = n; i >= 2; i--){
			if (null != Thread.currentThread().getStackTrace()[i]){
				t += "     [" + Thread.currentThread().getStackTrace()[2].getClassName() + "]";
				t += Thread.currentThread().getStackTrace()[i].getMethodName()+ "\n";
			}
		}
		if(debug){
			System.err.println(t);
		}
	}

	public static String toTxt(StatOnLongs stol) {
		String ret = "(" + stol.size() +") [";
		String pre = "";
		ListOfLong lol = stol.getData();
		for (Long l : lol){
			ret += pre + l;
			pre = ", ";
		}
		ret += "]";
		return ret;
	}

	public static void unimplemented(String msg) {
		String methodName = "[" + Thread.currentThread().getStackTrace()[2].getClassName() + "]" + Thread.currentThread().getStackTrace()[2].getMethodName();
		String t =  " *** Unimplemented Method ***" + methodName + msg + "\n";
		System.err.println(t);
	}


	// HTML generation methods
	static int htmlTabsMem = 0;
	
	public static String htmlTabs(int nb)
	{
		String retString = "";
		String tab = "  ";
		htmlTabsMem = Math.max(0, nb);
		for (int i = 1; i <= htmlTabsMem; i++){
			retString += tab;
		}
		return retString;
	}

	public static String htmlTabs(){
		return htmlTabs(htmlTabsMem);
	}
	
	public static String htmlTabsp(){
		return htmlTabs(++htmlTabsMem);		
	}
	

	public static String htmlTabsm(){
		return htmlTabs(--htmlTabsMem);		
	}

	static void htmlWriteNP(StringBuffer htmlText, String mess) {
		htmlText.append(htmlTabs() + mess + "\n");
	}

	static void htmlWrite(StringBuffer htmlText, String mess) {
		htmlText.append(htmlTabs() +"<p>"+ mess + "</p>\n");
	}

	static String comment2html(String comment)
	{
		if (comment == null || comment.trim().isEmpty() || comment.equals(Report.EMPTY_COMMENT))
		{
			return "<span class=\"notice\">No comment.</span>";
		}
		else
		{
			return
				"<span class=\"comment\">"
				+ comment.replaceAll("\n", "<br>\n")
				+ "</span>";
		}
	}
}
