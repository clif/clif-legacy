package org.ow2.clif.analyze.lib.report;

import java.util.Set;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.ow2.clif.analyze.lib.report.Dataset.ChartType;
import org.ow2.clif.analyze.statistics.util.data.StatOnLongs;

public class ChartDataValues{
	Dataset dataset;
	String xLabel;
	String yLabel;

	Set<ChartType> chartTypesSet;		//  = new HashSet<ChartType>();
	private int step;
	private int timeWindow;

	private StatOnLongs stolDate;
	private StatOnLongs stol;


	XYSeriesCollection xySeriesCollection;			// for time based charts
	HistogramDataset histogramDataset;				// for histograms
	DefaultCategoryDataset defaultCategoryDataset;	// for quantiles

	String plotName;	// datasourceName
	int maxPointsNb = -1;			// -1 means all points
	PlotOrientation orientation;
	boolean bLegend;
	boolean bToolTips;
	boolean bUrls;

	////////////////////////////////////
	// Constructors
	////////////////////////////////////


	////////////////////////////////////
	// complete constructor

	public ChartDataValues(Dataset dataset, String xLabel, String yLabel,
			Set<ChartType> chartTypesSet, int step, int timeWindow,
			StatOnLongs stol,
			StatOnLongs stolDate,
			XYSeriesCollection xySeriesCollection,
			HistogramDataset histogramDataset,
			DefaultCategoryDataset defaultCategoryDataset, String plotName,
			PlotOrientation orientation, boolean bLegend, boolean bToolTips,
			boolean bUrls) {
		this.dataset = dataset;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		this.chartTypesSet = chartTypesSet;
		this.step = step;
		this.timeWindow = timeWindow;
		this.stol = stol;
		this.stolDate = stolDate;
		this.xySeriesCollection = xySeriesCollection;
		this.histogramDataset = histogramDataset;
		this.defaultCategoryDataset = defaultCategoryDataset;
		this.plotName = plotName;
		this.orientation = orientation;
		this.bLegend = bLegend;
		this.bToolTips = bToolTips;
		this.bUrls = bUrls;


		LogAndDebug.trace("(....)");
	}


	public ChartDataValues(Dataset dataset, String xLabel, String yLabel,
			Set<ChartType> chartTypesSet, int step, int timeWindow,
			StatOnLongs stol, StatOnLongs stolDate, String name) {
		this(dataset, xLabel, yLabel,
				chartTypesSet, step, timeWindow,
				stol, stolDate,
				null, null, null,
				name,
				PlotOrientation.HORIZONTAL,
				false, false, false);
	}


	//////////////////////////////////////

	public Dataset getDataset() {
		return dataset;
	}


	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}


	public String exportToTxt(int nbTabs) {
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		String txt = tabs + "ChartDataValue:\n";
		txt += tabs + "  plotName: " + plotName + "\n";
		txt += tabs + "  orientation: " + orientation + "\n";
		txt += tabs + "  xLabel: \"" + xLabel + "\"\n";
		txt += tabs + "  yLabel: \"" + yLabel + "\"\n";
		txt += tabs + "  chartTypesSet: " + chartTypesSet + "\n";
		txt += tabs + "  step:       " + step + " ms\n";
		txt += tabs + "  timeWindow: " + timeWindow + " ms\n";
		txt += tabs + "  stol:     " + LogAndDebug.toTxt(stol) + "\n";
		txt += tabs + "  stolDate: " + LogAndDebug.toTxt(stolDate) + "\n";
		txt += tabs + "  xySeriesCollection:     " + xySeriesCollection + "\n";
		txt += tabs + "  histogramDataset:       " + histogramDataset + "\n";
		txt += tabs + "  defaultCategoryDataset: " + defaultCategoryDataset + "\n";
		txt += tabs + "  bLegend:   " + bLegend + "\n";
		txt += tabs + "  bToolTips: " + bToolTips + "\n";
		txt += tabs + "  bUrls:     " + bUrls + "\n\n";
		return txt;
	}



/*	
 * Tomas version
 * 
 * public Object exportToXml(int nbTabs) {		// TODO for now it exports text format. Has to be Xml
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		String txt = tabs + "ChartDataValue:\n";
		txt += tabs + "  plotName: " + plotName + "\n";
		txt += tabs + "  orientation: " + orientation + "\n";
		txt += tabs + "  xLabel: \"" + xLabel + "\"\n";
		txt += tabs + "  yLabel: \"" + yLabel + "\"\n";
		txt += tabs + "  chartTypesSet: " + chartTypesSet + "\n";
		txt += tabs + "  step:       " + step + " ms\n";
		txt += tabs + "  timeWindow: " + timeWindow + " ms\n";
		txt += tabs + "  stol:     " + LogAndDebug.toTxt(stol) + "\n";
		txt += tabs + "  stolDate: " + LogAndDebug.toTxt(stolDate) + "\n";
		txt += tabs + "  xySeriesCollection:     " + xySeriesCollection + "\n";
		txt += tabs + "  histogramDataset:       " + histogramDataset + "\n";
		txt += tabs + "  defaultCategoryDataset: " + defaultCategoryDataset + "\n";
		txt += tabs + "  bLegend:   " + bLegend + "\n";
		txt += tabs + "  bToolTips: " + bToolTips + "\n";
		txt += tabs + "  bUrls:     " + bUrls + "\n\n";
		return txt;
	}
*/
	

	public Object exportToHtml(int nbTabs) {		// TODO for now it exports text format. Has to be Html
		String tabs = "";
		for (int i = 0 ; i<= nbTabs;i++){
			tabs += "\t";
		}
		String txt = tabs + "ChartDataValue:\n";
		txt += tabs + "  plotName: " + plotName + "\n";
		txt += tabs + "  orientation: " + orientation + "\n";
		txt += tabs + "  xLabel: \"" + xLabel + "\"\n";
		txt += tabs + "  yLabel: \"" + yLabel + "\"\n";
		txt += tabs + "  chartTypesSet: " + chartTypesSet + "\n";
		txt += tabs + "  step:       " + step + " ms\n";
		txt += tabs + "  timeWindow: " + timeWindow + " ms\n";
		txt += tabs + "  stol:     " + LogAndDebug.toTxt(stol) + "\n";
		txt += tabs + "  stolDate: " + LogAndDebug.toTxt(stolDate) + "\n";
		txt += tabs + "  xySeriesCollection:     " + xySeriesCollection + "\n";
		txt += tabs + "  histogramDataset:       " + histogramDataset + "\n";
		txt += tabs + "  defaultCategoryDataset: " + defaultCategoryDataset + "\n";
		txt += tabs + "  bLegend:   " + bLegend + "\n";
		txt += tabs + "  bToolTips: " + bToolTips + "\n";
		txt += tabs + "  bUrls:     " + bUrls + "\n\n";
		return txt;

	}



	//////////////////////////////////////
	
	// getters and setters


	public String getxLabel() {
		return xLabel;
	}

	public void setxLabel(String str) {
		this.xLabel = str;
	}

	public String getyLabel() {
		return yLabel;
	}

	public void setyLabel(String str) {
		this.yLabel = str;
	}

	public XYSeriesCollection getXySeriesCollection() {
		return xySeriesCollection;
	}

	public void setXySeriesCollection(XYSeriesCollection xySeriesCollection) {
		this.xySeriesCollection = xySeriesCollection;
	}

	public HistogramDataset getHistogramDataset() {
		return histogramDataset;
	}

	public void setHistogramDataset(HistogramDataset histogramDataset) {
		this.histogramDataset = histogramDataset;
	}

	public DefaultCategoryDataset getDefaultCategoryDataset() {
		return defaultCategoryDataset;
	}

	public void setDefaultCategoryDataset(DefaultCategoryDataset defaultCategoryDataset) {
		this.defaultCategoryDataset = defaultCategoryDataset;
	}

	public PlotOrientation getOrientation() {
		return orientation;
	}

	public void setOrientation(PlotOrientation orientation) {
		this.orientation = orientation;
	}

	public boolean isbLegend() {
		return bLegend;
	}

	public void setbLegend(boolean b) {
		this.bLegend = b;
	}

	public boolean isbToolTips() {
		return bToolTips;
	}

	public void setbToolTips(boolean b) {
		this.bToolTips = b;
	}

	public boolean isbUrls() {
		return bUrls;
	}

	public void setbUrls(boolean b) {
		this.bUrls = b;
	}


	public Set<ChartType> getChartTypesSet() {
		return chartTypesSet;
	}


	public void setChartTypesSet(Set<ChartType> chartTypesSet) {
		this.chartTypesSet = chartTypesSet;
	}


	public int getStep() {
		return step;
	}


	public int getTimeWindow() {
		return timeWindow;
	}


	public void setTimeWindow(int i) {
		this.timeWindow = i;
	}


	public String getPlotName() {
		return plotName;
	}


	public void setPlotName(String str) {
		this.plotName = str;
	}
	
	public StatOnLongs getStolDate() {
		return stolDate;
	}


	public void setStolDate(StatOnLongs stolDate) {
		this.stolDate = stolDate;
	}


	public StatOnLongs getStol() {
		return stol;
	}


	public void setStol(StatOnLongs stol) {
		this.stol = stol;
	}


	public int getMaxPointsNb() {
		return maxPointsNb;
	}


	public void setMaxPointsNb(int i) {
		this.maxPointsNb = i;
	}

	public String  getChartDataValuesName(){
		return plotName;
	}

	public String getname() {
		String name=plotName;
		return name;
	}
	


}

