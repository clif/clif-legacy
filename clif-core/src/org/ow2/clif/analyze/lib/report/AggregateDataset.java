package org.ow2.clif.analyze.lib.report;

import java.util.ArrayList;
import java.util.List;

//package org.ow2.clif.analyze.lib.graph;


/**
 * @author Tomas Perez Segovia
 *
 */

public class AggregateDataset extends Dataset {

	static DatasetType datasetType = DatasetType.AGGREGATE_DATASET;

	/**
	 * @param testName
	 * @param bladeId
	 * @param eventType
	 */
	public AggregateDataset(String testName, String bladeId, String eventType) {
		super(testName, bladeId, eventType);
	}

	public AggregateDataset(String testName, String bladeId) {
		this(testName, bladeId, "unknown_eventType");
	}

	public AggregateDataset() {
		this("", "", "");
	}

	
	@Override
	public List<Statistics> getStatistics()
	{
		List<Statistics> ret = new ArrayList<Statistics>();
		if (null == statistics){
			System.err.println("ERROR [AggregateDataset]getStaistics():  Dataset.getStatistics: statistics have not been set");
			ret = null;
		}else{
			ret.add(statistics);
		}
		return ret;
	}

	@Override
	public void setStatistics(List<Statistics> _statistics){
		if (1 == _statistics.size()){
			this.statistics = _statistics.get(0);
		}else{
			System.err.println("ERROR AggregateDataset.setStatistics: given statistics have not correct size. "
					+ _statistics.size() + " instead of 1.");

		}
	}

}

