package org.ow2.clif.analyze.lib.report;

import java.util.ArrayList;
import java.util.List;
//package org.ow2.clif.analyze.lib.graph;


public class SimpleDataset extends Dataset {

	// constructors

	// complete constructor
	public SimpleDataset(String name, String testName, String bladeId, String eventType) {
		super(name, testName, bladeId, eventType);

		setDatasetType(DatasetType.SIMPLE_DATASET);
	}


	// constructor without field
	public SimpleDataset(String name, String testName, String bladeId) {
		this(name, testName, bladeId, (String) null);
	}


	public SimpleDataset() {
		this((String) null, (String) null, (String) null, (String) null);
	}


	// 
	@Override
	public List<Statistics> getStatistics(){
		List<Statistics> ret = new ArrayList<Statistics>();
		if (null == statistics){
			System.err.println("ERROR SimpleDataset.getStatistics: statistics have not been set");
			ret = null;
		}else{
			ret.add(statistics);
		}
		return ret;
	}

	@Override
	public void setStatistics(List<Statistics> _statistics){
		if (1 == _statistics.size()){
			this.statistics = _statistics.get(0);
		}else{
			System.err.println("ERROR SimpleDataset.setStatistics: given statistics have not correct size. "
					+ _statistics.size() + " instead of 1.");

		}
	}

}
