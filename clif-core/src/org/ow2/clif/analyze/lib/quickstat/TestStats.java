/*
* CLIF is a Load Injection Framework
* Copyright (C) 2012, 2013 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.quickstat;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;
import org.ow2.clif.supervisor.api.ClifException;

/**
 * Simple statistical report generator on injectors'
 * response times and throughput. Aggregates all measures
 * with the same "action type".
 * 
 * @author Bruno Dillenseger
 */
public class TestStats
{
	static public final String CLEANINGFACTOR_PROPNAME = "clif.quickstat.cleanfactor";
	static public final String CLEANINGFACTOR_PROPDEFAULT = "0"; //disabled
	static public final String CLEANINGLIMIT_PROPNAME = "clif.quickstat.cleanlimit";
	static public final String CLEANINGLIMIT_PROPDEFAULT = "95.4";

	static public TestDescriptor getLatestTest(StorageRead sr)
	throws ClifException
	{
		TestDescriptor[] tests = sr.getTests(
			new TestFilter()
			{
				private static final long serialVersionUID = 7957412648755763755L;
				TestDescriptor latest = null;

				public boolean accept(TestDescriptor test)
				{
					if (latest == null || test.getDate().compareTo(latest.getDate()) > 0)
					{
						latest = test;
						return true;
					}
					else
					{
						return false;
					}
				}
			} );
		return tests[tests.length-1];
	}


	private StorageRead sr;
	private String testName;
	private Map<String,ActionStat> injectorStats;
	private ActionStat globalStats;
	private double cleaningFactor, cleaningLimit;


	/**
	 * Initiates a simple statistical report generator for the
	 * latest test execution in the provided storage object.
	 * @param storage a CLIF storage implementation
	 * @throws ClifException could not find a test execution
	 */
	public TestStats(StorageRead storage)
	throws ClifException
	{
		this(storage, null);
	}


	/**
	 * Initiates a simple statistical report generator for the
	 * given test execution in the provided storage object.
	 * @param storage a CLIF storage implementation
	 * @param testName the name of the target test execution
	 * When null or empty, the latest test execution is chosen.
	 * @throws ClifException could not find the test execution
	 */
	public TestStats(StorageRead storage, String testName)
	throws ClifException
	{
		this.sr = storage;
		if (testName == null || testName.trim().isEmpty())
		{
			this.testName = getLatestTest(storage).getName();
		}
		else
		{
			this.testName = testName;
		}
	}


	/**
	 * The first call to this method actually reads measures and
	 * computes statistics. Possible next calls do nothing. The
	 * computation progress may be monitored, and stopped.
	 * @param monitor an optional object for monitoring
	 * the progress of the statistics computation, and
	 * possibly for stopping it.
	 * @throws ClifException troubles with the CLIF storage
	 * component
	 */
	public void compute(QuickstatProgress monitor)
	throws ClifException
	{
		if (injectorStats == null)
		{
			cleaningFactor = Double.parseDouble(System.getProperty(CLEANINGFACTOR_PROPNAME, CLEANINGFACTOR_PROPDEFAULT));
			cleaningLimit = Double.parseDouble(System.getProperty(CLEANINGLIMIT_PROPNAME, CLEANINGLIMIT_PROPDEFAULT));
			injectorStats = new TreeMap<String,ActionStat>();
			globalStats = new ActionStat(cleaningFactor, cleaningLimit);
			BladeDescriptor[] allBlades = sr.getTestPlan(testName, null);
			List<BladeDescriptor> injectorBlades = new ArrayList<BladeDescriptor>(allBlades.length);
			for (BladeDescriptor desc : allBlades)
			{
				if (desc.isInjector()
					&& Arrays.asList(desc.getEventTypeLabels()).contains(ActionEvent.EVENT_TYPE_LABEL))
				{
					injectorBlades.add(desc);
				}
			}
			if (monitor != null)
			{
				monitor.quickstatStarted(testName, injectorBlades.size());
			}
			int progress = 0;
			for (BladeDescriptor blade : injectorBlades)
			{
				if (monitor != null)
				{
					monitor.quickstatProgress(blade.getId(), progress++);
				}
				Serializable iter = sr.getEventIterator(
					testName,
					blade.getId(),
					ActionEvent.EVENT_TYPE_LABEL,
					null);
				if (iter != null)
				{
					BladeEvent[] events;
					while (
						(events = sr.getNextEvents(iter, 1000)).length > 0
						&& (monitor == null || !monitor.quickstatIsCanceled()))
					{
						for (BladeEvent evt : events)
						{
							String actionType = ((ActionEvent)evt).getType();
							ActionStat stats = injectorStats.get(actionType);
							if (stats == null)
							{
								stats = new ActionStat(cleaningFactor, cleaningLimit);
								injectorStats.put(actionType, stats);
							}
							stats.add((ActionEvent)evt);
							globalStats.add((ActionEvent)evt);
						}
					}
					sr.closeEventIterator(iter);
				}
			}
			if (monitor != null && monitor.quickstatIsCanceled())
			{
				injectorStats = null;
				globalStats = null;
			}
			else if (monitor != null)
			{
				monitor.quickstatComplete();
			}
		}
	}


	/**
	 * First calls the compute method() to make sure the statistics are
	 * ready, then prints the report into the provided stream.
	 * The report gives for each action type value, and then globally:
	 * <ul>
	 *   <li>the number of successful requests</li>
	 *   <li>the minimum response time</li>
	 *   <li>the maximum response time</li>
	 *   <li>the response time mean</li>
	 *   <li>the response time median</li>
	 *   <li>the response time standard deviation</li>
	 *   <li>the successful requests throughput</li>
	 *   <li>the response time mean</li>
	 *   <li>the number of request errors</li>
	 * </ul>
	 * Value "NaN" means the value is not available.
	 * @param out the stream where to print the report.
	 * @see #compute(QuickstatProgress)
	 */
	public void report(PrintStream out)
	throws ClifException
	{
		compute(null);
		out.flush();
		if (cleaningFactor > 0 && globalStats.size() > 0)
		{
			out.printf(
				"Measures cleaning enabled: discarded %d outstanding measures (factor=%2.1f, kept %3.1f%% of measures)\n",
				(globalStats.size() - globalStats.getStatSortDataNumber()),
				cleaningFactor,
				(100 * globalStats.getStatSortDataNumber()) / (float)globalStats.size());
		}
		out.println("action type\tcalls\tmin\tmax\tmean\tmedian\tstd dev\tthroughput\terrors");
		for (Map.Entry<String,ActionStat> entry : injectorStats.entrySet())
		{
			out.print(entry.getKey());
			out.print(" \t");
			entry.getValue().report(out);
			out.println();
		}
		out.print("GLOBAL LOAD\t");
		globalStats.report(out);
		out.println();
	}
}
