package org.ow2.clif.analyze.lib.graph;

/**
 * FieldFilterAndOp contains three values = field, filter and operator
 * @author Olivier LIU
 * @author Bruno Dillenseger
 */
public class FieldFilterAndOp {
	String field;
	String value;
	String operator;


	public FieldFilterAndOp(String field, String value, String operator) {
		this.field = field;
		this.value = value;
		this.operator = operator;
	}

	public String getField(){
		return field;
	}

	public void setField(String field){
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	@Override
	public String toString(){
		return "\"" + field + "\" " + operator + " \"" + value + "\"";
	}
}

