/*
* CLIF is a Load Injection Framework
* Copyright (C) 2008 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF
*@author Olivier Liu
*
*
*/

package org.ow2.clif.analyze.lib.graph;


/**
 * All the graph options are stocked here for each dataset
 */
public class DatasetOptions {
	//datasetID
	private int datasetId;

	//true if the graph is draw
	private boolean isDrawn;

	//NoChart, TimeChart, FrequencyChart, QuantilChart
	private String pageType;
	
	//graph type : raw, moving...
	private String GraphType;

	private int numberofPoints;

	private int timeStart;

	private int timeEnd;
	
	private int timeWindow;
	
	private int step;
	
	private int numberOfSlices;

	public DatasetOptions(int datasetId, 
			boolean isDrawn, 
			String pageType,
			String graphType, 
			int numberofPoints,
			int timeStart, 
			int timeEnd,
			int timeWindow,
			int step, 
			int numberOfSlices) {
		super();
		this.datasetId = datasetId;
		this.isDrawn = isDrawn;
		this.pageType = pageType;
		GraphType = graphType;
		this.numberofPoints = numberofPoints;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.timeWindow = timeWindow;
		this.step = step;
		this.numberOfSlices = numberOfSlices;
	}

	public DatasetOptions(){

	}

	public DatasetOptions(
			int datasetId, 
			boolean isDrawn, 
			String graphType,
			int numberofPoints, 
			int timeStart, 
			int timeEnd) {
		super();
		this.datasetId = datasetId;
		this.isDrawn = isDrawn;
		GraphType = graphType;
		this.numberofPoints = numberofPoints;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
	}

	public int getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(int timeEnd) {
		this.timeEnd = timeEnd;
	}

	public int getDatasetId() {
		return datasetId;
	}

	public boolean isDrawn() {
		return isDrawn;
	}

	public String getGraphType() {
		return GraphType;
	}

	public int getNumberofPoints() {
		return numberofPoints;
	}

	public int getTimeStart() {
		return timeStart;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public int getTimeWindow() {
		return timeWindow;
	}

	public void setTimeWindow(int timeWindow) {
		this.timeWindow = timeWindow;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public int getNumberOfSlices() {
		return numberOfSlices;
	}

	public void setNumberOfSlices(int numberOfSlices) {
		this.numberOfSlices = numberOfSlices;
	}


}
