/*
* CLIF is a Load Injection Framework
* Copyright (C) 2006 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name$
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.analyze.lib.list;

import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeFilter;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;

/**
 * Dummy analyzer: prints a list of tests and their blades, possibly filtering
 * on the test names' prefix and the blades' class name. This class intends 
 * to be a sample for programming analyzers and accessing the storage system.
 *
 * @author Bruno Dillenseger
 */
abstract public class AnalyzerImpl
{
	protected StorageRead store;


	/**
	 * @param args args[0] is the classname of the storage component;
	 * args[1] is optional and may give a prefix that test names must match
	 * to be printed. A - (minus sign) character or an empty string sets an
	 * empty prefix matching all test names;
	 * args[2] is optional and may specify a blade class name that blades
	 * must be instances of to be listed (an empty string is ignored).
	 */
	static public void main(String[] args)
	{
		try
		{
			// get access to the storage component
			StorageRead store = (StorageRead)Class.forName(args[0]).newInstance();
			// set test filter
			TestFilter test_filter = null;
			if (args.length > 1 && !args[1].equals("-"))
			{
				test_filter = new PrefixTestFilter(args[1]);
			}
			// get list of tests
			TestDescriptor[] tests = store.getTests(test_filter);
			BladeFilter blade_filter = null; 
			if (args.length > 2 && args[2].length() > 0)
			{
				blade_filter = new ClassnameBladeFilter(args[2]);
			}
			for (int i=0 ; i<tests.length ; ++i)
			{
				BladeDescriptor[] blades = store.getTestPlan(tests[i].getName(), blade_filter);
				if (blades.length > 0)
				{
					System.out.println(tests[i]);
					for (int j=0 ; j<blades.length ; ++j)
					{
						System.out.println("\t" + blades[j]);
					}
				}
			}
		}
		catch (Throwable t)
		{
			t.printStackTrace(System.err);
		}
	}
}
