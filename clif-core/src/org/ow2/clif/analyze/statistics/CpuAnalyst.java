/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.analyze.statistics.profiling.Datum;
import org.ow2.clif.analyze.statistics.profiling.ProbeDatum;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.supervisor.api.ClifException;

import java.util.List;

/**
 * This class is intended to analyze the measurements of CPU load
 * measured by CLIF probes.
 * 
 * @author Guy Vachet
 */
public class CpuAnalyst extends ProbeAnalyst {
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	public static final String TYPE_LABEL = CPU_EVENT_TYPE_LABEL;
	public static final String FIELD_LABEL = CPU_EVENT_FIELD_LABEL;
//	other available fields: USER_CPU_EVENT_FIELD_LABEL
//							KERNEL_CPU_EVENT_FIELD_LABEL

	/**
	 * constructor
	 */
	public CpuAnalyst() {
		setLabel(TYPE_LABEL);
		StringBuffer sb = new StringBuffer(getLabel());
		System.out.println(sb.append(" analysis:"));
	}

	public CpuAnalyst(String analyzeRange) {
		setLabel(TYPE_LABEL);
		StringBuffer sb = new StringBuffer(getLabel());
		System.out.println(sb.append(" analysis").append(analyzeRange));
	}

	@Override
	public void addProfilingData(BladeStoreReader reader, EventFilter filter)
			throws ClifException {
		String bladeId = reader.getBladeDescriptor().getId();
		List<Datum> cpuData = new CpuReader(reader).getProfilingData(filter);
		addBladeData(bladeId, cpuData);
		if (VERBOSE)
			System.out.println("Size of data = " + cpuData.size());
	}

	/**
	 */
	class CpuReader extends BladeDatumReader {

		/**
		 */
		public CpuReader(BladeStoreReader bladeReader) {
			setBladeStoreReader(bladeReader);
			setEventTypeLabel(TYPE_LABEL);
		}
		
		/**
		 * @param event
		 * @param minTime
		 * @return
		 * @see org.ow2.clif.probe.cpu.CPUEvent
		 */
		@Override
		public Datum convert2Datum(BladeEvent event, long minTime) {
			return new ProbeDatum(event.getDate() - minTime, ((Long) event
					.getFieldValue(FIELD_LABEL)).longValue());
		}

	}

}
