/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics.util.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Guy Vachet
 */
public class Generator {

    private static final String LORENZO = "Veux-tu donc que je sois un spectre, " +
            "et qu'en frappant sur ce squelette il n'en sorte aucun son ? " +
            "Si je suis l'ombre de moi-meme, veux-tu donc que je rompe le seul " +
            "fil qui rattache aujourd'hui mon coeur a quelques fibres de mon coeur " +
            "d'autrefois ? Songes-tu que ce meurtre, c'est tout ce qui reste de " +
            "ma vertu ? Songes-tu que je glisse depuis deux ans sur un rocher " +
            "taille a pic, et que ce meurtre est le seul brin d'herbe ou j'aie " +
            "pu cramponner mes ongles ? Crois-tu donc que je n'aie plus d'orgueil, " +
            "parce que je n'ai plus de honte ? et veux-tu que je laisse mourir " +
            "en silence l'enigme de ma vie ?";
    private static char[] shuffledChar = null;
    // generate more random numbers than by java.lang.Math.random()
    private static final int BUFFER_SIZE = 257;
    private static double[] randoms;

    static {
        randoms = new double[BUFFER_SIZE];
        for (int i = 0; i < BUFFER_SIZE; i++)
            randoms[i] = Math.random();
        char[] charArray = LORENZO.toCharArray();
        Character[] characterArray = new Character[charArray.length];
        for (int i = 0; i < charArray.length; i++)
            characterArray[i] = new Character(charArray[i]);
        List<Character> l = Arrays.asList(characterArray);
        Collections.shuffle(l);
        Character[] newCharacterArray = (Character[]) l.toArray();
        shuffledChar = new char[charArray.length];
        for (int i = 0; i < charArray.length; i++)
            shuffledChar[i] = newCharacterArray[i].charValue();
    }

    /**
     * @return a randomly shuffled text
     */
    public static String getDummyString() {
        return new String(shuffledChar);
    }

    /**
     * @param length
     * @return a randomly shuffled text
     */
    public static String getDummyString(int length) {
        if (length == LORENZO.length())
            return new String(shuffledChar);
        else if (length < LORENZO.length()) {
            String s = new String(shuffledChar);
            return s.substring(0, length);
        } else {
            char[] arrayOfChar = new char[length];
            for (int i = 0; i < arrayOfChar.length; i++)
                arrayOfChar[i] = shuffledChar[i % LORENZO.length()];
            return new String(arrayOfChar);
        }
    }

    /**
     * randoms a int value between 0 (included) to maxValue (excluded) with
     * (approximately) uniform distribution from that range
     * @param maxValue
     * @return a random value between 0 (included) to maxValue (excluded)
     */
    public static int randomInt(int maxValue) {
        int pos = (int) (Math.random() * BUFFER_SIZE);
        int randomInt = (int) (maxValue * randoms[pos]);
        randoms[pos] = Math.random();
        return randomInt;
    }

    /**
     * randoms a long value between 0 (included) to maxValue (excluded) with
     * (approximately) uniform distribution from that range
     * @param maxValue
     * @return a random value between 0 (included) to maxValue (excluded)
     */
    public static long randomLong(long maxValue) {
        int pos = (int) (Math.random() * BUFFER_SIZE);
        long randomTime = (long) (maxValue * randoms[pos]);
        randoms[pos] = Math.random();
        return randomTime;
    }

}
