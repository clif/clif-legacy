/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;

import java.util.regex.Pattern;

/**
 * Filter tests according to a given name.
 * 
 * @author Bruno Dillenseger
 */
public class TestFilterImpl implements TestFilter {

	private static final long serialVersionUID = -9196476241839419062L;
	private String filter = null;
	private Pattern pattern = null;

	/**
	 * Creates a new name filter for test descriptors with a null match, which
	 * means all test names will match this new filter.
	 */
	public TestFilterImpl() {
		this(null);
	}

	/**
	 * Creates a new filter for test name with the given string.
	 * 
	 * @param prefix
	 *            the filter that test name must match. If null, all
	 *            test names will match.
	 */
	public TestFilterImpl(String prefix) {
		filter = prefix;
		if (filter != null) {
			pattern = Pattern.compile(prefix);
		}		
	}

	/**
	 * Accepts all test names matching this filter's expression attribute.
	 * 
	 * @param desc
	 *            the test descriptor to accept or reject.
	 * @return true if the given test's name matches the filter's expression,
	 *         false otherwise.
	 */
	@Override
	public boolean accept(TestDescriptor desc) {
		if (filter == null) {
			return true;
		} else if (pattern.matcher(desc.getName()).matches()) {
			return true;
		} else {
			return pattern.split(desc.getName(), -1).length > 1;
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(AnalyzerImpl.LABEL);
		if (filter == null) {
			return sb.append(" without test id filter").toString();
		} else {
			sb.append(" test filtered through \"").append(filter);
			return sb.append("\"").toString();
		}
	}

}
