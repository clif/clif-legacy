/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.analyze.statistics.profiling.Datum;
import org.ow2.clif.analyze.statistics.profiling.ProbeDatum;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.EventFilter;
import org.ow2.clif.supervisor.api.ClifException;

import java.util.List;

/**
 *  Analyze memory probes of CLIF
 * 
 * @author Guy Vachet
 */
public class MemoryAnalyst extends ProbeAnalyst {
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	public static final String TYPE_LABEL = MEMORY_EVENT_TYPE_LABEL;
	public static final String FIELD_LABEL = USED_RAM_EVENT_FIELD_LABEL;
//	other available fields: CACHED_EVENT_FIELD_LABEL
//							BUFFERED_EVENT_FIELD_LABEL
//							USED_SWAP_EVENT_FIELD_LABEL

	/**
	 * constructor
	 */
	public MemoryAnalyst() {
		setLabel(TYPE_LABEL);
		StringBuffer sb = new StringBuffer(getLabel());
		System.out.println(sb.append(" analysis:"));
	}

	public MemoryAnalyst(String analyzeRange) {
		setLabel(TYPE_LABEL);
		StringBuffer sb = new StringBuffer(getLabel());
		System.out.println(sb.append(" analysis").append(analyzeRange));
	}

	@Override
	public void addProfilingData(BladeStoreReader reader, EventFilter filter)
			throws ClifException {
		String bladeId = reader.getBladeDescriptor().getId();
		List<Datum> data = new MemoryReader(reader).getProfilingData(filter);
		addBladeData(bladeId, data);
		if (VERBOSE)
			System.out.println("Size of data = " + data.size());
	}

	/**
	 */
	class MemoryReader extends BladeDatumReader {

		public MemoryReader(BladeStoreReader bladeReader) {
			setBladeStoreReader(bladeReader);
			setEventTypeLabel(TYPE_LABEL);
		}

		/**
		 * @param event
		 * @param minTime
		 * @return
		 */
		@Override
		public Datum convert2Datum(BladeEvent event, long minTime) {
			return new ProbeDatum(event.getDate() - minTime, ((Long) event
					.getFieldValue(FIELD_LABEL)).longValue());
		}

	}

}
