/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.analyze.statistics.profiling.Datum;
import org.ow2.clif.storage.api.*;
import org.ow2.clif.supervisor.api.ClifException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Guy Vachet
 */
public abstract class BladeDatumReader {
	//
	private BladeStoreReader bladeStoreReader = null;
	private String eventTypeLabel = null;

	public void setBladeStoreReader(BladeStoreReader bladeStoreReader) {
		this.bladeStoreReader = bladeStoreReader;
	}

	public void setEventTypeLabel(String eventTypeLabel) {
		this.eventTypeLabel = eventTypeLabel;
	}

	public abstract Datum convert2Datum(BladeEvent event, long minTime);

	public List<Datum> getProfilingData(EventFilter filter)
			throws ClifException {
		List<Datum> profilingData = new ArrayList<Datum>();
		BufferedReader reader = null;
		EventFactory factory = null;
		BladeEvent event;
		long minTime = 0;
		try {
			if ((filter != null) && filter instanceof DateFilter)
				minTime = ((DateFilter) filter).getMinTime();
			factory = BladeStoreReader.getEventFactory(eventTypeLabel);
			File file = new File(bladeStoreReader.getBladeDir(), eventTypeLabel);
			reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			while (line != null) {
				if (!BladeStoreReader.isCommentLine(line)) {
					event = factory.makeEvent(AbstractEvent.DEFAULT_SEPARATOR,
							line);
					if ((filter == null) || filter.accept(event)) {
						profilingData.add(convert2Datum(event, minTime));
					}
				}
				line = reader.readLine();
			}
		} catch (ClifException e) {
			throw e;
		} catch (NoMoreEvent e) {
			// silent
		} catch (Exception e) {
			StringBuffer sb = new StringBuffer("Error when getting ");
			if (factory == null)
				sb = new StringBuffer("Unable to get factory for ");
			else if (reader == null)
				sb = new StringBuffer("Unable to find ");
			sb.append(eventTypeLabel).append(" events from blade ");
			sb.append(bladeStoreReader.getBladeDescriptor().getId());
			sb.append(" (data size = ").append(profilingData.size());
			throw new ClifException(sb.append(")").toString(), e);
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException ex) {
				ex.printStackTrace(System.err);
			}
		}
		return profilingData;
	}

}
