/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.analyze.statistics.util.data.LongStatistics;
import org.ow2.clif.analyze.statistics.util.data.Math4Long;
import org.ow2.clif.analyze.statistics.util.data.SliceSummary;

import java.util.*;

/**
 * Generic analyst of the CLIF probes.
 * 
 * @author Guy Vachet
 */
public abstract class ProbeAnalyst extends AbstractAnalyst {

	/**
	 */
	public void outputProbeHistory(List<SliceSummary> sliceSummaries,
			long sliceSize) {
		StringBuffer sb = new StringBuffer(getLabel());
		sb.append(" data versus elapsed time (slicing size: ");
		sb.append(sliceSize / 1000);
		// sb.append(" s) as\nRange (s)\tNumber\tResult\n");
		sb.append(" s) as\nRange (s)\tNumber\tMedian\tMean\tStd\tMin\tMax\n");
		for (Iterator<SliceSummary> iter = sliceSummaries.iterator(); iter
				.hasNext();) {
			// sb.append(iter.next()).append("\n");
			sb.append(iter.next().displayStatistics());
			sb.append("\n");
		}
		System.out.println(sb);
	}

	/**
	 */
	public void outputHistories(Map<String, List<SliceSummary>> histories,
			int maxSliceNb, long sliceSize) {
		String[] probeIds = getBladeIdentifiers();
		long sliceMinTime = 0;
		double mean;
		StringBuffer sb = new StringBuffer("All ");
		sb.append(getLabel()).append(" vs elapsed time (slicing size = ");
		sb.append(sliceSize / 1000).append(" s) as\nProbeId");
		for (int column = 0; column < probeIds.length; column++)
			sb.append("\t").append(probeIds[column]);
		for (int line = 0; line < maxSliceNb; line++) {
			sb.append("\n[").append(sliceMinTime).append("..");
			sb.append(sliceMinTime + sliceSize / 1000).append("[");
			for (int column = 0; column < probeIds.length; column++) {
				List<SliceSummary> slices = histories.get(probeIds[column]);
				if (line < slices.size()) {
					mean = (slices.get(line)).getDataMean();
					sb.append("\t").append(Long.toString(Math.round(mean)));
				} else
					sb.append("\t").append("-");
			}
			sliceMinTime += sliceSize / 1000;
		}
		System.out.println(sb);
	}

	/**
	 * 
	 * @param isDetailed
	 *            if true display more analysis
	 * @param sliceSize
	 *            size of elapsed time in order to analyze subpopulation
	 */
	@Override
	public void outputAnalysis(boolean isDetailed, long sliceSize) {
		String[] bladeIds;
		ProfilingStatistics profilStat;
		Map<String, List<SliceSummary>> probeHistories = null;
		int size, maxSliceNumber = 0;
		LongStatistics longResults;
		List<SliceSummary> sliceSummaries;
		StringBuffer sb;
		long time;
		probeHistories = new HashMap<String, List<SliceSummary>>();
		if (isEmpty()) {
			System.out.println("Any probe to analyze..");
			return;
		}
		System.out.println(getLabel() + " results: \tmean \tstd");
		bladeIds = getBladeIdentifiers();
		for (int i = 0; i < bladeIds.length; i++) {
			profilStat = getProfilingStatistics(bladeIds[i]);
			if (profilStat.isEmpty()) {
				sb = new StringBuffer("Any data by blade # ");
				System.out.println(sb.append(bladeIds[i]).append(", so .."));
				probeHistories.put(bladeIds[i], new ArrayList<SliceSummary>());
			} else {
				time = profilStat.getMaxTimestamp() - profilStat.getMinTimestamp();
				longResults = profilStat.getLongResults();
				// size = longResults.getStatDataNumber();
				size = longResults.size();
				sb = new StringBuffer(bladeIds[i]).append(" \t");
				//sb.append(Math4Long.round(longResults.getStatSortMean(), size));
				sb.append(Math4Long.round(longResults.getMean(), size));
				sb.append(" \t");
				//sb.append(Math4Long.round(longResults.getStatSortStd(), size));
				sb.append(Math4Long.round(longResults.getStd(), size));
				sb.append(" \t(").append(size).append(" data within ");
				sb.append(Math4Long.displayMillisecTime(time));
				System.out.println(sb.append(")"));
				if (isDetailed) {
					// results.outputStatisticalSortDataFrequency(PROBE_FREQUENCY_CLASS_NUMBER);
					longResults.outputRawDataFrequency(PROBE_FREQUENCY_CLASS_NUMBER);
					// sliceSummaries =
					// extractor.getHistoryOfStatLongResults(results,
					// sliceSize);
					sliceSummaries = profilStat.getHistoryOfRawLongResults(
							longResults, sliceSize);
					// set the max number of slices
					if (sliceSummaries.size() > maxSliceNumber)
						maxSliceNumber = sliceSummaries.size();
					// outputProbeHistory(sliceSummaries, sliceSize);
					probeHistories.put(bladeIds[i], sliceSummaries);
				}
			}
		}
		if (isDetailed && (probeHistories.size() > 0)) {
			outputHistories(probeHistories, maxSliceNumber, sliceSize);
		}
	}

}
