/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.probe.cpu.CPUEvent;
import org.ow2.clif.probe.jvm.JVMEvent;
import org.ow2.clif.probe.memory.MemoryEvent;
import org.ow2.clif.probe.network.NetworkEvent;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.storage.api.AlarmEvent;
import org.ow2.clif.storage.api.LifeCycleEvent;

/**
 * @author Guy Vachet
 */
public interface Constants {
	String ACTION_EVENT_TYPE_LABEL = ActionEvent.EVENT_TYPE_LABEL;
	String ALARM_EVENT_TYPE_LABEL = AlarmEvent.EVENT_TYPE_LABEL;
	String CPU_EVENT_TYPE_LABEL = CPUEvent.EVENT_TYPE_LABEL;
	String JVM_EVENT_TYPE_LABEL = JVMEvent.EVENT_TYPE_LABEL;
	// String JMX_JVM_EVENT_TYPE_LABEL = "JMX_JVM";
	String LIFECYCLE_EVENT_TYPE_LABEL = LifeCycleEvent.EVENT_TYPE_LABEL;
	String MEMORY_EVENT_TYPE_LABEL = MemoryEvent.EVENT_TYPE_LABEL;
	String NETWORK_EVENT_TYPE_LABEL = NetworkEvent.EVENT_TYPE_LABEL;

	String ACTION_EVENT_CLASS = "org.ow2.clif.storage.api.ActionEvent";
	String ALARM_EVENT_CLASS = "org.ow2.clif.storage.api.AlarmEvent";
	String CPU_EVENT_CLASS = "org.ow2.clif.probe.cpu.CPUEvent";
	String DATA_POWER_SNMP_EVENT_CLASS = "org.ow2.clif.probe.datapowerSystemUsage.DatapowerSystemUsageEvent";
	String JVM_EVENT_CLASS = "org.ow2.clif.probe.jvm.JVMEvent";
	// String JMX_JVM_EVENT_CLASS =
	// "org.ow2.clif.probe.jmx_jvm.JMX_JVMEvent";
	String LIFECYCLE_EVENT_CLASS = "org.ow2.clif.storage.api.LifeCycleEvent";
	String MEMORY_EVENT_CLASS = "org.ow2.clif.probe.memory.MemoryEvent";
	String NETWORK_EVENT_CLASS = "org.ow2.clif.probe.network.NetworkEvent";

	// event field labels of action event, usefull for profiling
	String DURATION_EVENT_FIELD_LABEL = "duration";
	String RESULT_EVENT_FIELD_LABEL = "result";
	String SUCCESS_EVENT_FIELD_LABEL = "success";
	// event field labels of CPU event, usefull for profiling
	String CPU_EVENT_FIELD_LABEL = "%CPU";
	String USER_CPU_EVENT_FIELD_LABEL = "%CPU user";
	String KERNEL_CPU_EVENT_FIELD_LABEL = "%CPU kernel";
	// event field labels of memory event, usefull for profiling
	String USED_RAM_EVENT_FIELD_LABEL = "% used RAM";
	String CACHED_EVENT_FIELD_LABEL = "% Cached";
	String BUFFERED_EVENT_FIELD_LABEL = "% Buffers";
	String USED_SWAP_EVENT_FIELD_LABEL = "% used swap";
	// event field labels of JVM event, usefull for profiling
	String FREE_MEMORY_EVENT_FIELD_LABEL = "free memory (MB)";
	String USED_MEMORY_EVENT_FIELD_LABEL = "used memory %";
	String FREE_USABLE_MEMORY_EVENT_FIELD_LABEL = "free usable memory %";
	String GC_ARGUMENT = "garbage collection";
	// event field labels of SNMP System Usage event, usefull for profiling
	String DATA_POWER_SNMP_EVENT_FIELD_LABEL = "%System usage per second";

	// @see BladeState
	int BLADE_STATE_INITIALIZED_CODE = 4;
	int BLADE_STATE_STARTING_CODE = 5;
	int BLADE_STATE_RUNNING_CODE = 6;
	int BLADE_STATE_COMPLETED_CODE = 10;
	int BLADE_STATE_STOPPING_CODE = 11;

	// String STORE_CLASS =
	// "org.ow2.clif.storage.lib.filestorage.ConsoleFileStorageImpl";
	// String STORE_CLASS =
	// "org.ow2.clif.analyze.statistics.FileStoreReader";
	// String COMMENT_PREFIX = ConsoleFileStorageImpl.COMMENT_PREFIX;
	String COMMENT_PREFIX = "#";

	// default analysis parameters
	String DEFAULT_ANALYZE_PROPERTY_FILE = "etc/analyze.properties";
	String DEFAULT_REPORT_PATH = "report";
	String DEFAULT_REGEX_CTRL_ACTION = "OK";
	long DEFAULT_MIN_TIME = 0;
	long DEFAULT_MAX_TIME = Long.MAX_VALUE;
	long DEFAULT_TIME_RANGE_START = DEFAULT_MIN_TIME;
	long DEFAULT_TIME_RANGE_END = DEFAULT_MAX_TIME;
	boolean DEFAULT_IS_DETAILED_ANALYSIS = true;
	long DEFAULT_SLICE_SIZE = 30;
	boolean DEFAULT_IS_OUTPUT_FILE = false;
	String DEFAULT_RESULT_PATH = "analysis";
	// factor of statistical sort (in order to ignore aberrant data)
	double DEFAULT_STATISTICAL_SORT_FACTOR = 2.0;
	double DEFAULT_STATISTICAL_SORT_RATIO = 95.4;
	// parameters of distribution analyzes
	int INJECTOR_FREQUENCY_CLASS_NUMBER = 25;
	int INJECTOR_QUANTILE_CLASS_NUMBER = 7;
	int PROBE_FREQUENCY_CLASS_NUMBER = 5;

}
