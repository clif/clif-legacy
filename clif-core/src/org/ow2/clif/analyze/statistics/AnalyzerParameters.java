/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * This class is designed to present the analysis parameters of the
 * CLIF test.
 * 
 * @author Guy Vachet
 */
public class AnalyzerParameters implements Constants {
	private static final boolean VERBOSE = false;
	// which CLIF test and how to analyze
	private String analyzePropFileName = DEFAULT_ANALYZE_PROPERTY_FILE;
	private String reportPath = null;
	private String testNameFilter = null;
	// set of optional CLIF blade id in order to reduce analysis field
	private Set<String> bladeIdFilter = null;
	private Set<String> eventTypeFilter = null;
	// regular expression to check action result
	private String regexCtrl = null;
	// beginning and end of elapsed time (in milliseconds) to be analyzed
	private long timeStart;
	private long timeEnd;
	// tuning of atypical data sort
	private double injectorStatSortFactor;
	private double injectorStatSortRatio;
	private double probeStatSortFactor;
	private double probeStatSortRatio;
	private boolean isDetailedAnalysis;
	// in order to analyze in function of elapsed time (size in milliseconds).
	private long sliceSize;
	private boolean isOutputFile;
	private String resultFilePath = null;

	/**
	 */
	public AnalyzerParameters() {
	}

	// setters and getters
	/**
	 * input of analyze parameters 
	 */
	public void setAnalyzePropFileName(String analyzePropFile) {
		analyzePropFileName = analyzePropFile;
	}

	/**
	 * input of analyze parameters 
	 */
	public String getAnalyzePropFileName() {
		return analyzePropFileName;
	}

	/*
	 * All parameters
	 */
	public void setReportPath(String path) {
		reportPath = path;
	}

	public String getReportPath() {
		return reportPath;
	}

	public void setTestNameFilter(String id) {
		testNameFilter = id;
	}

	public String getTestNameFilter() {
		return testNameFilter;
	}

	public void setBladeIdFilter(Set<String> ids) {
		bladeIdFilter = ids;
	}

	public Set<String> getBladeIdFilter() {
		return bladeIdFilter;
	}

	public void setEventTypeFilter(Set<String> eventTypes) {
		this.eventTypeFilter = eventTypes;
	}

	public Set<String> getEventTypeFilter() {
		return eventTypeFilter;
	}

	public void setRegexCtrl(String regexCtrl) {
		this.regexCtrl = regexCtrl;
	}

	public String getRegexCtrl() {
		return regexCtrl;
	}

	public void setTimeStart(long time) {
		timeStart = time;
	}

	public long getTimeStart() {
		return timeStart;
	}

	public void setTimeEnd(long time) {
		timeEnd = time;
	}

	public long getTimeEnd() {
		return timeEnd;
	}
	
	public void setInjectorStatSortFactor(double injectorStatSortFactor) {
		this.injectorStatSortFactor = injectorStatSortFactor;
	}

	public double getInjectorStatSortFactor() {
		return injectorStatSortFactor;
	}

	public void setInjectorStatSortRatio(double injectorStatSortRatio) {
		this.injectorStatSortRatio = injectorStatSortRatio;
	}

	public double getInjectorStatSortRatio() {
		return injectorStatSortRatio;
	}

	public void setProbeStatSortFactor(double probeStatSortFactor) {
		this.probeStatSortFactor = probeStatSortFactor;
	}

	public double getProbeStatSortFactor() {
		return probeStatSortFactor;
	}

	public void setProbeStatSortRatio(double probeStatSortRatio) {
		this.probeStatSortRatio = probeStatSortRatio;
	}

	public double getProbeStatSortRatio() {
		return probeStatSortRatio;
	}

	public void setIsDetailedAnalysis(boolean detailedAnalysis) {
		isDetailedAnalysis = detailedAnalysis;
	}

	public boolean isDetailedAnalysis() {
		return isDetailedAnalysis;
	}

	public void setSliceSize(long size) {
		sliceSize = size;
	}

	public long getSliceSize() {
		return sliceSize;
	}

	public void setIsOutputFile(boolean isOutputFile) {
		this.isOutputFile = isOutputFile;
	}

	public boolean isOutputFile() {
		return isOutputFile;
	}

	public void setResultFilePath(String resultFilePath) {
		this.resultFilePath = resultFilePath;
	}

	public String getResultFilePath() {
		return resultFilePath;
	}

	/**
	 */
	private Set<String> getSetFromArg(String arg) {
		if (arg == null) {
			return null;
		} else {
			Set<String> items = new HashSet<String>();
			StringTokenizer st = new StringTokenizer(arg, ",");
			while (st.hasMoreTokens()) {
				items.add(st.nextToken());
			}
			return items;
		}
	}

	private long parseTime(String arg, long defaultValue) {
		if (arg == null) {
			return defaultValue;
		} else {
			return Long.parseLong(arg) * 1000;
		}
	}

	private double parseDouble(String arg, double defaultValue) {
		if (arg == null) {
			return defaultValue;
		} else {
			return Double.parseDouble(arg);
		}
	}

	private boolean parseBoolean(String arg, boolean defaultValue) {
		if (arg == null) {
			return defaultValue;
		} else {
			return Boolean.parseBoolean(arg);
		}
	}

	/**
	 * load analyze parameters from properties
	 */
	public void loadParameters(Properties props) throws Exception {
		String param = null;
		setReportPath(props.getProperty("report.path", DEFAULT_REPORT_PATH));
		setTestNameFilter(props.getProperty("test.name.filter"));
		setEventTypeFilter(getSetFromArg(props.getProperty("event.type.filter")));
		setBladeIdFilter(getSetFromArg(props.getProperty("blade.id.filter")));
		setRegexCtrl(props.getProperty("regex.ctrl.action", DEFAULT_REGEX_CTRL_ACTION));
		setTimeStart(parseTime(props.getProperty("time.start"), DEFAULT_TIME_RANGE_START));
		setTimeEnd(parseTime(props.getProperty("time.end"), DEFAULT_TIME_RANGE_END));
		// all stuff needed for statistical sort
		param = props.getProperty("injector.statistical.factor");
		setInjectorStatSortFactor(parseDouble(param, DEFAULT_STATISTICAL_SORT_FACTOR));
		param = props.getProperty("injector.statistical.ratio");
		setInjectorStatSortRatio(parseDouble(param, DEFAULT_STATISTICAL_SORT_RATIO));
		param = props.getProperty("probe.statistical.factor");
		setProbeStatSortFactor(parseDouble(param, DEFAULT_STATISTICAL_SORT_FACTOR));
		param = props.getProperty("probe.statistical.ratio");
		setProbeStatSortRatio(parseDouble(param, DEFAULT_STATISTICAL_SORT_RATIO));
		param = props.getProperty("is.detailed.analysis");
		setIsDetailedAnalysis(parseBoolean(param, DEFAULT_IS_DETAILED_ANALYSIS));
		setSliceSize(parseTime(props.getProperty("slice.size"), DEFAULT_SLICE_SIZE * 1000));		
		param = props.getProperty("is.ouput.file");
		setIsOutputFile(parseBoolean(param, DEFAULT_IS_OUTPUT_FILE));		
		setResultFilePath(props.getProperty("ouput.path", DEFAULT_RESULT_PATH));
		// last but not least ?
		if (getTimeStart() < DEFAULT_TIME_RANGE_START)
			throw new Exception(
					"Unconsistant beginning of time range to analyze");
		if (getTimeEnd() < getTimeStart())
			throw new Exception("Unconsistant end of time range to analyze");
	}

	/**
	 * load analyze parameters from property file
	 */
	public void loadParameters(String fileName) throws Exception {
		Properties properties = new Properties();
		properties.load(new FileInputStream(new File(fileName)));
		loadParameters(properties);
	}

	/**
	 * load default analyze parameters
	 */
	public void loadParameters() throws Exception {
		loadParameters(System.getProperties());
	}

	/**
	 * in order to display time range to be analyzed
	 */
	public String getAnalyzeRangeLabel() {
		StringBuffer sb = new StringBuffer(" within [");
		sb.append(getTimeStart() / 1000).append(" s.. ");		
		if (timeEnd == DEFAULT_MAX_TIME) {
			sb.append(" end of elapsed time]:");
		} else {
			sb.append(getTimeEnd() / 1000).append(" s]:");			
		}
		return sb.toString();
	}

	/**
	 */
	@Override
	public String toString() {
		Set<String> bladeIds = getBladeIdFilter();
		Set<String> evtTypes = getEventTypeFilter();
		StringBuffer sb = new StringBuffer(AnalyzerImpl.LABEL);
		sb.append(" based on \'").append(getAnalyzePropFileName());
		sb.append("\' parameters as:");
	    sb.append("\nCLIF measure path:                   \t");
		sb.append(getReportPath());
		if (getTestNameFilter() != null) {
			sb.append("\nTest name filter:                    \t");
			sb.append(getTestNameFilter());
		} else
			sb.append("\nAnalyze all tests within this location.");
		if (evtTypes != null && !evtTypes.isEmpty()) {
			sb.append("\nEvent types to be retained:          \t");
			sb.append(evtTypes);
		} else {
			sb.append("\nAll event types are retained.");
		}
		if (bladeIds != null && !bladeIds.isEmpty()) {
			sb.append("\nBlade identifiers to be retained:    \t");
			sb.append(bladeIds);
		} else {
			sb.append("\nAll blades are retained.");
		}
		sb.append("\nBeginning of time range (in seconds): \t");
		sb.append(getTimeStart() / 1000);
		if (timeEnd != DEFAULT_MAX_TIME) {
			sb.append("\nEnd of time to be analyzed (seconds): \t");
			sb.append(getTimeEnd() / 1000);
		} else
			sb.append("\nThe end of elapsed time range is not limited.");
		if (VERBOSE) {
			sb.append("\nInjector statistical sort factor: \t");
			sb.append(getInjectorStatSortFactor());
			sb.append("\nInjector statistical sort ratio:  \t");
			sb.append(getInjectorStatSortRatio());
			sb.append("\nProbe statistical sort factor:    \t");
			sb.append(getProbeStatSortFactor());
			sb.append("\nProbe statistical sort ratio:     \t");
			sb.append(getProbeStatSortRatio());
		}
		if (isDetailedAnalysis()) {
			sb.append("\nDisplay a detailed analyze.");
			if (VERBOSE) {
				sb.append("\n  note: Size of elapsed-time slice =  \t");
				sb.append(getSliceSize() / 1000).append(" seconds.");
			}
		} else
			sb.append("\nDisplay an analysis summary.");
		if (isOutputFile()) {
			sb.append("\nAnalyze results redirect to:      \t");
			sb.append(getResultFilePath());
		}
		return sb.append("\n").toString();
	}

}
