/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.analyze.statistics;

import org.ow2.clif.console.lib.ClifDeployDefinition;
import org.ow2.clif.console.lib.TestPlanReader;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeFilter;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.api.TestFilter;
import org.ow2.clif.storage.lib.filestorage.BladeDescriptorImpl;
import org.ow2.clif.storage.lib.filestorage.TestDescriptorImpl;
import org.ow2.clif.supervisor.api.ClifException;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Partial but enhanced file storage reader (it allows outer path).
 * 
 * @author Guy Vachet
 */
public class FileStoreReader implements Constants {
	// in order to analyze CLIF results out of the current project base dir
	private String testDirbase = null;

	/**
	 * empty constructor
	 */
	public FileStoreReader() {
	}

	/**
	 * constructor
	 */
	public FileStoreReader(String testDirbase) {
		this.testDirbase = testDirbase;
	}

	public void setDirbase(String testDirbase) {
		this.testDirbase = testDirbase;
	}

	public String getDirbase() {
		return testDirbase;
	}

	public File getTestDir(String testName) {
		return new File(testDirbase, testName);
	}

	public File getCtpFile(String testName) {
		return getTestDir(testName + TestPlanReader.FILE_EXT);
	}

	public File getBladeDir(String testName, String bladeId) {
		return new File(getTestDir(testName), bladeId);
	}

	public BladeStoreReader getBladeReader(String testName, BladeDescriptor desc) {
		return new BladeStoreReader(desc, getBladeDir(testName, desc.getId()));
	}

	/**
	 * @param filter
	 *            only tests matching this filter will be retained
	 * @return a browser object to browse among available test execution results
	 */
	public TestDescriptor[] getTests(final TestFilter filter)
			throws ClifException {
		final List<TestDescriptor> descriptors = new ArrayList<TestDescriptor>();
		try {
			new File(testDirbase).listFiles(new FileFilter() {
				@Override
				public boolean accept(File path) {
					if (path.isDirectory()) {
						try {
							TestDescriptor desc = new TestDescriptorImpl(path);
							if (filter == null || filter.accept(desc)) {
								if (getCtpFile(desc.getName()).exists()) {									
									descriptors.add(desc);
									return true;
								} else {
									return false;
								}
							} else {
								return false;
							}
						} catch (Exception ex) {
							StringBuffer sb = new StringBuffer("Error");
							sb.append(" when getting test descriptor for path: ");
							throw new RuntimeException(sb.append(path).toString());
						}
					} else {
						return false;
					}
				}
			});
		} catch (Exception e) {
			throw new ClifException("Could not get tests", e);
		}
		return descriptors
				.toArray(new TestDescriptor[descriptors.size()]);
	}

	public BladeDescriptor[] getTestPlan(String testName, BladeFilter filter)
			throws ClifException {
		List<BladeDescriptor> blades = new ArrayList<BladeDescriptor>();
		Iterator<Map.Entry<String, ClifDeployDefinition>> it = null;
		File file = null;
		BladeDescriptor desc;
		try {
			FileInputStream in = new FileInputStream(getCtpFile(testName));
			it = TestPlanReader.readFromProp(in).entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, ClifDeployDefinition> entry = it.next();
				file = new File(getTestDir(testName), entry.getKey());
				desc = new BladeDescriptorImpl(file, entry.getKey(),
						entry.getValue());
				if (filter == null || filter.accept(desc)) {
					blades.add(desc);
				}
			}
		} catch (Exception e) {
			throw new ClifException("Can not get test plan for " + testName, e);
		}
		return blades.toArray(new BladeDescriptor[blades
				.size()]);
	}

}
