/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name:  $
 *
 * Contact: clif@ow2.org
 */
package org.ow2.clif.analyze.statistics;

import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeFilter;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Instances of this class filter blade descriptors according to what you want..
 * this implementation aims to filter blades by event type (shorcut of class
 * name) or by blade identifier.
 * 
 * @author Guy Vachet
 */
public class BladeFilterImpl implements BladeFilter, Constants {

	private static final long serialVersionUID = 2772915210665749995L;
	// verbose mode for verification
	private static final boolean VERBOSE = false;
	// filters
	private Set<String> retainedEventTypes;
	private Set<String> retainedBladeIds;

	/**
	 * Creates a new filter for blades with any filtering, which means all
	 * blades will be retained by this new filter.
	 */
	public BladeFilterImpl() {
		retainedEventTypes = new TreeSet<String>();
		retainedBladeIds = new TreeSet<String>();
	}

	/**
	 * add a new blade filter for blade descriptors with the given event type
	 * 
	 * @param eventTypeLabel
	 *            only blades with the given event will be retained by this
	 *            filter. If null, all blades will be retained.
	 */
	public void addEventFilter(String eventTypeLabel) {
		String className = BladeStoreReader.getEventClassName(eventTypeLabel);
		if (className != null) {
			retainedEventTypes.add(eventTypeLabel);
			if (VERBOSE) {
				StringBuffer sb = new StringBuffer("Filter ");
				System.out.println(sb.append(eventTypeLabel).append(" event."));
			}
		}
	}

	/**
	 * add a new blade filter for blade descriptors with the given id.
	 * 
	 * @param bladeId
	 *            only blades with the given id will be retained by this filter.
	 *            If null, all blades will be retained.
	 */
	public void addIdFilter(String bladeId) {
		if (bladeId != null) {
			retainedBladeIds.add(bladeId);
			if (VERBOSE) {
				System.out.println(new StringBuffer("Filter id ")
						.append(bladeId));
			}
		}
	}

	private boolean matchEvent(BladeDescriptor desc) {
		boolean match = false;
		String[] eventTypeLabels = desc.getEventTypeLabels();
		for (int i = 0; i < eventTypeLabels.length; i++) {
			match = retainedEventTypes.contains(eventTypeLabels[i]);
			if (match)
				break;
		}
		return match;
	}

	/**
	 * Filters blade descriptors according to the blade id and event type.
	 * 
	 * @param desc
	 *            the blade descriptor or accept or reject
	 * @return true if the event name of the given blade descriptor equals to
	 *         this filter's or if the blade id of the given blade descriptor
	 *         matchs the identifier list, false otherwise.
	 */
	@Override
	public boolean accept(BladeDescriptor desc) {
		if (retainedEventTypes.isEmpty() && retainedBladeIds.isEmpty())
			return true;
		else if (retainedEventTypes.isEmpty())
			return retainedBladeIds.contains(desc.getId());
		else if (retainedBladeIds.isEmpty())
			return matchEvent(desc);
		else
			return matchEvent(desc) && retainedBladeIds.contains(desc.getId());
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(AnalyzerImpl.LABEL)
				.append(" blade filter:");
		// Filtering based on blade event type
		if (retainedEventTypes.isEmpty()) {
			sb.append("\n\tno filtering based on blade type.\n");
		} else {
			sb.append("\n\tretained blade classes are:\n");
			for (Iterator<String> iter = retainedEventTypes.iterator(); iter
					.hasNext();)
				sb.append("\t\t").append(iter.next()).append("\n");
		}
		// Filtering based on blade identifier
		if (retainedBladeIds.isEmpty()) {
			sb.append("\tno filtering based on blade identifier.\n");
		} else {
			sb.append("\tretained blade identifiers are:\n\t");
			for (Iterator<String> iter = retainedBladeIds.iterator(); iter
					.hasNext();)
				sb.append("\t").append(iter.next());
			sb.append("\n");
		}
		return sb.toString();
	}

}
