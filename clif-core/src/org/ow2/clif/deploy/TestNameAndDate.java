/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.deploy;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Elaborated implementation for test ids, holding
 * its instantiation date. Useful to propagate the
 * test initialization date, when NTP (or the like)
 * global time is used.
 * 
 * @author Bruno Dillenseger
 */
public class TestNameAndDate implements Serializable
{
	static private final long serialVersionUID = 3343965309510859028L;
	static private GregorianCalendar calendar = new GregorianCalendar();
	static private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH'h'mm'm'ss");

	static
	{
		format.setCalendar(calendar);
	}

	private long date;
	private String name;
	private String toStringValue;

	/**
	 * Creates a new test identifier with the provided name,
	 * and remembers current time.
	 * @param testName the test run name
	 */
	public TestNameAndDate(String testName)
	{
		name = testName;
		date = System.currentTimeMillis();
		toStringValue = name + "_" + format.format(new Date(date));
	}

	public String getName()
	{
		return name;
	}

	/**
	 * @return this object creation date in ms,
	 * which is supposed to be interpreted as
	 * the test initialization date.
	 * @see System#currentTimeMillis()
	 */
	public long getDate()
	{
		return date;
	}

	/**
	 * @return a string starting by the test run id, followed 
	 * by a readable and easily sortable representation of its
	 * initialization date.
	 * Example: run1_2010-03-18_18h20m55
	 */
	public String toString()
	{
		return toStringValue; 
	}
}
