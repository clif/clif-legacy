/*
* CLIF is a Load Injection Framework
* Copyright (C) 2003 France Telecom R&D
* Copyright (C) 2003 INRIA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.api;


import java.io.Serializable;

import org.ow2.clif.supervisor.api.ClifException;


/**
 * Generic control of an activity
 *
 * @author Bruno Dillenseger
*/
public abstract interface ActivityControl
{
	/**
	 * Initialize the activity
	 * @param testId an arbitrary initialization name
	 * @throws ClifException 
	 */
	public void init(Serializable testId) throws ClifException;

	/**
	 * Initial start of the activity
	 */
	public void start();

	/**
	 * Final stop of the activity
	 */
	public void stop();

	/**
	 * Suspend the activity
	 */
	public void suspend();

	/**
	 * Resume the activity (if suspended)
	 */
	public void resume();

	/**
	 * Waits until the end of the activity
	 */
	public void join();
}
