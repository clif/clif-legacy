/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011 France Telecom R&D
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.server.lib;

import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.server.api.Synchronizer;

/**
 * Just a plain implementation of Synchronizer interface.
 * Refer to the Synchronizer interface documentation.
 * @see Synchronizer
 *
 * @author Bruno Dillenseger
 */
public class SynchronizerImpl implements Synchronizer
{
	// lock objects: lock name as key, notify counter as value (used as lock)
	private Map<String,NotifyCount> locks = new HashMap<String,NotifyCount>();
	private Map<String,Long> rendezVous = new HashMap<String,Long>();


	////////////////////////////
	// Synchronizer interface //
	////////////////////////////


	public void setRendezVous(String lockName, long count)
	{
		synchronized (rendezVous)
		{
			rendezVous.put(lockName, count);
			rendezVous.notifyAll();
		}
	}


	public boolean getRendezVous(String lockName, long timeoutMs)
		throws InterruptedException, IllegalArgumentException
	{
		if (timeoutMs < 0)
		{
			throw new IllegalArgumentException("A negative time out is not permitted: " + timeoutMs);
		}
		boolean noTimeOut = (timeoutMs == 0);
		Long count;
		synchronized (rendezVous)
		{
			long time1 = System.currentTimeMillis();
			long time2;
			while ((count = rendezVous.get(lockName)) == null && (noTimeOut || timeoutMs > 0))
			{
				rendezVous.wait(timeoutMs);
				if (! noTimeOut)
				{
					time2 = System.currentTimeMillis();
					timeoutMs -= (time2 - time1);
					time1 = time2;
				}
			}
		}
		if (noTimeOut || timeoutMs > 0)
		{
			return wait(lockName, timeoutMs, count);
		}
		else
		{
			return false;
		}
	}


	public void notify(String lockName)
	{
		NotifyCount notifyLock;
		synchronized (locks)
		{
			notifyLock = locks.get(lockName);
			if (notifyLock == null)
			{
				notifyLock = new NotifyCount();
				locks.put(lockName, notifyLock);
				newLock(lockName);
				locks.notifyAll();
			}
			else
			{
				notifyLock.inc();
			}
		}
		synchronized (notifyLock)
		{
			notifyLock.notifyAll();
		}
	}


	/**
	 * Does nothing.
	 * Override this method as a call-back to be informed when a lock
	 * is notified for the first time.
	 * @param lockName the name of the new lock
	 */
	protected void newLock(String lockName)
	{
	}


	public void wait(String lockName)
		throws InterruptedException
	{
		wait(lockName, 0, 1);
	}


	public boolean wait(String lockName, long timeoutMs)
		throws InterruptedException, IllegalArgumentException
	{
		return wait(lockName, timeoutMs, 1);
	}


	public boolean wait(String lockName, long timeoutMs, long number)
		throws InterruptedException, IllegalArgumentException
	{
		if (timeoutMs < 0)
		{
			throw new IllegalArgumentException("A negative time out is not permitted: " + timeoutMs);
		}
		boolean noTimeOut = (timeoutMs == 0);
		boolean completed = false;
		NotifyCount notifyLock = null;
		synchronized (locks)
		{
			notifyLock = locks.get(lockName);
			long time1 = System.currentTimeMillis();
			long time2;
			while (notifyLock == null && (noTimeOut || timeoutMs > 0))
			{
				locks.wait(timeoutMs);
				notifyLock = locks.get(lockName);
				if (! noTimeOut)
				{
					time2 = System.currentTimeMillis();
					timeoutMs -= (time2 - time1);
					time1 = time2;
				}
			}
		}
		if (notifyLock != null)
		{
			synchronized (notifyLock)
			{
				long time1 = System.currentTimeMillis();
				long time2;
				while (notifyLock.get() < number && (noTimeOut || timeoutMs > 0))
				{
					notifyLock.wait(timeoutMs);
					if (! noTimeOut)
					{
						time2 = System.currentTimeMillis();
						timeoutMs -= (time2 - time1);
						time1 = time2;
					}
				}
				completed = notifyLock.get() >= number;
			}
		}
		return completed;
	}


	public boolean wasNotified(String lockName)
	{
		return wasNotified(lockName, 1);
	}


	public boolean wasNotified(String lockName, long number)
	{
		return getCount(lockName) >= number;
	}


	public long getCount(String lockName)
	{
		long count = 0;
		synchronized (locks)
		{
			NotifyCount notifyLock = locks.get(lockName);
			if (notifyLock != null)
			{
				count = notifyLock.get();
			}
		}
		return count;
	}


	public void reset()
	{
		synchronized (locks)
		{
			locks = new HashMap<String,NotifyCount>();
			rendezVous = new HashMap<String,Long>();
		}
	}


	public long clear(String lockName)
	{
		long count = 0;
		synchronized (locks)
		{
			NotifyCount notifyLock = locks.remove(lockName);
			if (notifyLock != null)
			{
				count = notifyLock.get();
			}
		}
		return count;
	}


	public String toString()
	{
		return locks.toString();
	}


	/**
	 * Inner class for actual lock objects. 
	 * These objects also hold a notification (aka lock release) counter.
	 */
	private class NotifyCount
	{
		private long count;

		NotifyCount()
		{
			count = 1;
		}

		void inc()
		{
			++count;
		}

		long get()
		{
			return count;
		}

		public String toString()
		{
			return String.valueOf(count);
		}
	}
}
