/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2005, 2006 France Telecom
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * CLIF $Name$
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.util.ExecutionContext;

/**
 * Batch command to deploy and to run a test plan as defined in a given test plan file.
 * The deployed test plan is given a name (registered in the Registry) for
 * further reference with other batch commands. The CLIFRegistry must be running
 * before using this command.
 * 
 * @author Bruno Dillenseger
 */
public class LaunchCmd
{
	/**
	 * @see #run(String, String, String)
	 * @param args args[0] is the name for the deployed test plan,
	 * args[1] is the file name of the test plan definition to be deployed
	 */
	static public void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		if (args.length != 3)
		{
			BatchUtil.usage("expected arguments: <deployed test plan name> <test plan definition file> <test run identifier>");
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], args[1], args[2])); 
	}


	/**
	 * Deploys a test plan (i.e. a set of blades among CLIF servers) according to the given test plan definition.
	 * @param deployName name for the new deployed test plan
	 * @param fileName file name of the test plan definition to be deployed
	 * @return command status code (@see BatchUtil)
	 */
	static public int run(String deployName, String fileName, String testRunId)
	{
		int result = DeployCmd.run(deployName, fileName);
		if (result == BatchUtil.SUCCESS)
		{
			result = RunCmd.run(deployName, testRunId, null);
		}
		return result;
	}
}
