/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2006 France Telecom
* Copyright (C) 2018-2019 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.deploy.ClifAppFacade;
import org.ow2.clif.supervisor.api.BladeState;
import org.ow2.clif.util.ExecutionContext;
import org.ow2.clif.util.StringHelper;

/**
 * Batch command to start blades of an initialized test plan.
 * 
 * @author Joan Chaumont
 * @author Bruno Dillenseger
 * @author Stephanie Monticelli
 */
public class StartCmd
{
	/**
	 * @see #run(String, String[])
	 * @param args args[0] is the name of the target initialized test plan,
	 * optional args[1] is a list of identifiers of blades to be started,
	 * separated with : character bladeId1:bladeId2:...bladeId3
	 */
	static public void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		if(args.length < 1)
		{
		    BatchUtil.usage("arguments expected: <test plan name> [<bladeId1:bladeId2:...bladeIdn>]");
		}
		ExecutionContext.init("./");
		System.exit(run(args[0], BatchUtil.getSelBlades(args, 1)));
	}


	/**
	 * Starts all blades of an initialized test plan, or just a subset of them if specified.
	 * @param testPlanName the name of the target initialized test plan
	 * @param bladeIds array of blade identifiers that must be started in the given test plan. If null,
	 * all blades are started.
	 * @return command status code (@see BatchUtil)
	 */
	static public int run(String testPlanName, String[] bladeIds)
	{
		try
		{
		    ClifAppFacade clifApp = BatchUtil.getClifAppFacade(testPlanName);
		    if (clifApp == null)
		    {
		        System.err.println("Error: no such deployed test plan");
		        return BatchUtil.ERR_DEPLOY;
		    }
		    boolean monitored = StringHelper.isEnabled(System.getProperty("clif.monitoring", "false"));
			if (monitored) {
				System.out.println("Starting monitoring ...");
				long period = Long.parseLong(System.getProperty("clif.monitoring.period", "1000"));
				clifApp.startMonitoring(bladeIds, period);
			}
		    
		    System.out.println("Starting " + testPlanName + " test plan...");
			int res = clifApp.start(bladeIds);
            if (res == BatchUtil.SUCCESS)
            {
                res = clifApp.waitForState(bladeIds, BladeState.RUNNING);
                if (res == BatchUtil.SUCCESS)
                {
                    System.out.println("Started");
                    return BatchUtil.SUCCESS;
                }
            }
            System.err.println("Error: blades are not in the initialized state");
            return BatchUtil.ERR_LIFECYCLE;
		}
		catch (Exception ex)
		{
		    System.err.println("Error: execution problem while starting blades");
		    ex.printStackTrace(System.err);
		    return BatchUtil.ERR_EXEC;
		}
	}
}
