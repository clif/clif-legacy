/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2014 Orange
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib.batch;

import org.ow2.clif.probe.util.AbstractDumbInsert;
import org.ow2.clif.util.ExecutionContext;


/**
 * Batch command to get help for a given probe. 
 * 
 * @author Bruno Dillenseger
 */
abstract public class ProbeHelpCmd
{
	/**
	 * @param args args[0] is the type of probe
	 */
	static public void main(String[] args)
	{
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new SecurityManager());
		}
		if (args.length != 1)
		{
			BatchUtil.usage("expected arguments: <probe type>");
		}
		ExecutionContext.init("./");
		int res = run(args[0]);
		if (res != BatchUtil.SUCCESS)
		{
			System.exit(res);
		}
	}


	/**
	 * Prints the help message about the probe's arguments.
	 * @param probeType the probe type (e.g. cpu, memory, disk, network...),
	 * actually a sub-package of package org.ow2.clif.probe. defining an Insert
	 * class derived from the AbstractDumbInsert class.
	 * @return command status code (@see BatchUtil)
	 */
	static public int run(String probeType)
	{
		try
		{
			AbstractDumbInsert insert = (AbstractDumbInsert) Class.forName("org.ow2.clif.probe." + probeType + ".Insert").newInstance();
			System.out.println(insert.getHelpMessage());
			return BatchUtil.SUCCESS;
		}
		catch (Exception ex)
		{
			System.out.println("This type of probe is not supported.");
			ex.printStackTrace(System.err);
			return BatchUtil.ERR_EXEC;
		}
	}
}
