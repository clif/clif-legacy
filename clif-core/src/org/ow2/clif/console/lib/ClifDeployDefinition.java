/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004,2011 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: clif@ow2.org
 */

package org.ow2.clif.console.lib;

import org.ow2.clif.deploy.DeployDefinition;
import java.io.Serializable;
import java.util.Map;


/**
 *
 * @author Bruno Dillenseger
 */
public class ClifDeployDefinition extends DeployDefinition implements Serializable
{
	private static final long serialVersionUID = -5693689774386863992L;
	private boolean isProbe;
    
    /**
     * Construct a new clif definition.
     * @param server
     * @param adlDefinition
     * @param context
     * @param argument
     * @param comment
     * @param isProbe
     */
    public ClifDeployDefinition(
            String server,
            String adlDefinition,
            Map<String,String> context,
            String argument,
            String comment,
            boolean isProbe) {
        super(server, adlDefinition, context, argument, comment);
        this.isProbe = isProbe;
    }
    
    /**
     * Test if this blade is a probe
     * @return boolean true if the blade is a probe
     */
    public boolean isProbe() {
        return isProbe;
    }
    
    /**
     * Return the probe name (cpu, memory, jvm) if blade is probe or else return injector.
     * @return the class name of this blade definition
     */
    public String getClassName () {
        String bClass = "injector";
        if(isProbe()) {
            String probeName = getContext().get("insert");
            bClass = probeName.replaceFirst("org.ow2.clif.probe.","");
            bClass = bClass.substring(0,bClass.lastIndexOf(".Insert"));
        }
        
        return bClass;
    }
}
