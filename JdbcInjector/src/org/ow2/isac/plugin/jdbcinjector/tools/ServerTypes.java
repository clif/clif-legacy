/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * Contact: clif@ow2.org
 *
 * @author P Crepieux 18/04/2007
 */

package org.ow2.isac.plugin.jdbcinjector.tools;

import java.util.HashMap;
import java.util.ArrayList;

public final class ServerTypes {
	public static HashMap<String, Integer> toServerMap=new HashMap<String, Integer>();
	public static ArrayList toUrlMap=new ArrayList();
	public static ArrayList toDriverMap=new ArrayList();
	
	public final static int POSTGRESQL	=	0;
	public final static int MYSQL		=	1;
    public final static int SOLID       =   2;
    public final static int TIMESTEN    =   3;
    
	
	static {
		
		toServerMap.put("PostgreSQL"    ,new Integer(POSTGRESQL));
		toServerMap.put("MySQL"    	,new Integer(MYSQL));
        toServerMap.put("Solid"         ,new Integer(SOLID));
        toServerMap.put("TimesTen"         ,new Integer(TIMESTEN));
		
		
		toUrlMap.add(POSTGRESQL,"jdbc:postgresql:");
		toUrlMap.add(MYSQL,"jdbc:mysql:");
        toUrlMap.add(SOLID,"jdbc:solid:");
        toUrlMap.add(TIMESTEN,"jdbc:timesten:");
		
		toDriverMap.add(POSTGRESQL,"org.postgresql.Driver");
		toDriverMap.add(MYSQL,"com.mysql.jdbc.Driver");
        toDriverMap.add(SOLID,"solid.jdbc.SolidDriver");
        toDriverMap.add(TIMESTEN,"com.timesten.jdbc.TimesTenClientDriver");
		
	}
}
