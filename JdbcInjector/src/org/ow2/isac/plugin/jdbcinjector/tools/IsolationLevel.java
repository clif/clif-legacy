/*
 * CLIF is a Load Injection Framework
 * Copyright (C) 2004 France Telecom R&D
 *
 * Contact: clif@ow2.org
 *
 * @author P Crepieux 18/04/2007
 */
package org.ow2.isac.plugin.jdbcinjector.tools;
import java.util.HashMap;

public final class IsolationLevel {
	public static HashMap toJdbcMap=new HashMap();;
	static {
		toJdbcMap.put("none"    ,new Integer(java.sql.Connection.TRANSACTION_NONE));
		toJdbcMap.put("read_committed" ,new Integer(java.sql.Connection.TRANSACTION_READ_COMMITTED));
		toJdbcMap.put("read_uncommitted" ,new Integer(java.sql.Connection.TRANSACTION_READ_UNCOMMITTED));
		toJdbcMap.put("repeatable_read",new Integer(java.sql.Connection.TRANSACTION_REPEATABLE_READ));
		toJdbcMap.put("serializable" ,new Integer(java.sql.Connection.TRANSACTION_SERIALIZABLE));
	}
}
