package org.ow2.isac.plugin.svninjector;

import java.util.HashMap;
import java.util.Map;
import org.ow2.clif.storage.api.ActionEvent;
import org.ow2.clif.util.Random;

/**
 * Stand-alone test class for the SvnInjector plug-in.
 * Performs a commit with 3 directories and 2 files, and then a check-out of 1 of these directories
 * Arguments: SVNrepoURL login password
 * 
 * @author Bruno Dillenseger
 */
abstract public class CommitTest
{
	static public void main(String[] args)
	{
		Map<String,String> params = new HashMap<String,String>();

		// login
		SessionObject so = new SessionObject(params);

		// connect
		params.clear();
		params.put(SessionObject.CONTROL_CONNECT_URL, args[0]);
		params.put(SessionObject.CONTROL_CONNECT_LOGIN, args[1]);
		params.put(SessionObject.CONTROL_CONNECT_PASSWORD, args[2]);
		so.doControl(SessionObject.CONTROL_CONNECT, params);
		
		// add a file to the root
		params.clear();
		params.put(SessionObject.CONTROL_ADDFILE_NAME, String.valueOf(new Random().nextInt(100000000)) + ".txt");
		params.put(SessionObject.CONTROL_ADDFILE_SIZE, "0");
		so.doControl(SessionObject.CONTROL_ADDFILE, params);

		// add a directory
		params.clear();
		String dir1 = String.valueOf(new Random().nextInt(100000000)); 
		params.put(SessionObject.CONTROL_ADDDIR_PATH, dir1);
		so.doControl(SessionObject.CONTROL_ADDDIR, params);

		// add a file to the directory
		params.clear();
		params.put(SessionObject.CONTROL_ADDFILE_NAME, String.valueOf(new Random().nextInt(100000000)) + ".bin");
		params.put(SessionObject.CONTROL_ADDFILE_SIZE, "2");
		so.doControl(SessionObject.CONTROL_ADDFILE, params);

		// add a directory in the previous directory
		params.clear();
		String dir2 = String.valueOf(new Random().nextInt(100000000)); 
		params.put(SessionObject.CONTROL_ADDDIR_PATH, dir1 + "/" + dir2);
		so.doControl(SessionObject.CONTROL_ADDDIR, params);

		// add a directory at the root
		params.clear();
		String dir3 = String.valueOf(new Random().nextInt(100000000)); 
		params.put(SessionObject.CONTROL_ADDDIR_PATH, dir3);
		so.doControl(SessionObject.CONTROL_ADDDIR, params);

		// commit
		params.clear();
		params.put(SessionObject.SAMPLE_COMMIT_MESSAGE, "commit test");
		System.out.println(so.doSample(SessionObject.SAMPLE_COMMIT, params, new ActionEvent()).toString());

		// check-out
		params.clear();
		params.put(SessionObject.SAMPLE_CHECKOUT_PATH, dir1);
		System.out.println(so.doSample(SessionObject.SAMPLE_CHECKOUT, params, new ActionEvent()).toString());
	}
}
