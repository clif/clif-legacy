/*
* CLIF is a Load Injection Framework
* Copyright (C) 2007 France Telecom
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF Jordan BRUNIER Grégory CALONNIER
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.analyze.lib.graph.util;

import java.io.File;

import javax.swing.filechooser.FileFilter;


/**
 * The Class SaveFileFilter.
 */
public class LoadFileFilter extends FileFilter {
	
	private String description = "";
	private String[] extensions={"."};
	
	/**
	 * Instantiates a new save file filter.
	 */
	public LoadFileFilter() {
		
	}
	
	//------------------------ Public Methods ----------------------//
	/**
	 * Sets the extensions.
	 * Format : {".txt", ".jpp", ...}, including "." 
	 * @param _extensions the new extensions
	 */
	public void setExtensions(String[] _extensions) {
		this.extensions = _extensions;
	}
	
	/**
	 * Sets the description.
	 * 
	 * @param _description the new description
	 */
	public void setDescription(String _description) {
		this.description = _description;
	}
	
	//------------------------ FileFilter Methods ----------------------//
	/* (non-Javadoc)
	 * @see java.io.FileFilter#accept(java.io.File)
	 */
	@Override
	public boolean accept(File pathname) {
		if (pathname.isDirectory()) {
			return true;
		}

		String fileName = pathname.getPath().toLowerCase();
		int n = extensions.length;
		for (int i = 0; i < n; i++) {
			if (fileName.endsWith(extensions[i])) {
				return true;
			}
		}
		return false; 
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.filechooser.FileFilter#getDescription()
	 */
	@Override
	public String getDescription() {
		return(this.description);
	}
}
