/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005, 2011, 2013 France Telecom R&D
* Copyright (C) 2016 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/
package org.ow2.clif.analyze.lib.graph;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.beans.PropertyVetoException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.WindowConstants;
import org.objectweb.fractal.api.control.BindingController;
import org.ow2.clif.analyze.api.AnalyzerLink;
import org.ow2.clif.analyze.lib.graph.gui.DataAccess;
import org.ow2.clif.analyze.lib.graph.gui.ReportManager;
import org.ow2.clif.storage.api.StorageRead;
import org.ow2.clif.storage.lib.filestorage.FileStorageReader;
import org.ow2.clif.util.ExecutionContext;

/**
 * Implementation of the Analyzer Fractal component.
 * 
 * @author Gregory Calonnier
 * @author Jordan Brunier
 * @author Tomas Perez-Segovia
 * @author Bruno Dillenseger
 */
public class AnalyzerImpl implements BindingController, AnalyzerLink
{
	/** Fractal ADL definition file of the CLIF Analyzer. */
	public static final String CLIF_ANALYZER = "org.ow2.clif.analyze.lib.graph.ClifAnalyzer";
	
	/** Frame title */
	static private final String LABEL = "Quick Graphical Analyzer";

	/** Frame icons names */
	static private final String CLIF_ICON_16PX = "icons/logo_clif_16px.gif";
	static private final String CLIF_ICON_32PX = "icons/logo_clif_32px.gif";
			
	private static AnalyzerImpl singleton;
	private static DataAccess data;
	private static ReportManager reportManager;

	// client interfaces names
	static private String[] itfList = new String[] {
		StorageRead.STORAGE_READ
	};

	private StorageRead srItf = null;

	private static JDesktopPane desktop;
	
	public static JInternalFrame quickAnalyzer;
	
	public static JFrame frame;
	
	/**
     * The main method.
     * 
     * @param args the arguments
     */
	static public void main(String[] args)
	{
		try
		{
	        if (System.getSecurityManager() == null)
	        {
	            System.setSecurityManager(new SecurityManager());
	        }
	        ExecutionContext.init("./");
            init(true);
            frame.setVisible(true);
		}
		catch (Throwable t)
		{
			t.printStackTrace(System.err);
			System.exit(-1);
		}
	}

    public static void init(boolean standalone)
    {
        // Create Frame
        frame = new JFrame(LABEL);
        ClassLoader cl = AnalyzerImpl.class.getClassLoader();
        List<Image> clifLogos = new LinkedList<Image>();
        clifLogos.add(new ImageIcon(cl.getResource(CLIF_ICON_16PX)).getImage());
        clifLogos.add(new ImageIcon(cl.getResource(CLIF_ICON_32PX)).getImage());
        frame.setIconImages(clifLogos);
        if (standalone)
        {
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        else
        {
        	frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        }
        Dimension screenDim = frame.getToolkit().getScreenSize();
        frame.setSize((screenDim.width*3)/4, (screenDim.height*3)/4);

        desktop = new JDesktopPane();
        frame.getContentPane().add(desktop);
        frame.addWindowStateListener(new WindowStateListener(){ 
            @Override
			public void windowStateChanged(WindowEvent e) {
                windowFrameStateChanged(e);
            }
        });
    	quickAnalyzer = new JInternalFrame("Analyzer", true, false, true, true);
    	quickAnalyzer.setSize((screenDim.width*9)/16, (screenDim.height*9)/16);
    	quickAnalyzer.setVisible(true);

    	desktop.add(quickAnalyzer);

    	try
    	{
    		quickAnalyzer.setMaximum(true);
    	}
    	catch (PropertyVetoException e)
    	{
    		e.printStackTrace(System.err);
    	}
    	singleton = new AnalyzerImpl();
    	singleton.setUIContext(quickAnalyzer);
    }

    protected static void windowFrameStateChanged(WindowEvent e) {
		Dimension frameDim = new Dimension();
		frameDim = frame.getSize();
		quickAnalyzer.setSize(frameDim);
	}


	/**
     * Does nothing.
     */
	public AnalyzerImpl()
	{
	}


	public DataAccess getDataAccess() {
		return data;
	}


	/////////////////////////////////
	// interface BindingController //
	/////////////////////////////////


	/* (non-Javadoc)
	 * @see org.objectweb.fractal.api.control.BindingController#lookupFc(java.lang.String)
	 */
	@Override
	public Object lookupFc(String clientItfName)
	{
		if (clientItfName.equals(StorageRead.STORAGE_READ))
		{
			return srItf;
		}
		else
		{
			return null;
		}
	}


	/* (non-Javadoc)
	 * @see org.objectweb.fractal.api.control.BindingController#bindFc(java.lang.String, java.lang.Object)
	 */
	@Override
	public synchronized void bindFc(String clientItfName, Object serverItf)
	{
		if (clientItfName.equals(StorageRead.STORAGE_READ)){
			srItf = (StorageRead) serverItf;
		}
	}


	/* (non-Javadoc)
	 * @see org.objectweb.fractal.api.control.BindingController#unbindFc(java.lang.String)
	 */
	@Override
	public synchronized void unbindFc(String clientItfName)
	{
        if (clientItfName.equals(StorageRead.STORAGE_READ))
        {
            srItf = null;
        }
	}


	/* (non-Javadoc)
	 * @see org.objectweb.fractal.api.control.BindingController#listFc()
	 */
	@Override
	public String[] listFc()
	{
		return itfList;
	}


	////////////////////////////
	// interface AnalyzerLink //
	////////////////////////////


	/* (non-Javadoc)
	 * @see org.ow2.clif.analyze.api.AnalyzerLink#getLabel()
	 */
	@Override
	public String getLabel()
	{
		return LABEL;
	}


	/* (non-Javadoc)
	 * @see org.ow2.clif.analyze.api.AnalyzerLink#setUIContext(java.lang.Object)
	 */
	@Override
	public void setUIContext(Object internalFrame)
	{
		try
		{
			data = new DataAccess(
	    		(JInternalFrame)internalFrame,
	    		new FileStorageReader());
			if (reportManager != null)
			{				
				((JInternalFrame)internalFrame).getContentPane().removeAll();
			}
			((JInternalFrame)internalFrame).setTitle(getLabel());
			reportManager = new ReportManager((JInternalFrame)internalFrame, this);	
		}
		catch (Exception ex)
		{
			throw new Error(
				"Can't find a compatible Analyzer user interface for " 
				+ internalFrame, ex);
		}
	}
}
