/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011, 2012 France Telecom R&D
* Copyright (C) 2016-2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import org.ow2.clif.analyze.lib.graph.AnalyzerImpl;
import org.ow2.clif.analyze.lib.report.Dataset;
import org.ow2.clif.analyze.lib.report.Dataset.DatasetType;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.analyze.lib.report.Section;

/**
 * @author Tomas Perez-Segovia
 */
public class ReportManager {
	/*
	 * Phase scheduling (Controllers and views) - Selection - Filtering -
	 * Presentation
	 * 
	 * 
	 * 
	 * Each PhaseController is related to its view as well as to the DataAccess
	 */

	// Dataset Type
	public enum Phases {
		INIT, REPORT_MANAGER, WIZARD_1_SELECTION, WIZARD_2_FILTERING, EXIT;
	}

	DataAccess data;
	ReportManagerView view;

	Wizard1MainView w1View;
	Wizard2MainView w2View;

	JInternalFrame intFrame;

	AnalyzerImpl analyzer;
	Phases previousState = Phases.INIT;
	Phases currentState = Phases.INIT;

	DatasetType newDsType = DatasetType.SIMPLE_DATASET;



	// Constructors //

	public ReportManager(JInternalFrame _frame, AnalyzerImpl analyzerImpl) {
		intFrame = _frame;
		analyzer = analyzerImpl;
		data = analyzer.getDataAccess();
		view = new ReportManagerView(this, data);
		jfSelection = new JFrame("Selection");
		w1View = new Wizard1MainView(this, jfSelection);
		jfFiltering = new JFrame("Filtering");
		w2View = new Wizard2MainView(this, jfFiltering);
		
		currentState = Phases.INIT;
		nextState(Phases.REPORT_MANAGER);
	}

	
	// ------------------------ Graphic Methods ----------------------//
	
	public JInternalFrame getInternalFrame() {
		return intFrame;
	}
	
	public DataAccess getDataAccess() {
		return data;
	}

	
	// Scheduling methods
	JFrame jfFiltering ;
	JFrame jfSelection;
	

	public void nextState(Phases _next) {
		LogAndDebug.tracep("(" + _next +")");
		LogAndDebug.log(" +   "+ Thread.currentThread().getStackTrace()[1].getMethodName() + ": " +  currentState + " -> " + _next);
		previousState = currentState;

		switch(previousState){
		case INIT:
		case REPORT_MANAGER:
//			data.w1DatasetInit(newDsType);
			break;
		case WIZARD_1_SELECTION:
//			data.wizardDataset = new Dataset();
			jfSelection.setVisible(false);
			break;
		case WIZARD_2_FILTERING:
			jfFiltering.setVisible(false);
			break;
		case EXIT:
		default:
			;
		}
		currentState = _next;
		switch (_next) {
		case WIZARD_1_SELECTION:
			w1View.initializeFrom(previousState);
			jfSelection.setLocationRelativeTo(intFrame);
			jfSelection.setVisible(true);
			break;
		case WIZARD_2_FILTERING:
			w2View.initializeFrom(previousState);
			jfFiltering.setLocationRelativeTo(intFrame);
			jfFiltering.setVisible(true);
			break;
		case REPORT_MANAGER:
			data.verifyWizardDataset();
			initializeFrom(previousState);
			view.setVisible(true);
			break;
		case EXIT:
			System.exit(0);
			break;
		default:
			currentState = Phases.REPORT_MANAGER;
			nextState(Phases.REPORT_MANAGER);
		}
		LogAndDebug.tracem("("+currentState+")");
	}

	public void nextState(Phases _next, DatasetType dsType) {
		LogAndDebug.trace("("+ _next + ", " + dsType+")");
		data.wizardDataset = new Dataset(dsType);
		newDsType = dsType;
		nextState(_next);
	}

	private void nextState(Phases _next, DatasetType dsType, Section sec) {
		LogAndDebug.trace("("+ _next + ", " + dsType+", "+ LogAndDebug.toText(sec)+")");
		
		data.wizardDataset = new Dataset(dsType);
		data.wizardDataset.setSection(sec);
		newDsType = dsType;
		nextState(_next);
	}





	public void initializeFrom(Phases previousState) {
		LogAndDebug.tracep("(" + previousState + ")");
		switch(previousState){
		case INIT:
//			addInitialDataset(DatasetType.SIMPLE_DATASET, "1", "1", "JVM", "raw");
//			addInitialDataset(DatasetType.SIMPLE_DATASET, "1", "3", "CPU", "raw min max");
//			addInitialDataset(DatasetType.MULTIPLE_DATASET, "1 2 3 4", "3 2 1 2", "CPU", "raw");
//			addInitialDataset(DatasetType.AGGREGATE_DATASET, "1 2 3 4", "3 2 1 2", "CPU", "raw");
//			view.updateSelections();
			break;

		case REPORT_MANAGER:
			break;
		
		case WIZARD_1_SELECTION:
			break;
		
		case WIZARD_2_FILTERING:
			if (null != data.wizardDataset){
				copyCurrentDsToReport(data.wizardDataset);
			}
			view.updateSelections();
			break;
		case EXIT:
		default:
			;
		}
		LogAndDebug.tracep("(" + previousState + ")");
	}


	private void copyCurrentDsToReport (Dataset dataset) {
		Section section = dataset.getSection();
		if (null == section){
			section = data.report.addDatasetToReport(dataset);
			view.rmReportTree.addSectionToReport(section);
			view.updateReportDisplay();
		}else{
			section.addDatasetToSection(dataset);
			view.rmReportTree.addDatasetToSection(section, dataset);
//			data.computeSectionStatistics(sec);
//			sec.createChart();
//			sec.updateChart();
			view.selectSection(section, 1);
			view.updateSectionDisplay(section);
		}
		view.setSelectedDataset(dataset, 1);
	}

	public void addDatasetInNewSection() {
		nextState(Phases.WIZARD_1_SELECTION, DatasetType.SIMPLE_DATASET);
	}

	public void addSimpleDataset() {
		nextState(Phases.WIZARD_1_SELECTION, DatasetType.SIMPLE_DATASET);
	}

	public void addMultipleDataset() {
		nextState(Phases.WIZARD_1_SELECTION, DatasetType.MULTIPLE_DATASET);
	}

	public void addAggregateDataset() {
		nextState(Phases.WIZARD_1_SELECTION, DatasetType.AGGREGATE_DATASET);
	}


	public void addDataset(Section sec) {
		nextState(Phases.WIZARD_1_SELECTION, DatasetType.SIMPLE_DATASET, sec);
	}


	public void addSimpleDataset(Section sec) {
		nextState(Phases.WIZARD_1_SELECTION, DatasetType.SIMPLE_DATASET, sec);
	}


	public void addMultipleDataset(Section sec) {
		nextState(Phases.WIZARD_1_SELECTION, DatasetType.MULTIPLE_DATASET, sec);
	}

	public void addAggregateDataset(Section sec) {
		nextState(Phases.WIZARD_1_SELECTION, DatasetType.AGGREGATE_DATASET, sec);
	}



	public void removeDataset(Dataset dataset) {
		Section sec = data.getSection(dataset);
		if (sec.removeDataset(dataset)){
			view.updateSectionDisplay(sec);
		}
		view.setSelectedSection(sec, 1);
	}
	
	public void clearReport(){
		LogAndDebug.tracep("()");
		data.report.clearAll();
		view.updateReportDisplay();
		LogAndDebug.tracem("()");
	}

	public void removeSection(Section sec) {
		LogAndDebug.tracep("("+LogAndDebug.toText(sec)+")");
		Section other = view.getOtherSection(sec);
		if (data.report.removeSection(sec)){
			view.updateReportDisplay();
			if(null != other){
				view.selectSection(other, 1);
			}else{
				view.selectReport();
			}
		}else{
			LogAndDebug.trace("() could not remove section " + LogAndDebug.toText(sec));
		}
		LogAndDebug.tracem("("+LogAndDebug.toText(sec)+")");
	}


	public void moveDatasetUp(Dataset dataset) {
		LogAndDebug.unimplemented();
	}

	public void moveDatasetDown(Dataset dataset) {
		LogAndDebug.unimplemented();
	}

	

}
