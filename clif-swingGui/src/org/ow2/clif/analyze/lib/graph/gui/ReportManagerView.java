/*
* CLIF is a Load Injection Framework
* Copyright (C) 2011-2014 France Telecom R&D
* Copyright (C) 2016-2017 Orange SA
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.graph.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.ow2.clif.analyze.lib.graph.gui.ReportManager.Phases;
import org.ow2.clif.analyze.lib.report.Dataset;
import org.ow2.clif.analyze.lib.report.Datasource;
import org.ow2.clif.analyze.lib.report.LogAndDebug;
import org.ow2.clif.analyze.lib.report.Report;
import org.ow2.clif.analyze.lib.report.Section;
import org.ow2.clif.util.FileUtil;


/**
 * @author Tomas Perez Segovia
 * @author Bruno Dillenseger
 */
public class ReportManagerView extends JPanel{

	private static final long serialVersionUID = 4204057489812159199L;

	public enum SelectedObjectType { REPORT, SECTION, /* CHART,*/ DATASET, NO_SELECTION};


	JInternalFrame frame;
	ReportManager control;
	DataAccess data; 
	String name = "Report Manager View";
	JPanel panel = new JPanel();
	Phases next;
	private StringBuffer htmlText;

	private JPanel mainPanel = null;
	private JLabel statusLine = null;

	private Object selectedObject = null;


	RMReportTree rmReportTree = null;
	RMOptionsView rmOptions = null;
	RMChartsView rmCharts = null;
	

	public ReportManagerView(ReportManager _control, DataAccess _data) {
		data = _data;
		LogAndDebug.tracep("(_control, _data)");
		frame = data.getInternalFrame();
		control = _control;
		frame.add(mainPanelCreation()) ;
		LogAndDebug.tracem("(_control, _frame)");
	}


	public void setStatus(String info)
	{
		statusLine.setText(info);
	}


	private JPanel mainPanelCreation() {
		//  LogAndDebug.tracep();
		mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(intPanelCreation(), BorderLayout.CENTER);		// internal panel
		statusLine = new JLabel("Welcome to the CLIF graphical analyzer");
		mainPanel.add(statusLine, BorderLayout.PAGE_END);			// status line
		//  LogAndDebug.tracem();
		return mainPanel;
	}


	private JSplitPane intPanelCreation() {
		//  LogAndDebug.tracep();
		JSplitPane mainSplitPanel = new JSplitPane(
			JSplitPane.VERTICAL_SPLIT,
			true,
			treeAndOptionsPanelCreation(), 		// top panel (tree, options[report, section, dataset]
			rmChartsCreation());				// bottom panel chart
		mainSplitPanel.setResizeWeight(0.0);
		mainSplitPanel.setOneTouchExpandable(true);
		//  LogAndDebug.tracem();
		return mainSplitPanel;
	}


	private JSplitPane treeAndOptionsPanelCreation() {
		//  LogAndDebug.tracep();
		JSplitPane leftSplitPanelCreation =  
			new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, rmReportTreeCreation(), rmOptionsCreation());
		leftSplitPanelCreation.setResizeWeight(1);
		//  LogAndDebug.tracem();
		return leftSplitPanelCreation;
	}


	private Component rmReportTreeCreation() {
		rmReportTree = new RMReportTree(control, this, data, frame);
		return rmReportTree;
	}


	private Component rmOptionsCreation() {
		rmOptions  = new RMOptionsView(control, this, data, frame);
		return rmOptions;
	}


	private Component rmChartsCreation() {
		rmCharts = new RMChartsView(control, this, data, frame);
		return rmCharts;
	}


	public Object getSelectedObject() {
		if (null == selectedObject){
			selectedObject = data.report;
		}
		LogAndDebug.trace("() -> " + LogAndDebug.toText(selectedObject));
		return selectedObject;
	}


	public void unselectObject() {
		LogAndDebug.tracep();
		this.selectedObject = null;
		LogAndDebug.tracem();
	}


	public Section getSelectedSection() {
		Section ret = null;
		if (null == selectedObject){
			;
		}else{
			String className = selectedObject.getClass().getSimpleName();
			if (className.equals("Section")){
				ret = (Section) selectedObject;
			}
		}
		return ret;
	}


	public void setSelectedSection(Section sec, int depth) {
		LogAndDebug.tracep("("+LogAndDebug.toText(sec)+", "+depth+")");
		if(selectedObject == sec){
			LogAndDebug.trace("("+LogAndDebug.toText(sec)+", "+depth+") already selected");
		}else{
			setSelectedObject(sec, depth);
		}
		LogAndDebug.tracem("("+LogAndDebug.toText(sec)+", "+depth+")");
	}


	public Dataset getSelectedDataset()
	{
		Dataset ret = null;
		if (selectedObject != null && selectedObject instanceof Dataset)
		{
			ret = (Dataset) selectedObject;
		}
		return ret;
	}


	public void setSelectedDataset(Dataset dataset, int depth) {
		LogAndDebug.tracep("("+LogAndDebug.toText(dataset)+", "+depth+")");
		if(selectedObject == dataset){
			LogAndDebug.trace("("+LogAndDebug.toText(dataset)+", "+depth+") already selected");
		}else{
			setSelectedObject(dataset, depth);
		}
		LogAndDebug.tracem("("+LogAndDebug.toText(dataset)+", "+depth+")");
	}


	public void setSelectedDatasource(Datasource datasource, int depth) {
		LogAndDebug.tracep("("+LogAndDebug.toText(datasource)+", "+depth+")");
		if(selectedObject == datasource){
			LogAndDebug.trace("("+LogAndDebug.toText(datasource)+", "+depth+") already selected");
		}else{
			setSelectedObject(datasource, depth);
		}
		LogAndDebug.tracem("("+LogAndDebug.toText(datasource)+", "+depth+")");
	}


	public void setSelectedObject(Object obj, int depth) { 
		LogAndDebug.tracep("("+LogAndDebug.toText(obj)+", "+depth+")");
		if (null == obj){
			this.selectedObject = null;
		}else if (this.selectedObject == obj){
			LogAndDebug.trace("("+LogAndDebug.toText(obj)+", "+depth+") already selected");
		}else{
			this.selectedObject = obj;
			if(depth>0){
				--depth;
				rmReportTree.setselectedObject(obj, depth);
				rmOptions.setselectedObject(obj, depth);
				rmCharts.setSelectedObject(obj, depth);
			}
		}
		LogAndDebug.tracem("("+LogAndDebug.toText(obj)+", "+depth+")");
	}


	public Datasource getSelectedDatasource() {
		Datasource ret = null;
		if (null == selectedObject){
			;
		}else{
			String className = selectedObject.getClass().getSimpleName();
			if (className.equals("Datasource")){
				ret =  (Datasource) selectedObject;
			}
		}
		return ret;
	}


	// update methods 
	public void updateReportManagerView() {
		LogAndDebug.tracep();
		//		updateToolbar();
		updateData();
		updateSelections();
		LogAndDebug.tracem();
	}


	public void updateData(){
		LogAndDebug.tracep();
		rmOptions.updateOptions();
		rmCharts.updateChartsView();
		LogAndDebug.tracem();
	}


	public void updateSelections(){
		LogAndDebug.tracep();
		rmReportTree.updateReportTreeSelections();  // not needed. may be it is needed...
		updateOptionsSelections();
		updateChartsSelections();
		setStatus("");
		LogAndDebug.tracem();
	}


	private void updateOptionsSelections(){
		LogAndDebug.tracep();
		if (null != rmOptions){
			rmOptions.updateOptions();
		}
		LogAndDebug.tracem();
	}


	private void updateChartsSelections(){
		LogAndDebug.tracep();
		if (null != rmCharts){
			rmCharts.updateChartsView();
		}
		rmCharts.selectObject(selectedObject, 0);
		LogAndDebug.tracem();
	}


	//  selecting an object implies selecting and updating a treePath, an options tab and a table's row

	public void selectObject(Object obj) {
		String className = obj.getClass().getSimpleName();

		if (className.equals("Report")){		// it's the report itself
			selectReport(((Report)obj), 1);
		} else if (className.equals("Section")){	// it's a Section
			selectSection(((Section)obj), 1);
		}else if (className.equals("Dataset")){	// it's a Dataset
			selectDataset(((Dataset)obj));
		}else{
			LogAndDebug.trace("("+LogAndDebug.toText(obj)+") could not select "+className+ " object.");
		}
	}


	public void selectReport(Report report, int depth) {
		LogAndDebug.tracep("("+LogAndDebug.toText(report)+", "+depth+")");
		if (null != rmOptions){
			if (depth >0){
				rmReportTree.selectReport(report, --depth);
				rmOptions.selectReport(report, --depth);
				rmCharts.selectObject(report, --depth);
			}
			else
			{
				rmReportTree.selectReport(report, 0);
			}
		}
		LogAndDebug.tracem("("+LogAndDebug.toText(report)+", "+depth+")");
	}


	public void selectSection(Section sec, int depth) {
		LogAndDebug.tracep("("+LogAndDebug.toText(sec)+", "+depth+")");
		if (sec == selectedObject){
			LogAndDebug.trace("("+LogAndDebug.toText(sec)+", "+depth+") already selected");
		}else{
			if (depth >0){
				rmReportTree.selectSection(sec, depth - 1); 
				rmOptions.selectSection(   sec, depth - 1);
				rmCharts.selectObject(     sec, depth - 1);
			}
		}
		LogAndDebug.tracem("("+LogAndDebug.toText(sec)+", "+depth+")");
	}


	public void selectSection(String secName, int depth) {
		LogAndDebug.tracep("(<sectionName> "+secName+", "+depth+")");
		for (Section sec : data.report.getSections()){
			if (secName.equals(sec.getTitle())){
				selectSection(sec, depth);
				break;
			}
		}
		LogAndDebug.tracem("(<sectionName> "+secName+", "+depth+")");
	}


	public void selectDataset(Dataset dataset) {
		LogAndDebug.tracep("(" + LogAndDebug.toText(dataset)+")");
		rmReportTree.selectDataset(dataset, 0);		// TODO in some cases the selected object is a section ... (why?)
		rmOptions.selectDataset(dataset, 0);
		rmCharts.selectDataset(dataset, 0);
		LogAndDebug.tracem("(" + LogAndDebug.toText(dataset)+")");
	}


	// get all the datasets in section "_sec"
	public Dataset[] getDatasets(Section _sec) {
		List<Dataset> list = new ArrayList<Dataset>();

		if (null == _sec){
			return null;
		}
		for (Dataset ds : _sec.getDatasets()){
			list.add(ds);
		}
		// Convert list to array
		return list.toArray(new Dataset[list.size()]);
	}


	public Report getReport() {
		return data.report;
	}


	public Section getSection(String name) {
		Section ret = null;
		for(Section sec : data.report.getSections()){
			if(name.equals(sec.getTitle())){
				ret = sec;
				break;
			}
		}
		return ret;
	}


	protected void updateReportTree()
	{
		rmReportTree.clear();
		for(Section sec : data.report.getSections())
		{
			rmReportTree.addSectionToReport(sec);
		}
		rmReportTree.clearSelection();
		rmReportTree.requestFocus();
	}

	/**
	 * re-creates all displays from report model
	 */
	public void updateReportDisplay() {
		LogAndDebug.tracep("()");
		rmReportTree.clear();		// tree
		rmCharts.removeAll();		// charts
		for(Section sec : data.report.getSections()){
			rmReportTree.addSectionToReport(sec);		// tree
			rmOptions.updateOptions();					// options
			data.computeSectionStatistics(sec);			// statistics
			sec.updateChart();							// charts
		}
		LogAndDebug.tracem("()");
	}


	/**
	 * 	Updates all displays after a change in chartRepresentation (TIME_BASED, HISTOGRAM, QUANTILE, BOXPLOT)
	 * 	or after a modification on datasets (adding or removing), or on draw start and end times.
	 * 	Re-generates statistics and chart.
	 * 	
	 * @param sec
	 */
	public void updateSectionDisplay(Section sec) {
		LogAndDebug.tracep("("+LogAndDebug.toText(sec)+")");
		rmOptions.updateSectionFromGui(sec);
		updateReportDisplay();			// TODO for now we re-build all report. To be replaced with:
//		data.computeSectionStatistics(sec);
//		sec.createChart();
//		sec.updateChart();
		selectSection(sec, 1);
		LogAndDebug.tracem("("+LogAndDebug.toText(sec)+")");
	}


	/**
	 * re-computes filtering and re-generates chart for this dataset
	 * @param dataset to update filtering
	 */
	public void updateFiltering(Dataset dataset) {
		LogAndDebug.tracep("("+LogAndDebug.toText(dataset)+")");
		updateDatasetDisplay(dataset);
		LogAndDebug.tracem("("+LogAndDebug.toText(dataset)+")");
	}


	void updateDatasetDisplay(Dataset dataset) {
		selectDataset(dataset);
	}


	public void selectReport()
	{
		selectReport(data.report, 0);
	}

	
	public Section getOtherSection(Section sec)
	{
		Section other = getNextSection(sec);
		if (null == other){
			other = getPreviousSection(sec);
		}
		return other;
	}


	public Section getNextSection(Section sec)
	{
		Section ret = null;
		boolean found = false;
		for(Section currSec : data.report.getSections()){
			if (found){
				ret = currSec;
				break;
			}
			if(sec == currSec){found = true;}
		}
		return ret;
	}


	public Section getPreviousSection(Section sec)
	{
		Section ret = null;
		for(Section currSec : data.report.getSections()){
			if(sec == currSec){
				break;
			}
			ret = currSec;
		}
		return ret;
	}

/* TODO : rewrite this method
	public void exportReportToTxt(){
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("text files (.txt)", "txt");
		chooser.setFileFilter(filter);
		chooser.addChoosableFileFilter(filter);
		String title = data.report.getTitle();
		String proposedFileName = title + ".txt";
		chooser.setSelectedFile(new File(proposedFileName));

		int returnVal = chooser.showSaveDialog(getParent());
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			File mainFile =  chooser.getSelectedFile();
			try {
				FileWriter out = new FileWriter(mainFile);
				System.out.println("Saving text into file: " + mainFile);;
				out.write(data.report.exportToTxt(mainFile));
				out.close();

			} catch (FileNotFoundException e) {
				System.err.println("Error: Cannot open file for writing.");
			} catch (IOException e) {
				System.err.println("Error: Cannot write to file.");
			}
		}
	}
*/


	public void exportReportToHtml()
	{
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("html files (.html)", "html");
		chooser.setFileFilter(filter);
		chooser.addChoosableFileFilter(filter);
		String title = data.report.getTitle();
		String proposedFileName = title + ".html";
		chooser.setSelectedFile(new File(proposedFileName));

		int returnVal = chooser.showSaveDialog(getParent());
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			try
			{
				File mainFile =  chooser.getSelectedFile();
				String mainFilePath = mainFile.getPath();
				File resourceFolder = new File(mainFilePath.substring(0, mainFilePath.lastIndexOf('.')));
				// create CSS file
				File cssFile = new File(resourceFolder, "styles.css");
				if (! cssFile.exists())
				{
					FileUtil.copyFromResource("org/ow2/clif/analyze/lib/graph/gui/styles.css", cssFile);
				}
				// generate text file
				cssFile = new File(resourceFolder.getName(), "styles.css"); // change for a relative path 
				FileWriter out = new FileWriter(mainFile);
				htmlText = new StringBuffer();
				htmlWrite("<!DOCTYPE html>");
				htmlWrite("<html lang=\"en-US\">");
				tabsp();
				htmlWrite("<head>");
				tabsp();
				htmlWrite("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=" + Charset.defaultCharset() + "\">");
				htmlWrite("<meta content=\"text/html\" charset=\"utf-8\" http-equiv=\"Content-Type\">");
				htmlWrite("<title>" + title + "</title>");
				htmlWrite("<meta name=\"Robots\" content=\"noindex, follow\">");
				htmlWrite("<meta content=\"CLIF team\" name=\"author\">");
				htmlWrite("<meta content=\"clif@ow2.org\" name=\"email\">");
				htmlWrite("<link rel=\"stylesheet\" href=\"" + cssFile + "\">");
				tabsm();
				htmlWrite("</head>");
	
				htmlWrite("<body>");
				tabsp();
				htmlText.append(data.report.exportToHtml(2, resourceFolder));
				tabsm();
				htmlWrite("</body>");
				tabsm();
				htmlWrite("</html>");
				out.write(htmlText.toString());
				out.close();
				setStatus("Report successfully exported to " + mainFile);
			}
			catch (Exception ex)
			{
				setStatus("Failed to generate HTML report: " + ex.getMessage());
			}
		}
	}


/* TODO: rewrite this method	
	public void exportReportToXml(){
		
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("xml files (.xml)", "xml");
		chooser.setFileFilter(filter);
		chooser.addChoosableFileFilter(filter);
		String title = data.report.getTitle();
		String proposedFileName = title + ".xml";
		chooser.setSelectedFile(new File(proposedFileName));

		int returnVal = chooser.showSaveDialog(getParent());
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			File mainFile =  chooser.getSelectedFile();
			try {
				FileWriter out = new FileWriter(mainFile);
				System.out.println("Saving XML text into file: " + mainFile);
				out.write(data.report.exportXML(mainFile));				
				out.close();

			} catch (FileNotFoundException e) {
				System.err.println("Error: Cannot open file for writing.");
			} catch (IOException e) {
				System.err.println("Error: Cannot write to file.");
			}
			
		}
	}
*/
		
	public void saveMacro(){
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("xml files (.xml)", "xml");
		chooser.setFileFilter(filter);
		chooser.addChoosableFileFilter(filter);
		String title = data.report.getTitleMacro();
		String proposedFileName = title + ".xml";
		chooser.setSelectedFile(new File(proposedFileName));
		
		int returnVal = chooser.showSaveDialog(getParent());
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			File mainFile =  chooser.getSelectedFile();
			try {
				FileWriter out = new FileWriter(mainFile);
				System.out.println("Saving macro into file: " + mainFile);
			out.write(data.report.saveMacroXml());
				out.close();

			} catch (FileNotFoundException e) {
				System.err.println("Error: Cannot open file for writing.");
			} catch (IOException e) {
				System.err.println("Error: Cannot write to file.");
			} 
		}
	}
	
	// not OK 
	// use for load 
	public void loadMacro(){
// file macro
		FileSystemView fsv = FileSystemView.getFileSystemView();		
		
		File defaultRepository = fsv.getDefaultDirectory();		
		JFileChooser Chooser = new JFileChooser(defaultRepository);				
		Chooser.showOpenDialog(null);
		
		File mainFile = Chooser.getSelectedFile();		
		
	// parser
		SAXBuilder saxbuilder=new SAXBuilder();
		Element root =new Element("report");
		
		try{
			Document doc= saxbuilder.build(mainFile);
			root = doc.getRootElement();			
			// OK bonne recup du fichier + parser OK => affichage correct sans passages à la ligne
			// System.out.println(XMLToString(root)); 
			data.report.loadMacro(root);
			if (rmReportTree!= null){
				updateReportDisplay();
				rmReportTree.updateSelection();
				rmReportTree.updateReportTreeSelections();
			}
			else 
			{
				RMReportTree rmReportTreeNew =(RMReportTree) rmReportTreeCreation(); 
						// new RMReportTree();
				updateReportDisplay();
				rmReportTreeNew.updateSelection();
				rmReportTreeNew.updateReportTreeSelections();
			}
			
		}
		
		catch(JDOMException jdomex){
			System.err.println("Error: Cannot open file for read.");
		} 
		catch (IOException e)
		{
			System.err.println("Error: Cannot read the file.");
		}
		
	}
	
	public void loadReport(){
			// file macro
					FileSystemView fsv = FileSystemView.getFileSystemView();		
					
					File defaultRepository = fsv.getDefaultDirectory();		
					JFileChooser Chooser = new JFileChooser(defaultRepository);				
					Chooser.showOpenDialog(null);
					
					File mainFile = Chooser.getSelectedFile();		
					
				// parser
					SAXBuilder saxbuilder=new SAXBuilder();
					Element root =new Element("report");
					
					try{
						Document doc= saxbuilder.build(mainFile);
						root = doc.getRootElement();			
						// OK bonne recup du fichier + parser OK => affichage correct sans passages à la ligne
					//	 System.out.println(XMLToString(root)); 
						 System.out.println(data.toString()); 
						
						data.report=data.report.loadReportFromXLM(root);
						
						updateReportManagerView();
						updateChartsSelections();
						if (rmReportTree!= null){
							
							updateReportDisplay();
							rmReportTree.updateSelection();
							rmReportTree.updateReportTreeSelections();
							}
						else 
						{
							RMReportTree rmReportTreeNew =(RMReportTree) rmReportTreeCreation(); 
									// new RMReportTree();
							updateReportDisplay();
							rmReportTreeNew.updateSelection();
							rmReportTreeNew.updateReportTreeSelections();
						}
						
					}
					
					catch(JDOMException jdomex){
						System.err.println("Error: Cannot open file for read.");
					} 
					catch (IOException e)
					{
						System.err.println("Error: Cannot read the file.");
					}
					
				}

	public String XMLToString(Element current){
		
		String elementName= current.getName();
		List<?> childrens = current.getChildren();
		
		Iterator<?> it= childrens.iterator();
		if(!it.hasNext()){
			return elementName;
		}
		else
		{	current =(Element)it.next() ;
			String nextName= XMLToString(current);
			
			while( it.hasNext())
			{
				nextName += ",";
				current =(Element)it.next() ;
				nextName += XMLToString(current);
			}
			return elementName + "(" + nextName + ")";
		}		
		
	}
	
	/////////////////////////////
	// HTML generation methods
	/////////////////////////////
	int tabsMem = 0;
	
	public String tabs(int nb)
	{
		String retString = "";
		String tab = "  ";
		tabsMem = Math.max(0, nb);
		for (int i = 1; i <= tabsMem; i++){
			retString += tab;
		}
		return retString;
	}


	public String tabs(){
		return tabs(tabsMem);
	}


	public String tabsp(){
		return tabs(++tabsMem);		
	}
	

	public String tabsm(){
		return tabs(--tabsMem);		
	}


	private void htmlWrite(String mess) {
		// TODO Auto-generated method stub
		htmlText.append(tabs() + mess + "\n");
	}
}