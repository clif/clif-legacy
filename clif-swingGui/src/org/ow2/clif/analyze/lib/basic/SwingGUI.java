/*
* CLIF is a Load Injection Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* CLIF $Name: not supported by cvs2svn $
*
* Contact: clif@ow2.org
*/

package org.ow2.clif.analyze.lib.basic;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.ow2.clif.storage.api.BladeDescriptor;
import org.ow2.clif.storage.api.BladeEvent;
import org.ow2.clif.storage.api.TestDescriptor;
import org.ow2.clif.storage.lib.util.DateEventFilter;
import org.ow2.clif.supervisor.api.ClifException;
import org.ow2.clif.util.ThrowableHelper;
import org.ow2.clif.util.gui.GuiAlert;


/**
 *
 * @author Bruno Dillenseger
 */
class SwingGUI implements ListSelectionListener, ActionListener
{
	private AnalyzerImpl analyzer;
	private JLabel statusLine;
	private JList<TestDescriptor> testLst;
	private JList<BladeDescriptor> injectorLst, probeLst;
	private JLabel injectorLbl, probeLbl;
	private JComboBox<String> injectorEventCbb, probeEventCbb;
	private JButton refreshTestListBtn;
	private JButton injectorEventBtn, probeEventBtn;
	private JButton injectorPropertiesBtn, probePropertiesBtn;
	private JTextField injectorServerFld, injectorClassFld, injectorArgumentFld;
	private JTextField probeServerFld, probeClassFld, probeArgumentFld;
	private JInternalFrame frame;


	SwingGUI(JInternalFrame frame, AnalyzerImpl analyzer)
		throws ClifException
	{
		this.analyzer = analyzer;
		this.frame = frame;

		// GUI setup
		Container pane = frame.getContentPane();
		pane.setLayout(new GridBagLayout());
		GridBagConstraints cons = new GridBagConstraints();

		// column 0

		cons.gridx = 0;

		// test list title
		cons.gridy = 0;
		cons.weightx = 1;
		cons.weighty = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.NONE;
		cons.anchor = GridBagConstraints.CENTER;
		pane.add(new JLabel("available tests"), cons);

		// test list
		cons.gridy = 1;
		cons.weightx = 1;
		cons.weighty = 1;
		cons.gridwidth = 2;
		cons.gridheight = 3;
		cons.fill = GridBagConstraints.BOTH;
		testLst = new JList<TestDescriptor>(new BrowserListModel<TestDescriptor>(analyzer.getTests(null)));
		testLst.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		testLst.addListSelectionListener(this);
		pane.add(new JScrollPane(testLst), cons);

		// status line
		cons.gridy = 4;
		cons.weightx = 1;
		cons.weighty = 0;
		cons.gridwidth = 5;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.HORIZONTAL;
		statusLine = new JLabel();
		setDefaultStatusLine();
		pane.add(statusLine, cons);

		// column 1

		cons.gridx = 1;

		// test list refresh button
		cons.gridy = 0;
		cons.weightx = 1;
		cons.weighty = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.NONE;
		cons.anchor = GridBagConstraints.CENTER;
		refreshTestListBtn = new JButton("refresh");
		refreshTestListBtn.addActionListener(this);
		pane.add(refreshTestListBtn, cons);

		// column 2

		cons.gridx = 2;

		// injector list title
		cons.gridy = 0;
		cons.weightx = 2;
		cons.weighty = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.NONE;
		cons.anchor = GridBagConstraints.CENTER;
		pane.add(new JLabel("injectors"), cons);

		// injector list
		cons.gridy = 1;
		cons.weightx = 2;
		cons.weighty = 1;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.BOTH;
		injectorLst = new JList<BladeDescriptor>(new BrowserListModel<BladeDescriptor>());
		injectorLst.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		injectorLst.addListSelectionListener(this);
		pane.add(new JScrollPane(injectorLst), cons);

		// probe list title
		cons.gridy = 2;
		cons.weightx = 2;
		cons.weighty = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.NONE;
		cons.anchor = GridBagConstraints.CENTER;
		pane.add(new JLabel("probes"), cons);

		// probe list
		cons.gridy = 3;
		cons.weightx = 2;
		cons.weighty = 1;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.BOTH;
		probeLst = new JList<BladeDescriptor>(new BrowserListModel<BladeDescriptor>());
		probeLst.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		probeLst.addListSelectionListener(this);
		pane.add(new JScrollPane(probeLst), cons);

		// column 3

		cons.gridx = 3;

		// injector information title
		cons.gridy = 0;
		cons.weightx = 0;
		cons.weighty = 0;
		cons.gridwidth = 2;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.NONE;
		cons.anchor = GridBagConstraints.CENTER;
		pane.add(injectorLbl = new JLabel(""), cons);

		// injector information labels
		cons.gridy = 1;
		cons.weightx = 0;
		cons.weighty = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.NONE;
		cons.anchor = GridBagConstraints.EAST;
		JPanel attrPnl = new JPanel();
		attrPnl.setLayout(new GridLayout(5, 1));
		attrPnl.add(new JLabel("server: "));
		attrPnl.add(new JLabel("class: "));
		attrPnl.add(new JLabel("argument: "));
		attrPnl.add(new JLabel("properties: "));
		attrPnl.add(injectorEventCbb = new JComboBox<String>(new String[0]));
		pane.add(attrPnl, cons);

		// probe information title
		cons.gridy = 2;
		cons.weightx = 0;
		cons.weighty = 0;
		cons.gridwidth = 2;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.NONE;
		cons.anchor = GridBagConstraints.CENTER;
		pane.add(probeLbl = new JLabel(""), cons);

		// probe information labels
		cons.gridy = 3;
		cons.weightx = 0;
		cons.weighty = 1;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.NONE;
		cons.anchor = GridBagConstraints.EAST;
		attrPnl = new JPanel();
		attrPnl.setLayout(new GridLayout(5, 1));
		attrPnl.add(new JLabel("server: "));
		attrPnl.add(new JLabel("resource: "));
		attrPnl.add(new JLabel("argument: "));
		attrPnl.add(new JLabel("properties: "));
		attrPnl.add(probeEventCbb = new JComboBox<String>(new String[0]));
		pane.add(attrPnl, cons);

		// column 4

		cons.gridx = 4;

		// injector information values
		cons.gridy = 1;
		cons.weightx = 1;
		cons.weighty = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.HORIZONTAL;
		cons.anchor = GridBagConstraints.WEST;
		attrPnl = new JPanel();
		attrPnl.setLayout(new GridLayout(5, 1));
		attrPnl.add(injectorServerFld = new JTextField(15));
		injectorServerFld.setEditable(false);
		attrPnl.add(injectorClassFld = new JTextField(15));
		injectorClassFld.setEditable(false);
		attrPnl.add(injectorArgumentFld = new JTextField(15));
		injectorArgumentFld.setEditable(false);
		attrPnl.add(injectorPropertiesBtn = new JButton("view..."));
		injectorPropertiesBtn.setEnabled(false);
		injectorPropertiesBtn.addActionListener(this);
		attrPnl.add(injectorEventBtn = new JButton("browse..."));
		injectorEventBtn.addActionListener(this);
		injectorEventBtn.setEnabled(false);
		pane.add(attrPnl, cons);

		// probe information values
		cons.gridy = 3;
		cons.weightx = 1;
		cons.weighty = 0;
		cons.gridwidth = 1;
		cons.gridheight = 1;
		cons.fill = GridBagConstraints.HORIZONTAL;
		cons.anchor = GridBagConstraints.WEST;
		attrPnl = new JPanel();
		attrPnl.setLayout(new GridLayout(5, 1));
		attrPnl.add(probeServerFld = new JTextField(15));
		probeServerFld.setEditable(false);
		attrPnl.add(probeClassFld = new JTextField(15));
		probeClassFld.setEditable(false);
		attrPnl.add(probeArgumentFld = new JTextField(15));
		probeArgumentFld.setEditable(false);
		attrPnl.add(probePropertiesBtn = new JButton("view..."));
		probePropertiesBtn.setEnabled(false);
		probePropertiesBtn.addActionListener(this);
		attrPnl.add(probeEventBtn = new JButton("browse..."));
		probeEventBtn.addActionListener(this);
		probeEventBtn.setEnabled(false);
		pane.add(attrPnl, cons);

		pane.validate();
	}


	private void setDefaultStatusLine()
	{
		statusLine.setText(testLst.getModel().getSize() + " available test run(s)");
	}


	/////////////////////////////////////
	// ListSelectionListener interface //
	/////////////////////////////////////


	@Override
	public void valueChanged(ListSelectionEvent ev)
	{
		// selection on test list
		if (ev.getSource() == testLst)
		{
			TestDescriptor desc = testLst.getSelectedValue();
			if (desc == null)
			{
				((BrowserListModel<BladeDescriptor>)injectorLst.getModel()).setElements(null);
				((BrowserListModel<BladeDescriptor>)probeLst.getModel()).setElements(null);
				setDefaultStatusLine();
			}
			else
			{
				try
				{
					BladeDescriptor[] blades = analyzer.getTestPlan(desc.getName(), null);
					ArrayList<BladeDescriptor> probes = new ArrayList<BladeDescriptor>(blades.length);
					ArrayList<BladeDescriptor> injectors = new ArrayList<BladeDescriptor>(blades.length);
					for (int i=0 ; i<blades.length ; ++i)
					{
						if (blades[i].isInjector())
						{
							injectors.add(blades[i]);
						}
						if (blades[i].isProbe())
						{
							probes.add(blades[i]);
						}
					}
					injectorLst.clearSelection();
					probeLst.clearSelection();
					((BrowserListModel<BladeDescriptor>)injectorLst.getModel()).setElements(
						injectors.toArray(new BladeDescriptor[injectors.size()]));
					((BrowserListModel<BladeDescriptor>)probeLst.getModel()).setElements(
						probes.toArray(new BladeDescriptor[probes.size()]));
				}
				catch (Exception ex)
				{
					ex.printStackTrace(System.err);
					new GuiAlert(
						frame.getDesktopPane(),
						"Can't get blades for test run " + desc,
						ThrowableHelper.getMessages(ex)).alert();
				}
			}
		}
		// selection on injector list
		else if (ev.getSource() == injectorLst)
		{
			@SuppressWarnings("unchecked")
			BladeDescriptor blade = ((JList<BladeDescriptor>)ev.getSource()).getSelectedValue();
			if (blade == null)
			{
				injectorLbl.setText("");
				injectorServerFld.setText("");
				injectorClassFld.setText("");
				injectorArgumentFld.setText("");
				injectorEventCbb.removeAllItems();
				injectorEventBtn.setEnabled(false);
				injectorPropertiesBtn.setEnabled(false);
			}
			else
			{
				injectorLbl.setText(blade.toString());
				injectorServerFld.setText(blade.getServerName());
				injectorClassFld.setText(blade.getClassname());
				injectorArgumentFld.setText(blade.getArgument());
				injectorEventCbb.setModel(new DefaultComboBoxModel<String>(blade.getEventTypeLabels()));
				injectorEventBtn.setEnabled(true);
				injectorPropertiesBtn.setEnabled(true);
			}
		}
		// selection on probe list
		else if (ev.getSource() == probeLst)
		{
			@SuppressWarnings("unchecked")
			BladeDescriptor blade = ((JList<BladeDescriptor>)ev.getSource()).getSelectedValue();
			if (blade == null)
			{
				probeLbl.setText("");
				probeServerFld.setText("");
				probeClassFld.setText("");
				probeArgumentFld.setText("");
				probeEventCbb.removeAllItems();
				probeEventBtn.setEnabled(false);
				probePropertiesBtn.setEnabled(false);
			}
			else
			{
				probeLbl.setText(blade.toString());
				probeServerFld.setText(blade.getServerName());
				String resourceType = blade.getClassname();
				resourceType = resourceType.substring(0, resourceType.lastIndexOf('.'));
				probeClassFld.setText(
					resourceType.substring(1 + resourceType.lastIndexOf('.')));
				probeArgumentFld.setText(blade.getArgument());
				probeEventCbb.setModel(new DefaultComboBoxModel<String>(blade.getEventTypeLabels()));
				probeEventBtn.setEnabled(true);
				probePropertiesBtn.setEnabled(true);
			}
		}
	}


	//////////////////////////////
	// ActionListener interface //
	//////////////////////////////


	@Override
	public void actionPerformed(ActionEvent ev)
	{
		if (ev.getSource() == injectorPropertiesBtn)
		{
			openPropertiesInternalFrame(
				testLst.getSelectedValue(),
				(BladeDescriptor)injectorLst.getSelectedValue());
		}
		else if (ev.getSource() == probePropertiesBtn)
		{
			openPropertiesInternalFrame(
				testLst.getSelectedValue(),
				probeLst.getSelectedValue());
		}
		else if (ev.getSource() == refreshTestListBtn)
		{
			testLst.removeAll();
			try
			{
				testLst.setModel(new BrowserListModel<TestDescriptor>(analyzer.getTests(null)));
				setDefaultStatusLine();
			}
			catch (ClifException ex)
			{
				statusLine.setText("Can't get test list");
			}
		}
		else if (ev.getSource() == injectorEventBtn)
		{
			openEventBrowserInternalFrame(
				testLst.getSelectedValue(),
				injectorLst.getSelectedValue(),
				injectorEventCbb.getItemAt(injectorEventCbb.getSelectedIndex()));
		}
		else if (ev.getSource() == probeEventBtn)
		{
			openEventBrowserInternalFrame(
				testLst.getSelectedValue(),
				probeLst.getSelectedValue(),
				probeEventCbb.getItemAt(probeEventCbb.getSelectedIndex()));
		}
	}


	private void openPropertiesInternalFrame(TestDescriptor test, BladeDescriptor blade)
	{
		if (test != null && blade != null)
		{
			try
			{
				PropertiesView view = new PropertiesView(
					"Properties for " + test + ", " + blade,
					analyzer.getBladeProperties(test.getName(), blade.getId()));
				JDesktopPane pane = frame.getDesktopPane();
				view.setSize(pane.getWidth() / 2, pane.getHeight() / 2);
				view.setLocation(pane.getWidth() / 4, pane.getHeight() / 4);
				pane.add(view);
				view.setVisible(true);
			}
			catch (Exception ex)
			{
				statusLine.setText(ex.getMessage());
			}
		}
	}


	private void openEventBrowserInternalFrame(
		TestDescriptor test,
		BladeDescriptor blade,
		String eventTypeLabel)
	{
		if (test != null && blade != null && eventTypeLabel != null)
		{
			try
			{
				EventBrowser browser = new EventBrowser(
					test,
					blade,
					eventTypeLabel);
				JDesktopPane pane = frame.getDesktopPane();
				browser.setSize(pane.getWidth(), 3 * pane.getHeight() / 4);
				browser.setLocation(0, pane.getHeight() / 4);
				pane.add(browser);
				browser.setVisible(true);
				browser.pack();
			}
			catch (Exception ex)
			{
				statusLine.setText(ex.getMessage());
			}
		}
	}


	/////////////////////////////////////////////////////////////////////////////
	// inner class implementing a model for tests, injectors and probes JLists //
	/////////////////////////////////////////////////////////////////////////////


	class BrowserListModel<T> extends AbstractListModel<T>
	{
		private static final long serialVersionUID = -5125627480415738932L;
		private T[] elements;

		BrowserListModel()
		{
			this(null);
		}

		BrowserListModel(T[] values)
		{
			super();
			elements = values;
		}

		@Override
		public int getSize()
		{
			return elements == null ? 0 : elements.length;
		}

		@Override
		public T getElementAt(int index)
		{
			return elements == null ? null : elements[index];
		}

		public void setElements(T[] newElements)
		{
			int oldSize;
			if (elements == null)
			{
				oldSize = 0;
			}
			else
			{
				oldSize = elements.length;
			}
			elements = newElements;
			int newSize;

			if (newElements == null)
			{
				newSize = 0;
			}
			else
			{
				newSize = newElements.length;
			}
			if (newSize > oldSize)
			{
				fireIntervalAdded(this, oldSize, newSize - 1);
			}
			else if (newSize < oldSize)
			{
				fireIntervalRemoved(this, newSize, oldSize - 1);
			}
			fireContentsChanged(this, 0, newSize - 1);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////
	// inner class for viewing injector or probe properties in a dedicated JInternalFrame //
	////////////////////////////////////////////////////////////////////////////////////////


	class PropertiesView extends JInternalFrame
	{
		private static final long serialVersionUID = -7442286710886505570L;

		PropertiesView(String title, Properties props)
		{
			super(title, true, true, true, true);
			JTextArea area;
			try
			{
				SortedMap<String,String> map = new TreeMap<String,String>();
				for (String propName : props.stringPropertyNames())
				{
					map.put(propName, props.getProperty(propName));
				}
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				Iterator<Map.Entry<String,String>> lister = map.entrySet().iterator();
				while (lister.hasNext())
				{
					Map.Entry<String,String> entry = lister.next();
					out.write((entry.getKey() + "=" + entry.getValue() + "\n").getBytes());
				}
				area = new JTextArea(out.toString());
			}
			catch (Exception ex)
			{
				area = new JTextArea("Error: could not get properties\n" + ex);
			}
			area.setEditable(false);
			getContentPane().setLayout(new BorderLayout());
			getContentPane().add(BorderLayout.CENTER, new JScrollPane(area));
		}
	}


	/////////////////////////////////////////////////////////////////////////////////////
	// inner class for browsing injector or probe events in a dedicated JInternalFrame //
	/////////////////////////////////////////////////////////////////////////////////////


	class EventBrowser extends JInternalFrame implements ActionListener
	{
		private static final long serialVersionUID = -7117622208883712066L;

		TestDescriptor test;
		BladeDescriptor blade;
		BladeEvent firstEvent, lastEvent;
		String eventTypeLabel;
		JButton addBtn, removeBtn, defaultRangeBtn, setRangeBtn;
		JTextField fromFld, toFld;
		long from = -1, to = -1;
		JComboBox<String> fieldCbb;
		JTextArea infoTxt;
		JFreeChart chart;
		Map<String,XYSeries> seriesByName = new HashMap<String,XYSeries>();
		XYSeriesCollection seriesData = new XYSeriesCollection();
		BladeEvent[] events = null;


		EventBrowser(
			TestDescriptor test,
			BladeDescriptor blade,
			String eventTypeLabel)
		{
			super(
				eventTypeLabel + " events for " + blade + " from " + test,
				true, true, true, true);
			this.test = test;
			this.blade = blade;
			this.eventTypeLabel = eventTypeLabel;
			try
			{
				// control and info area (upper part)
				JPanel upperPnl = new JPanel();
				upperPnl.setLayout(new FlowLayout());
				upperPnl.add(new JLabel("time range: from "));
				fromFld = new JTextField(8);
				upperPnl.add(fromFld);
				upperPnl.add(new JLabel(" to "));
				toFld = new JTextField(8);
				upperPnl.add(toFld);
				setRangeBtn = new JButton("set");
				setRangeBtn.addActionListener(this);
				upperPnl.add(setRangeBtn);
				defaultRangeBtn = new JButton("full");
				defaultRangeBtn.addActionListener(this);
				upperPnl.add(defaultRangeBtn);
				fieldCbb = new JComboBox<String>(
					analyzer.getEventFieldLabels(
						test.getName(),
						blade.getId(),
						eventTypeLabel));
				fieldCbb.addActionListener(this);
				upperPnl.add(fieldCbb);
				addBtn = new JButton("draw");
				addBtn.addActionListener(this);
				upperPnl.add(addBtn);
				removeBtn = new JButton("undraw");
				removeBtn.addActionListener(this);
				removeBtn.setEnabled(false);
				upperPnl.add(removeBtn);

				// plot area (center part)
				chart = ChartFactory.createXYLineChart(
					eventTypeLabel,
					"time (s)",
					null,
					seriesData,
					PlotOrientation.VERTICAL,
					true, true, false);
				XYDotRenderer renderer = new XYDotRenderer();
//				renderer.setSeriesVisibleInLegend(Boolean.TRUE);
				chart.getXYPlot().setRenderer(renderer);
				ChartPanel plotArea = new ChartPanel(chart, true);

				// info area (left part)
				infoTxt = new JTextArea();
				infoTxt.setEditable(false);

				// full panel layout
				Container pane = getContentPane();
				pane.setLayout(new BorderLayout());
				pane.add(BorderLayout.NORTH, upperPnl);
				pane.add(BorderLayout.CENTER, plotArea);
				pane.add(BorderLayout.WEST, infoTxt);

				// get events
				fetchEvents();
				firstEvent = events[0];
				lastEvent = events[events.length-1];
			}
			catch (Exception ex)
			{
				ex.printStackTrace(System.err);
				new GuiAlert(
					frame.getDesktopPane(),
					"Can't retrieve events for " + blade + " from " + test,
					ThrowableHelper.getMessages(ex)).alert();
			}
		}


		private void fetchEvents()
			throws ClifException
		{
			events = analyzer.getEvents(
				test.getName(),
				blade.getId(),
				eventTypeLabel,
				new DateEventFilter(from, to),
				0,
				-1);
			if (events.length == 0)
			{
				infoTxt.append("No " + eventTypeLabel + " event\n");
			}
			else
			{
				from = events[0].getDate();
				fromFld.setText(String.valueOf(from));
				to = events[events.length - 1].getDate();
				toFld.setText(String.valueOf(to));
				infoTxt.append(
					events.length
					+ " " + eventTypeLabel
					+ " events in period ["
					+ from + ", " + to + "]\n"
					+ "average event throughput: "
					+ (1000 * events.length / (to - from)) + "/s\n");
			}
		}


		private void addSerie(String eventFieldLabel)
		{
			if (seriesByName.containsKey(eventFieldLabel) || events.length == 0)
			{
				return;
			}
			double min, max;
			XYSeries serie = new XYSeries(eventFieldLabel, true, true);
			seriesByName.put(eventFieldLabel, serie);
			if (events[0].getFieldValue(eventFieldLabel) instanceof Number)
			{
				double total = 0;
				double totalSquare = 0;
				min = ((Number)events[0].getFieldValue(eventFieldLabel)).doubleValue();
				max = ((Number)events[0].getFieldValue(eventFieldLabel)).doubleValue();
				long minDate = events[0].getDate();
				long maxDate = minDate;
				for (int i = 0 ; i < events.length ; ++i)
				{
					double value = ((Number)events[i].getFieldValue(eventFieldLabel)).doubleValue();
					serie.add(events[i].getDate() / 1000, value);
					total += value;
					totalSquare += value * value;
					if (value < min)
					{
						min = value;
						minDate = events[i].getDate();
					}
					else if (value > max)
					{
						max = value;
						maxDate = events[i].getDate();
					}
				}
				double average = total / events.length;
				double stdDeviation = Math.sqrt(totalSquare / events.length - average * average);
				infoTxt.append(
					"statistics for field " + eventFieldLabel + ":"
					+ "\naverage = " + average
					+ "\nmin = " + min + " at date " + minDate
					+ "\nmax = " + max + " at date " + maxDate
					+ "\nstandard deviation = " + stdDeviation
					+ "\n");
			}
			else if (events[0].getFieldValue(eventFieldLabel) instanceof Boolean)
			{
				min = 0;
				max = 1;
				long trueOcc = 0;
				for (int i = 0 ; i < events.length ; ++i)
				{
					if (((Boolean)events[i].getFieldValue(eventFieldLabel)).booleanValue())
					{
						serie.add(events[i].getDate() / 1000, max);
						++trueOcc;
					}
					else
					{
						serie.add(events[i].getDate() / 1000, min);
					}
				}
				double trueThruput = trueOcc;
				double falseThruput = events.length - trueOcc;
				if (events.length > 1)
				{
					long timeFrame = (events[events.length -1].getDate() - events[0].getDate()) / 1000;  
					trueThruput /= timeFrame;
					falseThruput /= timeFrame;
				}
				infoTxt.append(
					"statistics for boolean field " + eventFieldLabel + ":"
					+ "\ntrue - " + trueOcc + " occurrences ("
					+ ((1000 * trueOcc) / events.length) + " o/oo), "
					+ trueThruput + "/s throughput"  
					+ "\nfalse - " + (events.length - trueOcc) + " occurrences, "
					+ falseThruput +"/s throughput\n");
			}
			else
			{
				min = 0;
				max = 1;
				for (int i = 0 ; i < events.length ; ++i)
				{
					serie.add(events[i].getDate() / 1000, max);
				}
			}
			ValueAxis yAxis = chart.getXYPlot().getRangeAxis();
			yAxis.setLowerBound(min - ((max - min)) / 50.0);
			yAxis.setUpperBound(max + ((max - min)) / 50.0);
			seriesData.addSeries(serie);
		}


		private void removeSerie(String eventFieldLabel)
		{
			if (seriesByName.containsKey(eventFieldLabel))
			{
				seriesData.removeSeries(seriesByName.remove(eventFieldLabel));
			}
		}


		//////////////////////////////
		// ActionListener interface //
		//////////////////////////////


		@Override
		public void actionPerformed(ActionEvent evt)
		{
			try
			{
				if (evt.getSource() == defaultRangeBtn)
				{
					from = firstEvent.getDate();
					fromFld.setText(String.valueOf(from));
					to = lastEvent.getDate();
					toFld.setText(String.valueOf(to));
					fetchEvents();
				}
				else if (evt.getSource() == setRangeBtn)
				{
					from = Long.parseLong(fromFld.getText());
					to = Long.parseLong(toFld.getText());
					fetchEvents();
				}
				else if (evt.getSource() == addBtn)
				{
					addBtn.setEnabled(false);
					addSerie(fieldCbb.getItemAt(fieldCbb.getSelectedIndex()));
					removeBtn.setEnabled(true);
				}
				else if (evt.getSource() == removeBtn)
				{
					removeBtn.setEnabled(false);
					removeSerie(fieldCbb.getItemAt(fieldCbb.getSelectedIndex()));
					addBtn.setEnabled(true);
				}
				else if (evt.getSource() == fieldCbb)
				{
					if (seriesByName.containsKey(fieldCbb.getSelectedItem()))
					{
						addBtn.setEnabled(false);
						removeBtn.setEnabled(true);
					}
					else
					{
						addBtn.setEnabled(true);
						removeBtn.setEnabled(false);
					}
				}
			}
			catch (ClifException ex)
			{
				ex.printStackTrace(System.err);
				new GuiAlert(
					frame.getDesktopPane(),
					"Can't retrieve events for " + blade + " from " + test,
					ThrowableHelper.getMessages(ex)).alert();
			}
		}
	}
}
